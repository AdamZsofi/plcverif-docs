# Summary

  * [Introduction](README.md)
  
---
## User documentation
  * [Introduction](userdocs/README.md)
  * [Quick introduction](userdocs/QuickStart.md)
  * [Example verification walkthrough](userdocs/Demo.md)
  * [Plug-ins](userdocs/Plugins.md)
  * [Command line arguments](userdocs/CmdlineArgs.md)
---
## Project description
  * [Problem definition](project/ProblemDefinition.md)
  * [Competitors and similar tools](project/Competitors.md)
  * [User stories](project/UserStories.md)
  * [Feature list](project/FeatureList.md)
  * [High-level principles](project/HighlevelPrinciples.md)
  * [Licenses](project/License.md)
---
## Architecture
  * [Overview](architecture/ArchitectureOverview.md)
  * [Metamodels](architecture/metamodels/README.md)
    * [Expression metamodel](architecture/metamodels/ExpressionMetamodel.md)
    * [CFA Base](architecture/metamodels/CfaBase.md)
    * [CFA Declaration (CFD)](architecture/metamodels/CfaDeclaration.md)
    * [CFA Instance (CFI)](architecture/metamodels/CfaInstance.md)
    * [Comparison of CFD and CFI](architecture/metamodels/CfdVsCfi.md)
  * [Platform features](architecture/api/README.md)
    * [Settings](architecture/api/Settings.md)
    * [Developing plug-ins](architecture/api/PluginDevelopment.md)
    * [Extensions](architecture/api/PlatformExtensions.md)
  * [Verification job](architecture/verif/VerificationJob.md)
    * [Detailed example](architecture/verif/VerificationExample.md)
  * [Summary job](architecture/verif/SummaryJob.md)
  * [CLI](architecture/Cli.md)
---
## Library
  * [Overview](library/README.md)
  * [Language frontends](library/frontends/README.md)
    * [Implement a new frontend](library/frontends/ImplementNew.md)
  * [Reductions](library/reductions/README.md)
    * [Basic reductions](library/reductions/BasicReductions.md)
  * Verification
    * [Requirements](library/requirements/README.md)
      * [Assertion handling](library/requirements/AssertionHandling.md)
    * [Verification backends](library/backends/README.md)
      * [NuSMV / nuXmv](library/backends/Nusmv.md)
      * [Theta](library/backends/Theta.md)
      * [CBMC](library/backends/Cbmc.md)
      * [Implement a new backend](library/backends/ImplementNew.md)
    * [Reporters](library/reporters/README.md)
  * [Summary reporters](library/summaryreporters/README.md)
---
## STEP 7 frontend
  * [Overview](s7frontend/README.md)
  * [Supported SCL features](s7frontend/SclFeatures.md)
  * [Supported STL features](s7frontend/StlFeatures.md)
  * [AST transformations](s7frontend/AstTransformations.md)
  * [CFA representation](s7frontend/CfaRepresentation.md)
    * [Variable views](s7frontend/VariableViews.md)
  * [GUI features](s7frontend/Step7Gui.md)
  * [TIA Portal FBD translation](s7frontend/FbdTranslation.md)
---
## Development
  * [Development guidelines](development/DevGuidelines.md)
  * [GitLab project structure](development/GitLab.md)
  * [Getting started for developers](development/DevGettingStarted.md)
  * [Git workflow](development/GitWorkflow.md)
---
## Reference (Annex)
  * [Expression metamodel](reference/ExprXcore.md)
  * [Expression parser grammar](reference/ExpressionGrammar.md)
  * [CFA Base metamodel](reference/CfaBaseXcore.md)
  * [CFA Declaration (CFD) metamodel](reference/CfaDeclarationXcore.md)
  * [CFA Instance (CFI) metamodel](reference/CfaInstanceXcore.md)
  * [STEP 7 grammar](reference/Step7Grammar.md)
  * [Overview figures](reference/OverviewFigures.md)