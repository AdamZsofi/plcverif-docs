# Competitors and similar tools

Formal verification of PLC programs is the aim of various research projects at different universities and research institutes. A broad overview can be read in [doi 10.1007/s10270-014-0448-7](http://doi.org/10.1007/s10270-014-0448-7) or in [CERN-THESIS-2017-050, Section 3.9](https://cds.cern.ch/record/2267174/files/CERN-THESIS-2017-050.pdf#page=79).

Here we focus on the tools which may be available for usage "out of the box". For practical reasons, we exclude all tools which are not mentioned in publications during the last 5 years, also the ones where the existence is not justified at all (e.g., no screenshots, no detailed description, no link).

## Arcade.PLC

**Provider**: RWTH University, Aachen, Germany


### Product

"Arcade.PLC consists of a simulator for different PLC languages, a model-checker and a static analysis interface.
It features a graphical user interface (see below) under a varity of operating systems." [ref](https://arcade.embedded.rwth-aachen.de/doku.php?id=arcade.plc)

![](img/arcadeplc_1.png)

![](img/arcadeplc_2.png)

![](img/arcadeplc_3.png)


#### Maturity
Arcade.PLC has proven its usability in several scientific papers. The Arcade.PLC version currently available from their [website](https://arcade.embedded.rwth-aachen.de/doku.php?id=download) is a rather old version (several years old at least) and it has various problems preventing to try and evaluate the tool in detail.

#### Supported languages
* IEC 61131 ST and IL [ref](https://arcade.embedded.rwth-aachen.de/doku.php?id=arcade.plc)
* Siemens STL [ref](https://arcade.embedded.rwth-aachen.de/doku.php?id=arcade.plc)

#### Notable features
* ACTL and ptLTL support
* Eclipse-based editor

### Business model
An old version of the tool is available on their [website](https://arcade.embedded.rwth-aachen.de/doku.php?id=download). It is not known how is the newest version of the tool used, nor it is known if there is any commercial use.

### Case studies
* PLCopen's safety-critical FBs ([1])
* CEGAR-based algorithm, hierarchical predicate abstraction (see [1])
* Static analysis (range / value set inference and check) (see [1])

### Notes
The approach is very promising. However, the tool does not seem to be in active development any more. Also, the lack of SCL support and the difficult extensibility makes it non-applicable to our case. Furthermore, the ACTL/ptLTL-based requirement specification is difficult to be used by PLC developers.

### Contact information
* [Dimitri Bohlender](mailto:bohlender[at]embedded[dot]rwth-aachen[dot]de)
* [Hendrik Simon](mailto:simon[at]embedded[dot]rwth-aachen[dot]de)
* [Marcus Völker](mailto:voelker[at]embedded[dot]rwth-aachen[dot]de)

### References
[1] [S. Biallas et al. Arcade.PLC: A Verification Platform for Programmable Logic Controllers. ASE 2012.](http://doi.org/10.1145/2351676.2351741)




## MODCHK: VTT model checking toolset

**Provider**: VTT Technical Research Centre of Finland Ltd.

### Product
"MODCHK is a modelling tool that provides a graphical user interface for modelling function block diagrams and animating counter example traces." [ref](http://www.vttresearch.com/services/smart-industry/eco-efficient-products/machine-automation-systems/model-checking)
The Eclipse-based, graphical model checking tool is not publicly available, only some screenshots can be found. 

![Screenshot of the MODCHK tool](img/modcheck-plugin.png)

The tool is based on the open source tool Simantics, that is an "open, ontology-based integration platform for different modelling and simulation tools" [1].

#### Maturity

"Still in development, MODCHK is already used in-house, both for customer projects and research purposes." [ref](http://www.vttresearch.com/services/smart-industry/eco-efficient-products/machine-automation-systems/model-checking)

#### Supported languages
* FBD (according to the screenshots)

#### Notable features
* Visualization of counterexamples on the FBD, as animation
* Eclipse-based editor

### Business model
The tool is not publicly available, nor can be commercially licensed. VTT provides verification services based on model checking and consultancy. [ref](http://www.vttresearch.com/services/smart-industry/eco-efficient-products/machine-automation-systems/model-checking)

### Case studies
According to their [website](https://www.simulationstore.com/node/52), model checking was successfully applied in various nuclear I&C systems in Finland, in collaboration with the Finnish Radiation and Nuclear Safety Authority (STUK), and a major energy provider company (Fortum).

### Notes
The tool is not publicly available. It seems that there is no support for textual languages.

### Contact information
* Antti Pakonen
* Janne Valkonen

### References
- [1] [A. Pakonen et al. A toolset for model checking of PLC software. ETFA, IEEE, 2013](http://www.vtt.fi/inf/julkaisut/muut/2013/OA-a_toolset_for_model.pdf)
- [2] [A. Pakonen et al. Model Checking for Licensing Support in the Finnish Nuclear Industry. ISOFIC 2014.](http://www.vtt.fi/inf/julkaisut/muut/2014/ISOFIC_2014_Pakonen_et_al_FINAL.pdf)
- [3] [J. Lahtinen. Model checking large nuclear power plant safety system designs. PhD Thesis, Aalto University, 2016.](http://urn.fi/URN:ISBN:978-951-38-8447-5)
- [More publications](http://www.vttresearch.com/services/smart-industry/eco-efficient-products/machine-automation-systems/model-checking)

## PLC Checker

**Provider**: Itris Automation (acquired by Schneider)

### Product
The PLC Checker of Itris is a static analysis tool to "maintain the quality of your PLC programs and save time" [ref](http://www.itris-automation.com/plc-checker/) "for audits, code reviews, compliance, and controlling program quality" [ref](http://www.itris-automation.com/plc-checker/).

[Some figures](http://www.itris-automation.com/wp-content/uploads/cycle-en-v.png) and notes suggest that model checking is part of the tool or at least it is a planned future work, but no detailed information is available on this.


#### Maturity
Mature commercial product.

#### Supported languages
* All major variants and flavors of PLC programming languages (e.g. all Siemens PLC programming languages, both in STEP 7 and TIA Portal flavors)

### Business model
* Commercial tool

### Case studies
The static analysis tool is being used by various industrial companies, e.g. EDF, PSA Peugeot Citroen, Safran. [ref](http://www.itris-automation.com/clients/), [1]

### Contact information
http://www.itris-automation.com/contact-us/

### References
[1] [F. Parisot. La qualité des programmes vérifiée par leurs concepteurs. Measures (826), 2010.](http://www.mesures.com/pdf/old/826-verification-codes-Itris-PSA.pdf)


<!--
## (Template)
**Provider**: ...
### Product
#### Maturity
#### Supported languages
#### Notable features
### Business model
### Case studies
### Contact information
### References
-->