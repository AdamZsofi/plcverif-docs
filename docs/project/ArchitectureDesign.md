# Architecture design

## High-level design questions
- Requirements
   * How to represent requirements? It should cover at least requirement patterns, TL requirements, assertions, tabular input/output sequences.
- CFA
   * Precise (abstract) syntax and semantics of the CFA?
   * Concrete syntax
   * Traceability
   * Representation of compounds (arrays)
   * Representation of AT
   * Representation of bitwise operations
   * Handling built-in functions (e.g. ROL, SHR, TAN, ABS, LOG, MAX, ...) 
   * Represent bit-level magic of STL programs (e.g. `NEGD`, `ACCU1-L-L` and its `INC`)
- Reductions
   * How to represent reductions? What should be their interface?
- Verification tools
   * Wrappers and descriptors for various verification tools (nuXmv, Theta, CBMC, SA tools, ...)
   * Parsing their outputs
   * Interfaces
- Overall architecture
   * Modules, interfaces between modules
   * GUI and command line interfaces
- Grammar
   * Handling the symbol table
  ...