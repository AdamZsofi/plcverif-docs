# Overview the library of plug-ins

PLCverif is shipped with a set of essential PLCverif plug-ins.

Two jobs are defined:
- `verif`: [Verification job](../architecture/verif/VerificationJob.md) that performs verification of given requirements on PLC programs using a chosen verification backend.
- `summary`: [Summary reporter job](../architecture/verif/SummaryJob.md) that can sum up several verification job executions and present in a single report.

The jobs can be further extended using plug-ins.

Every **PLC job** can be extended with:
 - [**Language frontends**](frontends/README.md) which are responsible for translating the PLC code into control-flow automaton,
   - PLCverif is shipped with the [_STEP 7 language frontend_](../s7frontend/README.md)
 - [**Reductions**](reductions/README.md) which are responsible for reducing the size and complexity of the control-flow automata.
   - PLCverif is shipped with [_a set of basic reductions_](reductions/BasicReductions.md)

The **verification job** can be extended with:
 - [**Requirements**](requirements/README.md) which are responsible for providing a user-friendly requirement definition syntax and its translation into temporal logic,
   - PLCverif is shipped with [_assertion requirement representation_](requirements/README.md#assertion) and [_pattern-based requirement representation_](requirements/README.md#pattern)
 - [**Verification backends**](backends/README.md) which are responsible for checking the reduced control-flow automaton using an external verification program or a verification algorithm,
   - PLCverif is shipped with verification backend for the [_NuSMV_](backends/Nusmv.md), [_Theta_](backends/Theta.md), and [_CBMC_](backends/Cbmc.md) model checkers
 - [**Reporters**](reporters/README.md) which are  responsible for visualizing the result of the verification for the user or for other tools.
    - PLCverif is shipped with human-readable [_plain text_](reporters/README.md#plaintext) and [_rich text (HTML)_](reporters/README.md#html) reporters, and machine-readable reporters in [_Jenkins XML_](reporters/README.md#junit) and [_internal PLCverif verification summary_](reporters/README.md#summary) format


![Plug-ins in the formal verification workflow](../userdocs/img/workflow.png)
