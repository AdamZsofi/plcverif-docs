## Basic reductions

### Universal reductions (both for CFA declaration and instance)

<!--- ==================================================================== --->
#### RemoveLocationsWithNoIncomingTransition

Removes the locations with no incoming transition.

###### Pattern to match
```dot
digraph G {
	mindist = 0.02;
	temp1, temp2 [style=invis, shape=point];

	temp1 -> loc [style=dashed, color=red, arrowhead=onormal, label=<<FONT color="red"><I>none</I></FONT>>, labelfontcolor="red"];
	loc -> temp2, temp2, temp2;
}
```

- **location _loc_**
   - no incoming transition
   - not frozen, nor initial or end location
   - has no frozen outgoing transition

###### Modification
- all outgoing transitions of _loc_ are deleted
- location _loc_ is deleted
   
<!--- ==================================================================== --->
#### OverlapVarAssignments

CFA instance and CFA declaration (latter with name `OverlapAssignments`) reduction.
This reduction tries to merge consecutive transitions by moving the variable assignments of the second transition to the first one.

###### Pattern to match
```dot
digraph G {
	loc1 -> loc2 [label = " tA\l [c]\l /VA1:=EA1;\l  VA2:=EA2;\l  ..."];
	loc2 -> loc3 [label = " tB\l [TRUE]\l /VB1:=EB1;\l  VB2:=EB2;\l  ..."];
	
	l0, l1 [style=invis, shape=point];
	loc1->l0, l1->loc3 [style=invis, shape=point];
    l0 -> loc2 [style=dashed, color=red, arrowhead=onormal, label=<<FONT color="red"><I>no</I></FONT>>, labelfontcolor="red"];
    loc2 -> l1 [style=dashed, color=red, arrowhead=onormal, label=<<FONT color="red"><I>no</I></FONT>>, labelfontcolor="red"];
}
```

- **assignment transition _tA_**
   - may have a guard (_c_)
   - source=_loc1_, target=_loc2_
   - has variable assignments _VA1:=EA1; VA2:=EA2; ..._
   - not frozen
- **assignment transition _tB_**
   - no guard (TRUE literal)
   - source=_loc2_, target=_loc3_
   - has variable assignments _VB1:=EB1; VB2:=EB2; ..._
   - not frozen
- **location _loc2_**
   - 1 incoming transition (_t1_)
   - 1 outgoing transition (_t2_)
   - not frozen, not initial location, not end location
- no conflict between the variable assignments, i.e., 
   * $$Vars(VA) \cap Vars(EB) = \emptyset$$ (the variables assigned in _tA_ are not read in _tB_)
   * $$Vars(VB) \cap Vars(EA) = \emptyset$$ (the variables assigned in _tB_ are not read in _tA_)
   * $$Vars(VA) \cap Vars(VB) = \emptyset$$ (the variables assigned in _tA_ are not assigned in _tB_)
   * Note: $$VA = \bigcup_{i}{VA_i}$$, $$VB = \bigcup_{i}{VB_i}$$, $$EA = \bigcup_{i}{EA_i}$$, $$EB = \bigcup_{i}{EB_i}$$.
   
###### Modification
- all assignments of _tB_ (_VB1:=EB1; VB2:=EB2; ..._) are moved to _tA_
- target of transition _tA_ is modified to _loc3_ (instead of _loc2_)
- location _loc2_ is deleted
- transition _tB_ deleted

###### Result
```dot
digraph G {
	loc1 -> loc3 [label = " tA\l [c]\l /VA1:=EA1;\l  VA2:=EA2;\l  ...\l  VB1:=EB1;\l  VB2:=EB2;\l  ..."];
}
```

<!--- ==================================================================== --->
#### RemoveEmptyTransitions

Merges the two ends of the transitions where the guard is a constant true literal and there is no variable assignment.

###### Pattern to match
```dot
digraph G {
    mindist = 0.02;
	temp1, temp2, temp3 [style=invis, shape=point];

	loc1 -> temp1 [style=dashed, color=red, arrowhead=onormal, label=<<FONT color="red"><I>none</I></FONT>>, labelfontcolor="red"];
	temp3 -> loc2 [style=dashed, color=red, arrowhead=onormal, label=<<FONT color="red"><I>none</I></FONT>>, labelfontcolor="red", len="0.001"];
	loc2 -> temp2, temp2, temp2;
	
	loc1 -> loc2 [label = "t\l [TRUE] \l/---\l", weight=1000];
}
```

- **assignment transition _t_**
   - guard: 'TRUE'
   - not frozen
   - source: _loc1_, target: _loc2_
   - has no assignments
- **location _loc1_**
   - not frozen, not initial location, not end location
   - 1 outgoing transition (_t_)
- **location _loc2_**
   - not frozen, not initial location, not end location
   - 1 incoming transition (_t_)

###### Modification
- transition _t_ is deleted
- all outgoing transitions of _loc2_ are moved to _loc1_
- location _loc2_ is deleted
- The CFD reduction will move the annotations of _loc2_ to _loc1_. The CFI reduction assumes that there are no annotations.

###### Result
```dot
digraph G {
    mindist = 0.02;
	temp2 [style=invis, shape=point];

	loc1 -> temp2, temp2, temp2;
}
```



### CFA instance reductions



<!--- ==================================================================== --->
#### ConditionPushDown

It aims to push guard conditions deeper, essentially merging nested conditional branches and to reduce the number of locations.

###### Pattern to match
```dot
digraph G {
	{node[style=invis, shape=point] dummy1, dummy2, dummy3, dummy4;}
		
	loc1 -> loc2 [label = " t\l [c]\l"];
	loc2 -> dummy1 [label = " t1\l [c1]\l"];
	loc2 -> dummy2 [label = " t2\l [c2]\l"];
	loc2 -> dummy3 [label = " ..."];
	dummy4 -> loc2 [style=dashed, color=red, arrowhead=onormal, label=<<FONT color="red"><I>none</I></FONT>>, labelfontcolor="red"];
}
```

- **assignment transition _t_**
   - has no variable assignment
   - has guard (_c_)
   - source=_loc1_, target=_loc2_
   - not frozen
- **location _loc1_**
   - has 0..* (any) incoming transition
   - has 1..* (at least one) outgoing transition (including _t_)
   - not frozen
- **location _loc2_**
   - exactly 1 incoming transition (_t_)
   - 2..* outgoing *assignment* transitions (with guards _c1_, _c2_, ...)
   - not frozen

###### Modification
- all outgoing transitions of _loc2_ will be extended with "c AND" and their source changed do _loc1_
- location _loc2_ deleted
- transition _t_ deleted

###### Result
```dot
digraph G {
	{node[style=invis, shape=point] dummy1, dummy2, dummy3;}
		
	loc1 -> dummy1 [label = " t1\l [c AND c1]\l"];
	loc1 -> dummy2 [label = " t2\l [c AND c2]\l"];
	loc1 -> dummy3 [label = " ..."];
}

```


<!--- ==================================================================== --->
#### RemoveEmptyConditionalBranch

Removes unnecessary parallel empty transitions.

###### Pattern to match
```dot
digraph G {
	loc_invis1,loc_invis2,loc_invis3 [style=invis, shape=point];
		
	loc_invis1 -> loc1;
	loc_invis2 -> loc1;
	loc_invis3 -> loc1;
		
	
	loc1 -> loc2 [label = " t1\l [c1]\l /---\l"];
	loc1 -> loc2 [label = " t2\l [c2 == not c1]   \l /---\l"];
}
```
- **location _loc1_**
   - has exactly 2 outgoing transitions (_t1_, _t2_) and they both lead to the same location (_loc2_)

- **assignment transition _t1_**
   - has a guard _c1_
   - does not have any variable assignments
   - may be frozen

- **assignment transition _t2_**
   - has a guard _c2_ which equals to _NOT c1_
   - does not have any variable assignments 
   - not frozen

###### Modification
- _t1_'s guard changed to 'TRUE'
- _t2_ deleted
Note that if _t1_ is frozen, _t2_ will still be merged into _t1_ by modifying its guard.

###### Result
```dot
digraph G {
	loc_invis1,loc_invis2,loc_invis3 [style=invis, shape=point];
		
	loc_invis1 -> loc1;
	loc_invis2 -> loc1;
	loc_invis3 -> loc1;
		
	
	loc1 -> loc2 [label = " t1\l [TRUE]\l /---\l"];
}
```


<!--- ==================================================================== --->
#### RemoveFalseTransitions
CFI reduction. Removes the transitions where the guard is a constant false literal.

###### Pattern to match
```dot
digraph G {
	{node[style=invis, shape=point] loc0, loc1;}
		
	loc0 -> loc1 [label = "t\l [FALSE]\l"];
}
```

- **assignment transition _t_**
   - guard is 'FALSE'
   - not frozen

###### Modification
- transition _t_ is deleted


<!--- ==================================================================== --->
#### RemoveIdentityAssignments

Removes the variable assignments where the left and right values equal.

###### Pattern to match
```dot
digraph G {
	{node[style=invis, shape=point] loc0, loc1;}
	loc0 -> loc1 [label = "t\l [c]\l/...\l  &alpha; := &alpha;; (amt)\l  ...\l"];
}
```

- **assignment _amt_**
 - not frozen
 - its left and right value equal (_&alpha; := &alpha;_)

###### Modification
- variable assignment _amt_ is deleted

###### Result
```dot
digraph G {
	{node[style=invis, shape=point] loc0, loc1;}
	loc0 -> loc1 [label = "t\l [c]\l/...\l  ...\l"];
}
```



<!--- ==================================================================== --->
#### RemoveUnreadAssignments

Removes the variable assignments which are not read. I.e., if between the assignment `v1 := <Expr1>;` and `v1 := <Expr2>;` there is no reference to variable `v1`, then the first assignment.
The reduction will keep track of assignments of variables while traversing the CFA. If another assignment is found for a variable in a way that:
- There was no convergence or divergence in the CFA (each location inbetween had exactly 1 incoming and 1 outgoing transition),
- There was no call transition since the last assignment,
- There was no reference to the variable since the last assignment,
- The last assignment is not frozen,

then the previous assignment will be removed.





<!--- ==================================================================== --->
#### RemoveUnreadVariables

Reduction that removes the unread variables of the given CFA.
A variable will be removed if it is never read (and not frozen), independently from how many times it is assigned (zero, one or several times). If the variable is assigned and none of its assignments are frozen, the assignments are also removed. If any of the assignments of the variable is frozen, the variable will not be removed.

It is assumed that the variables in the requirement are frozen, thus they cannot be removed.




<!--- ==================================================================== --->
#### SimplifyTrivialConditionalBranches/1

It translates the trivial conditional branches assigning Boolean variables into (more complex) variable assignments by including the guard directly in the assignment.

###### Pattern to match
```dot
digraph G {
	loc_invis1 [style=invis, shape=point];
		
	loc_invis1 -> loc1;
	loc_invis1 -> loc1;
	loc_invis1 -> loc1;
		
	
	loc1 -> loc2 [label = " tA\l [c1]\l /boolVar:=&alpha;;\l VA1\l"];
	loc1 -> loc3 [label = " tB\l [c2 == NOT c1]   \l /boolVar:=&beta;;\l VA2\l"];
}
```

- **location _loc1_**
   - has two outgoing transitions (_tA_, _tB_), both assignment transitions
   - not frozen, not initial location, not end location

- **assignment transition _tA_**
   - has a guard _c1_
   - assigns the expression &alpha; to variable _boolVar_ (which has a type `BoolType`)
       * this assignment is not frozen
   - may have additional variable assignments (_VA1_)

- **assignment transition _tB_**
   - has a guard _c2_ which equals to _NOT c1_
   - assigns the expression &beta; to variable _boolVar_ (which has a type `BoolType`)
       * this assignment is not frozen   
   - may have additional variable assignments (_VA2_)
- The _boolVar_ variable cannot be used in _c1_ or _c2_. (In this case the extraction of the assignment would alter the satisfaction of the guard _c1_ and/or _c2_.)
   
###### Modification
- creates a new location _loc0_
- creates a new assignment transition _t0_ from _loc0_ to _loc1_
- creates a new assignment _boolVar := (c1 AND &alpha;) OR (NOT c1 AND &beta;)_ attached to _t0_
- removes the assignments of _boolVar_ from _tA_ and _tB_

###### Result
```dot
digraph G {
	loc_invis1 [style=invis, shape=point];
		
	loc_invis1 -> loc0;
	loc_invis1 -> loc0;
	loc_invis1 -> loc0;
		
	loc0 -> loc1 [label = "t0\l [TRUE]\l /boolVar:=(c1 AND &alpha;) OR (NOT c1 AND &beta;);\l"]
	loc1 -> loc2 [label = " tA\l [c1]\l /VA1\l"];
	loc1 -> loc3 [label = " tB\l [c2 == not c1]\l /VA2\l"];
}
```



<!--- ==================================================================== --->
#### SimplifyTrivialConditionalBranches/2

It translates the trivial conditional branches assigning Boolean variables into (more complex) variable assignments by including the guard directly in the assignment.

###### Pattern to match
```dot
digraph G {
	loc_invis1 [style=invis, shape=point];
		
	loc_invis1 -> loc1;
	loc_invis1 -> loc1;
	loc_invis1 -> loc1;
		
	
	loc1 -> loc2 [label = " tA\l [c1]\l /boolVar:=&alpha;;\l VA1\l"];
	loc1 -> loc3 [label = " tB\l [c2 == not c1]   \l /VA2\l"];
}
```

- **location _loc1_**
   - has two outgoing transitions (_tA_, _tB_), both assignment transitions
   - not frozen, not initial location, not end location

- **assignment transition _tA_**
   - has a guard _c1_
   - assigns the expression &alpha; to variable _boolVar_ (which has a type `BoolType`)
       * this assignment is not frozen
   - may have additional variable assignments (_VA1_)

- **assignment transition _tB_**
   - has a guard _c2_ which equals to _NOT c1_
   - DOES NOT assign the variable _boolVar_
   - may have variable assignments (_VA2_)
   
###### Modification
- creates a new location _loc0_
- creates a new assignment transition _t0_ from _loc0_ to _loc1_
- creates a new assignment _boolVar := (c1 AND &alpha;) OR (NOT c1 AND boolVar)_ attached to _t0_
- removes the assignment of _boolVar_ from _tA_

###### Result
```dot
digraph G {
	loc_invis1 [style=invis, shape=point];
		
	loc_invis1 -> loc0;
	loc_invis1 -> loc0;
	loc_invis1 -> loc0;
		
	loc0 -> loc1 [label = "t0\l [TRUE]\l /boolVar:=(c1 AND &alpha;) OR (NOT c1 AND boolVar);            \l"]
	loc1 -> loc2 [label = " tA\l [c1]\l /VA1\l"];
	loc1 -> loc3 [label = " tB\l [c2 == not c1]\l /VA2\l"];
}
```
   



<!--- ==================================================================== --->
#### ExpressionSimplification

Simplifies logic and arithmetic expressions.

* Negation expressions (`UnaryLogicExpression` with operator=`NEG`):
    -  !(FALSE) --> TRUE
	-  !(TRUE) --> FALSE
* AND expressions (`BinaryLogicExpression` with operator=`AND`):	
    - TRUE AND _right_ --> _right_
    - _left_ AND TRUE --> _left_
    - _left_ AND FALSE --> FALSE
    - FALSE AND _right_ --> FALSE
    - _left_ AND _left_ --> _left_
    - _left_ AND (NOT _left_) --> FALSE
    - (NOT _right_) AND _right_ --> FALSE
    - (_a_ OR _b_) AND (_a_ OR _c_) --> _a_ AND (_b_ OR _c_)   (in all 4 combinations)
    -  (_a_ AND _b_) AND (_a_ AND _c_) --> (_a_ AND _b_) AND_c_   (in all 4 combinations)
* OR expressions (`BinaryLogicExpression` with operator=`OR`):	
    - FALSE OR _right_ --> _right_
    - _left_ OR FALSE --> _left_
    - _left_ OR TRUE --> TRUE
    - TRUE OR _right_ --> TRUE
    - _left_ OR _left_ --> _left_
    - _left_ OR (NOT _left_) --> TRUE
    - (NOT _right_) OR _right_ --> TRUE
    - (_a_ OR _b_) OR (_a_ OR _c_) --> (_a_ OR _b_) OR _c_   (in all 4 combinations)
* IMPLIES expressions (`BinaryLogicExpression` with operator=`IMPLIES`):	
    - FALSE IMPLIES _right_ --> TRUE
    - TRUE IMPLIES _right_ --> _right_
* equality expressions (`ComparisonExpression` with operator=`EQUALS`):	
    - _alpha_ == _alpha_ --> TRUE
	- _alpha_ == TRUE --> _alpha_
* non-equality expressions (`ComparisonExpression` with operator=`NOT_EQUALS`):	
    - _alpha_ != _alpha_ --> FALSE
* binary arithmetic expressions and comparisons: replaces computable subtrees with the computed literals (e.g. `120 / 5` --> `24`; `TRUE == FALSE` --> `FALSE`, `2 > 1` --> `TRUE`).

	
	



<!--- ==================================================================== --->
#### ExpressionPropagation

Replaces the references to variables in guards and variable assignment right values to the definition of the referred variable, if possible.
E.g. the sequence `v1 := TRUE; v2 := v1;` can be simplified to `v1 := TRUE; v2 := TRUE;`.
If it is instantiated with `constantPropagationOnly`=true, only constants (literals) can be substituted. If `constantPropagationOnly`=false, expressions can also be propagated, i.e., `v1 := a OR b; v2 := v1;` can be reduced to `v1 := a OR b; v2 := a OR b;`.

The reduction infers for each transition the values of variables _at the end of the transition_ based on the guards and variable assignments. For example, based on a transition with guard `[a = TRUE AND b = FALSE]` and assignment `c := a AND d` it can be inferred that `a = TRUE; b = FALSE; c = TRUE and d`. This information (held by a _propagation table_) is propagated throughout the whole CFA and updated at each transition accordingly. In case of forks (i.e., a location has multiple outgoing transitions), the same propagation table is propagated to all outgoing transitions. In case of merges (i.e., a location has multiple incoming transitions), the propagation tables computed for each incoming transitions are _combined_ (intersected), i.e., their conflicting values are removed. For instance, the combination of `a=1; b=2; c=3` and `a=2; c=3` is `c=3`. In case it is not possible to calculate the propagation tables for each incoming transition (e.g., in case there is a loop in the CFA), the propagation is continued with an empty propagation table from that location.

Whenever an expression is recorded in the propagation table, it is simplified using the `ExpressionSimplification.trySimplifySubtree()` method. When the expression propagation replaces a variable with a propagated value, the whole expression subtree where the replacement happened will be simplified using the `ExpressionSimplification.trySimplifySubtree()` method.

Currently, the following information is used for the expression propagation reduction:
- Variable assignments, if they are deterministic
- Guards, if they are in one of the following forms: `[<variable> = <expr>]`, `[<expr> = <variable>]`, `[<boolean_variable>]`, `[NOT <boolean_variable>]`, or the AND connection of any of these.

The expression propagation shall never create conflicts. For instance, if the propagation table contains `[a = c]` and a transition has the variable assignments `b := a` and `c := d`, the `a` shall not be replaced by `c`, as it would cause conflicting variable assignments: `b := c; c := d;` which is forbidden.

{% hint style='example' %}
**Example.**

Take the following SCL code as example. (Assume that `b1`, `b2`, `b3` are BOOL variables, `i1`, `i2`, `i3` are INT variables.)
```scl
b1 := TRUE;
b2 := b1 AND I0.0;
b3 := b1 AND b2;

i1 := 5;
i2 := i2 * 3;
i3 := i1 + i2;
```

If it is translated into CFA and then expression propagation is used with `constantPropagationOnly`=true, the result will be equivalent to having the following code translated into CFA:

```scl
b1 := TRUE;
b2 := TRUE; // simplified from 'TRUE AND I0.0'
b3 := TRUE; // simplified from 'TRUE AND TRUE'

i1 := 5;
i2 := 15; // simplified from '5 * 3'
i3 := 20; // simplified from '5 + 15'
```

Notice that even if only the propagation of constants (literals) was permitted, due to the automated expression simplification it was able to propagate values for variables whose definition originally contained non-literals (e.g., `b2 := b1 AND I0.0;` was possible to be propagated even though `I0.0` is not a literal).
{% endhint %}

{% hint style='example' %}
**Example: propagation of guard values.**
Not only variable assignments can be the source of information for expression propagation.
Take the following SCL code as example: 
```scl
IF I0.0 = TRUE AND I0.1 = FALSE THEN
		b1 := I0.0;
      b2 := I0.1;
END_IF;
b3 := b1;
```

If it is translated into CFA and then expression propagation is used, the result will be equivalent to having the following code translated into CFA:

```scl
IF I0.0 = TRUE AND I0.1 = FALSE THEN
		b1 := TRUE;
      b2 := FALSE;
END_IF;
b3 := b1;
```
{% endhint %}

{% hint style='example' %}
**Example: propagation over conditional locations.**
Expressions can be propagated over forks and merges too. 

Take the following SCL code as example: 
```scl
a := 1;
IF b > 2 THEN
  c := b;
ELSE
  d := b - 1;
END_IF;
e := a; 
```

If it is translated into CFA and then expression propagation is used, the result will be equivalent to having `e := 1;` in the last line of the example code.
{% endhint %}


<!--- ==================================================================== --->
#### CoiReduction

The _cone of influence_ (COI) reduction explores the dependencies between variables, then removes the variables which the requirement does not depend on. For this, two kinds of dependencies are defined.
- There is an _assignment dependency_ between the variables `a` and `b` if `a` has an assignment which refers to the value of `b` (e.g., `a := 2 * b;`). In this case `a` is a dependent variable which depends on `b`.
- There is a _control dependency_ between the variables `a` and `b` if `a` has an assignment which is conditional, depending on the value of `b`. For example, if there is an assignment `a := c + d;` on a transition which includes `b` in its guard. Note that a variable `v` can have control dependency on variables which are not present in the guard of the transition assigning `v`. To determine the control dependencies an over-approximating heuristic is used. For this first the _unconditional locations_ are identified. A location is _unconditional_ if every path from the initial location will go through the given location. The control dependencies for a given assignment of a variable are determined based on the guards between the nearest unconditional location before the place of assignment and the transition of assignment.

The main steps of the algorithm are the following:
1. First, the reduction will find all frozen variables. The variables present in frozen assignments will be made frozen too.
2. Then, it will collect the variable dependencies using `VariableDependencies.collect()`. For diagnostic purposes, the variable dependency graph created this way can be visualized using `PrintVarDepGraph.print()`.
3. Next, it will collect the _needed variables_. A variable is _needed_ if it is frozen, or if there is an already needed variable which depends on it (either through an assignment or a control dependency). Here, a fixed point is computed.
4. The unnecessary variables are removed:
   - The assignments assigning unnecessary variables are removed.
   - Guards containing unnecessary variables are replaced with `[TRUE]`.
   - The last step may have lead to parallel transitions having `[TRUE]` guards and no assignments. They will be removed.
   - If there are any more uses of the unnecessary variables (which is unexpected), they are removed and logged (with _Warning_ severity).
   - Finally, the unnecessary variables are removed.

{% hint style='example' %}
**Example.**
Take the following SCL code as example: 
```scl
FUNCTION coi_func1 : INT
	VAR
		b, c, d : INT;
		x, y, z : INT;
	END_VAR

	IF x > 0 THEN
		y := z;
	END_IF;
	
	IF b > 0 THEN
		d := c;
	END_IF;
	
	//#ASSERT d > 0
END_FUNCTION
```

If it is translated into CFA and then expression propagation is used, the result will be equivalent to having the following code translated into CFA:

```scl
FUNCTION coi_func1 : INT
	VAR
		b, c, d : INT;
	END_VAR
	
   // 'x', 'y' and 'z' are not needed

	IF b > 0 THEN
		// 'b' is needed because it is a control dependency of 'd'
		// 'c' is needed because it is an assignment dependency of 'd'
		d := c;
	END_IF;
	
	//#ASSERT d > 0
	// 'd' is needed because of the requirement
END_FUNCTION
```
{% endhint %}