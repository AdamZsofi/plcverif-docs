# PLC Language Frontends

PLC language frontends are responsible for translating the PLC programs to be analyzed into control flow automata.

PLCverif is shipped with one PLC language frontend that can parse STEP 7 PLC programs written in SCL or STL language. Due to the complexity of this frontend, its description is located in a dedicated [chapter](../s7frontend/README.md).