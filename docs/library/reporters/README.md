## Verification reporters

The _verification reporters_ are responsible to represent the results of a verification job. Each _verification reporter implements the `cern.plcverif.verif.extensions.reporter` extension point, essentially providing an implementation for the `IVerificationReporter` extension for the [verification job](../../architecture/verif/VerificationJob.md). 

See more about verification reporter extensions in the [verification job documentation](../../architecture/verif/VerificationJob.md#reporter).

In the following, the verification reporters included in the library shipped with PLCverif are listed.

### Plain text reporter  {#plaintext}

The plain text reporter (`cern.plcverif.library.reporter.plaintext`, ID: `plaintext`) produces simple, textual report about the result of the verification job.

#### Settings

The plain text reporter plug-in can be configured using PLCverif settings (typically with the prefix `job.reporters.?.`) which will be represented by a `PlaintextReporterSettings` instance.

| Setting name (Type) | Description | Default value |
| ------------ | ----------- | ------------- |
| `minLogLevel` (`PlcverifSeverity`) | Minimum log message severity to report. | `Warning` |
| `writeToConsole` (`boolean`) | Writing the report to console enabled. | `true` |
| `writeToFile` (`boolean`) | Writing the report to file enabled. | `true` |
| `includeSettings` (`boolean`) | Includes all settings in the report. | `true` |

#### Output

The output of the plain text reporter is written to the `<model_id>.report.txt` file, inside the output directory set up for the verification job (`VerificationResult.outputDirectory`).

{% hint style='example' %}
**Example output.**
```
PlcPerf01-01
============

Requirement: The assertion 'assertion1' is not violated.
             (AG (EoC --> (__assertion_error = 0)))

**Result: the requirement is SATISFIED**

Backend: NusmvBackend



Stages
------
* Parsing source code (0 ms): Successful
* Control-flow declaration generation (1057 ms): Successful
* Requirement representation (166 ms): Successful
* Reductions (CFD) (27 ms): Successful
* Model preparation (69 ms): Successful
* Reductions (CFI) (275 ms): Successful
* NuSMV model building (21 ms): Successful
* NuSMV execution (111 ms): Successful
* NuSMV output parsing (7 ms): Successful
* Reporting (0 ms): Unknown


Console output
--------------
STDOUT

*** This is nuXmv 1.1.1 (compiled on Wed Jun  1 10:18:42 2016)
*** Copyright (c) 2014-2016, Fondazione Bruno Kessler

*** For more information on nuXmv see https://nuxmv.fbk.eu
*** or email to [...]
```
{% endhint %}


### HTML reporter {#html}

The HTML reporter (`cern.plcverif.library.reporter.html`, ID: `html`) produces a human-readable, rich HTML-based report about the result of the verification job.

#### Settings

The HTML reporter plug-in can be configured using PLCverif settings (typically with the prefix `job.reporters.?.`) which will be represented by a `HtmlReporterSettings` instance.

| Setting name (Type) | Description | Default value |
| ------------------- | ----------- | ------------- |
| `minLogLevel` (`PlcverifSeverity`) | Minimum log message severity to report. | `Warning` |

- Possible values for `minLogLevel`: `Error`, `Warning`, `Info`, `Debug` (see `PlcverifSeverity` enumeration).

#### Output

The output of the  HTML reporter is written to the `<model_id>.report.html` file, inside the output directory set up for the verification job (`VerificationResult.outputDirectory`).


### Summary reporter {#summary}

The summary reporter (`cern.plcverif.library.reporter.summary`, ID: `summary`) is responsible to generate machine-readable _summary mementos_ for each verification job execution. They can be later summarized by the [Summary job](../../architecture/verif/SummaryJob.md). 

It creates (fills based on the information available in the `VerificationJobResult`) and serializes a `VerificationMementoNode` (see at [Summary job](../../architecture/verif/SummaryJob.md#metamodel)) as settings. The `VerificationMementoNode` is defined in the `cern.plcverif.verif.summary.extensions.parsernodes` package.

#### Settings

The summary reporter plug-in can be configured using PLCverif settings (typically with the prefix `job.reporters.?.`) which will be represented by a `SummaryReporterSettings` instance.
Currently, it does not have any settings.

#### Output

The output of the summary reporter is written to the `<model_id>.report.summary` file, inside the output directory set up for the verification job (`VerificationResult.outputDirectory`).

{% hint style='example' %}
**Example output.**
```
-job = "verif"
-id = "model_PlcPerf01-01_algo_M1"
-name = "PlcPerf01-01"
-description = "PlcPerf01-1"
-sourcefiles.0 = "PlcPerf-common.scl"
-sourcefiles.1 = "PlcPerf01.scl"
-lf = "step7"
-inline = "false"
-info.backend_name = "NusmvBackend"
-info.backend_algorithm = "nuxmv-Classic-dynamic-df"
-info.requirement_text = "The assertion 'assertion1' is not violated."
-result = "Satisfied"
-result.runtime_ms = "1733"
-result.backend_exectime_ms = "111"
-result.cex_exists = "true"
-req = "assertion"
```
{% endhint %}