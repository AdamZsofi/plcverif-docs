## Requirements

The _requirements_ are responsible to represent the requirement(s) to be checked in a verification job. They may manipulate the CFA, and produce a temporal logic expression that is to be checked by the verification backend.
Each requirement type implements the `cern.plcverif.verif.extensions.requirement` extension point, essentially providing an implementation for the `IRequirement` extension for the [verification job](../../architecture/verif/VerificationJob.md). 
See more about requirement extensions in the [verification job documentation](../../architecture/verif/VerificationJob.md#requirement).

The requirement description's goal is to _represent the verification problem_. For this, two essential steps need to be done: representation of the PLC cycle and generation of the temporal logic expression that needs to be checked by the verification backend. After the execution of the verification backend, the requirement representation plug-in will be called again to try to diagnose the result, i.e., to give more information about the result based on its insights.

> [info] **PLC cycle representation**
> 
> It is important to note that the language frontend (parser) does not generate the CFA transitions required to represent the PLC cycle, as it does not have enough information for this, also because it may not be required.
> It is the task of the requirement plug-in (even though it is not evident from its name) to represent the verification problem, including the cyclic behaviour.

In the following, first the common features expected from requirement representation plugins are described. In addition, pointers are given to common facilities which help them performing these tasks.
After, the requirement description plug-ins included in the library shipped with PLCverif are listed.

> [info] **CFA model**
> 
> Note that as the requirement representation is done before the instantiation, independently from the CFA type to be used by the verification backend, the requirement representation plug-in will work on a CFA declaration.

### Common features

#### PLC cycle representation

The [`PlcCycleRepresentation`]({{ book.api_docs_core_url }}/cern/plcverif/verif/requirement/PlcCycleRepresentation.html) class provides utility methods to modify a CFA declaration model in a way that it works cyclically. This transformation is not only about creating a transition from the end location to the initial location though. It needs to take care of the proper handling of pattern variables and input variables. It is expected that the input variables are assigned a non-deterministic value at the beginning of each PLC cycle. The parameter variables have non-deterministic initial values, but they keep their values during the PLC execution.

The [`categorizeFields()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/requirement/PlcCycleRepresentation.html#categorizeFields-cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration-cern.plcverif.base.interfaces.IParserLazyResult-cern.plcverif.verif.requirement.AbstractRequirementSettings-) method can be used to determine based on the information provided by the parser and the requirement's settings, which fields need to be treated as inputs or parameters. In addition, it can also handle fixed fields, i.e., if certain inputs have constant values set up in the verification case.
This method assumes that the requirement's settings (which may override the default input variable collection inferred by the parser from the code) extends the abstract superclass `AbstractRequirementSettings`.

The [`representPlcCycle()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/requirement/PlcCycleRepresentation.html#representPlcCycle-cern.plcverif.verif.interfaces.data.VerificationProblem-cern.plcverif.verif.interfaces.data.VerificationResult-cern.plcverif.verif.requirement.PlcCycleRepresentation.FieldsInCategories-) will create a new main CFA automaton declaration, representing the verification loop (corresponding to the PLC cycle). It will include the necessary variable assignments too corresponding to the input and parameter values. It will also create the _end of cycle_ location that is often used in requirements.


#### Temporal logic requirement

It is an essential task of a requirement representation plug-in to provide the temporal requirement (CTL or LTL) necessary for the verification backend.
The backend can define the preferred format of the temporal requirement that the requirement representation plug-in needs to respect. The following three strategies are defined by the [`RequirementRepresentationStrategy`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/RequirementRepresentationStrategy.html) enumeration:
- `IMPLICIT`: Implicit strategy: the temporal logic requirement does not have reference to the end of the cycle.
- `EXPLICIT_WITH_VARIABLE`: Explicit strategy with variable: the temporal logic requirement itself ensures that the states where the current state does not represent the end of the cycle will not influence its satisfaction.
- `EXPLICIT_WITH_LOCATIONREF`: Explicit strategy with location reference: the temporal logic requirement itself ensures that the states where the current state does not represent the end of the cycle will not influence its satisfaction.

See the [API documentation of `RequirementRepresentationStrategy`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/RequirementRepresentationStrategy.html) for an example.

#### Superclass for requirement specific settings {#abstract_requirement_settings}

In order to benefit of the common facilities provided by PLCverif, it is expected that the requirement plug-in uses the _specific settings_ feature, extending the `AbstractRequirementSettings` class.
This class represents the following settings:

| Setting name (Type) | Description | Default value |
| ------------ | ----------- | ------------- |
| `inputs` (`List<String>`) | List of (PLC) variable names to be treated as inputs. | _empty list_ |
| `params` (`List<String>`) | List of (PLC) variable names to be treated as parameters. | _empty list_ |
| `bindings` (`RequirementInputBindings`) | PLC variable value bindings. | `null` |


The nested `RequirementInputBindings` has the following contents:

| Setting name (Type) | Description | Default value |
| ------------ | ----------- | ------------- |
| `variable` (`List<String>`) | Variables to be bound. | _empty list_ |
| `value` (`List<String>`) | Values to be used in binding. | _empty list_ |




### Assertion requirement {#assertion}

The assertion requirement (`cern.plcverif.library.requirement.assertion`, ID: `assertion`) represents some or all assertions present in the PLC code as temporal logic requirements.

When the PLC programs are parsed, the verification assertions present in the source code will be represented in the CFA as location annotations (`AssertionAnnotation`). The assertion requirement plug-in will create a verification problem based on these annotations.
When its `fillVerificationProblem()` is called, the following steps will be performed:
- It will check which fields are used as inputs and/or parameters (see above)
- It will filter the assertions based on the `assertion_to_check` setting, see below (i.e., it will remove the assertions which will not be checked in the current job execution)
- It will inline the assertions, i.e., the assertion annotations will be represented by transitions and a field. The value of the field will determine if any of the assertions has been violated. The field's type is determined by the chosen strategy (see the [API documentation of `AssertInliningStrategy`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/transformation/AssertInliningStrategy.html))
- It will represent the PLC cycle using the `PlcCycleRepresentation.representPlcCycle` method.
- It will create a temporal requirement representation corresponding to _none of the assertions has been violated_ in CTL.

After the execution of the verification backend, the assertion requirement plug-in will try to provide a diagnosis for violated requirements. It will try to determine which assertion was violated and in which cycle. Depending on the chosen assertion representation strategy, this may not be possible.

#### Settings

The plain text reporter plug-in can be configured using PLCverif settings (typically with the prefix `job.req.`) which will be represented by a `AssertionRequirementSettings` instance. It extends the `AbstractRequirementSettings` class, thus it also contains the [settings described above](#abstract_requirement_settings).

| Setting name (Type) | Description | Default value |
| ------------ | ----------- | ------------- |
| `assertion_to_check` (`String`) | The name of the assertion to be checked, or `*` if all assertions shall be checked. | `*` |
| `assert_representation_strategy` (`AssertRepresentationStrategy`) | Assertion (error field) representation strategy. | `INTEGER` |
| `max_expr_size_for_detailed_diagnosis` (`int`) | Maximum expression size that is considered for detailed diagnosis if the assertion has been violated. | `250` |


Note:
- Several assertions may have the same name. If multiple assertions have the given name (`assertion_to_check`), all of them will be checked, thus the verification will succeed if _all of them_ are satisfied by the program.



### Pattern requirement {#pattern}

The verification pattern requirement (`cern.plcverif.library.requirement.pattern`, ID: `pattern`) represents the given pattern-based requirement as temporal logic requirement.

A **requirement pattern** (represented internally by `Pattern`) is a temporal logic expression with pre-defined placeholders. The user can fill these placeholders with Boolean expressions over PLC variables. Each requirement pattern has a human-readable textual representation as well which contain the same placeholders. This way the user can describe a typical temporal logic requirement by filling in the gaps in a textual representation of the requirement pattern,.

#### Pattern definition

The patterns are described in an XML file, that is by default the `default-patterns.xml` in the `resources/settings` of the plug-in. It is also possible to define an external file containing other patterns. The pattern definition should match the [`patterns.xsd`](https://gitlab.cern.ch/plcverif/cern.plcverif/blob/master/bundles/library/cern.plcverif.library.requirement.pattern/resources/patterns.xsd) schema definition included in the plug-in.

#### Patterns included in the plug-in

Currently the following patterns are supported:

| Pattern ID | Textual representation | Formal representation |
| ---------- | ---------------------- | --------------------- |
| pattern-implication | If _{1}_ is true at the end of the PLC cycle, then _{2}_ should always be true at the end of the same cycle. | AG((_{PLC_END}_ AND (_{1}_)) --> (_{2}_)) |
| pattern-invariant | _{1}_ is always true at the end of the PLC cycle. | AG(_{PLC_END}_ --> (_{1}_)) |
| pattern-forbidden | _{1}_ is impossible at the end of the PLC cycle. | AG(_{PLC_END}_ --> NOT (_{1}_)) |
| pattern-statechange-duringcycle | If _{1}_ is true at the beginning of the PLC cycle, then _{2}_ is always true at the end of the same cycle. | AG((_{PLC_START}_ AND (_{1}_)) --> A[ NOT _{PLC_END}_ U _{PLC_END}_ AND (_{2}_) ]) |
| pattern-statechange-betweencycles | If _{1}_ is true at the end of cycle N and _{2}_ is true at the end of cycle N+1, then _{3}_ is always true at the end of cycle N+1. | G((_{PLC_END}_ AND (_{1}_) AND X( [ NOT _{PLC_END}_ U (_{PLC_END}_ AND (_{2}_)) ] )) --> X( [ NOT _{PLC_END}_ U (_{PLC_END}_ AND (_{3}_)) ] )) |
| pattern-reachability | It is possible to have _{1}_ at the end of a cycle. | EF(_{PLC_END}_ AND (_{1}_)) |
| pattern-repeatability | Any time it is possible to have eventually _{1}_ at the end of a cycle. | AG(EF(_{PLC_END}_ AND (_{1}_))) |
| pattern-leadsto | If _{1}_ is true at the end of a cycle, _{2}_ was true at the end of an earlier cycle. | NOT (E[(_{PLC_END}_ --> NOT (_{2}_)) U (_{PLC_END}_ AND _{1}_)]) |


#### Settings

The plain text reporter plug-in can be configured using PLCverif settings (typically with the prefix `job.req.`) which will be represented by a `AssertionRequirementSettings` instance. It extends the `AbstractRequirementSettings` class, thus it also contains the [settings described above](#abstract_requirement_settings).

| Setting name (Type) | Description | Default value |
| ------------ | ----------- | ------------- |
| `pattern_id` (`String`) | ID of the pattern to be checked. | --- (mandatory) |
| `pattern_params` (`List<String>`) | Parameters (expressions) to be substituted in the placeholders of the pattern. The number of given parameters shall match the number of placeholders in the selected pattern. | _empty list_ |
| `pattern_file` (`Path`) | Pattern definition XML file to be used. Leave empty to use the built-in patterns. | `null` |

Note:
- The `pattern_id` shall match one of the pattern IDs above if no `pattern_file` is given, or one of the patterns in the provided `pattern_file`.



#### Example pattern definition file

```xml
<?xml version="1.0" encoding="UTF-8"?>
<patterns>
  <pattern id="pattern-implication">
    <description>Implication</description>
    <tlexpr type="ctl">AG(({PLC_END} AND ({1})) --> ({2}))</tlexpr>
    <humanreadable>If {1} is true at the end of the PLC cycle, then {2} should always be true at the end of the same cycle.</humanreadable>
    <parameter key="1">
    	<description>Condition</description>
    </parameter>
    <parameter key="2">
    	<description>Implication</description>
    </parameter>
  </pattern>
  
  <pattern id="pattern-invariant">
    <description>Invariant</description>
    <tlexpr type="ctl">AG({PLC_END} --> ({1}))</tlexpr>
    <humanreadable>{1} is always true at the end of the PLC cycle.</humanreadable>
    <parameter key="1">
    	<description>Invariant to be respected at the end of each cycle.</description>
    </parameter>
  </pattern>  
</patterns>
```

Notes:
- The pattern parameters can be defined using the `parameter` tag. The key should be an integer.
- The temporal logic expression (`tlexpr`) should follow the textual syntax of the expression parser (see its [grammar](../../reference/ExpressionGrammar.md)).
  - It has to refer to the parameters using `{key}`.
- The `PatternUtils.validate()` method provides validation for a parsed pattern. See its [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/library/requirement/pattern/configuration/PatternUtils.html#validate-cern.plcverif.library.requirement.pattern.configuration.jaxb.Pattern-) for the checked features.