## Verification backends

The _verification backends_ are responsible to evaluate a given requirement on a given CFA in a verification job. Each verification backend implements the `cern.plcverif.verif.extensions.backend` extension point, essentially providing an implementation for the `IBackend` extension for the [verification job](../../architecture/verif/VerificationJob.md). 

See more about verification backend extensions in the [verification job documentation](../../architecture/verif/VerificationJob.md#backend).

Currently, the following verification backends are included in the library shipped with PLCverif:
- [NuSMV / nuXmv](Nusmv.md)
- [Theta](Theta.md)
- [CBMC](Cbmc.md) (experimental)