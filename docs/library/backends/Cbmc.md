### CBMC Integration

#### About CBMC

[CBMC](http://www.cprover.org/cbmc/) is a bounded model checker for C programs, developed at University of Oxford and the Carnegie Mellon University (CMU). It is distributed under a [custom license](http://www.cprover.org/cbmc/LICENSE) that permits usage and redistribution under certain conditions.


#### Overview and Architecture

The integration consists of the following three main steps, which are detailed in the next sections.

1. First, the verification problem (the CFA and the requirement) of PLCverif is _transformed_ to C. Depending on the configuration, the source CFA model may be an instance or a declaration model.
1. Then CBMC is _executed_ with the verification problem (C program that includes the requirement).
1. Finally, the output of CBMC is _parsed_ and _mapped back_ to the internal model of PLCverif. If the requirement is violated, a counterexample is also given.

The integration code is located in the project `cern.plcverif.library.backend.cbmc`. The main package contains some common classes required by the framework, or used by all three steps. Most of the steps are located in its own package as follows.

1. `model` contains the model transformation classes.
1. The implementation CBMC execution is directly in the `CbmcBackend` class.
1. `parser` contains the output parsing class.


#### Model generation for CBMC

The CBMC supports three representation types:
1. Instance-based, in which case the integration plug-in expects CFA models which are:
    *  instantiated (i.e., CFA is a `CfaNetworkInstance`),
    *  enumerated (i.e., there are no arrays in the CFA),
    *  inlined (i.e., there are no call transitions in the CFA).
1. Declaration-based, in which case the integration plug-in expects CFA models which are:
    *  non-instantiated (i.e., CFA is a `CfaNetworkDeclaration`),
    *  non-enumerated (i.e., there may be arrays in the CFA),
    *  non-inlined (i.e., there may be call transitions in the CFA).
1. Structured declaration-based, in which case the integration plug-in expects CFA models with the same qualities as the declaration-based, above. Furthermore, it requires the presence of CFD annotations present in the CFA (which also means that no reductions can be executed on the model).

The CBMC integration uses the `IMPLICIT` strategy for **requirement representation**. Note that only safety requirements that can be represented as assertions are supported.

##### Basic principles of C representation
The regular instance- and declaration-based C representations do not reconstruct the original structured program. Instead, each CFA location will correspond to a label in the C code. CFA transitions are represented using conditional statements (to represent the guards) and jumps (`goto`).

The structured declaration-based representation attempts to recreate the control structures found in the original program.
See [Gy. Sallai, D. Darvas, E. Blanco: Testing, simulation, and visualisation of PLC programs using x86 code generation. Techical report EDMS 1844850, CERN, 2017.](http://edms.cern.ch/document/1844850) for more details.

##### Instance-based C representation
TODO

##### Declaration-based C representation
TODO


#####  Known limitations
- TODO
- TODO


#### Executing CBMC

CBMC is executed through the command line. The parameters are passed via command line arguments.

CBMC is called using the following pattern, depending on the operating system: 
- On Windows: `cmd.exe /c "call <vs_init_batch> & <timeout_executor> "<binary_path>" "<model_path> --unwind <unwind> --verbosity <verbosity> --partial-loops --trace"`.
- On Linux: `'<binary_path>' <model_path> --unwind <unwind> --verbosity <verbosity> --partial-loops --trace"`.

Notes:
- CBMC relies on Visual Studio's C compiler. The `<vs_init_batch>` is a path to the batch file included in Visual Studio (`vcvars64.bat`, typically located at `C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat`) which sets up the necessary environment variables.
- Interruption of CBMC's execution upon timeout from Java is not reliable. Therefore on Windows the integration plug-in relies on an external program (located at `<timeout_executor>`) that enforces the set timeout.
- The option `--partial-loops` is required as there may be some spurious loops due to the "irregular" order of location representations.

##### Settings

The following settings are available for the CBMC backend (defined by `CbmcSettings`):

- `timeout`: Time limit for the execution of CBMC, in seconds. (default: 5 sec)
- `binary_path`: Path of the NuSMV/nuXmv binary. Can be a path relative to the program directory of PLCverif. (default: `.\tools\cbmc\cbmc.exe` on Windows and `./tools/cbmc/cbmc` on Linux)

- `vs_init_batch`: Path to the vcvars64.bat of Visual Studio (required on Windows only). (default: `.\tools\cbmc\TimeoutExecutor.dll` on Windows)
- `timeout_executor_path`: Full path of the TimeoutExecutor binary (required on Windows only).

- `unwind`: Loop unwind count. (default: 10)
- `verbosity`: CBMC output verbosity (1..10). With verbosity levels 1..3 the result of verification is not printed, thus it cannot be parsed by PLCverif. (default: 7)
- `model_variant`: Model variant (CFI or CFD). Permitted values: `CFI` and `CFD`. (default: `CFD`)


#### Parsing the output

CBMC writes its output to the standard output. It will contain the result of the verification (satisfied or violated), and the counterexample/witness if available.
The variables and values of the counterexample are mapped back to the CFA after parsing the CBMC output.
Parsing is done by `CbmcOutputParser`, then mapping back to CFA variables and values relies on the model builders, implementing `ITextualCexParser`.
