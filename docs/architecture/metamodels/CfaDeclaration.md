## CFA Declaration

The _CFA Declaration_ metamodel defines a control-flow network where the modeling is on the level of declarations. The defined automata are only structural definitions. To execute an automaton, a _context_ shall be given that is a field with a data type corresponding to the automaton. 

> **[info] Usage information**
>
> The _CFA Declaration_ metamodel is defined to make the instantiation independent from the used language frontend. Furthermore, it provides a higher abstraction level for certain verification tools, such as software model checkers or static analysis tools. This metamodel is not intended to be used for automata-based model checkers backends, such as NuSMV. 


### CFA declaration metamodel overview
![Main parts of the CFA declaration metamodel](img/CfaDeclaration.png)

The main concepts of the CFA instance metamodel are the following.

**Data handling.**

- A **field** (`Field`) is a typed variable declaration within a _data structure_. The type of a field can be an elementary type (defined in the [expression metamodel]()), an array type (`ArrayType`) or a data structure (`DataStructureRef`).
  - A field may have initial assignments which are to be performed before the initial location of the main automaton becomes active.
- A **data structure** (`DataStructure`) is a collection of _fields_ and named data structures ("type definitions").
- There is a single _root data structure_ defined for each _automata network_. Only the fields declared (directly or transitively) in this root data structure will be part of the state space. 
- Note that the data structures and fields build up a hierarchy. A **data reference** (`DataRef`) is a hierarchical reference to a given field. It can be _global_ if its root element is the root data structure, or it can be _local_ (relative). A data reference element can be a reference to a field (`FieldRef`) or an indexing (`Indexing`) in case of a field with an array type.

{% hint style='example' %}
**Example.**
Assume that the root data structure is `r`. 
Let `r` contain the nested data structure `s1` with fields `sf1 : int` and `sf2 : int`. Let `r` contain addition the field `f1 : int` and field `f2 : sf1`. Then the state space will contain the following integers: `r.f1`, `r.f2.sf1`, `r.f2.sf2`. If there is an automaton with `s1` as local data structure, then that automaton can be called with `r.f2` as callee context (or main context if that automaton is the main automaton). 
{% endhint %}

```dot
digraph G {
	r -> s1;
	s1 -> sf1, sf2;
	r -> f1, f2;

	r [label="r\n(root data structure)", style="filled", fillcolor="grey"];
	s1 [label="s1\n(data structure)", style="filled", fillcolor="grey"];
	f1 [label="f1\n(int field)"];
	f2 [label="f2\n(field of type s1)"];
	sf1 [label="sf1\n(int field)"];
	sf2 [label="sf2\n(int field)"];
}
```


**Automata.**

- A **network** (`CfaNetworkDeclaration`) consists of several _automaton declarations_, of which one is the so-called _main automaton_. It also contains a _root data structure_. 
- An **automaton declaration** (`AutomatonDeclaration`) describes the structure of the automaton. It contains _transitions_ and _locations_. Each automaton declaration refers to a _local data structure_, i.e., a data structure which contains the declarations of the local variables of the given automaton declaration, i.e., the variables which need to be instantiated with the automaton declaration.
  - The execution of a CFA declaration starts with the initial location of the defined main automaton (`CfaNetworkDeclaration.mainAutomaton`), with the main context (`CfaNetworkDeclaration.mainContext`) as context.
- The **transitions** in the _automaton declaration_ can be assignment and call transitions. A transition can only fire if its source location is active and its guard (`condition`) is satisfied.
  - When an **assignment transition** fires, the attached (zero, one or many) **variable assignments** (`VariableAssignment`) will be performed.
  - When a **call transition** fires, the following steps are performed for each defined call (`Call`), in a non-defined order between the different calls:
    1. The input variable assignments (`Call.inputAssignments`) are performed,
    2. The callee automaton's initial location (`initialLocation`) will be active, with the callee context (`calleeContext`) defined in the call. The `calleeContext` is expected to be a reference to a field or array element with the callee's local data structure (`AutomatonDeclaration.localDataStructure`) as type.
    3. When the callee automaton reaches its end location (`endLocation`), the output variable assignments (`Call.outputAssignments`) are performed.
    4. If all calls of the call transition have finished, the active location will be the target location of the call transition (`CallTransition.target`).


See more in the [detailed metamodel documentation](../../reference/CfaDeclarationXcore.md).



### Factory {#factory}

There is a `CfaDeclarationSafeFactory` defined for the safer and more convenient object creation. See the [description of `ExprSafeFactory`](ExpressionMetamodel.md#factory) for the ideas and conventions behind this factory. Note that as `CfaDeclarationSafeFactory` extends `ExpressionSafeFactory`, the objects of the expression metamodel can be created via an instance of the `CfaDeclarationSafeFactory` class.

### Utilities {#utils}

The `cern.plcverif.base.models.cfa` package provides various additional utilities for expression handling.

- **Builders.** The `cern.plcverif.base.models.cfa.builder` contains builder classes for more convenient creation of more complex objects. `SequentialAssignments` can create consecutive locations and transitions to represent a list of assignments in the automaton (without any interleaving). `TransitionBuilder` provides a more convenient, builder-based interface for CFD transitions.
  {% hint style='example' %}
  **Example (Transition builder).** The following Java code (not using the builder)
  ```java
  Transition t = CfaDeclarationSafeFactory.INSTANCE.createAssignmentTransition(
	  transitionName,
	  parentAutomaton, // shall equal to sourceLocation.getParentAutomaton()
	  sourceLocation,
	  targetLocation,
	  transitionGuard
  );
  CfaDeclarationSafeFactory.INSTANCE.createAssignment(t, left1, right1);
  CfaDeclarationSafeFactory.INSTANCE.createAssignment(t, left2, right2);
  ```
  is equivalent to:
  ```java
  Transition t = TransitionBuilder.builder()
	.from(sourceLocation)
	.to(targetLocation)
	.name(transitionName)
	.condition(transitionGuard)
	.addAssignment(VariableAssignmentSkeleton.create(left1, right1))
	.addAssignment(VariableAssignmentSkeleton.create(left2, right2))
	.build();
  ```
  or to:
  ```java
  Transition t = TransitionBuilder.builderBetween(sourceLocation, targetLocation)
	.name(transitionName)
	.condition(transitionGuard)
	.addAssignment(VariableAssignmentSkeleton.create(left1, right1))
	.addAssignment(VariableAssignmentSkeleton.create(left2, right2))
	.build();
  ```
  {% endhint %}
- **Serialization.** The `CfaToXmi` and `XmiToCfa` classes provide serialization and deserialization from and to XMI format for CFAs.
- **String representation.** The `CfaToString` class provides textual representation of expressions for diagnostic purposes.
- **CFA transformation.**
  - The `AssertInliner` will modify the structure of the automaton to express the assertion annotations with transitions having the appropriate conditions. It will create a new field whose value will permit checking whether the assertion has been violated. The type of this field is determined by the chosen `AssertionInliningStrategy`. Tracing will be provided by the returned `AssertionInliningResult`. More information about the generic assertion-handling PLCverif functionality can be read [here](../../library/requirements/AssertionHandling.md).
  - The `CfaInstantiator` class can create an equivalent CFA instance model for a given CFA declaration model. See the description of the [CFA instance metamodel](CfaInstance.md) for more details.
  - The `CfdInitialAssignmentsToTransition` class will modify the CFA declaration model in a way that the initial value assignments will be explicitly represented in the main automaton.
- **Validation.** The `CfaValidation.validate()` can be used to validate a CFA model. It will check the model against the EMF metamodel's constraints, as well as for certain additional validation rules (implemented in `CfaDeclarationValidation` and `ExprValidation`).
  - If called as `validate(CfaNetworkBase)`, it will throw a `CfaValidationException` exception is the model violates any of the requirements.
  - If called as `validate(CfaNetworkBase, IPlcverifLogger)`, it will not throw any exception, but it will log every violation to the given logger with error severity.
- **Utility classes.**
  - The `ArrayInitialValueHelper` helps determining the initial values of a field with array data type.
  - The `CfaDeclarationUtils` utility class contain helpful static methods for handling CFA declaration models, such as determining the referred field for a data reference (`getFieldOrFail()`), concatenating data references (`concatenateDataRefs()`), etc.
  - The `EquatableDataRef` is an optimized version of `EquatableEObject` specifically for CFA declaration data references. (`EquatableEObject` is a wrapper for any `EObject` whose `equals()` method does not check reference equality, but it calls `EcoreUtil.equals()`, thus it is suitable for `HashSet` and `HashMap`.)
  - The `CfaUtils` class provide various utility methods, mainly for handling (Reading and writing) annotations.
- **Visualization.** The `CfaToDot.represent()` method can be used to transform a CFA model into a GraphViz-compatible textual representation for visualization. The format can be altered by implementing the `ICfaToDotFormat` interface and pass it to the `representCfd` method.
  - The *locations* are denoted by ellipses. 
    
    {% graphviz %}digraph G { dummy [label="loc1", color="black", style="filled", fillcolor="white", shape="ellipse"]; }{% endgraphviz %}  
      * The *frozen* locations are shown in blue.
        
        {% graphviz %}
        digraph G { dummy [label="loc1", color="black", style="filled", fillcolor="cadetblue1", shape="ellipse"]; }
        {% endgraphviz %}    
      * The *initial* location of an automaton declaration is shown with double border.
        
        {% graphviz %}
        digraph G { dummy [label="init", color="black", style="filled", fillcolor="white", peripheries=2, shape="ellipse"]; }
        {% endgraphviz %} 
      * The *end* location of an automaton declaration is shown with thick border.
        
        {% graphviz %}digraph G { dummy [label="end", color="black", style="filled", fillcolor="white", style=bold, shape="ellipse"]; }{% endgraphviz %} 
  - The *transitions* are denoted by arrows from the source location to the target location.
    * The condition (guard) of a transition is shown between square brackets. The `[true]` condition is not shown.
      
      {% graphviz %}digraph G { 
         loc1 [label="loc1", color="black", style="filled", fillcolor="white", shape="ellipse"];
         loc2 [label="loc2", color="black", style="filled", fillcolor="white", shape="ellipse"];
         loc1 -> loc2 [color="black", label = "[in >= 0]"];
      }{% endgraphviz %} 
    * The variable assignments are shown below the guard, separated by `;`.
      
      {% graphviz %}digraph G { 
         loc1 [label="loc1", color="black", style="filled", fillcolor="white", shape="ellipse"];
         loc2 [label="loc2", color="black", style="filled", fillcolor="white", shape="ellipse"];
         loc1 -> loc2 [color="black", label = "[in >= 0]\la/b := TRUE;\la/c := FALSE;\l"];
      }{% endgraphviz %} 
    * The calls on call transitions are shown as `CALL <called automaton>:<call context> (IN: <input assignments> OUT: <output assignments>)`:
      
      {% graphviz %}digraph G { 
         loc1 [label="loc1", color="black", style="filled", fillcolor="white", shape="ellipse"];
         loc2 [label="loc2", color="black", style="filled", fillcolor="white", shape="ellipse"];
         loc1 -> loc2 [color="black", label="CALL: FB_XY:DB_XY(\l   IN: IN1 := LX0.0\l, IN2 := LX0.1\l,   OUT: DB_XY/RES := RES\l)\l"];
      }{% endgraphviz %} 
  - The *automata declarations* are shown as subgraphs (rectangles). The locations and the transitions of the given automaton are within this rectangle. The label on the top shows the name of the automaton declaration and its local data structure, separated by `:`. The annotations present in the given automaton are also shown here, as a rectangle.
    
    {% graphviz %}digraph G { 
        subgraph clusterDummy {
          color="black"; fontsize=7; ranksep=0.4;
          label="AutomatonName : AutomatonDS";
          annotations_pseudonode_Dummy [label="ANNOTATIONS", fontsize=9, margin="0.04,0.04", fillcolor="white", shape="rectangle", style="dashed"];
          loc1 [label="loc1", color="black", style="filled", fillcolor="white", peripheries=2,shape="ellipse"];
          loc2 [label="loc2", color="black", style="filled", fillcolor="white", style=bold, shape="ellipse"];
          loc1 -> loc2 [color="black", label = ""];
        }  
      }{% endgraphviz %}   
    * The main automaton is denoted by thick border.  
  - The data structures and fields are shown in another subgraph (cluster).
    * The defined data structures (`DataStructure`) are shown as gray folders.
      
      {% graphviz %}digraph G { dummy [label="VerificationLoopDS", shape="folder", style="filled"]; }{% endgraphviz %} 
    * The defined fields (`Field`) are shown as white folders. Their label is in the following format: `(F) <name> : <type>`. The type can be a reference to a defined data structure or an elementary type.
      
      {% graphviz %}digraph G { dummy [label = "(F) RET_VAL : signed int16", fillcolor="white", shape="folder", style="filled"]; }{% endgraphviz %} 
    * From any data structure there are arrows pointing to the data structures and fields directly contained in it.
    * The root data structure of the network (`CfaNetworkDeclaration.rootDataStructure`) is the root of the hierarchy.
    * The annotations and the initial values are shown in dashed rectangles next to the containing data structures.