## Detailed verification example

This detailed verification example will guide you through the complete verification workflow, describes the internal mechanisms of the PLCverif verification at a high level and gives pointers to the corresponding parts of the documentation for more information. Essentially, it shows what is under the hood after you click the _Verify!_ button.

The example source code and verification case used here is the same as the first [example in the user documentation](../../userdocs/Demo.md). It checks whether the output of an absolute value function is always non-negative.

{% hint style='example' %}
As a recap, the **source code under verification** is the following:
```scl
FUNCTION FuncAbs : INT
    VAR_INPUT
    in : INT;
    END_VAR
BEGIN
    IF in > 0 THEN
    FuncAbs := in;
    ELSE
    FuncAbs := -in;
    END_IF;
END_FUNCTION
```

The **verification case** is the following (in the textual representation):
```
 -description = "The return value of the absolute function shall always be non-negative (zero or positive)."
 -id = case1
 -job = verif
 -job.backend = nusmv
 -job.backend.algorithm = Classic
 -job.req = pattern
 -job.req.pattern_id = pattern-invariant
 -job.req.pattern_params.1 = "FuncAbs.RET_VAL >= 0"
 -lf = step7
 -lf.entry = FuncAbs
 -name = "Return value is always non-negative"
 -sourcefiles.0 = *.scl
```

Note that the ID of the verification case is `case1`. It will be used in the example later on.
See the [example in the user documentation](../../userdocs/Demo.md) for more details.
{% endhint %}

After clicking on the _Verify!_ button (or executing PLCverif from the command line), the following main steps will be executed.

1. **Parse settings.** The verification case file will be parsed as a [settings node hierarchy](../api/Settings.md). 

   {% hint style='example' %}
Once the above settings file is parsed using the [`SettingsSerializer.parseSettingsFromFile()`]({{ book.api_docs_core_url }}/cern/plcverif/base/common/settings/SettingsSerializer.html#parseSettingsFromFile-java.lang.String-) method, the settings node tree will look like the object model below.
   {% endhint %}

{% uml %}
@startuml

object ":SettingsElement" as root
root : name = null
root : value = null

object ":SettingsElement" as id
root *-- id : attributes["id"]
id : name = "id"
id : value = "case1"

object ":SettingsElement" as name
root *-- name : attributes["name"]
name : name = "name"
name : value = "Return value is always non-negative"

object ":SettingsElement" as description
root *-- description : attributes["description"]
description : name = "description"
description : value = "The return value [...]"

object ":SettingsElement" as job
root *-- job : attributes["job"]
job : name = "job"
job : value = "verif"

object ":SettingsElement" as job_backend
job *-- job_backend : attributes["backend"]
job_backend : name = "backend"
job_backend : value = "nusmv"

object ":SettingsElement" as job_backend_algo
job_backend *-- job_backend_algo : attributes["algorithm"]
job_backend_algo : name = "algorithm"
job_backend_algo : value = "Classic"

object ":SettingsElement" as job_req
job *-- job_req : attributes["req"]
job_req : name = "req"
job_req : value = "pattern"

object ":SettingsElement" as job_req_patternid
job_req *-- job_req_patternid : attributes["pattern_id"]
job_req_patternid : name = "pattern_id"
job_req_patternid : value = "pattern-invariant"

object ":SettingsList" as job_req_patternparams
job_req *-- job_req_patternparams : attributes["pattern_params"]
job_req_patternparams : name = "pattern_id"

object ":SettingsElement" as job_req_patternparam1
job_req_patternparams *-- job_req_patternparam1 : [1]
job_req_patternparam1 : name = "1"
job_req_patternparam1 : value = "FuncAbs.RET_VAL >= 0"

object ":SettingsElement" as lf
root *-- lf : attributes["lf"]
lf : name = "lf"
lf : value = "step7"

object ":SettingsElement" as lf_entry
lf *-- lf_entry : attributes["entry"]
lf_entry : name = "entry"
lf_entry : value = "FuncAbs"

object ":SettingsList" as sourcefiles
root *-- sourcefiles : attributes["sourcefiles"]
sourcefiles : name = "sourcefiles"

object ":SettingsElement" as sourcefile0
sourcefiles *-- sourcefile0 : [0]
sourcefile0 : name = "0"
sourcefile0 : value = "*.scl"
@enduml
{% enduml %}


1. **Create verification job.** Based on the content of the settings, an appropriate [job](../api/PlatformExtensions.md#job) object will be created (in this case a [`VerificationJob`]({{ book.api_docs_core_url }}/cern/plcverif/verif/job/VerificationJob.html) object) using the corresponding [job extension]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/IJobExtension.html). In this case, a [`VerificationJobExtension`]({{ book.api_docs_core_url }}/cern/plcverif/verif/job/VerificationJobExtension.html) object will be used to create the job.
2. The verification job creates a new [verification result](VerificationJob.md#verificationresult) object ([`VerificationResult`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationResult.html)) that will collect the results of the job.
3. **Parse PLC program.** Then, using the given language frontend (parser), the selected source files will be parsed via the [`IParser.parseCode()`]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/IParser.html#parseCode-cern.plcverif.base.interfaces.data.JobResult-) method.
   - As the selected parser is the _STEP 7_ parser, the [`Step7Parser.parseCode()`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7Parser.html) method will return a [`Step7ParserResult`]({{ book.api_docs_step7_url }}/cern/plcverif/plc/step7/extension/Step7ParserResult.html) object which implements [`IParserLazyResult`]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/IParserLazyResult.html). Note that at this point the program code is not parsed yet, only a lazy representation is returned.
4. **Generate CFA corresponding to the PLC program.** The verification job requests the CFA generation via the [`IParserLazyResult.generateCfa()`]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/IParserLazyResult.html#generateCfa-cern.plcverif.base.interfaces.data.JobResult-) method.
   
   In case of the STEP 7 parser, the following steps will be done:
     1. The selected source files will be loaded and parsed using the [Xtext STEP 7 grammar](../../reference/Step7Grammar.md). If there is a parsing error, an `AstParsingException` is thrown.
     2. If there are no errors, the CFA will be generated using the `Step7CodeToCfa.convert()` method.

   {% hint style='example' %}
The parsed SCL code can be shown using an intuitive textual format, depicting the EMF model hierarchy (containment) tree. Either right click on the source file in PLCverif and select _Generate textual representation_, or call the [`EmfModelPrinter.print()`]({{ book.api_docs_core_url }}/cern/plcverif/base/common/emf/textual/EmfModelPrinter.html) method.

For the example above, the textual representation is the following:
```
{ProgramFile}
 ├──verificationOptions: <empty>
 ├──globalVariables: <empty>
 └──programUnits: 
    {Function name=FuncAbs builtIn=false}
     ├──attributes: <null>
     ├──declarationSection: 
     │  {DeclarationSection}
     │   ├──variableDeclarations: 
     │   │  {VariableDeclarationBlock direction=VAR_INPUT}
     │   │   └──variables: 
     │   │      {VariableDeclarationLine initialized=false}
     │   │       ├──variables: 
     │   │       │  {Variable name=in reference=false ref:<null>}
     │   │       ├──type: 
     │   │       │  {ElementaryDT type=INT}
     │   │       └──initialization: <null>
     │   ├──constantDeclarations: <empty>
     │   └──labelDeclarations: <empty>
     ├──statements: 
     │  {SclStatementList}
     │   └──statements: 
     │      {SclIfStatement else=true}
     │       ├──condition: 
     │       │  {ComparisonExpression operator=>=}
     │       │   ├──left: 
     │       │   │  {DirectNamedRef ref:{NamedElement name=in}}
     │       │   └──right: 
     │       │      {IntConstant value=0}
     │       ├──thenBranch: 
     │       │  {SclStatementList}
     │       │   └──statements: 
     │       │      {SclAssignmentStatement}
     │       │       ├──leftValue: 
     │       │       │  {DirectNamedRef ref:{NamedElement name=FuncAbs}}
     │       │       └──rightValue: 
     │       │          {DirectNamedRef ref:{NamedElement name=in}}
     │       ├──elsifBranches: <empty>
     │       └──elseBranch: 
     │          {SclStatementList}
     │           └──statements: 
     │              {SclAssignmentStatement}
     │               ├──leftValue: 
     │               │  {DirectNamedRef ref:{NamedElement name=FuncAbs}}
     │               └──rightValue: 
     │                  {UnaryExpression op=-}
     │                   └──expr: 
     │                      {DirectNamedRef ref:{NamedElement name=in}}
     │      {SclVerificationAssertion tagged=true tag=name_of_the_assertion}
     │       └──expression: 
     │          {ComparisonExpression operator=>}
     │           ├──left: 
     │           │  {DirectNamedRef ref:{NamedElement name=FuncAbs}}
     │           └──right: 
     │              {IntConstant value=0}
     └──returnType: 
        {ElementaryDT type=INT}
```

The CFA representation created for a given source code is can be visualized by right-clicking on the source file, then selecting _Generate CFA representation_. 
Three of the generated files are interesting for us here:
- `<filename>.cfd` is an XMI representation of the 
  The first 10 lines (out of the total 110) will give you a taste of this representation:
  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
    <cfadeclaration:CfaNetworkDeclaration xmi:version="2.0" xmlns:xmi="http://www.omg.org/XMI"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xmlns:cfabase="cern.plcverif.base.models.cfa.cfabase"
        xmlns:cfadeclaration="cern.plcverif.base.models.cfa.cfadeclaration"
        xmlns:expr="cern.plcverif.base.models.expr" xmi:id="network"
        name="network" displayName="network" mainAutomaton="network_FuncAbs">
    <automata xmi:id="network_FuncAbs" name="FuncAbs" displayName="FuncAbs"
        initialLocation="network_FuncAbs_init" endLocation="network_FuncAbs_end">
        <locations xmi:id="network_FuncAbs_init" name="init" displayName="init"
            frozen="true" outgoing="network_FuncAbs_t1 network_FuncAbs_t3">
            <annotations xsi:type="cfabase:LineNumberAnnotation" xmi:id="id0"
                fileName="__synthetic0.SCL" lineNumber="6"/>
            <annotations xsi:type="cfabase:IfLocationAnnotation" xmi:id="id1"
                endsAt="network_FuncAbs_l5" then="network_FuncAbs_t1"/>
        </locations>
        <locations xmi:id="network_FuncAbs_l1" name="l1" displayName="l1"
            incoming="network_FuncAbs_t1" outgoing="network_FuncAbs_t2">
            <annotations xsi:type="cfabase:LineNumberAnnotation" xmi:id="id2"
                fileName="__synthetic0.SCL" lineNumber="7"/>
        </locations>
    [...]   
  ``` 
  The advantage of this representation is that it can be easily parsed into an EMF object model using the [`XmiToCfa.deserializeFromString()`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/serialization/XmiToCfa.html) or [`XmiToCfa.deserializeFromFile()`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/serialization/XmiToCfa.html#deserializeFromString-java.lang.String-)  method.
- `<filename>.cfd.txt` is a textual representation of the CFA declaration in the same format as above for the parsed source code. It can be regarded as a human-readable, reduced version of the above XMI representation. It starts as follows:
  ```
  {CfaNetworkDeclaration name=network displayName=network mainAutomaton:FuncAbs}
   ├──automata: 
   │  {AutomatonDeclaration name=FuncAbs displayName=FuncAbs initialLocation:init endLocation:end container:network}
   │   ├──locations: 
   │   │  {Location name=init displayName=init frozen=true parentAutomaton:FuncAbs incoming:[] outgoing:[t1,t3]}
   │   │   └──annotations: 
   │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=6 parentLocation:init}
   │   │      {IfLocationAnnotation parentLocation:init endsAt:l5 then:t1}
   │   │  {Location name=l1 displayName=l1 frozen=false parentAutomaton:FuncAbs incoming:[t1] outgoing:[t2]}
   │   │   └──annotations: 
   │   │      {LineNumberAnnotation fileName=__synthetic0.SCL lineNumber=7 parentLocation:l1}
   [...]
  ```
- `<filename>.cfd.dot` is a graphical representation of the CFA declaration network which can be visualized using GraphViz. The graphical representation for the CFA declaration network of this `FuncAbs` example is the following:
  
   ![CFA declaration network generated for the FuncAbs function](img/example-abs/step7-cfd.png)

   Note that there is no loop in the single automaton declaration (i.e., no transition from `end` to `init`). See the [CFA declaration documentation](../metamodels/CfaDeclaration.md#utils) for the legend of this graphical representation.
   {% endhint %}

6. Next, a [verification problem object](VerificationJob.md#verificationproblem) ([`VerificationProblem`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html)) is created. This will hold the model and the temporal logic requirement to be checked by the verification backend, i.e., this will be the input for the backend plug-in.
7. **Represent requirement and verification CFA.** The selected requirement (in this case the pattern requirement) will be used to represent the verification problem. For this purpose, the [`IRequirement.fillVerificationProblem()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IRequirement.html#fillVerificationProblem-cern.plcverif.verif.interfaces.data.VerificationProblem-cern.plcverif.base.interfaces.IParserLazyResult-cern.plcverif.verif.interfaces.data.VerificationResult-cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy-) method will be called (`PatternRequirement.fillVerificationProblem()` for the pattern requirement). The pattern requirement will do the following steps:
   - It will categorize the fields (parsed PLC variables) using [`PlcCycleRepresentation.categorizeFields()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/requirement/PlcCycleRepresentation.html#categorizeFields-cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration-cern.plcverif.base.interfaces.IParserLazyResult-cern.plcverif.verif.requirement.AbstractRequirementSettings-), as described [here in the documentation](../../library/requirements/README.md).
   - It will then create the representation for the cyclic PLC execution using [`PlcCycleRepresentation.representPlcCycle()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/requirement/PlcCycleRepresentation.html#representPlcCycle-cern.plcverif.verif.interfaces.data.VerificationProblem-cern.plcverif.verif.interfaces.data.VerificationResult-java.util.List-java.util.List-java.util.List-), as described [here in the documentation](../../library/requirements/README.md).
   - If needed for the requirement, depending on the [requirement description method](../../library/requirements/README.md#common-features), an additional `EoC` (end of cycle) variable can be generated too using the [`PlcCycleRepresentation.createEocField()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/requirement/PlcCycleRepresentation.html#createEocField-cern.plcverif.verif.interfaces.data.VerificationProblem-cern.plcverif.verif.interfaces.data.VerificationResult-) method, which can be used in the requirement description. The same is true for the beginning of cycle variable.
   - Using the temporal logic (TL) representation of the selected pattern and the given placeholders, it will generate the appropriate temporal logic expression describing the requirement. This step heavily relies on the [`ExpressionParser`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/exprparser/ExpressionParser.html) that can be used to parse PLCverif expressions, as described [here in the documentation](../metamodels/ExpressionMetamodel.md#utilities). 
   - The verification problem will be updated with the following information:
     - The automaton model that is to be represented by the verification backend ([`model`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html#setModel-cern.plcverif.base.models.cfa.cfabase.CfaNetworkBase-)),
     - The formal representation of the requirement ([`requirement`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html#setRequirement-cern.plcverif.base.models.expr.Expression-cern.plcverif.verif.interfaces.data.RequirementRepresentationStrategy-)), together with the type of the requirement ([`RequirementRepresentationStrategy`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/RequirementRepresentationStrategy.html)),
     - The human-readable, textual representation of the requirement ([`requirementDescription`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html#setRequirementDescription-cern.plcverif.base.common.formattedstring.BasicFormattedString-)),
     - The location representing the end of the PLC cycle ([`eocLocation`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html#setEocLocation-cern.plcverif.base.models.cfa.cfabase.Location-)),
     - The context of the entry block ([`entryBlockContext`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html#setEntryBlockContext-cern.plcverif.base.models.cfa.cfadeclaration.DataRef-)).

   {% hint style='example' %}
From the log (if the reporter includes messages with _debug_ severity) it can be seen that the only field treated as input is the one representing the `in` PLC variable.

![Log showing details about the PLC cycle representation](img/example-abs/field-categories-log.png)

If the generation of diagnostic outputs is enabled, the `<id>.verif.cfd-original.dot` file contains a graphical representation of the CFA declaration network that is the result of the requirement representation. The diagnostic outputs can be enabled in the _Preferences_ menu, under the _PLCverif / Verification job_ page, by ticking the _Diagnostic output generation enabled_ option.

![CFA network declaration after the requirement representation](img/example-abs/cfd-original.png)

It can be observed that a new main automaton declaration was created (named `VerificationLoop`). In an infinite loop, it sets the input fields to nondeterministic values and calls the entry point of the CFA declaration that was created by the PLC code parser. In this example, the `EoC` field and its assignments are included too.

If the generation of diagnostic outputs is enabled, the `<id>.req-cfd.txt` file contains the textual representation of the generated TL expression tree:

```
{UnaryCtlExpression operator=AG}
├──operand: 
│  {BinaryLogicExpression operator=IMPLIES}
│   ├──leftOperand: 
│   │  {FieldRef frozen=false field:EoC}
│   │   └──prefix: <null>
│   └──rightOperand: 
│      {ComparisonExpression operator=GREATER_EQ}
│       ├──leftOperand: 
│       │  {FieldRef frozen=false field:RET_VAL}
│       │   └──prefix: 
│       │      {FieldRef frozen=false field:FuncAbs1}
│       │       └──prefix: <null>
│       ├──rightOperand: 
│       │  {IntLiteral value=0}
│       │   └──type: 
│       │      {IntType signed=true bits=16}
│       └──type: 
│          {BoolType}
└──type: 
    {TemporalBoolType}
```

This corresponds to the _AG(`EoC` --> `FuncAbs1.RET_VAL` >= 0)_ expression, where EoC means _end of PLC cycle_. The `EoC` field was generated automatically as the requirement representation strategy requested by the NuSMV verification backend is [`EXPLICIT_WITH_VARIABLE`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/RequirementRepresentationStrategy.html#EXPLICIT_WITH_VARIABLE).
   {% endhint %}

1. **Determine the transformation goal.** The CFA transformation goal is determined then by the verification backend. PLCverif will try to provide a CFA representation convenient for the verification backend.
     - The CFA can be a [CFA declaration](../metamodels/CfaDeclaration.md) or a [CFA instance](../metamodels/CfaInstance.md),
     - The CFA can be [inlined](../metamodels/CfaInstance.md#cfi_inlining) (i.e., call transitions are substituted by the called automata) or not,
     - The CFA can be enumerated (i.e., the arrays are unfolded) or not.
  
    The required format is determined by the values returned by the verification backend in [`IBackend.getPreferredModelFormat()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IBackend.html#getPreferredModelFormat--) ([`CfaTransformationGoal`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/transformation/CfaTransformationGoal.html): either `DECLARATION`, `INSTANCE_ARRAYS`, or `INSTANCE_ENUMERATED`) and by [`IBackend.isInliningRequired()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IBackend.html#isInliningRequired--). Note that inlining is not compatible with the `DECLARATION` transformation goal.

    In addition, if the backed does not require an _annotated_ CFA, all non-local annotations (i.e., annotations which refer to model elements) will be removed in this phase.

2. **CFA declaration reductions.** Independently from the determined transformation goal, the CFA declaration will be reduced. Each set reduction plug-in will be called sequentially via the [`IReduction.reduce()`]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/IReduction.html) method. This is repeated until any of the called reduction plug-ins managed to perform some reduction on the model. Note that PLCverif is shipped with a single reduction plug-in ([`basic_reductions`](../../library/reductions/BasicReductions.md)).
    
  {% hint style='example' %}
If the generation of diagnostic outputs is enabled, the `<id>.verif.cfd-reduced.dot` file contains a graphical representation of the CFA declaration network that is the result of these reductions.

![CFA network declaration after reductions](img/example-abs/cfd-reduced.png)

You can observe that the only change performed by the reductions is in the `FuncAbs` automaton: the `init` .. `l5` part of the CFA was reduced by removing the empty transitions and merging some sequential transitions.
  {% endhint %}

10. **CFA transformation.** Depending on the determined transformation goal, the CFA is transformed.
    - If the transformation goal is `DECLARATION`, no operation is done. Inlining is not compatible with this goal.
    - Otherwise, the CFA declaration _and the requirement as well_ will be instantiated using the[`CfaInstantiator.transformCfaNetwork()`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/transformation/CfaInstantiator.html#transformCfaNetwork-cern.plcverif.base.models.cfa.cfadeclaration.CfaNetworkDeclaration-boolean-) method. The trace of this generation will be stored the verification result (using [`VerificationResult.setVariableTrace()`]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/data/JobResult.html#setVariableTrace-cern.plcverif.base.models.cfa.trace.IVariableTraceModel-)). If requested, after the instantiation, the inlining will be performed as well using [`CallInliner.transform()`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/transformation/CallInliner.html#transform-cern.plcverif.base.models.cfa.cfainstance.CfaNetworkInstance-).
        
  {% hint style='example' %}
If there was an instantiation step and If the generation of diagnostic outputs is enabled, the `<id>.verif.cfi-original.dot` file contains a graphical representation of the CFA instance network that is the result of the transformation.

In case of NuSMV backend, the preferred model format is an instantiated, enumerated (no arrays), inlined (no call transitions) CFA. Annotations are not required by this backend. Therefore the result of the CFA transformation step is the following automaton:

![CFA network instance after instantiation](img/example-abs/cfi-original.png)

Note that this is a CFA network _instance_. In addition, there is only a single automaton instance, there are no call transitions and all variables are global.
  {% endhint %}

11. **CFA instance reductions.** If the CFA transformation led to a CFA instance, it is required to execute the reductions again, by calling the same [`IReduction.reduce()`]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/IReduction.html) method. It will be done in the same fashion as before.
12. After this step, the final verification CFA is created. Before passing it to the verification backend, it will be validated by calling [`CfaValidation.validate()`]({{ book.api_docs_core_url }}/cern/plcverif/base/models/cfa/validation/CfaValidation.html). This validation will be done even if the _Strict internal checks enabled_ is NOT selected in the preferences of the verification job.

  {% hint style='example' %}
If the generation of diagnostic outputs is enabled, the `<id>.verif.cfa-final.dot` file contains a graphical representation of the CFA instance network that is the result of these reductions.

![Final CFA network instance for verification](img/example-abs/cfa-final.png)

You can see that in this simple case only minor modifications were done on the CFA instance. However, typically the CFA instance reductions do much more modifications than the CFA declaration reductions.
  {% endhint %}

13. **Verification backend execution.** The verification backend is called using the [`IBackend.execute()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IBackend.html#execute-cern.plcverif.verif.interfaces.data.VerificationProblem-cern.plcverif.verif.interfaces.data.VerificationResult-cern.plcverif.base.common.progress.ICanceling-) method. In this method the verification backend is typically expected to:
   1. Generate the equivalent of the given CFA and TL requirement in the format required by the represented verification engine,
   2. Execute the verification engine,
   3. Parse the output of the verification engine execution and set the verification result.

  {% hint style='example' %}
In this example the NuSMV verification backend is selected.

1. First, it will create the NuSMV model corresponding to the given CFA and TL requirement using the `NusmvModelBuilder.buildModel()` method. This will result in the following NuSMV model (named `<id>.smv`):
    ```smv
    -- Model
    MODULE main
        VAR
            loc : {init_pv, end, loop_start, callEnd, prepare_EoC, verificationLoop_VerificationLoop_init,
                verificationLoop_VerificationLoop_l5, verificationLoop_VerificationLoop_end};
            FuncAbs_in : signed word[16];
            FuncAbs_RET_VAL : signed word[16]; -- frozen
            EoC : boolean; -- frozen
            -- Random vars for nondeterministic INTs
            __random_FuncAbs_in : signed word[16];
        
        ASSIGN
            -- CFA structure (loc)
            init(loc) := init_pv;
            next(loc) := case
                loc = init_pv & (TRUE) : loop_start;
                loc = loop_start & (TRUE) : verificationLoop_VerificationLoop_init;
                loc = callEnd & (TRUE) : prepare_EoC;
                loc = prepare_EoC & (TRUE) : loop_start;
                loc = verificationLoop_VerificationLoop_init & ((FuncAbs_in) >= (0sd16_0)) : 
                    verificationLoop_VerificationLoop_l5;
                loc = verificationLoop_VerificationLoop_init & (!((FuncAbs_in) >= (0sd16_0))) : 
                    verificationLoop_VerificationLoop_l5;
                loc = verificationLoop_VerificationLoop_l5 & (TRUE) : 
                    verificationLoop_VerificationLoop_end;
                loc = verificationLoop_VerificationLoop_end & (TRUE) : callEnd;
                TRUE: loc;
            esac;
            
            init(FuncAbs_in) := 0sd16_0;
            next(FuncAbs_in) := case
                loc = loop_start & (TRUE) : __random_FuncAbs_in; -- Nondeterministic
                TRUE  : FuncAbs_in;
            esac;
            init(FuncAbs_RET_VAL) := 0sd16_0;
            next(FuncAbs_RET_VAL) := case
                loc = verificationLoop_VerificationLoop_init & 
                    ((FuncAbs_in) >= (0sd16_0)) : FuncAbs_in;
                loc = verificationLoop_VerificationLoop_init & 
                    (!((FuncAbs_in) >= (0sd16_0))) : -(FuncAbs_in);
                TRUE  : FuncAbs_RET_VAL;
            esac;
            init(EoC) := FALSE;
            next(EoC) := case
                loc = callEnd & (TRUE) : TRUE;
                loc = prepare_EoC & (TRUE) : FALSE;
                TRUE  : EoC;
            esac;

    -- Requirement
    CTLSPEC AG((EoC) -> ((FuncAbs_RET_VAL) >= (0sd16_0)));
    ```
    Note that it already contains the requirement to be checked in the format desired by NuSMV.
2. Next, NuSMV will be called. To simplify calling NuSMV, a script file is generated that contains most of the expected settings (named `<id>.smv.script`). The following script file was generated for this example, using the _Classic_ algorithm:
    ```
    set on_failure_script_quits
    set traces_hiding_prefix ___
    set default_trace_plugin 1
    go
    check_ctlspec -o "C:\temp\PLCverif-output1234\case1.smv.cex"
    quit
    ```
    Then, NuSMV will be called with a parameters similar to this: `nuxmv.exe -dynamic -source C:\temp\PLCverif-output1234\<id>.smv.script   C:\temp\PLCverif-output1234\<id>.smv`. The exact parameter list is logged with _Info_ severity.
3. According to the script above, the output of NuSMV will be written to the `<id>.smv.cex` file. In this specific case, this file contains a counterexample:
    ```
    -- specification AG (EoC -> FuncAbs_RET_VAL >= 0sd16_0)  is false
    -- as demonstrated by the following execution sequence
    Trace Description: CTL Counterexample 
    Trace Type: Counterexample 
    -> State: 1.1 <-
        loc = init_pv
        FuncAbs_in = 0sd16_0
        FuncAbs_RET_VAL = 0sd16_0
        EoC = FALSE
        __random_FuncAbs_in = 0sd16_0
    -> State: 1.2 <-
        loc = loop_start
        FuncAbs_in = 0sd16_0
        FuncAbs_RET_VAL = 0sd16_0
        EoC = FALSE
        __random_FuncAbs_in = -0sd16_32768
    -> State: 1.3 <-
        loc = verificationLoop_VerificationLoop_init
        FuncAbs_in = -0sd16_32768
        FuncAbs_RET_VAL = 0sd16_0
        EoC = FALSE
        __random_FuncAbs_in = 0sd16_0
    -> State: 1.4 <-
        loc = verificationLoop_VerificationLoop_l5
        FuncAbs_in = -0sd16_32768
        FuncAbs_RET_VAL = -0sd16_32768
        EoC = FALSE
        __random_FuncAbs_in = 0sd16_0
    -> State: 1.5 <-
        loc = verificationLoop_VerificationLoop_end
        FuncAbs_in = -0sd16_32768
        FuncAbs_RET_VAL = -0sd16_32768
        EoC = FALSE
        __random_FuncAbs_in = 0sd16_0
    -> State: 1.6 <-
        loc = callEnd
        FuncAbs_in = -0sd16_32768
        FuncAbs_RET_VAL = -0sd16_32768
        EoC = FALSE
        __random_FuncAbs_in = 0sd16_0
    -> State: 1.7 <-
        loc = prepare_EoC
        FuncAbs_in = -0sd16_32768
        FuncAbs_RET_VAL = -0sd16_32768
        EoC = TRUE
        __random_FuncAbs_in = 0sd16_0
    ```

    You can observe that this counterexample is lengthy compared to the one in the verification report. This is because NuSMV will report the values of the variables after each transition firing. Therefore this counterexample is parsed first, then filtered (using `NusmvOutputParser.parseCounterexample`). The result of this will be an [instance counterexample](VerificationJob.md#counterexample) ([`InstanceCounterexample`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/cex/InstanceCounterexample.html)), where each step corresponds to a PLC cycle. When it is stored in the verification result ([`VerificationResult.setInstanceCounterexample()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationResult.html#setInstanceCounterexample-cern.plcverif.verif.interfaces.data.cex.InstanceCounterexample-)), a declaration counterexample will be automatically created based on the variable trace.
  {% endhint %}

14. **Result diagnosis.** The requirement representation plug-in may have additional insights based on the result of the verification backend execution. For example, the assertion requirement plug-in may determine which assertion has been violated and in which cycle. Currently, the requirement pattern plug-in cannot provide additional information. The diagnosis is expected to be set in the [`IRequirement.diagnoseResult()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IRequirement.html#diagnoseResult-cern.plcverif.verif.interfaces.data.VerificationProblem-cern.plcverif.verif.interfaces.data.VerificationResult-) method, using the [`VerificationResult.setResultDiagnosis()`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationResult.html#setResultDiagnosis-cern.plcverif.base.common.formattedstring.BasicFormattedString-) method.

15. **Check unused settings.** As the settings file is very generic, a typo may not cause any issue in it when parsed. However, to help the diagnostics, at this stage a warning log message is generated for every settings node that has not been read so far, except for the reporter subtrees.
    
  {% hint style='example' %}
In this example the given settings is correct. However, if we add the `-dummy = dummy` settings to the verification case file (via the _Advanced settings_ section in the GUI for instance), the report will correctly show the following warning: _The settings element 'dummy' (value: dummy) has not been used. Maybe its name has a typo?_.
  {% endhint %}

16. **Reporting.** The last step of the verification job is to generate the necessary reports. Each set reporter plug-in will be called via the `IVerificationReporter.generateReport()` method. They will represent the verification results in different ways, in different formats. The results will be registered in the verification result, using the [`VerificationResult.addFileToSave()`]({{ book.api_docs_core_url }}/cern/plcverif/base/interfaces/data/BasicJobResult.html#addFileToSave-java.lang.String-java.lang.String-java.lang.String-) method. Note: in the GUI version only the files registered via this method will show up in the workspace's output folder.
   - In the GUI, the HTML and the summary reports will be generated even if they are not selected by the user.

  {% hint style='example' %}
In the example of the `FuncAbs` verification, several verification reports can be generated.
- The HTML report looks like this:
    
    ![HTML report example](img/example-abs/report-html.png)

- The plain text report looks like this:
      
      Return value is always non-negative
      ===================================
      
      Requirement: *FuncAbs.RET_VAL >= 0* is always true at the end of the PLC cycle.
                  (AG (EoC --> (FuncAbs/RET_VAL >= 0)))
      
      **Result: the requirement is VIOLATED**
      
      Backend: NusmvBackend
      Parameters: 
      Input bindings: 
      
      Counterexample/witness:
      Step Cycle 1
      - FuncAbs/RET_VAL = -32768 (FuncAbs.RET_VAL = -32768)
      - FuncAbs/in = -32768 (FuncAbs.in = -32768)
      
      Stages
      ------
      * Parsing source code (0 ms): Successful
      * Control-flow declaration generation (60 ms): Successful
      * Requirement representation (21 ms): Successful
      * Reductions (CFD) (15 ms): Successful
      * Model preparation (11 ms): Successful
      * Reductions (CFI) (19 ms): Successful
      * NuSMV model building (1 ms): Successful
      * NuSMV execution (66 ms): Successful
      * NuSMV output parsing (0 ms): Successful
      * Result diagnosis (0 ms): Successful
      * Reporting (0 ms): Unknown
      
      Console output
      --------------
      STDOUT
          
      [...]

- As the plain text report is in Markdown format, it can be presented more nicely too using a Markdown renderer. This is not done automatically.
  
  ![Plain text report rendered as Markdown text](img/example-abs/report-plaintext-md.png) 

- The summary report (which can be used by the [summary job](SummaryJob.md) to generate summary reports for several verification jobs) looks like this:
  ```
  -job = "verif"
  -id = "case1"
  -name = "Return value is always non-negative"
  -description = "The return value of the absolute function shall always be non-negative (zero or positive)."
  -sourcefiles.0 = "C:\workspaces\runtime-plcverif.gui.app.product\.builtin_Siemens S7-300\builtin.scl"
  -sourcefiles.1 = "C:\workspaces\runtime-plcverif.gui.app.product\demoabs2\source.scl"
  -lf = "step7"
  -inline = "false"
  -reductions.0 = "basic_reductions"
  -info.backend_name = "NusmvBackend"
  -info.backend_algorithm = "nuxmv-Classic-dynamic"
  -info.requirement_text = "FuncAbs.RET_VAL >= 0 is always true at the end of the PLC cycle."
  -info.plugin_store_keys.0 = "nusmv$$expression_type"
  -info.plugin_store_values.0 = "Ctl"
  -result = "Violated"
  -result.runtime_ms = "193"
  -result.exec_date = "2019-02-04 13:58:49"
  -result.backend_exectime_ms = "66"
  -result.cex_exists = "true"
  -job.req = "pattern"
  -job.req.inputs = {}
  -job.req.params = {}
  ```

The details of the verification reports can be fine-tuned in the preferences.    
  {% endhint %}