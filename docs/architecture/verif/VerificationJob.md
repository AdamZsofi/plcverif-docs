## Verification job

 As it was [previously discussed](../api/PlatformExtensions.md#job), PLCverif provides a generic *job interface* that can be used for implementing any workflow that is based on parsed CFAs of PLC programs. However, the main goal of PLCverif is to provide formal verification (model checking) for PLC programs. Thus the **verification job** that is shipped with PLCverif has utmost importance.

 A verification job takes a PLC code and a requirement. It represents the PLC code as CFA, the CFA will be reduced, the represented formal requirement will be checked on the CFA by a verification backend. The results will be made available by reporters.
 This section overviews the workflow implemented by the verification job and the key data structures used in it. 


### Verification workflow

The workflow implemented by the verification job (in `VerificationJob.execute()`) is summarized on the following figure.

```uml
@startuml

(*) --> "1. Create verification result object" as s1
s1 --> "2. Parse code" as s2
s2 --> "3. Generate CFA" as s3
s3 --> "4. Create verification problem" as s4
s4 --> "5. Embed requirement" as s5
s5 --> "6. Decide transformation goal" as s6
s6 --> "7. Apply reductions" as s7
s7 --> "8. Transform and prepare model" as s8
s8 --> "9. Apply reductions" as s9
s9 --> "10. Execute verification backend" as s10
s10 --> "11. Try to diagnose the result using the requirement" as s11
s11 --> "12. Report" as s12
s12 --> (*)

@enduml
```

The following _sequence diagram_ gives a more detailed overview of the steps.

```uml
@startuml

title Execution of a verification job

actor Client
activate Client

' ===========================================
' EXECUTION
' ===========================================

Client -> VerificationJob: execute()
	activate VerificationJob
	
	' 1. Create verification result
	note over VerificationJob
		Create VerificationResult
	end note

	' 2. Parse code
	VerificationJob -> IParser: parseCode(VerificationResult)
		activate IParser
		create IParserLazyResult
		IParser --> IParserLazyResult: new ()
		note over IParser
			Parse AST and saving results
			in IParserLazyResult
		end note
		VerificationJob <-- IParser: IParserLazyResult
		deactivate IParser
	
	' 3. Generate CFA
	VerificationJob -> IParserLazyResult: generateCfa(VerificationResult)
		activate IParserLazyResult
		VerificationJob <-- IParserLazyResult: CFA declaration
		deactivate IParserLazyResult
	
	' 4. Create VerificationProblem
	create VerificationProblem
	VerificationJob --> VerificationProblem: new ()
	VerificationJob -> VerificationProblem: setModel(CFA declaration)
	
	' 5. Embed requirement
	VerificationJob -> IRequirement: fillVerificationProblem(VerificationProblem, IParserLazyResult, VerificationResult)
		activate IRequirement
		loop for every atom to parse
		IRequirement -> IParserLazyResult: parseAtom(atom)
		note over IRequirement
			Create combined CFA
		end note
		note over IRequirement
			Build requirement
		end note
		IRequirement -> VerificationProblem: setModel(combined CFA)
		IRequirement -> VerificationProblem: setRequirement(requirement)
		VerificationJob <-- IRequirement
		deactivate IRequirement
		
	' 6. Decide transformation goal
	VerificationJob -> IBackend: getPreferredModelFormat()
	VerificationJob -> IBackend: isInliningRequired()
	
	' 7. Apply reductions
	loop for every reduction, while there are modifications
		VerificationJob -> IReduction: reduce(combined CFA, requirement, VerificationResult)
			activate IReduction
			note over IReduction
				Reduce CFA based on the requirement.
			end note
			VerificationJob <-- IReduction
			deactivate IReduction
	end
	
	' 8. Transform and prepare model
	opt instantiation is on
		VerificationJob -> VerificationJob: instantiate(CFA declaration, VerificationResult)
			activate VerificationJob
			VerificationJob -> VerificationProblem: setModel(CFA instance)
			deactivate VerificationJob
	end

	opt inlining is on
		VerificationJob -> VerificationJob: inline(CFA declaration, VerificationResult)
	end

	opt inlining or instantiation is on
		' 9. Apply reductions
		loop for every reduction, while there are modifications
			VerificationJob -> IReduction: reduce(combined CFA, requirement, VerificationResult)
				activate IReduction
				note over IReduction
					Reduce CFA based on the requirement.
				end note
				VerificationJob <-- IReduction
				deactivate IReduction
		end
	end
	
	' 10. Verify
	VerificationJob -> IBackend: execute(VerificationProblem, VerificationResult)
		activate IBackend
		note over IBackend
			Check Expression on CFA.
		end note
		VerificationJob <-- IBackend
		deactivate IBackend
	
	' 11. Diagnose result
	VerificationJob -> IRequirement: diagnoseResult(VerificationProblem, VerificationResult)
		activate IRequirement
		note over IRequirement
			Try to provide insights for the verification result
		end note
		VerificationJob <-- IRequirement
		deactivate IRequirement

	' 12. Report
	loop for every specified reporter
		VerificationJob -> IVerificationReporter: generateReport(VerificationResult)
			activate IVerificationReporter
			note over IVerificationReporter
				Generate report from VerificationResult
				and save content into VerificationResult
			end note
			VerificationJob <-- IVerificationReporter
			deactivate IVerificationReporter
	end
	Client <-- VerificationJob: VerificationResult
	deactivate VerificationJob
deactivate Client

@enduml
```


### Verification metamodel

The following figure overviews the data structures used by the verification job.
![Data structures used by the verification job](img/VerifApi.png)

- The `VerificationJob` class implements the verification workflow and orchestrates each of its steps
  - It extends the `AbstractBasicJob` abstract class, implements the `ICfaJob` interface
  - It relies on three types of extensions: requirement represented (exactly one), verification backend (exactly one), and reporters (zero or more). In addition, it relies on the more generic extensions defined by the platform and provided in the `ModelTasks` instance: the parser or language frontend (exactly one) and CFA reductions (zero or more). [Details can be found below.](#verif_components)
  - It is instantiated by the `VerificationJobExtension` factory class
- The `VerificationProblem` represents the verification (model checking) problem (model and requirement) for the verification backend.
- The `VerificationResult` aggregates the results of the steps of the verification job.
  - It inherits from `JobResult` and transitively from the `BasicJobResult` abstract classes. See more details about them in [their documentation](../api/PlatformExtensions.md#jobresult)
  - [Read more details below](#verificationresult)
- The `VerificationSettings` stores the settings relevant to the verification job in a parsed form.
  - [Read more details below](#settings)



### Verification problem (`VerificationProblem`) {#verificationproblem}

The _verification problem_ defines the inputs of the verification backend. It contains the model (CFA declaration or instance network) that needs to be represented by the verification backend, the temporal logic requirement (and its representation strategy), and certain additional useful information about the model (the _end of cycle_ location, the entry context, etc.).

See more details in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html).


### Verification result (`VerificationResult`) {#verificationresult}
The _verification result_ aggregates the results of the operations related to the verification job. It extends the [`JobResult` class](../api/PlatformExtensions.md#jobresult).

Further included data: 
 - The original main automaton of the parsed `CfaNetworkDeclaration` model (see `getOriginalMainAutomaton()`)
 - The original main context of the parsed `CfaNetworkDeclaration` model (see `getOriginalMainContext()`)
 - An `Expression` describing the requirement using `DataRef` references (see `getDeclarationRequirement()`)
 - An `Expression` describing the requirement using `AbstractVariableRef` references (see `getInstanceRequirement()`)
 - A set of additional variables introduced by the IRequirement or other plugins (see `getAdditionalVariables()`)
 - The list of fields treated as inputs (see `getInputFields()`)
 - The list of fields treated as parameters (see `getParamFields()`)
 - Map representing the field-value binding (see `getBindings()`)
 - The name of the `IBackend` (see `getBackendName()`)
 - The potential size of the state space (see `getPotentialStateSpaceSize()`)
 - The potential counterexample using DataRef instances (see `getDeclarationCounterexample()`)
 - The potential counterexample using AbstractVariableRef instances (see `getInstanceCounterexample()`)
 - The result of the verification (see `getResult()`)


| Property (Type) | Description | Example value | To be filled by | Can be null |
| -------- | ---- | ----------- | ------------- | --------------- | ----------- |
| `backendName` (`String`) | Human-readable name of the verification backend | `NuSMV` | `IBackend.execute` | no | 
| `backendAlgorithmId` (`String`) | Identifier of the exact algorithm/configuration used by the verification backend | _NuSMV-df-dyn-dcx_ | `IBackend.execute` | no |
| `backendStdout` (`CharSequence`) | The backend's output written to the standard output. | `IBackend.execute` (optional) | | no (can be empty) |
| `backendStderr` (`CharSequence`) | The backend's output written to the standard error output. | `IBackend.execute` (optional) | | no (can be empty) |
| `potentialStateSpaceSize` (`double`) | Size of the verification model's PSS. | _1.2E34_ | ? | no (<0 iff not set) |
| `result` (`ResultEnum`) | Verification result (Satisfied, Violated, Unknown, Error) | _Satisfied_ |  `IBackend.execute` (if known) | no |
| `declarationCounterexample` (`DeclarationCounterexample`) | The counterexample on the declaration CFA. | | `IBackend.execute` (optional) | no | 
| `instanceCounterexample` (`InstanceCounterexample`) | The counterexample on the instance CFA. | | `IBackend.execute` (optional) | no |
| `verifProblem` (`VerificationProblem`) | The verification problem to be checked. | | `VerificationJob` (verification problem creation) | no |
| `inputFields` (`List<DataRef>`) | List of fields treated as inputs. | | `IRequirement.execute` | no |
| `paramFields` (`List<DataRef>`) | List of fields treated as parameters. | | `IRequirement.execute` | no |
| `bindings` (`Map<DataRef, InitialValue>`) | Bindings used in the requirement representation. | | `IRequirement.execute` | no |

`VerificationResult` extends `JobResult`, thus it also stores the properties defined for the [job result](../api/PlatformExtensions.md#jobresult).

Notes:
- If the `instanceCounterexample` is set and the `variableTrace` is available, the `declarationCounterexample` will be created automatically.

See more details in the [API documentation]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationResult.html).

### Components of verification {#verif_components}

The verification-specific extensions are defined in `cern.plcverif.verif.interfaces`.

The connection between the verification job and the different extensions is shown on the following figure.
![Verification job and the extensions it uses](img/API_full.png)

#### Language frontend
The language frontend is responsible for parsing the input PLC program. This is a generic platform extension, thus more details can be found in the 
[description of the platform extensions](../api/PlatformExtensions.md#parser).

#### Reductions
The CFA reductions are responsible for simplify and reduce the CFA representations of the parsed programs. This is a generic platform extension, thus more details can be found in the  [description of the platform extensions](../api/PlatformExtensions.md#reduction).

#### Requirement {#requirement}
A requirement should provide a way to specify formal properties subject to
verification. It is also responsible for generating the full [`VerificationProblem`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html). Furthermore, the requirement can provide additional diagnosis once the verification engine has been executed and the verification results are available.

See more details in the [API documentation of `IRequirement`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IRequirement.html).

> **[info] More information**
>
> PLCverif is shipped with several requirement representation extensions.
> See the [library documentation](../../library/requirements/README.md) to find more information about the built-in requirements.

#### Verification backend {#backend}
A verification backend is a component that performs the formal verification 
of a requirement on a CFA model (possible with the help of an external tool), 
also parsing the results of the analysis. They are typically wrappers of external model checker tools, but it is also possible to implement a custom model checking algorithm as a verification backend extension.

This extension is responsible to solve the verification problem ([`VerificationProblem`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationProblem.html)) that represents a formal verification problem, specifying the model and the property to check.

See more details in the [API documentation of `IBackend`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IBackend.html).

> **[info] More information**
> 
> PLCverif is shipped with several verification backend extensions.
> See the [library documentation](../../library/backends/README.md) to find more information about the built-in verification backends.

#### Verification reporter {#reporter}

A verification reporter is a component that transforms a [`VerificationProblem`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/data/VerificationResult.html) into some other artifact that can serve as a
processable report.

A reporter may log to the console but *should not create files*, as it
is up to the client code invoking the job to decide what to do with the
generated artifacts (it is possible that the results are only displayed in a
window). Any artifact that is supposed to be persisted should be added to the
result itself in `JobResult.addFileToSave()` by specifying the desired
*relative* file path (relative to the specified output directory) and
the content of the file.
 
See more details in the [API documentation of `IVerificationReporter`]({{ book.api_docs_core_url }}/cern/plcverif/verif/interfaces/IVerificationReporter.html).


> **[info] More information**
>
> See the [library documentation](../../library/reporters/README.md) to find more information about the built-in verification reporters.


### Counterexample representation {#counterexample}

The verification result can store a counterexample (or witness). Two different class structure exist for representing counterexamples, depending on whether it is a counterexample on a CFA instance or on a CFA declaration. The two class structures are very similar.
- The root object of a counterexample is a `DeclarationCounterexample` or `InstanceCounterexample` that represents a full counterexample (or witness)
- The counterexample consists of steps (`DeclarationCexStep` or `InstanceCexStep`). These are consecutive sequential steps in the counterexample. A step is expected to correspond to a PLC cycle. Each step can be named.
- Each step contains a collection of variable-value pairs (`DeclarationCexStep.VariableValuePair`, `InstanceCounterexample.VariableValuePair`). The `variable` in those pairs need to be a reference to a variable in the CFA declaration (i.e., a `DataRef`) or in the CFA instance (i.e., a `AbstractVariableRef`). The `value` shall be a literal in both cases.

> [info] **Automated generation of declaration counterexample**
> 
> If the variable trace generated during the instantiation of the CFA declaration is available, the verification result object will take care of automatically generating the declaration counterexample based on the instance counterexample. Therefore it can be expected that the declaration counterexample is always available, no matter if the chosen backend uses CFA declaration or CFA instance models.



### Verification job settings {#settings}

The verification job plug-in can be configured using PLCverif settings (typically with the prefix `job.`) which will partially be represented by a `VerificationJobSettings` instance. The rest of the settings will be parsed by the `VerificationJobExtension` and directly handed over to the `VerificationJob`.

| Setting name (Type) | Description | Mandatory | Default value | Included in specific settings |
| ------------------- | ----------- | --------- | ------------- | ----------------------------- |
| `backend` (`String`) | Backend to be used. | yes | --- | no |
| `requirement` (`String`) | Requirement type to be used. | yes | --- | no |
| `reporters` (`List<String>`) | List of reporters to be used. | no | empty | no |
| `strict` (`boolean`) | Strictness of the various checks (assertions and validations). If true, more assertions are used for the intermediate states of the CFAs to help diagnosing problems, but it has a strong impact on the performance. | no | `false` | yes |
| `diagnostic_outputs` (`boolean`) | Enable diagnostic outputs (such as intermediary models, CFA visualization). | no | `false` | yes |

