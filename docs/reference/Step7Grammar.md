##  STEP 7 grammar documentation

(C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
Contributors: Daniel Darvas

Grammar for Siemens STEP 7 SCL and STL programming languages.
Includes additions to support the newer versions.

Based on the following document: [S7-SCL V5.3 for S7-300/400,
A5E00324650-01](https://support.industry.siemens.com/cs/attachments/5581793/SCL_e.pdf)
The indicated page numbers are references to this document unless otherwise indicated.
Additional related materials:
[S7v14] SIMATIC STEP 7 Professional V14 System Manual, 2016.
[STL] Statement List (STL) for S7-300 and S7-400 Programming, 2002.


Included metamodels:
- ecore (`http://www.eclipse.org/emf/2002/Ecore`)
- dt (`http://plcverif.cern/plc/step7/datatypes`)

###  Rules
####  ProgramFile  {#ProgramFile}
A **program file** represents a collection of _program units_, 
potentially extended with verification options and global variables.

A program file is not a standalone unit, there may be connections between
units in different program files.


**Refers to:**
- [GlobalVariableBlock](#GlobalVariableBlock)
- [ProgramUnit](#ProgramUnit)
- [VerificationOption](#VerificationOption)



```
ProgramFile:
	{ProgramFile}
	(verificationOptions+=VerificationOption)*
	(globalVariables+=GlobalVariableBlock)*
	(programUnits+=ProgramUnit)*;
```



####  ProgramUnit  {#ProgramUnit}
A **program unit** is (_block_ in STEP 7 terminology) is a container
for executable statements, data elements or type definitions.
			

- **Validation:**
   * [NON\_UNIQUE\_BLOCK\_NAME] Program unit names should be unique.

**Refers to:**
- [DataBlock](#DataBlock)
- [ExecutableProgramUnit](#ExecutableProgramUnit)
- [UserDefinedDataType](#UserDefinedDataType)

**Referred by:**
- [NamedElement](#NamedElement)
- [ProgramFile](#ProgramFile)


```
ProgramUnit:
	ExecutableProgramUnit | DataBlock | UserDefinedDataType;
```



####  ExecutableProgramUnit  {#ExecutableProgramUnit}
An **executable program unit** is an _organization block_,
_function_ or _function block_.


**Refers to:**
- [Function](#Function)
- [FunctionBlock](#FunctionBlock)
- [OrganizationBlock](#OrganizationBlock)

**Referred by:**
- [ProgramUnit](#ProgramUnit)


```
ExecutableProgramUnit:
	OrganizationBlock | Function | FunctionBlock;
```



####  OrganizationBlock  {#OrganizationBlock}
**Organization block** declaration.
			

- **Validation:**
   * [NON_UNIQUE_BLOCK_NAME] Should not have the same `name` as any Function, FunctionBlock, OrganizationBlock, DataBlock or UserDefinedDataType
   * [INVALID_BLOCK_KEYWORD] If the `name` is a block identifier (e.g. FB123), it should start with "OB"
   * [INVALID_DECLARATION_SECTIONS] The `declarationSection` may only contain CONST, LABEL, VAR_TEMP blocks (p. 15-37)
   * [INVALID_STATEMENTLIST_LANGUAGE] The `statements` must be an SclStatementList or StlStatementList (OB definition is not supported in SFC)

**Refers to:**
- [BlockAttributes](#BlockAttributes)
- [BuiltinKeyword](#BuiltinKeyword)
- [DeclarationSection](#DeclarationSection)
- [IdOrSymbol](#IdOrSymbol)
- [StatementList](#StatementList)

**Referred by:**
- [ExecutableProgramUnit](#ExecutableProgramUnit)


```
OrganizationBlock:
	'ORGANIZATION_BLOCK' name=IdOrSymbol
		(attributes=BlockAttributes)?
		declarationSection=DeclarationSection
	'BEGIN'?
		(builtIn?=BuiltinKeyword)?
		(statements=StatementList)?
	'END_ORGANIZATION_BLOCK';
```



####  Function  {#Function}
A **function** declaration.
			

Note: see special typing rule in case of `ANY` on p. 12-36.
			

- **Validation:**
   * [NON_UNIQUE_BLOCK_NAME] Should not have the same `name` as any Function, FunctionBlock, OrganizationBlock, DataBlock or UserDefinedDataType.
   * [INVALID_BLOCK_KEYWORD] If the `name` is a block identifier (e.g. FB123), it should start with "FC".
   * [INVALID_DECLARATION_SECTIONS] The `declarationSection` may only contain CONST, LABEL, VAR_TEMP, VAR_INPUT, VAR_IN_OUT, VAR_OUTPUT blocks (p. 15-37).
   * The `returnType` cannot be an ARRAY or (inline) STRUCT (however, UDT name is permitted).
   * [MISSING_RETURN_VALUE] If `returnType` is not VOID, then there should be a "return assignment" in the statement list.

**Refers to:**
- [BlockAttributes](#BlockAttributes)
- [BuiltinKeyword](#BuiltinKeyword)
- [DataType](#DataType)
- [DeclarationSection](#DeclarationSection)
- [IdOrSymbol](#IdOrSymbol)
- [StatementList](#StatementList)

**Referred by:**
- [ExecutableProgramUnit](#ExecutableProgramUnit)


```
Function:
	'FUNCTION' name=IdOrSymbol ':' returnType=DataType
		(attributes=BlockAttributes)?	
		declarationSection=DeclarationSection
	'BEGIN'?
		(builtIn?=BuiltinKeyword)?
		(statements=StatementList)?
	'END_FUNCTION';
```



####  FunctionBlock  {#FunctionBlock}
A **function block** declaration.
			

- **Validation:**
   * [NON_UNIQUE_BLOCK_NAME] Should not have the same `name` as any Function, FunctionBlock, OrganizationBlock, DataBlock or UserDefinedDataType
   * [INVALID_BLOCK_KEYWORD] If the `name` is a block identifier (e.g. FB123), it should start with "FB"
   * The start and end keyword should match.

**Refers to:**
- [BlockAttributes](#BlockAttributes)
- [BuiltinKeyword](#BuiltinKeyword)
- [DeclarationSection](#DeclarationSection)
- [IdOrSymbol](#IdOrSymbol)
- [StatementList](#StatementList)

**Referred by:**
- [ExecutableProgramUnit](#ExecutableProgramUnit)


```
FunctionBlock:
	('FUNCTION_BLOCK' | 'PROGRAM') name=IdOrSymbol
		(attributes=BlockAttributes)?
		declarationSection=DeclarationSection
	'BEGIN'?
		(builtIn?=BuiltinKeyword)?
		(statements=StatementList)?
	('END_FUNCTION_BLOCK' | 'END_PROGRAM');
```



####  BuiltinKeyword  {#BuiltinKeyword}


**Referred by:**
- [Function](#Function)
- [FunctionBlock](#FunctionBlock)
- [OrganizationBlock](#OrganizationBlock)

**Returns:** `ecore::EString`

```
BuiltinKeyword: '//#BUILTIN';
```



####  DataBlock  {#DataBlock}
A **data block** declaration.
It may represent an _instance data block_ or a _shared data block_.
			

If it represents an _instance data block_, `structure` is a
[FbOrUdtDT](#FbOrUdtDT) referring to a [FunctionBlock](#FunctionBlock).

If it represents a _shared data block_, `structure` is a 
[FbOrUdtDT](#FbOrUdtDT) referring to a [UserDefinedDataType](#UserDefinedDataType) (UDT),
or `structure` is a structure definition ([StructDT](#StructDT)).


**Refers to:**
- [BlockAttributes](#BlockAttributes)
- [DATABLOCK_FIELD_ADDRESS](#DATABLOCK_FIELD_ADDRESS)
- [DataBlockInitialAssignment](#DataBlockInitialAssignment)
- [DataBlockStructure](#DataBlockStructure)
- [IdOrSymbol](#IdOrSymbol)

**Referred by:**
- [ProgramUnit](#ProgramUnit)


```
DataBlock:
	'DATA_BLOCK' name=(IdOrSymbol|DATABLOCK_FIELD_ADDRESS)
		(attributes=BlockAttributes)?
		structure=DataBlockStructure ';'?
	'BEGIN'
		(initialAssignments+=DataBlockInitialAssignment ';')*
	'END_DATA_BLOCK';
```



####  DataBlockStructure  {#DataBlockStructure}
A **data block structure** (_DB declaration section_ in
STEP 7 terminology) defines the data variables stored in a data block.
			

It can be either a reference to a _function block_ (FB) or a <i>user-defined
data type</i> (UDT) or the _structure data type specification_.


**Refers to:**
- [FbOrUdtDT](#FbOrUdtDT)
- [StructDT](#StructDT)

**Referred by:**
- [DataBlock](#DataBlock)


```
DataBlockStructure:
	FbOrUdtDT | StructDT;
```



####  DataBlockInitialAssignment  {#DataBlockInitialAssignment}
...
Note: The `constant` will never be a named constant (as named constants cannot be locally defined in a DB).

- **Validation:**
   * The `constant` shall be a simple expression (thus can evaluated).
   * [CONST_AS_LEFTVALUE] The `leftValue` shall not be a [DirectNamedRef](#DirectNamedRef) pointing to a [NamedConstantDeclaration](#NamedConstantDeclaration). (Scoping will also prevent this.) See [LeftValue](#LeftValue) for more details.

**Refers to:**
- [LeftValue](#LeftValue)
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)

**Referred by:**
- [DataBlock](#DataBlock)


```
DataBlockInitialAssignment:
	leftValue=LeftValue ':=' constant=NamedOrUnnamedConstantRef;
```



####  UserDefinedDataType  {#UserDefinedDataType}
A **user-defined data type** defines a structured data type that can be used
for variable definition, data block definition, or as function return type.


**Refers to:**
- [BlockAttributes](#BlockAttributes)
- [IdOrSymbol](#IdOrSymbol)
- [StructDT](#StructDT)

**Referred by:**
- [ProgramUnit](#ProgramUnit)


```
UserDefinedDataType:
	'TYPE' name=IdOrSymbol
		(attributes=BlockAttributes)?
		':'? declaration=StructDT ';'?
	'END_TYPE';
```



####  DeclarationSection  {#DeclarationSection}
A **declaration section** contains variable, constant and label declarations
within _executable program units_.
			

Note: it is possible to have several label declaration blocks in STEP 7 SCL blocks. (Checked.)
			

- **Validation:**
   * [NON\_UNIQUE\_FIELD\_NAME] Variables and constants defined within the same block should have unique names.
   * [ILLEGAL_STL_ELEMENT] Declaration sections of program units with STL implementation should not contain any label declaration block (`labelDeclarations` should be empty/null).
   * [ILLEGAL_STL_ELEMENT] Declaration sections of program units with STL implementation should not contain any constant declaration block (`constantDeclarations` should be empty/null).

**Refers to:**
- [ConstDeclarationBlock](#ConstDeclarationBlock)
- [LabelDeclarationBlock](#LabelDeclarationBlock)
- [VariableDeclarationBlock](#VariableDeclarationBlock)

**Referred by:**
- [Function](#Function)
- [FunctionBlock](#FunctionBlock)
- [OrganizationBlock](#OrganizationBlock)


```
DeclarationSection:
	{DeclarationSection}
	(
		variableDeclarations+=VariableDeclarationBlock | 
		constantDeclarations+=ConstDeclarationBlock |
		labelDeclarations+=LabelDeclarationBlock
	)*;
```



####  VariableDeclarationBlock  {#VariableDeclarationBlock}
A **variable declaration block** declares variables and/or block parameters belonging to the same category.
			

The `direction` defines the category of the variables, see [VariableDeclarationDirection](#VariableDeclarationDirection).
			

Note: The STEP 7 terminology distinguishes between variables and block parameters, however, PLCverif does not.


**Refers to:**
- [VariableDeclarationDirection](#VariableDeclarationDirection)
- [VariableDeclarationLine](#VariableDeclarationLine)

**Referred by:**
- [DeclarationSection](#DeclarationSection)


```
VariableDeclarationBlock:
	{VariableDeclarationBlock}
	direction=VariableDeclarationDirection
		(variables+=VariableDeclarationLine)*
	'END_VAR';
```



####  VariableDeclarationDirection (enum) {#VariableDeclarationDirection}
A **variable declaration direction** defines a category of a group of variables.
It can be:
_static variable_ (`VAR`),
_temporary variable_ (`VAR_TEMP`),
_input block parameter_ (`VAR_INPUT`),
_output block parameter_ (`VAR_OUTPUT`),
_in/out block parameter_ (`VAR_IN_OUT`).


Literals:
- INOUT (`VAR_IN_OUT`)
- INPUT (`VAR_INPUT`)
- OUTPUT (`VAR_OUTPUT`)
- STATIC (`VAR`)
- TEMP (`VAR_TEMP`)

```
enum VariableDeclarationDirection:
	INPUT = 'VAR_INPUT' | 
	OUTPUT = 'VAR_OUTPUT' | 
	STATIC = 'VAR' | 
	TEMP = 'VAR_TEMP' | 
	INOUT = 'VAR_IN_OUT';
```



####  GlobalVariableBlock  {#GlobalVariableBlock}
A **global variable declaration block** declares variables belonging to the same category
in the global scope. **PLCverif extension, this is only for verification, not part of STEP 7.**
			

The `direction` defines the category of the variables, see [GlobalVariableDeclarationDirection](#GlobalVariableDeclarationDirection).


**Refers to:**
- [GlobalVariableDeclarationDirection](#GlobalVariableDeclarationDirection)
- [VariableDeclarationLine](#VariableDeclarationLine)

**Referred by:**
- [ProgramFile](#ProgramFile)


```
GlobalVariableBlock: 	direction=GlobalVariableDeclarationDirection
		(variables+=VariableDeclarationLine)+
	'END_VAR';
```



####  GlobalVariableDeclarationDirection (enum) {#GlobalVariableDeclarationDirection}
A **global variable declaration direction** defines a category of a group of global variables.
It can be:
_static variable_ (`VAR_GLOBAL`),
_input variable_ (`VAR_GLOBAL_INPUT`),
_output variable_ (`VAR_GLOBAL_OUTPUT`).
			

**PLCverif extension, this is only for verification, not part of STEP 7.**


Literals:
- INPUT (`VAR_GLOBAL_INPUT`)
- OUTPUT (`VAR_GLOBAL_OUTPUT`)
- STATIC (`VAR_GLOBAL`)

```
enum GlobalVariableDeclarationDirection:
	STATIC = 'VAR_GLOBAL' | 
	INPUT = 'VAR_GLOBAL_INPUT' | 
	OUTPUT = 'VAR_GLOBAL_OUTPUT';
```



####  ConstDeclarationBlock  {#ConstDeclarationBlock}
A **constant declaration block** declares named constants in an executable
program unit.


**Refers to:**
- [NamedConstantDeclaration](#NamedConstantDeclaration)

**Referred by:**
- [DeclarationSection](#DeclarationSection)


```
ConstDeclarationBlock:
	{ConstDeclarationBlock}
	'CONST'
		(constants+=NamedConstantDeclaration ';')*
	'END_CONST';
```



####  NamedConstantDeclaration  {#NamedConstantDeclaration}
A **named constant declaration** represents a named value.
			

- **Validation:**
   * [INVALID_SIMPLE_EXPRESSION] The `value` shall be a simple expression (p. 15-38).
   * Warning should be issued for unused constants.

**Refers to:**
- [Expression](#Expression)
- [IdLike](#IdLike)

**Referred by:**
- [ConstDeclarationBlock](#ConstDeclarationBlock)
- [VariableOrConstDeclaration](#VariableOrConstDeclaration)


```
NamedConstantDeclaration:
	name=IdLike ':=' value=Expression;
```



####  LabelDeclarationBlock  {#LabelDeclarationBlock}
A **label declaration block** declares labels that can be used as jump targets 
in its defining executable program unit.


**Refers to:**
- [Label](#Label)

**Referred by:**
- [DeclarationSection](#DeclarationSection)


```
LabelDeclarationBlock:
	{LabelDeclarationBlock}
	'LABEL'
		(labels+=Label (',' labels+=Label)* ';')*
	'END_LABEL';
```



####  Label  {#Label}
A **label** is a named potential target for a jump statement.
			

Labels of an SCL program unit are declared in a [LabelDeclarationBlock](#LabelDeclarationBlock).
Labels of an STL program unit do not have to be pre-declared, they are defined
right at the target statements, see [LabeledStlStatement](#LabeledStlStatement).
			

- **Validation:**
   * [DUPLICATE\_LABEL] Each label can be used at most once in a LabeledSclStatement (forbidden to use the same label multiple times).
   * [UNUSED\_LABEL] Warning should be issued for unused labels.
   * [ILLEGAL\_STL\_ELEMENT] The label should not be longer than 4 characters if used in STL.

**Refers to:**
- [IdLike](#IdLike)

**Referred by:**
- [LabelDeclarationBlock](#LabelDeclarationBlock)
- [LabeledStlStatement](#LabeledStlStatement)
- [NamedElement](#NamedElement)


```
Label:
	name=IdLike;
```



####  VariableDeclarationLine  {#VariableDeclarationLine}
A **variable declaration line** defines one or more _variable_ with the same _data type_.
The direction (category) of the variables is defined by the containing _variable declaration block_.
			

The variable, depending on its type can be: 

  - **Elementary variable**, if it has an elementary data type ([ElementaryDT](#ElementaryDT)), such as `INT` or `BOOL`; or a string data type ([StringDT](#StringDT));
  - **Array variable**, if it has an array data type ([ArrayDT](#ArrayDT));
  - **Structured variable**, if it has a data structure as data type, either unnamed ([StructDT](#StructDT)) or named ([FbOrUdtDT](#FbOrUdtDT) referring to an `UserDefinedDataType`);
  - **Instance variable**, if it represents an instance of a _function block_ ([FbOrUdtDT](#FbOrUdtDT) referring to an `FunctionBlock`);
  - **Parameter variable**, if it has a parameter data type ([ParameterDT](#ParameterDT)), such as `ANY` or `COUNTER`.

			

- **Validation:**
   * [INVALID_LOCAL_FBINSTANCE] The `type` can only be a FbOrUdtDT with a FunctionBlock `type` if the variable declaration is inside a FunctionBlock's VAR block (pp. 15-39--15-40)
   * [FORBIDDEN_VARIABLE_INITIALIZATION] Initialization is forbidden (`initialized` shall be false) if at least one of the variables is a reference variable (`reference` is true) (p. 8-2)
   * [FORBIDDEN_VARIABLE_INITIALIZATION] Initialization is forbidden (`initialized` shall be false) if `variables` contain multiple (>=2) Variables (p. 15-39)
   * [FORBIDDEN_VARIABLE_INITIALIZATION] Initialization is forbidden (`initialized` shall be false) in VAR_IN_OUT or VAR_TEMP blocks (pp. 15-40--15-41)
   * [INVALID_LOCAL_FBINSTANCE] Initialization is forbidden (`initialized` shall be false) if the `type` is a FunctionBlock
   * [INVALID_STRUCT_VARIABLE] The `variables` should not contain >=2 elements if VariableDeclarationLine is included inside a STRUCT definition which is in an executable program unit.
   * Warning should be issued for unused variables.
- **Examples:**
   * `var1, var2 : INT;`
   * `var3 : INT := 10;`

**Refers to:**
- [DataType](#DataType)
- [Variable](#Variable)
- [VariableInitialization](#VariableInitialization)

**Referred by:**
- [GlobalVariableBlock](#GlobalVariableBlock)
- [StructDT](#StructDT)
- [VariableDeclarationBlock](#VariableDeclarationBlock)


```
VariableDeclarationLine:
	variables+=Variable (',' variables+=Variable)*
		":" type=DataType (initialized?=':=' initialization=VariableInitialization)? ';';
```



####  Variable  {#Variable}
A **variable** is a named data storage element, with a given type.
			

The type of the variable is defined by the _variable declaration line_
([VariableDeclarationLine](#VariableDeclarationLine)) which defines it.
			

A **variable view** is also defined as a variable, however, for that 
the `reference` field will be set to `true`, and the referred
variable is provided in the field `ref`.
			

A variable name may start with '#' if otherwise it would collide with a built-in keyword (p. 8-2).

- **Validation:**
   * [FORBIDDEN\_TRANSITIVE\_VIEW] Reference to a variable that is a reference itself is not permitted.
   * [INCOMPATIBLE\_VAR\_VIEWS] The referred variable needs to be compatible with the viewer (p. 8-6)

**Refers to:**
- [IdLike](#IdLike)
- [IdOrSymbol](#IdOrSymbol)

**Referred by:**
- [VariableDeclarationLine](#VariableDeclarationLine)
- [VariableOrConstDeclaration](#VariableOrConstDeclaration)


```
Variable:
	name=IdOrSymbol (reference?='AT' ref=[Variable|IdLike])?;
```



####  VariableInitialization  {#VariableInitialization}
A **variable initialization** provides initial values for an _elementary_
or _array variable_. In the former case, it should describe a single value 
([ConstantInitializationElement](#ConstantInitializationElement)), in the latter case the initialization may
contain multiple elements.
			

- **Validation:**
   * [UNBALANCED\_VAR\_INIT\_BRACKETS] Brackets around the initialization are not mandatory, but they shall be balanced.
   * [INVALID\_VAR\_INIT\_TYPE] An elementary variable shall be initialized with a single [ConstantInitializationElement](#ConstantInitializationElement) element.

**Refers to:**
- [ArrayInitializationElement](#ArrayInitializationElement)

**Referred by:**
- [ArrayInitializationElement](#ArrayInitializationElement)
- [VariableDeclarationLine](#VariableDeclarationLine)


```
VariableInitialization returns VariableInitialization:
	'['? ArrayInitializationElement ( {ArrayInitialization.elements+=current} (',' elements+=ArrayInitializationElement)+ )? ']'?;
```



####  ArrayInitializationElement  {#ArrayInitializationElement}
An **array initialization element** describes the whole or a part of the 
variable initialization.
A **constant initialization element** (`ConstantInitializationElement`)
defines a single value as initialization element.
A **constant repetition list** (`InitializationRepetitionList`) defines a list of values by repeating a given variable initialization ([VariableInitialization](#VariableInitialization))
a predefined number of times (`times`).
Multiple array initialization elements can be concatenated together in an `VariableInitialization`.

			

- **Validation:**
   * [INVALID\_ARRAY\_INIT\_SIZE] The array initialization should match the size of the array.

**Refers to:**
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)
- [VariableInitialization](#VariableInitialization)

**Referred by:**
- [VariableInitialization](#VariableInitialization)


```
ArrayInitializationElement:
	{ConstantInitializationElement} constant=NamedOrUnnamedConstantRef |
	{InitializationRepetitionList} times=NamedOrUnnamedConstantRef '(' toRepeat=VariableInitialization ')';
```



####  DataType  {#DataType}
A **data type** defines the size and structure of a defined variable.


**Refers to:**
- [ArrayDT](#ArrayDT)
- [ElementaryDT](#ElementaryDT)
- [FbOrUdtDT](#FbOrUdtDT)
- [ParameterDT](#ParameterDT)
- [StringDT](#StringDT)
- [StructDT](#StructDT)

**Referred by:**
- [ArrayDT](#ArrayDT)
- [Function](#Function)
- [VariableDeclarationLine](#VariableDeclarationLine)


```
DataType:
	ElementaryDT | StringDT | ArrayDT | StructDT | FbOrUdtDT | ParameterDT;
```



####  ElementaryDT  {#ElementaryDT}
An **elementary data type** defines the data type of an elementary
(non-string) variable.


**Refers to:**
- [ElementaryTypeEnum](#ElementaryTypeEnum)

**Referred by:**
- [DataType](#DataType)


```
ElementaryDT:
	type=ElementaryTypeEnum;
```



####  ElementaryTypeEnum (enum) {#ElementaryTypeEnum}
An **elementary type enumeration** literal defines an elementary data type.
			

The literal `VOID` is also included, but can only be used as return type of a function.
			

Certain new data types are only supported in S7-1200/1500 with Tia Portal (see [S7v14] pp. 3449-3453):
`WCHAR`, `WSTRING`, `SINT`, `USINT`, `UINT`, `UDINT`,
`LINT`, `ULINT`, `LREAL`, `DATE_AND_LTIME`, `DTL`.
			

Certain data types are only supported in S7-1500 with Tia Portal:
`LWORD`, `LTIME`, `LTIME_OF_DAY`.

- **Validation:**
   * [ILLEGAL\_USE\_OF\_VOID] The type VOID is only permitted as the return data type of a Function
   * [INVALID\_ELEMENTARY\_TYPE] The new data types (WCHAR, ...) can only be used in S7-1200/1500 + Tia Portal
   * [INVALID\_ELEMENTARY\_TYPE] Type LDT can only be used in S7-1500, DTL can only be used in S7-1200/1500

Literals:
- BOOL (`BOOL`)
- BYTE (`BYTE`)
- CHAR (`CHAR`)
- DATE (`DATE`)
- DATE_AND_LTIME (`LDT`, `DATE_AND_LTIME`)
- DATE_AND_TIME (`DT`, `DATE_AND_TIME`)
- DINT (`DINT`)
- DTL (`DTL`)
- DWORD (`DWORD`)
- INT (`INT`)
- LINT (`LINT`)
- LREAL (`LREAL`)
- LTIME (`LTIME`)
- LTIME_OF_DAY (`LTOD`, `LTIME_OF_DAY`)
- LWORD (`LWORD`)
- REAL (`REAL`)
- S5TIME (`S5TIME`)
- SINT (`SINT`)
- TIME (`TIME`)
- TIME_OF_DAY (`TIME_OF_DAY`, `TOD`)
- UDINT (`UDINT`)
- UINT (`UINT`)
- ULINT (`ULINT`)
- USINT (`USINT`)
- VOID (`VOID`)
- WCHAR (`WCHAR`)
- WORD (`WORD`)
- WSTRING (`WSTRING`)

```
enum ElementaryTypeEnum:
		BOOL='BOOL' | BYTE='BYTE' | WORD='WORD' | DWORD='DWORD' |
		CHAR='CHAR' |
		INT='INT' | DINT='DINT' | REAL='REAL' |
		S5TIME='S5TIME' | TIME='TIME' | TIME_OF_DAY='TIME_OF_DAY' | TIME_OF_DAY='TOD' | DATE='DATE' |
		VOID='VOID' |
		WCHAR='WCHAR' | WSTRING='WSTRING' |
	SINT='SINT' | USINT='USINT' | UINT='UINT' | UDINT='UDINT' | LINT='LINT' | ULINT='ULINT' | LREAL='LREAL' | 
		LWORD='LWORD' | LTIME='LTIME' | LTIME_OF_DAY='LTOD' | LTIME_OF_DAY='LTIME_OF_DAY' |
		DATE_AND_TIME='DT' | DATE_AND_TIME='DATE_AND_TIME' |
		DATE_AND_LTIME='LDT' | DATE_AND_LTIME='DATE_AND_LTIME' |
	DTL='DTL';
```



####  StringDT  {#StringDT}
A **string data type** defines a character string with given maximum length.
			

The maximum length is defined by the `dimension` field, or assumed to be 254 if omitted. Use `Step7LanguageHelper.dimensionInt()` to get the dimension.

- **Validation:**
   * [INVALID_SIMPLE_EXPRESSION] The `dimension` shall be a simple expression (i.e., computable in compilation time)
   * [INVALID_STRINGDT_DIMENSION] The `dimension` should be within 1..254 (p. 7-7)

**Refers to:**
- [Expression](#Expression)

**Referred by:**
- [DataType](#DataType)


```
StringDT:
	{StringDT}
	'STRING' ('[' dimension=Expression ']')?;
```



####  ArrayDT  {#ArrayDT}
An **array data type** defines a certain number of components with a given base data type (`baseType`, also known as element type).

- **Validation:**
   * [TOO_MANY_ARRAY_DIMENSIONS] An array may have up to 6 dimensions, i.e., `dimensions` shall contain 1..6 ArrayDimension items (p. 15-43)
   * [INVALID\_ARRAY\_TYPE] Parameter types must not be used as base data type. (p. 7-9)

**Refers to:**
- [ArrayDimension](#ArrayDimension)
- [DataType](#DataType)

**Referred by:**
- [DataType](#DataType)


```
ArrayDT:
	'ARRAY' '[' dimensions+=ArrayDimension (',' dimensions+=ArrayDimension)* ']' 'OF' baseType=DataType;
```



####  ArrayDimension  {#ArrayDimension}
An **array dimension** defines the lower and upper limits of one dimension of an array.
Both values are inclusive.
			

The `ArrayDimensionRange` represents a range that can be parsed by the grammar.
If there is no whitespace around the `..` separators, this cannot be done. Such cases
are parsed as `ArrayDimensionString`, where the rang is given by a single string.
Use the `DataTypeUtil.arrayRangeLower` and `DataTypeUtil.arrayRangeUpper`
methods to cover both cases.
			

Note: currently the use of simple expressions without spaces around '..' is not possible. However,
there is sanitization for this in the `Step7CodeSanitizer` class.
TIA Grammar: Tia Portal supports ARRAY[*] for parameters
			

- **Validation:**
   * [INVALID_ARRAY_RANGE] The values `from` and `to` shall be within -32768..32767. (p. 7-9)
   * [INVALID_ARRAY_RANGE] The value `from` must not be greater than `to`.
   * [INVALID_SIMPLE_EXPRESSION] The expressions `from` and `to` shall be simple expressions.
- **Examples:**
   * `1 .. N+1` (parsed as `ArrayDimensionRange`)
   * `0..15` (parsed as `ArrayDimensionString`)

**Refers to:**
- [CONSTANT_RANGE](#CONSTANT_RANGE)
- [DOUBLE_DOT](#DOUBLE_DOT)
- [Expression](#Expression)

**Referred by:**
- [ArrayDT](#ArrayDT)


```
ArrayDimension:
	{ArrayDimensionRange} from=Expression DOUBLE_DOT to=Expression |
	{ArrayDimensionString} rangeString=CONSTANT_RANGE;
```



####  DOUBLE_DOT (terminal) {#DOUBLE_DOT}
Separator string used between an array dimension's lower and upper value.


**Referred by:**
- [ArrayDimension](#ArrayDimension)
- [CaseElementValue](#CaseElementValue)

```
terminal DOUBLE_DOT:
	'..';
```



####  CONSTANT_RANGE (terminal) {#CONSTANT_RANGE}
Workaround to parse array dimensions when proper parsing is not possible
due to the lack of whitespace around the dimension separator (`..`).

- **Examples:**
   * `3..10`

**Refers to:**
- [INTSTR](#INTSTR)

**Referred by:**
- [ArrayDimension](#ArrayDimension)
- [CaseElementValue](#CaseElementValue)

```
terminal CONSTANT_RANGE:
	INTSTR '..' INTSTR;
```



####  StructDT  {#StructDT}
A **structure data type** describes a memory area consists of 
a given collection of named variables with various types.
The contained variables are defined within the `members` field.

- **Validation:**
   * [EMPTY\_STRUCT] Empty structures are not permitted.

**Refers to:**
- [VariableDeclarationLine](#VariableDeclarationLine)

**Referred by:**
- [DataBlockStructure](#DataBlockStructure)
- [DataType](#DataType)
- [UserDefinedDataType](#UserDefinedDataType)


```
StructDT:
	{StructDT}
	'STRUCT'
		(members+=VariableDeclarationLine)*
	'END_STRUCT';
```



####  FbOrUdtDT  {#FbOrUdtDT}
A **function block or UDT type** defines a data type based
on a named program unit that is either a _function block_ or 
a _user-defined type_.

- **Validation:**
   * [INVALID_FBORUDT_TYPE] The `type` is expected to be UDT or FB

**Refers to:**
- [IdOrSymbol](#IdOrSymbol)

**Referred by:**
- [DataBlockStructure](#DataBlockStructure)
- [DataType](#DataType)


```
FbOrUdtDT:
	type=[ProgramUnit|IdOrSymbol];
```



####  ParameterDT  {#ParameterDT}
A **parameter data type**.

- **Validation:**
   * [INVALID\_PARAMDT\_USAGE] A parameter data type can only be used as data type in FB's or FC's VAR\_INPUT block or in FB's VAR\_IN\_OUT block (p. Glossary-10)

**Refers to:**
- [ParameterTypeEnum](#ParameterTypeEnum)

**Referred by:**
- [DataType](#DataType)


```
ParameterDT:
	type=ParameterTypeEnum;
```



####  ParameterTypeEnum (enum) {#ParameterTypeEnum}


Literals:
- ANY (`ANY`)
- ANY_BIT (`xxxANY_BIT`)
- ANY_NUM (`ANY_NUM`)
- BLOCK_DB (`BLOCK_DB`)
- BLOCK_FB (`BLOCK_FB`)
- BLOCK_FC (`BLOCK_FC`)
- BLOCK_SDB (`BLOCK_SDB`)
- COUNTER (`COUNTER`)
- POINTER (`POINTER`)
- TIMER (`TIMER`)

```
enum ParameterTypeEnum:
	TIMER='TIMER' | COUNTER='COUNTER' |
	ANY='ANY' | ANY_NUM='ANY_NUM' | ANY_BIT='xxxANY_BIT' | 
	POINTER='POINTER' | 
	BLOCK_FC='BLOCK_FC' | BLOCK_FB='BLOCK_FB' | BLOCK_DB='BLOCK_DB' | BLOCK_SDB='BLOCK_SDB';
```



####  BlockAttributes  {#BlockAttributes}
The **block attributes** define metadata for a program unit (block), 
such as author, version, etc.


**Refers to:**
- [BlockAttribute](#BlockAttribute)

**Referred by:**
- [DataBlock](#DataBlock)
- [Function](#Function)
- [FunctionBlock](#FunctionBlock)
- [OrganizationBlock](#OrganizationBlock)
- [UserDefinedDataType](#UserDefinedDataType)


```
BlockAttributes:
	(attributes+=BlockAttribute)+;
```



####  BlockAttribute  {#BlockAttribute}
A **block attribute** defines a characteristic (metadata) of a program unit. 
It may define the
title ([TitleAttribute](#TitleAttribute)),
author (`AuthorAttribute`),
know-how protection (`ProtectionAttribute`),
name (`NameAttribute`),
family (`FamilyAttribute`),
version (`VersionAttribute`) 
of the defined block.
In case of data blocks, in addition, the retention policy can also be altered (using `NonRetainAttribute`).
			

- **Validation:**
   * [INVALID_BLOCK_ATTRIBUTE] If `author`, `content` or `family` is an ID, it shall be max. 8 characters long (p. 6-6)
   * The field `version` should only be FIX_POINT_NUMBER if used in DataBlock (p. 6-5) -- It is not strictly enforced.
   * The field `version` should only be STRING_CONST if used elsewhere than in DataBlock (p. 6-5) -- It is not strictly enforced.
   * [INVALID_BLOCK_ATTRIBUTE] The `NonRetainAttribute` is only valid for data blocks.
   * [INVALID_BLOCK_ATTRIBUTE] The `version` should be in format A.B, where both A and B shall be within 0..15 (p. 15-31)
- **Examples:**
   * `AUTHOR : 'John Doe'`
   * `KNOW_HOW_PROTECT`

**Refers to:**
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [IdLike](#IdLike)
- [STRING_CONST](#STRING_CONST)
- [TitleAttribute](#TitleAttribute)

**Referred by:**
- [BlockAttributes](#BlockAttributes)


```
BlockAttribute:
	TitleAttribute | 
	{AuthorAttribute} 'AUTHOR' ('=' | ':') author=(STRING_CONST|IdLike) |
	{ProtectionAttribute} 'KNOW_HOW_PROTECT' |
	{NameAttribute} 'NAME' ('=' | ':') content=(STRING_CONST|IdLike) |
	{FamilyAttribute} 'FAMILY' ('=' | ':') family=(STRING_CONST|IdLike) |
	{VersionAttribute} 'VERSION'('=' | ':') version=(FIX_POINT_NUMBER | STRING_CONST) |
	{NonRetainAttribute} 'NON_RETAIN';
```



####  TitleAttribute  {#TitleAttribute}
A **title attribute** defines the title of a program unit or an STL network.
For blocks, it can be parsed as a `TitleAttribute`, where the title name
is in the field `title`. However, the syntax of STL networks is more permissive,
the title name do not have to be surrounded by `'` characters even if it 
contains whitespace. Those cases are covered by `TitleAttributeText` which
is a wrapper of a [TITLE_LINE](#TITLE_LINE).
			

- **Examples:**
   * `TITLE = 'Calculation'` (parsed as `TitleAttribute`)
   * `TITLE= Calculation of some value` (parsed as `TitleAttributeText`)

**Refers to:**
- [IdLike](#IdLike)
- [STRING_CONST](#STRING_CONST)
- [TITLE_LINE](#TITLE_LINE)

**Referred by:**
- [BlockAttribute](#BlockAttribute)
- [StlNetwork](#StlNetwork)


```
TitleAttribute:
	({TitleAttribute} 'TITLE' ('=' | ':') title=(STRING_CONST|IdLike)) | 
	{TitleAttributeText} title=TITLE_LINE;
```



####  TITLE_LINE (terminal) {#TITLE_LINE}
Title block attribute line for STL networks.


**Referred by:**
- [TitleAttribute](#TitleAttribute)

```
terminal TITLE_LINE:
	'TITLE=' -> '\n';
```



####  StatementList  {#StatementList}
A **statement list** is an ordered list of statements written in a STEP 7
programming language, to be executed sequentially.
Currently, SCL ([SclStatementList](#SclStatementList)) and STL ([StlStatementList](#StlStatementList))
statement lists are supported.


**Refers to:**
- [SclStatementList](#SclStatementList)
- [StlStatementList](#StlStatementList)

**Referred by:**
- [Function](#Function)
- [FunctionBlock](#FunctionBlock)
- [OrganizationBlock](#OrganizationBlock)


```
StatementList:
	SclStatementList | StlStatementList;
```



####  StlStatementList  {#StlStatementList}
An **STL statement list** is an ordered list of one or more 
[STL networks](#StlNetwork).

While it is permitted to have empty STL blocks (i.e., with
zero networks), it will be parsed as an empty SCL statement
list, as we don't have any ways to distinguish otherwise.


**Refers to:**
- [StlNetwork](#StlNetwork)

**Referred by:**
- [StatementList](#StatementList)


```
StlStatementList:
	{StlStatementList}
	(networks+=StlNetwork)+;
```



####  StlNetwork  {#StlNetwork}
An **STL network** is a group of [STL statements](#AbstractStlStatement)
to be executed sequentially. Certain status bits are cleared between 
different STL networks, providing some level of independence between the networks.


**Refers to:**
- [AbstractStlStatement](#AbstractStlStatement)
- [TitleAttribute](#TitleAttribute)

**Referred by:**
- [StlStatementList](#StlStatementList)


```
StlNetwork:
	{StlNetwork}
	'NETWORK'
	(titleAttribute=TitleAttribute)?
	(statements+=AbstractStlStatement)*;
```



####  AbstractStlStatement  {#AbstractStlStatement}
An **abstract STL statement** is either a 
[labeled STL statement](#LabeledStlStatement) or a
[non-labeled STL statement](#StlStatement).


**Refers to:**
- [LabeledStlStatement](#LabeledStlStatement)
- [StlStatement](#StlStatement)

**Referred by:**
- [StlNetwork](#StlNetwork)


```
AbstractStlStatement: 
	LabeledStlStatement | StlStatement;
```



####  LabeledStlStatement  {#LabeledStlStatement}
A **labeled STL statement** is an STL statement (`statement`)
with one or more [labels](#Label) attached to it.

- **Validation:**
   * For each defined [Label](#Label) there should be 0 or 1 [LabeledStlStatement](#LabeledStlStatement) only.

**Refers to:**
- [Label](#Label)
- [StlStatement](#StlStatement)

**Referred by:**
- [AbstractStlStatement](#AbstractStlStatement)
- [LabeledStatement](#LabeledStatement)


```
LabeledStlStatement:
	label=Label ':' statement=StlStatement;
```



####  StlStatement  {#StlStatement}
An **STL statement** (or STL instruction) is the basic unit of
execution in the STL language.


**Refers to:**
- [StlArgumentlessStatement](#StlArgumentlessStatement)
- [StlJumpStatement](#StlJumpStatement)
- [StlParameterlessCallStatement](#StlParameterlessCallStatement)
- [StlSubroutineCall](#StlSubroutineCall)
- [StlUnaryConstantStatement](#StlUnaryConstantStatement)
- [StlUnaryStatement](#StlUnaryStatement)
- [StlVerificationAssertion](#StlVerificationAssertion)

**Referred by:**
- [AbstractStlStatement](#AbstractStlStatement)
- [LabeledStlStatement](#LabeledStlStatement)


```
StlStatement: 
	StlUnaryStatement |
	StlUnaryConstantStatement |
	StlArgumentlessStatement | 	
		StlJumpStatement | 
	StlSubroutineCall | 
	StlParameterlessCallStatement | 
	StlVerificationAssertion;
```



####  StlUnaryStatement  {#StlUnaryStatement}
A **unary STL statement** is an STL statement that has one 
[expression](#UnaryOrPrimaryExpression) as argument.
			

- **Examples:**
   * `R I0.0;`
   * `L 0;`

**Refers to:**
- [StlBitLogicOpMnemonic](#StlBitLogicOpMnemonic)
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)

**Referred by:**
- [StlStatement](#StlStatement)


```
StlUnaryStatement:
		{StlBitLogicStatement} mnemonic=StlBitLogicOpMnemonic arg=UnaryOrPrimaryExpression  ';' |
	{StlAssignStatement} '=' arg=UnaryOrPrimaryExpression ';' |
	{StlResetStatement} 'R' arg=UnaryOrPrimaryExpression ';' |
	{StlSetStatement} 'S' arg=UnaryOrPrimaryExpression ';' |
	{StlFpStatement} 'FP' arg=UnaryOrPrimaryExpression ';' |
	{StlFnStatement} 'FN' arg=UnaryOrPrimaryExpression ';' |
		{StlLoadStatement} 'L' arg=UnaryOrPrimaryExpression ';' | 
	{StlTransferStatement} 'T' arg=UnaryOrPrimaryExpression ';'
;
```



####  StlUnaryConstantStatement  {#StlUnaryConstantStatement}
A **unary STL statement** is an STL statement that has one 
[expression](#UnaryOrPrimaryExpression) as argument.
			

- **Validation:**
   * [INVALID_STL_ARGUMENT] The value `arg` of `StlAccuIncrementStatement` shall be a constant within 0..255.
   * [INVALID_STL_ARGUMENT] The value `arg` of `StlAccuDecrementStatement` shall be a constant within 0..255.
   * [INVALID_STL_ARGUMENT] The value `arg` of `StlBldStatement` shall be a constant within 0..255. ([STL] p. 14-12)
   * [INVALID_STL_ARGUMENT] The value `arg` of `StlNopStatement` shall be a constant within 0..1. ([STL] p. 14-13)
- **Examples:**
   * `NOP 0;`

**Refers to:**
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)
- [NegatedUnnamedConstantRef](#NegatedUnnamedConstantRef)
- [StlShiftRotateStatement](#StlShiftRotateStatement)
- [StlWordLogicMnemonic](#StlWordLogicMnemonic)
- [UnnamedConstantRef](#UnnamedConstantRef)

**Referred by:**
- [StlStatement](#StlStatement)


```
StlUnaryConstantStatement: 
		{StlAddIntConstStatement} '+' arg=(NegatedUnnamedConstantRef|UnnamedConstantRef) ';' |
		StlShiftRotateStatement |
		{StlWordLogicStatement} mnemonic=StlWordLogicMnemonic arg=NamedOrUnnamedConstantRef ';' |
		{StlAccuIncrementStatement} 'INC' arg=NamedOrUnnamedConstantRef ';' |
	{StlAccuDecrementStatement} 'DEC' arg=NamedOrUnnamedConstantRef ';' |
	{StlBldStatement} 'BLD' arg=NamedOrUnnamedConstantRef ';' |
	{StlNopStatement} 'NOP' arg=NamedOrUnnamedConstantRef ';'
;
```



####  StlShiftRotateStatement  {#StlShiftRotateStatement}
**Shift/rotate STL statement**.
Optionally it can have an argument specifying the amount of shift/rotation.
			

If `arg` is not provided, it is assumed to be 1. It is not permitted to 
specify `arg` if `mnemonic` is `RLDA` or `RRDA` (thus 1 is always assumed).
They are included here only for the unified handling of shift and rotation.
			

- **Validation:**
   * [INVALID_STL_ARGUMENT] If `mnemonic` is `RLDA` or `RRDA`, the `arg` shall not be present.
- **Examples:**
   * `SLW`
   * `SLW -3`
   * `RRDA`

**Refers to:**
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)
- [StlShiftRotateMnemonic](#StlShiftRotateMnemonic)

**Referred by:**
- [StlUnaryConstantStatement](#StlUnaryConstantStatement)


```
StlShiftRotateStatement:
	mnemonic=StlShiftRotateMnemonic (arg=NamedOrUnnamedConstantRef)? ';';
```



####  StlArgumentlessStatement  {#StlArgumentlessStatement}
An **argumentless STL statement** is an STL statement that 
has no argument.
			

- **Validation:**
   * [ILLEGAL_STL_ELEMENT] In `StlConversionStatement`, whitespace is not permitted between the `comparison` and `dataType`.
- **Examples:**
   * `O;`
   * `SAVE;`

**Refers to:**
- [StlAccuArgumentlessMnemonic](#StlAccuArgumentlessMnemonic)
- [StlArithmeticMnemonic](#StlArithmeticMnemonic)
- [StlBitLogicOpMnemonic](#StlBitLogicOpMnemonic)
- [StlBlockEndMnemonic](#StlBlockEndMnemonic)
- [StlComparisonDataType](#StlComparisonDataType)
- [StlComparisonMnemonic](#StlComparisonMnemonic)
- [StlConversionMnemonic](#StlConversionMnemonic)
- [StlFloatMathFunctionMnemonic](#StlFloatMathFunctionMnemonic)
- [StlMcrMnemonic](#StlMcrMnemonic)
- [StlWordLogicMnemonic](#StlWordLogicMnemonic)

**Referred by:**
- [StlStatement](#StlStatement)


```
StlArgumentlessStatement:
		{StlAndBeforeOr} 'O' ';' |
	{StlBitLogicNestingOpenStatement} mnemonic=StlBitLogicOpMnemonic '(' ';' |
	{StlBitLogicNestingCloseStatement} ')' ';' |
	{StlNegateRloStatement} 'NOT' ';' |
	{StlSetRloStatement} 'SET' ';' |
	{StlClrRloStatement} 'CLR' ';' |
	{StlSaveRloStatement} 'SAVE' ';' |
		{StlComparisonStatement} comparison=StlComparisonMnemonic dataType=StlComparisonDataType ';' | 
		{StlConversionStatement} mnemonic=StlConversionMnemonic ';' |
		{StlModStatement} 'MOD' ';' |
		{StlArithmeticStatement} mnemonic=StlArithmeticMnemonic dataType=StlComparisonDataType ';' |
		{StlFloatMathFunction} mnemonic=StlFloatMathFunctionMnemonic ';' |
		{StlLoadStwStatement} 'L' 'STW' ';' |
	{StlTransferStwStatement} 'T' 'STW' ';' |
		{StlBlockEndStatement} mnemonic=StlBlockEndMnemonic ';' |
	{StlMcrStatement} mnemonic=StlMcrMnemonic ';' |
		{StlWordLogicArglessStatement} mnemonic=StlWordLogicMnemonic ';' |
		{StlAccuArglessStatement} mnemonic=StlAccuArgumentlessMnemonic ';'
;
```



####  StlMcrMnemonic (enum) {#StlMcrMnemonic}
STL MCR manipulation instruction mnemonic.
It is used to specify the type of MCR instruction described by 
a `StlMcrStatement`.


Literals:
- ACTIVATE_MCR (`MCRA`)
- BEGIN_MCR (`MCR(`)
- DEACTIVATE_MCR (`MCRD`)
- END_MCR (`)MCR`)

```
enum StlMcrMnemonic:
	BEGIN_MCR = 'MCR(' |
	END_MCR = ')MCR' |
	ACTIVATE_MCR = 'MCRA' |
	DEACTIVATE_MCR = 'MCRD';
```



####  StlConversionMnemonic (enum) {#StlConversionMnemonic}
STL conversion instruction mnemonic.
It is used to specify the type of conversion described by 
a `StlConversionStatement`.


Literals:
- BCD_TO_DINT (`BTD`)
- BCD_TO_INT (`BTI`)
- CAD (`CAD`)
- CAW (`CAW`)
- DINT_ONES_COMPLEMENT (`INVD`)
- DINT_TO_BCD (`DTB`)
- DINT_TO_REAL (`DTR`)
- DINT_TWOS_COMPLEMENT (`NEGD`)
- INT_ONES_COMPLEMENT (`INVI`)
- INT_TO_BCD (`ITB`)
- INT_TO_DINT (`ITD`)
- INT_TWOS_COMPLEMENT (`NEGI`)
- REAL_NEGATE (`NEGR`)
- RND_MINUS (`RND-`)
- RND_PLUS (`RND+`)
- ROUND (`RND`)
- TRUNCATE (`TRUNC`)

```
enum StlConversionMnemonic:
	INT_TO_DINT = 'ITD' |
	DINT_TO_REAL = 'DTR' |
	INT_ONES_COMPLEMENT = 'INVI' |
	DINT_ONES_COMPLEMENT = 'INVD' |
	INT_TWOS_COMPLEMENT = 'NEGI' |
	DINT_TWOS_COMPLEMENT = 'NEGD' |
	REAL_NEGATE = 'NEGR' |
	ROUND = 'RND' |
	TRUNCATE = 'TRUNC' |
	RND_PLUS = 'RND+' |
	RND_MINUS = 'RND-' |
		BCD_TO_INT = 'BTI' |
	BCD_TO_DINT = 'BTD' |
	INT_TO_BCD = 'ITB' |
	DINT_TO_BCD = 'DTB' |
	CAW = 'CAW' |
	CAD = 'CAD';
```



####  StlArithmeticMnemonic (enum) {#StlArithmeticMnemonic}
STL arithmetic instruction mnemonic.
It is used to specify the type of arithmetic operation described by 
a `StlArithmeticStatement`.


Literals:
- DIVISON (`/`)
- MINUS (`-`)
- MULTIPLICATION (`*`)
- PLUS (`+`)

```
enum StlArithmeticMnemonic:
	PLUS = '+' |
	MINUS = '-' |
	MULTIPLICATION = '*' |
	DIVISON = '/';
```



####  StlFloatMathFunctionMnemonic (enum) {#StlFloatMathFunctionMnemonic}
STL floating-point mathematical function mnemonic.
It is used to specify the type of floating-point mathematical
operation described by a `StlFloatMathFunction`.


Literals:
- ABS (`ABS`)
	 : _Absolute value of ACCU1_
- ACOS (`ACOS`)
	 : _Arc cosine of ACCU1_
- ASIN (`ASIN`)
	 : _Arc sine of ACCU1_
- ATAN (`ATAN`)
	 : _Arc tangent of ACCU1_
- COS (`COS`)
	 : _Cosine of ACCU1_
- EXP (`EXP`)
	 : _EXP of ACCU1 (e^ACCU1)_
- LN (`LN`)
	 : _Natural logarithm of ACCU1_
- SIN (`SIN`)
	 : _Sine of ACCU1_
- SQR (`SQR`)
	 : _Square of ACCU1_
- SQRT (`SQRT`)
	 : _Square root of ACCU1_
- TAN (`TAN`)
	 : _Tangent of ACCU1_

```
enum StlFloatMathFunctionMnemonic:
	ABS = 'ABS' |
	SQR = 'SQR' |
	SQRT = 'SQRT' |
	EXP = 'EXP' |
	LN = 'LN' |
	SIN = 'SIN' |
	COS = 'COS' |
	TAN = 'TAN' |
	ASIN = 'ASIN' |
	ACOS = 'ACOS' |
	ATAN = 'ATAN';
```



####  StlComparisonMnemonic (enum) {#StlComparisonMnemonic}
STL comparison instruction mnemonic.
It is used to specify the type of comparison described by 
a `StlComparisonStatement`.


Literals:
- EQ (`==`)
- GE (`>=`)
- GT (`>`)
- LE (`<=`)
- LT (`<`)
- NEQ (`<>`)

```
enum StlComparisonMnemonic:
	EQ = '==' |
	NEQ = '<>' |
	LT = '<' |
	GT = '>' |
	LE = '<=' |
	GE = '>=';
```



####  StlComparisonDataType (enum) {#StlComparisonDataType}
STL comparison data type mnemonic.
It is used to specify the data type used in a comparison
described by a `StlComparisonStatement`.


Literals:
- DINT (`D`)
- INT (`I`)
- REAL (`R`)

```
enum StlComparisonDataType:
	INT = 'I' | DINT = 'D' | REAL = 'R';
```



####  StlBitLogicOpMnemonic (enum) {#StlBitLogicOpMnemonic}
STL nesting bit logic operation instruction mnemonic.
It is used to specify the type of bit logic operation
described by a `StlBitLogicNestingOpenStatement`.


Literals:
- AND (`A`)
- AND_NOT (`AN`)
- OR (`O`)
- OR_NOT (`ON`)
- XOR (`X`)
- XOR_NOT (`XN`)

```
enum StlBitLogicOpMnemonic:
	AND = 'A' | AND_NOT = 'AN' | 
	OR = 'O' | OR_NOT = 'ON' | 
	XOR = 'X' | XOR_NOT = 'XN';
```



####  StlShiftRotateMnemonic (enum) {#StlShiftRotateMnemonic}
Mnemonics that can be used in shift/rotate STL instructions.


Literals:
- ROTATE_LEFT_DWORD (`RLD`)
	 : _Rotates to the left, works on DWORD (ACCU 1)._
- ROTATE_LEFT_DWORD_VIA_CC1 (`RLDA`)
	 : _Rotates to the left via CC1, works on DWORD (ACCU 1)._
- ROTATE_RIGHT_DWORD (`RRD`)
	 : _Rotates to the right, works on DWORD (ACCU 1)._
- ROTATE_RIGHT_DWORD_VIA_CC1 (`RRDA`)
	 : _Rotates to the right via CC1, works on DWORD (ACCU 1)._
- SHIFT_LEFT_DWORD (`SLD`)
	 : _Shifts to the left, works on DWORD (ACCU 1)._
- SHIFT_LEFT_WORD (`SLW`)
	 : _Shifts to the left, works on WORD (ACCU 1-L)._
- SHIFT_RIGHT_DWORD (`SRD`)
	 : _Shifts to the right, works on DWORD (ACCU 1)._
- SHIFT_RIGHT_SIGN_DINT (`SSD`)
	 : _Shifts to the right with sign, works on DINT (ACCU 1)._
- SHIFT_RIGHT_SIGN_INT (`SSI`)
	 : _Shifts to the right with sign, works on INT (ACCU 1-L)._
- SHIFT_RIGHT_WORD (`SRW`)
	 : _Shifts to the right, works on WORD (ACCU 1-L)._

```
enum StlShiftRotateMnemonic:
	
	SHIFT_RIGHT_SIGN_INT = 'SSI' |
	SHIFT_RIGHT_SIGN_DINT = 'SSD' |
	SHIFT_LEFT_WORD = 'SLW' |
	SHIFT_RIGHT_WORD = 'SRW' |
	SHIFT_LEFT_DWORD = 'SLD' |
	SHIFT_RIGHT_DWORD = 'SRD' |
		
	ROTATE_LEFT_DWORD = 'RLD' |
	ROTATE_RIGHT_DWORD = 'RRD' |
	ROTATE_LEFT_DWORD_VIA_CC1 = 'RLDA' |
	ROTATE_RIGHT_DWORD_VIA_CC1 = 'RRDA';
```



####  StlBlockEndMnemonic (enum) {#StlBlockEndMnemonic}
STL mnemonic to define the exact behavior of block end instruction.


Literals:
- BLOCK_END (`BE`)
- BLOCK_END_CONDITIONAL (`BEC`)
- BLOCK_END_UNCONDITIONAL (`BEU`)

```
enum StlBlockEndMnemonic:
	BLOCK_END = 'BE' |
	BLOCK_END_CONDITIONAL = 'BEC' |
	BLOCK_END_UNCONDITIONAL = 'BEU';
```



####  StlWordLogicMnemonic (enum) {#StlWordLogicMnemonic}
STL mnemonic for word logic instructions, defining the operation and the
data size of the arguments.


Literals:
- AND_DWORD (`AD`)
- AND_WORD (`AW`)
- OR_DWORD (`OD`)
- OR_WORD (`OW`)
- XOR_DWORD (`XOD`)
- XOR_WORD (`XOW`)

```
enum StlWordLogicMnemonic:
	AND_WORD = 'AW' |
	OR_WORD = 'OW' |
	XOR_WORD = 'XOW' |
	AND_DWORD = 'AD' |
	OR_DWORD = 'OD' |
	XOR_DWORD = 'XOD';
```



####  StlAccuArgumentlessMnemonic (enum) {#StlAccuArgumentlessMnemonic}
STL mnemonic to define operation for argumentless accumulator instructions.


Literals:
- ENTER_ACCU_STACK (`ENT`)
- LEAVE_ACCU_STACK (`LEAVE`)
- POP_ACCU (`POP`)
- PUSH_ACCU (`PUSH`)
- TOGGLE_ACCUS (`TAK`)

```
enum StlAccuArgumentlessMnemonic:
	TOGGLE_ACCUS = 'TAK' |
	PUSH_ACCU = 'PUSH' |
	POP_ACCU = 'POP' | 
	ENTER_ACCU_STACK = 'ENT' |
	LEAVE_ACCU_STACK = 'LEAVE';
```



####  StlJumpStatement  {#StlJumpStatement}
**STL jump statement** to a given label.


**Refers to:**
- [IdLike](#IdLike)
- [StlJumpMnemonic](#StlJumpMnemonic)

**Referred by:**
- [StlStatement](#StlStatement)


```
StlJumpStatement:
	mnemonic=StlJumpMnemonic targetLabel=[Label|IdLike] ';';
```



####  StlJumpMnemonic (enum) {#StlJumpMnemonic}
STL jump type mnemonic.
It is used to specify the type of jump used in a jump
instruction described by a [StlJumpStatement](#StlJumpStatement).


Literals:
- JUMP_IF_BR_FALSE (`JNBI`)
- JUMP_IF_BR_TRUE (`JBI`)
- JUMP_IF_MINUS (`JM`)
- JUMP_IF_MINUS_OR_ZERO (`JMZ`)
- JUMP_IF_NOT_ZERO (`JN`)
- JUMP_IF_OS_TRUE (`JOS`)
- JUMP_IF_OV_TRUE (`JO`)
- JUMP_IF_PLUS (`JP`)
- JUMP_IF_PLUS_OR_ZERO (`JPZ`)
- JUMP_IF_RLO_FALSE (`JCN`)
- JUMP_IF_RLO_FALSE_WITH_BR (`JNB`)
- JUMP_IF_RLO_TRUE (`JC`)
- JUMP_IF_RLO_TRUE_WITH_BR (`JCB`)
- JUMP_IF_UNORDERED (`JUO`)
- JUMP_IF_ZERO (`JZ`)
- JUMP_UNCONDITIONAL (`JU`)
- LOOP (`LOOP`)

```
enum StlJumpMnemonic:
	JUMP_UNCONDITIONAL = 'JU' |
	JUMP_IF_RLO_TRUE = 'JC' |
	JUMP_IF_RLO_FALSE = 'JCN' |
	JUMP_IF_RLO_TRUE_WITH_BR = 'JCB' |
	JUMP_IF_RLO_FALSE_WITH_BR = 'JNB' |
	JUMP_IF_BR_TRUE = 'JBI' |
	JUMP_IF_BR_FALSE = 'JNBI' |
	JUMP_IF_OV_TRUE = 'JO' | 
	JUMP_IF_OS_TRUE = 'JOS' |
	JUMP_IF_ZERO = 'JZ' |
	JUMP_IF_NOT_ZERO = 'JN' |
	JUMP_IF_PLUS = 'JP' |
	JUMP_IF_MINUS = 'JM' |
	JUMP_IF_PLUS_OR_ZERO = 'JPZ' |
	JUMP_IF_MINUS_OR_ZERO = 'JMZ' |
	JUMP_IF_UNORDERED = 'JUO' |
	LOOP = 'LOOP'
;
```



####  StlSubroutineCall  {#StlSubroutineCall}
**STL call statement**.
It represents a call of a function or function block instance. In case of a function block call,
either the `calledUnit` is an instance declaration (of type [Variable](#Variable)), or 
a function block, with an explicit data block defined (`hasExplicitDataBlock` is true and 
`dataBlock` is a data block of type `calledUnit`).
			

- **Validation:**
   * See at 'SubroutineCall'.
- **Examples:**
   * `CALL function1;` (parameterless function call)
   * `CALL function1(in := TRUE);` (function call with a parameter)
   * `CALL fblock1, dblock1;` (function block call with a given data block)
   * `CALL fblockinstance1;` (function block instance call)

**Refers to:**
- [CallParameter](#CallParameter)
- [IdOrSymbol](#IdOrSymbol)

**Referred by:**
- [StlStatement](#StlStatement)
- [SubroutineCall](#SubroutineCall)


```
StlSubroutineCall:
	'CALL' calledUnit=[NamedElement|IdOrSymbol] (hasExplicitDataBlock?=',' dataBlock=[DataBlock|IdOrSymbol])? 
		('(' (callParameter=CallParameter)? ')')? ';';
```



####  StlParameterlessCallStatement  {#StlParameterlessCallStatement}
**Parameterless STL call statement**.
May be conditional (executed only if `RLO=1`) or unconditional,
depending on the value of the `mnemonic`.
It can only be used to call functions (FCs and SFCs) without any
parameter.

- **Examples:**
   * `CC FC1;`
   * `UC FC2;`

**Refers to:**
- [IdOrSymbol](#IdOrSymbol)
- [StlParameterlessCallMnemonic](#StlParameterlessCallMnemonic)

**Referred by:**
- [StlStatement](#StlStatement)


```
StlParameterlessCallStatement:
	mnemonic=StlParameterlessCallMnemonic calledUnit=[Function|IdOrSymbol] ';';
```



####  StlParameterlessCallMnemonic (enum) {#StlParameterlessCallMnemonic}
Mnemonics for parameterless STL call statements.


Literals:
- CONDITIONAL_CALL (`CC`)
	 : _Mnemonic for **conditional STL call**. Executed only if `RLO=1`._
- UNCONDITIONAL_CALL (`UC`)
	 : _Mnemonic for **unconditional STL call**._

```
enum StlParameterlessCallMnemonic:
	UNCONDITIONAL_CALL = 'UC' |
	CONDITIONAL_CALL = 'CC';
```



####  SclStatementList  {#SclStatementList}
An **SCL statement list** is a group of [(labeled or unlabeled) SCL statements](#AbstractSclStatement)
to be executed sequentially.


**Refers to:**
- [AbstractSclStatement](#AbstractSclStatement)

**Referred by:**
- [ElsifBlock](#ElsifBlock)
- [SclForStatement](#SclForStatement)
- [SclIfStatement](#SclIfStatement)
- [SclRepeatStatement](#SclRepeatStatement)
- [SclWhileStatement](#SclWhileStatement)
- [StatementList](#StatementList)


```
SclStatementList:
	{SclStatementList}
	(statements+=AbstractSclStatement)+;
```



####  SclUnLabeledStatementList  {#SclUnLabeledStatementList}
An **SCL unlabeled statement list** is a group of
[unlabeled SCL statements](#SclStatement)
to be executed sequentially. 
			

It is a special SCL statement list that can only contain
unlabeled statements. Used in `CASE` statements.


**Refers to:**
- [SclStatement](#SclStatement)

**Referred by:**
- [CaseElement](#CaseElement)
- [SclCaseStatement](#SclCaseStatement)


```
SclUnLabeledStatementList returns SclStatementList:
	(statements+=SclStatement)+;
```



####  AbstractSclStatement  {#AbstractSclStatement}
An **abstract SCL statement** is either a 
[labeled SCL statement](#LabeledSclStatement) or a
[non-labeled SCL statement](#SclStatement).


**Refers to:**
- [LabeledSclStatement](#LabeledSclStatement)
- [SclStatement](#SclStatement)

**Referred by:**
- [SclStatementList](#SclStatementList)


```
AbstractSclStatement:
	LabeledSclStatement | SclStatement;
```



####  LabeledSclStatement  {#LabeledSclStatement}
A **labeled SCL statement** is an SCL statement (`statement`)
with one or more [labels](#Label) attached to it.

- **Validation:**
   * For each defined [Label](#Label) there should be 0 or 1 [LabeledSclStatement](#LabeledSclStatement) only.

**Refers to:**
- [IdLike](#IdLike)
- [SclStatement](#SclStatement)

**Referred by:**
- [AbstractSclStatement](#AbstractSclStatement)
- [LabeledStatement](#LabeledStatement)


```
LabeledSclStatement:
	(labels+=[Label|IdLike] ':')+ statement=SclStatement;
```



####  SclStatement  {#SclStatement}
An **SCL statement** is the basic unit of
execution in the SCL language.
It can represent:

  - An empty statement (`SclNullStatement`);
  - An assignment statement ([SclAssignmentStatement](#SclAssignmentStatement));
  - An IF conditional statement ([SclIfStatement](#SclIfStatement));
  - A CASE conditional statement ([SclCaseStatement](#SclCaseStatement));
  - A FOR loop statement ([SclForStatement](#SclForStatement));
  - A WHILE loop statement ([SclWhileStatement](#SclWhileStatement));
  - A REPEAT loop statement ([SclRepeatStatement](#SclRepeatStatement));
  - A (function or function block) call statement ([SclSubroutineCall](#SclSubroutineCall));
  - A return statement (`SclReturnStatement`);
  - A continue statement (`SclContinueStatement`);
  - An exit statement (`SclExitStatement`);
  - A jump statement (`SclGotoStatement`).

In addition, an SCL verification assertion ([SclVerificationAssertion](#SclVerificationAssertion)) 
behaves also like an SCL statement.
			

- **Validation:**
   * (included in the grammar) `SclReturnStatement` may only be used in an FC, FB, or OB (p. 12-26)
   * [INVALID_LOOP_STATEMENT] `SclContinueStatement` and `SclExitStatement` shall only be used inside a loop (FOR, WHILE or REPEAT) (p. 12-23)
   * (enforced by scoping) `SclGotoStatement`'s `label` should point to a label defined in the same block (defined locally) (p. 12-25)
   * [INVALID_GOTO_STATEMENT] `SclGotoStatement`'s `label` should not point into a loop (it is forbidden to jump into a loop) (p. 12-25)
   * [INVALID_GOTO_STATEMENT] `SclGotoStatement`'s `label` should be present in the local program unit

**Refers to:**
- [SclAssignmentStatement](#SclAssignmentStatement)
- [SclCaseStatement](#SclCaseStatement)
- [SclForStatement](#SclForStatement)
- [SclGotoStatement](#SclGotoStatement)
- [SclIfStatement](#SclIfStatement)
- [SclRepeatStatement](#SclRepeatStatement)
- [SclSubroutineCall](#SclSubroutineCall)
- [SclVerificationAssertion](#SclVerificationAssertion)
- [SclWhileStatement](#SclWhileStatement)

**Referred by:**
- [AbstractSclStatement](#AbstractSclStatement)
- [LabeledSclStatement](#LabeledSclStatement)
- [SclUnLabeledStatementList](#SclUnLabeledStatementList)


```
SclStatement:
	{SclNullStatement} ';' | 
	SclAssignmentStatement ';' |
	SclIfStatement ';' |
	SclCaseStatement ';' |
	SclForStatement ';' |
	SclWhileStatement ';' |
	SclRepeatStatement ';' |
	SclSubroutineCall ';' |
	{SclReturnStatement} 'RETURN' ';' |
	{SclContinueStatement} 'CONTINUE' ';' |
	{SclExitStatement} 'EXIT' ';' | 
	SclGotoStatement ';' |
	SclVerificationAssertion;
```



####  SclAssignmentStatement  {#SclAssignmentStatement}
An **SCL value assignment statement** replaces the value of its
left-hand side (_left value_) with the value of the expression 
on the right-hand side (_right value_) on execution.
			

The left value can be a memory address, a local variable, a variable
in a locally defined structure, a variable in a data block or a symbol.
			

- **Validation:**
   * If `leftValue` is a DirectNamedRef to a Function, then the Function should be the one that contains the current SclAssignmentStatement (special case for return values).
   * If `leftValue` is a DirectNamedRef, it should not point to an OB or FB.
   * [CONST_AS_LEFTVALUE] The `leftValue` shall not be a [DirectNamedRef](#DirectNamedRef) pointing to a [NamedConstantDeclaration](#NamedConstantDeclaration). See [LeftValue](#LeftValue) for more details.

**Refers to:**
- [Expression](#Expression)
- [LeftValue](#LeftValue)

**Referred by:**
- [SclForStatement](#SclForStatement)
- [SclStatement](#SclStatement)


```
SclAssignmentStatement:
	leftValue=LeftValue ':=' rightValue=Expression;
```



####  SclIfStatement  {#SclIfStatement}
An **SCL IF conditional statement** executes one of the
statement lists contained depending on the evaluation of its condition(s).
			

If the `condition` is true, the contents of the `thenBranch`
will be executed. Otherwise, the first [ElsifBlock](#ElsifBlock)'s statement list
(`elsifBranch`) will be executed where the `condition` is satisfied.
If none of them is satisfied, the else branch will be executed (`elseBranch`),
if exists (i.e, `else` is true).
			

The ELSIF and ELSE branches are optional. If there is no else branch
(`else` is false), the value of `elseBranch` will be null.
The `thenBranch` is never null, but can be an empty statement list.
			


**Refers to:**
- [ElsifBlock](#ElsifBlock)
- [Expression](#Expression)
- [SclStatementList](#SclStatementList)

**Referred by:**
- [SclStatement](#SclStatement)


```
SclIfStatement:
	'IF' condition=Expression 'THEN'
		thenBranch=SclStatementList
	(elsifBranches+=ElsifBlock)*
	(else?='ELSE' 
		elseBranch=SclStatementList
	)?
	'END_IF';
```



####  ElsifBlock  {#ElsifBlock}
An **ELSIF block** describes an ELSIF condition of an
[IF conditional statement](#SclIfStatement), together
with a statement list to be executed if the given condition is 
the first one satisfied in the containing IF statement.


**Refers to:**
- [Expression](#Expression)
- [SclStatementList](#SclStatementList)

**Referred by:**
- [SclIfStatement](#SclIfStatement)


```
ElsifBlock:
	'ELSIF' condition=Expression 'THEN'
		elsifBranch=SclStatementList;
```



####  SclCaseStatement  {#SclCaseStatement}
An **SCL CASE statement** is used to select one of the
contained statement lists to be executed based on the value 
of a _selection expression_ (`selection`).
			

It contains one or more [case elements](#CaseElement).
The statement list in in the first one with matching value(s)
will be executed. If none of them has matching value, then
the statement list of the ELSE branch (`elseStatements))
will be executed (if it is defined, otherwise no statement is
executed).
			

The ELSE branch is optional. If it is not present ({@code else`
is false), then the value of `elseStatements` is
`null`.
			


**Refers to:**
- [CaseElement](#CaseElement)
- [Expression](#Expression)
- [SclUnLabeledStatementList](#SclUnLabeledStatementList)

**Referred by:**
- [SclStatement](#SclStatement)


```
SclCaseStatement:
	'CASE' selection=Expression 'OF'
		(elements+=CaseElement)+
		(else?='ELSE' ':'? elseStatements=SclUnLabeledStatementList)?
	'END_CASE';
```



####  CaseElement  {#CaseElement}
A **case element** defines one or more values, and a
statement list (`statements`) to be executed if the
selection expression of the containing [statement](#SclCaseStatement--case) matches one of the values.
			

It contains one or more [CaseElementValue](#CaseElementValue)s.
A case element value may be a single value or a range of values.


**Refers to:**
- [CaseElementValue](#CaseElementValue)
- [SclUnLabeledStatementList](#SclUnLabeledStatementList)

**Referred by:**
- [SclCaseStatement](#SclCaseStatement)


```
CaseElement:
	values+=CaseElementValue (',' values+=CaseElementValue)* ':' statements=SclUnLabeledStatementList;
```



####  CaseElementValue  {#CaseElementValue}
A **case element value** can be used to define when should the 
statement list of a [case element](#CaseElement) be executed.
			

It can be a single value (`SingleCaseElementValue`) or
a consecutive range of values (`RangeCaseElementValue`),
defined by the lower and upper bound of the range (both inclusive).
If the range does not have whitespace preceding the `..`
separator, it will be parsed as a [CONSTANT_RANGE](#CONSTANT_RANGE) and 
represented as `RangeCaseElementString`. Use
[Step7LanguageHelper.caseValueRangeLower](#Step7LanguageHelper.caseValueRangeLower) and
[Step7LanguageHelper.caseValueRangeUpper](#Step7LanguageHelper.caseValueRangeUpper) to extract the
values.
			

Expressions (even expressions that can be evaluated in compilation
time) are not permitted (checked on STEP 7 v5.6).

- **Validation:**
   * [INVALID_CASE_RANGE] In case of range case element values (`RangeCaseElementValue` or `RangeCaseElementString`),  the lower bound must not greater than the upper bound.

**Refers to:**
- [CONSTANT_RANGE](#CONSTANT_RANGE)
- [DOUBLE_DOT](#DOUBLE_DOT)
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)

**Referred by:**
- [CaseElement](#CaseElement)


```
CaseElementValue:
	{SingleCaseElementValue} value=NamedOrUnnamedConstantRef |
	{RangeCaseElementValue} lowerBound=NamedOrUnnamedConstantRef DOUBLE_DOT upperBound=NamedOrUnnamedConstantRef |
	{RangeCaseElementString} rangeString=CONSTANT_RANGE;
```



####  SclForStatement  {#SclForStatement}
An **SCL FOR loop statement** defines the execution of the contained
statement list multiple times. 
			

Before the first execution, the `initialStatement` assignment is 
executed. Then the contained statement list is executed until the 
_loop variable_ (left value of `initialStatement`) reaches
the value `finalValue`. After each execution of the statement list
the value of the loop variable is increased by the `increment` value
(or by 1, if `increment` is not defined, i.e., `by` is false,
see p. 12-19).
The `increment` may be negative.
			

Note: depending on the initial and final values, the statement list 
may not be executed one single time.
			

Note: if a CONTINUE statement (`SclContinueStatement`) is executed
within the loop, it will also increment the loop variable. (p. 12-23)
			

Note: the execution of the loop may also be terminated by a CONTINUE 
(`SclContinueStatement`), EXIT (`SclExitStatement`) or
GOTO (`SclGotoStatement`) statement.
			

- **Validation:**
   * [INVALID_FOR_STATEMENT] The `initialStatement`'s `leftValue` shall be a variable of type INT or DINT (p. 12-18)
   * [INVALID_FOR_STATEMENT] The `initialStatement`'s `rightValue` shall be a basic expression
   * [INVALID_FOR_STATEMENT] The `finalValue` shall be a basic expression (p. 15-53)
   * [INVALID_FOR_STATEMENT] The `increment` shall be a basic expression (p. 15-53)

**Refers to:**
- [Expression](#Expression)
- [SclAssignmentStatement](#SclAssignmentStatement)
- [SclStatementList](#SclStatementList)

**Referred by:**
- [SclStatement](#SclStatement)


```
SclForStatement:
	'FOR' initialStatement=SclAssignmentStatement 'TO' finalValue=Expression
	(by?='BY' increment=Expression)? 'DO'
		statements=SclStatementList
	'END_FOR';
```



####  SclWhileStatement  {#SclWhileStatement}
An **SCL WHILE loop statement** defines the execution of the contained
statement list multiple times, while the given _entry condition_
is satisfied (evaluated to true).
			

The statement list may not be executed if the entry condition is 
false before the first execution.
			

Note: the execution of the loop may also be terminated by a CONTINUE 
(`SclContinueStatement`), EXIT (`SclExitStatement`) or
GOTO (`SclGotoStatement`) statement.
			


**Refers to:**
- [Expression](#Expression)
- [SclStatementList](#SclStatementList)

**Referred by:**
- [SclStatement](#SclStatement)


```
SclWhileStatement:
	'WHILE' entryCondition=Expression 'DO'
		statements=SclStatementList
	'END_WHILE';
```



####  SclRepeatStatement  {#SclRepeatStatement}
An **SCL REPEAT loop statement** defines the execution of 
the contained statement list multiple times, until the given
_termination condition_ (or exit condition, break condition)
becomes satisfied (evaluated to true).
			

The statement list will be executed at least once.
			

Note: the execution of the loop may also be terminated by a CONTINUE 
(`SclContinueStatement`), EXIT (`SclExitStatement`) or
GOTO (`SclGotoStatement`) statement.
			


**Refers to:**
- [Expression](#Expression)
- [SclStatementList](#SclStatementList)

**Referred by:**
- [SclStatement](#SclStatement)


```
SclRepeatStatement:
	'REPEAT' 
		statements=SclStatementList
	'UNTIL' exitCondition=Expression 'END_REPEAT';
```



####  SclGotoStatement  {#SclGotoStatement}
An **SCL GOTO statement** (jump) interrupts the execution at the given
location and continues at another location, defined by a label.
			

- **Validation:**
   * The destination of the jump must be in the same block. (p. 12-25) (Ensured by scoping.)
   * [INVALID\_GOTO\_STATEMENT] It is not possible to jump into a loop (but it is possible to leave a loop by a jump). (p. 12-25)

**Refers to:**
- [IdLike](#IdLike)

**Referred by:**
- [SclStatement](#SclStatement)


```
SclGotoStatement:
	'GOTO' label=[Label|IdLike];
```



####  AbstractExpression  {#AbstractExpression}
An **abstract expression** is either an 
[expression tree node](#Expression), or a
constant (named constant or literal).
			

This rule is not used in the grammar, included only
to generate a common ancestor for these two types.


**Refers to:**
- [Expression](#Expression)
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)



```
AbstractExpression: 	Expression | NamedOrUnnamedConstantRef;
```



####  Expression  {#Expression}
An **expression tree node** is either a node describing a hierarchy of 
operations (via [ImpliesExpression](#ImpliesExpression)), or a standalone expression that 
cannot be contained in an expression tree.


**Refers to:**
- [ImpliesExpression](#ImpliesExpression)
- [StandaloneExpression](#StandaloneExpression)

**Referred by:**
- [AbstractExpression](#AbstractExpression)
- [ArrayDimension](#ArrayDimension)
- [CallParameter](#CallParameter)
- [CallParameterItem](#CallParameterItem)
- [DirectRef](#DirectRef)
- [ElsifBlock](#ElsifBlock)
- [NamedConstantDeclaration](#NamedConstantDeclaration)
- [PrimaryExpression](#PrimaryExpression)
- [SclAssignmentStatement](#SclAssignmentStatement)
- [SclCaseStatement](#SclCaseStatement)
- [SclForStatement](#SclForStatement)
- [SclIfStatement](#SclIfStatement)
- [SclRepeatStatement](#SclRepeatStatement)
- [SclVerificationAssertion](#SclVerificationAssertion)
- [SclWhileStatement](#SclWhileStatement)
- [StlVerificationAssertion](#StlVerificationAssertion)
- [StringDT](#StringDT)
- [UnqualifiedRef](#UnqualifiedRef)


```
Expression:
	ImpliesExpression | StandaloneExpression;
```



####  StandaloneExpression  {#StandaloneExpression}
A **standalone expression** is an expression that cannot be contained 
in an expression tree.
			

Standalone expressions are timer and counter constants and the special 
`NIL` value.
They cannot be part of any other expressions.


**Refers to:**
- [Step7CounterConst](#Step7CounterConst)
- [Step7TimerConst](#Step7TimerConst)

**Referred by:**
- [Expression](#Expression)


```
StandaloneExpression returns Expression:
	{TimerConstant} value=Step7TimerConst |
	{CounterConstant} value=Step7CounterConst |
	{NilValue} 'NIL';
```



####  ImpliesExpression  {#ImpliesExpression}
**Implication expression**.
			

The implication operation is not part of the STEP 7 languages, included only 
to facilitate the description of verification assertions.
			

Implication is not associative, thus parentheses shall explicitly be used.

- **Validation:**
   * [INVALID\_USE\_OF\_IMPLICATION] Implication is only permitted in verification assertions.
- **Examples:**
   * `a --> b` (and `a --> b --> c` would be incorrect)

**Refers to:**
- [OrExpression](#OrExpression)

**Referred by:**
- [Expression](#Expression)


```
ImpliesExpression returns Expression:
	OrExpression ({ImpliesExpression.left=current} '-->' right=OrExpression)?;
```



####  OrExpression  {#OrExpression}


**Refers to:**
- [XorExpression](#XorExpression)

**Referred by:**
- [ImpliesExpression](#ImpliesExpression)


```
OrExpression returns Expression:
	XorExpression ({OrExpression.left=current} 'OR' right=XorExpression)*;
```



####  XorExpression  {#XorExpression}


**Refers to:**
- [AndExpression](#AndExpression)

**Referred by:**
- [OrExpression](#OrExpression)


```
XorExpression returns Expression:
	AndExpression ({XorExpression.left=current} 'XOR' right=AndExpression)*;
```



####  AndExpression  {#AndExpression}


**Refers to:**
- [EqualityExpression](#EqualityExpression)

**Referred by:**
- [XorExpression](#XorExpression)


```
AndExpression returns Expression:
	EqualityExpression ({AndExpression.left=current} opString=('&' | 'AND') right=EqualityExpression)*;
```



####  EqualityExpression  {#EqualityExpression}


**Refers to:**
- [ComparisonExpression](#ComparisonExpression)
- [EqualityOperator](#EqualityOperator)

**Referred by:**
- [AndExpression](#AndExpression)


```
EqualityExpression returns Expression:
	ComparisonExpression ({EqualityExpression.left=current} operator=EqualityOperator right=ComparisonExpression)?;
```



####  ComparisonExpression  {#ComparisonExpression}


**Refers to:**
- [AdditiveExpression](#AdditiveExpression)
- [ComparisonOperator](#ComparisonOperator)

**Referred by:**
- [EqualityExpression](#EqualityExpression)


```
ComparisonExpression returns Expression:
	AdditiveExpression ({ComparisonExpression.left=current} operator=ComparisonOperator right=AdditiveExpression)?;
```



####  AdditiveExpression  {#AdditiveExpression}


**Refers to:**
- [AdditionOperator](#AdditionOperator)
- [MultiplicativeExpression](#MultiplicativeExpression)

**Referred by:**
- [ComparisonExpression](#ComparisonExpression)


```
AdditiveExpression returns Expression:
	MultiplicativeExpression ({AdditiveExpression.left=current} operator=AdditionOperator right=MultiplicativeExpression)*;
```



####  MultiplicativeExpression  {#MultiplicativeExpression}


**Refers to:**
- [MultiplicationOperator](#MultiplicationOperator)
- [PowerExpression](#PowerExpression)

**Referred by:**
- [AdditiveExpression](#AdditiveExpression)


```
MultiplicativeExpression returns Expression:
	PowerExpression ({MultiplicativeExpression.left=current} operator=MultiplicationOperator right=PowerExpression)*;
```



####  PowerExpression  {#PowerExpression}


**Refers to:**
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)

**Referred by:**
- [MultiplicativeExpression](#MultiplicativeExpression)


```
PowerExpression returns Expression:
	UnaryOrPrimaryExpression ({PowerExpression.left=current} '**' right=UnaryOrPrimaryExpression)?;
```



####  UnaryOrPrimaryExpression  {#UnaryOrPrimaryExpression}


**Refers to:**
- [PrimaryExpression](#PrimaryExpression)
- [UnaryExpression](#UnaryExpression)

**Referred by:**
- [PowerExpression](#PowerExpression)
- [StlUnaryStatement](#StlUnaryStatement)
- [UnaryExpression](#UnaryExpression)


```
UnaryOrPrimaryExpression returns Expression:
	PrimaryExpression | UnaryExpression;
```



####  UnaryExpression  {#UnaryExpression}


**Refers to:**
- [UnaryOperator](#UnaryOperator)
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)

**Referred by:**
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)


```
UnaryExpression returns Expression:
	{UnaryExpression} op=UnaryOperator expr=UnaryOrPrimaryExpression;
```



####  EqualityOperator (enum) {#EqualityOperator}
**Equality operators**.


Literals:
- EQUALS (`=`)
- NOT_EQUALS (`<>`)

```
enum EqualityOperator:
	EQUALS = '=' | NOT_EQUALS = '<>';
```



####  ComparisonOperator (enum) {#ComparisonOperator}
**Comparison operators**.


Literals:
- GREATER_EQ (`>=`)
- GREATER_THAN (`>`)
- LESS_EQ (`<=`)
- LESS_THAN (`<`)

```
enum ComparisonOperator:
	LESS_THAN = '<' | GREATER_THAN = '>' | LESS_EQ = '<=' | GREATER_EQ = '>=';
```



####  AdditionOperator (enum) {#AdditionOperator}
**Addition operators**.


Literals:
- MINUS (`-`)
- PLUS (`+`)

```
enum AdditionOperator:
	PLUS = '+' | MINUS = '-';
```



####  MultiplicationOperator (enum) {#MultiplicationOperator}
**Multiplication and division operators**.


Literals:
- DIVISION (`/`)
- INTEGER_DIVISION (`DIV`)
- MODULO (`MOD`)
- MULTIPLICATION (`*`)

```
enum MultiplicationOperator:
	MULTIPLICATION = '*' | DIVISION = '/' | MODULO = 'MOD' | INTEGER_DIVISION = 'DIV';
```



####  UnaryOperator (enum) {#UnaryOperator}
**Unary (logic and arithmetic) operators**.


Literals:
- MINUS (`-`)
- NOT (`NOT`)
- PLUS (`+`)

```
enum UnaryOperator:
	NOT = 'NOT' | PLUS = '+' | MINUS = '-';
```



####  PrimaryExpression  {#PrimaryExpression}
A **primary expression** can represent:

  - An unnamed constant (literal) ([UnnamedConstant](#UnnamedConstant)),
  - Left value ([LeftValue](#LeftValue)) that can be assigned,
  - An expression in parentheses ([Expression](#Expression)),
  - An SCL call ([SclSubroutineCall](#SclSubroutineCall)).
 
			

- **Validation:**
   * [part of typing rules] If the primary expression is a SclSubroutineCall, it should only represent a call of a Function, with data type other than VOID

**Refers to:**
- [Expression](#Expression)
- [LeftValue](#LeftValue)
- [SclSubroutineCall](#SclSubroutineCall)
- [UnnamedConstant](#UnnamedConstant)

**Referred by:**
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)


```
PrimaryExpression returns Expression:
	UnnamedConstant | LeftValue | '(' Expression ')' | SclSubroutineCall;
```



####  LeftValue  {#LeftValue}
A **left value** is a data element that can be assigned.
It can be a named value reference (qualified or unqualified name optionally
with indexing), or an absolute address.
			

Any left value may be a right-value or a primary expression too.
			

Note: As it is not possible to differentiate at the grammar level between
a variable and a named constant, a named constant is also of type 
`LeftValue`, however, it shall not be assigned.

- **Validation:**
   * If `LeftValue` is used on the left-hand side of an assignment,
   the `NamedValueRef` should not point to a named constant. See 
   {@link }

**Refers to:**
- [MemoryAddress](#MemoryAddress)
- [NamedValueRef](#NamedValueRef)

**Referred by:**
- [DataBlockInitialAssignment](#DataBlockInitialAssignment)
- [PrimaryExpression](#PrimaryExpression)
- [SclAssignmentStatement](#SclAssignmentStatement)


```
LeftValue:
	NamedValueRef | MemoryAddress;
```



####  NamedValueRef  {#NamedValueRef}
A **named value reference** is a qualified or unqualified 
reference to a data element, optionally with indexing.
It can also be a flag (such as EN, ENO, OK), or an address 
within a data block.

- **Examples:**
   * `var1`
   * `datablock1.var1`
   * `datablock1.var1[0, 7]`
   * `datablock1.DB1`
   * `ENO`

**Referred by:**
- [LeftValue](#LeftValue)


```
NamedValueRef: QualifiedRef;
```



####  QualifiedRef  {#QualifiedRef}
A **qualified reference** is a hierarchy of consecutive unqualified
references, separated by `.`s.

- **Examples:**
   * `dataBlock1.struct2.var3`

**Refers to:**
- [UnqualifiedRef](#UnqualifiedRef)



```
QualifiedRef returns NamedValueRef:
	UnqualifiedRef ({QualifiedRef.prefix=current} '.' ref=UnqualifiedRef)*;
```



####  UnqualifiedRef  {#UnqualifiedRef}
An **unqualified reference** is a [direct reference](#DirectRef),
or an array reference (`ArrayRef`) that is a direct reference together
with a list of indices.
			

- **Validation:**
   * [INVALID_ARRAYREF_INDEX] The `index` should match the dimensions and the ranges of the array.
- **Examples:**
   * `var1`
   * `var1[12]`
   * `var1[N, N+1]`

**Refers to:**
- [DirectRef](#DirectRef)
- [Expression](#Expression)

**Referred by:**
- [QualifiedRef](#QualifiedRef)


```
UnqualifiedRef returns NamedValueRef:
	DirectRef ({ArrayRef.ref=current} '[' index+=Expression (',' index+=Expression)* ']')*;
```



####  DirectRef  {#DirectRef}
A **direct reference** is a reference to a data element, which can be
also part of a qualified reference.
			

A direct reference is either:

  - A **direct named reference** ([DirectNamedRef](#DirectNamedRef)), a reference
to a variable or a named constant;
  - A **datablock field address reference** (`DbFieldAddressRef`),
that is the address of a field in a data block;
  - A **datablock field indexed reference**  (`DbFieldIndexedRef`),
that is an indexed address of a field in a data block;
  - A **flag** ([FlagRef](#FlagRef)); or
  - A **status word bit reference** ([StwBitRef](#StwBitRef)).

- **Validation:**
   * A named constant should not be qualified.
   * A `DbFieldAddressRef` shall not be a prefix in `QualifiedRef`.
- **Examples:**
   * `#var1` (parsed as `DirectNamedRef`)
   * `DW12` (parsed as `DbFieldAddressRef`)
   * `DW[12]` (parsed as `DbFieldIndexedRef`)
   * `DW[N + 1]` (parsed as `DbFieldIndexedRef`)
   * `OK` (parsed as `FlagRef`)

**Refers to:**
- [DatablockFieldAddress](#DatablockFieldAddress)
- [DirectNamedRef](#DirectNamedRef)
- [Expression](#Expression)
- [FlagRef](#FlagRef)
- [S7DatablockFieldIndexedPrefix](#S7DatablockFieldIndexedPrefix)
- [StwBitRef](#StwBitRef)

**Referred by:**
- [UnqualifiedRef](#UnqualifiedRef)


```
DirectRef:
	DirectNamedRef | 	{DbFieldAddressRef} field=DatablockFieldAddress |
	{DbFieldIndexedRef} fieldSize=S7DatablockFieldIndexedPrefix expr=Expression ']' |
	FlagRef |
	StwBitRef;
```



####  DirectNamedRef  {#DirectNamedRef}
MISSING\_DOCS


**Refers to:**
- [IdOrSymbol](#IdOrSymbol)

**Referred by:**
- [DirectRef](#DirectRef)


```
DirectNamedRef:
	ref=[NamedElement|IdOrSymbol];
```



####  FlagRef  {#FlagRef}
**Flag**.

  - An ENO flag (`EnoFlag`) is a special output parameter of blocks.
  - OK flag (`OkValRef`).
  - RET_VAL reference, referring to the return value of the given function.

Note:  EN cannot be used as expression, only as special call parameter. See p. 12-41. (But ENO can: p. 12-42, also OK: p. 8-9)

- **Validation:**
   * [ILLEGAL_SCL_ELEMENT] `RET_VAL` shall only be used in functions implemented in STL.

**Referred by:**
- [DirectRef](#DirectRef)


```
FlagRef: 
		{EnoFlag} 'ENO' | 	{OkValRef} 'OK' | 
	{RetValRef} ('RET_VAL'|'#RET_VAL');
```



####  StwBitRef  {#StwBitRef}
STL **status word bit reference**. The exact referred bit is determined 
by the `mnemonic`.

- **Validation:**
   * [ILLEGAL\_SCL\_ELEMENT] These bit references can only be used in STL language.
   * [INVALID\_STL\_ARGUMENT] These bit references are read-only.

**Refers to:**
- [StwBitRefMnemonic](#StwBitRefMnemonic)

**Referred by:**
- [DirectRef](#DirectRef)


```
StwBitRef: 
	mnemonic=StwBitRefMnemonic;
```



####  StwBitRefMnemonic (enum) {#StwBitRefMnemonic}


Literals:
- BIT_RESULT (`BR`)
- OVERFLOW (`OV`)
- OVERFLOW_STORED (`OS`)

```
enum StwBitRefMnemonic:
	OVERFLOW = 'OV' |
	OVERFLOW_STORED = 'OS' |
	BIT_RESULT = 'BR';
```



####  SclSubroutineCall  {#SclSubroutineCall}
An **SCL call** (to function or function block instance) executes an 
program unit with the given parameters.
It represents a call of a function or function block instance. In case of a 
function block call, either the `calledUnit` is an instance 
declaration (of type [Variable](#Variable)), or a function block, with an 
explicit data block defined (`hasExplicitDataBlock` is true and 
`dataBlock` is a data block of type `calledUnit`).
			

Note: in case of FB calls, input and in-out parameter assignments are optional (pp. 12-30--12-31), output parameter assignments are forbidden
Note: in case of FC calls, all input, in-out and output (?) parameters have to be assigned (pp. 12-38--12-39)
			

This rule represents the same contents as the [StlSubroutineCall](#StlSubroutineCall), 
but with slightly different syntax.

- **Validation:**
   * See at 'SubroutineCall'.

**Refers to:**
- [CallParameter](#CallParameter)
- [IdOrSymbol](#IdOrSymbol)

**Referred by:**
- [PrimaryExpression](#PrimaryExpression)
- [SclStatement](#SclStatement)
- [SubroutineCall](#SubroutineCall)


```
SclSubroutineCall:
	calledUnit=[NamedElement|IdOrSymbol] (hasExplicitDataBlock?='.' dataBlock=[DataBlock|IdOrSymbol])? '('
		(callParameter=CallParameter)?
	')';
```



####  CallParameter  {#CallParameter}
A **call parameter** is the definition of the paremeters to be used in a 
STEP 7 (SCL or STL) call.
			

It can be a `SingleCallParameter` if the called function has one 
single input parameter, thus it is not necessary to pass the parameters by
name. It can also be a `CallParameterList` that is a list of one or 
more [call parameter items](#CallParameterItem).

- **Validation:**
   * [INVALID_CALL_PARAMETERS] `SingleCallParameter` is valid only if the called unit has exactly one variable defined in `VAR_INPUT`
   * [INVALID_CALL_PARAMETERS] `SingleCallParameter` is valid only if the called unit is a `Function` (pp. 15-50--15-51)
   * [INVALID\_CALL\_PARAMETERS] It is forbidden to assign a parameter variable multiple times.
- **Examples:**
   * `1`
   * `IN1:=1, IN2:=TRUE`

**Refers to:**
- [CallParameterItem](#CallParameterItem)
- [Expression](#Expression)

**Referred by:**
- [SclSubroutineCall](#SclSubroutineCall)
- [StlSubroutineCall](#StlSubroutineCall)


```
CallParameter:
	{SingleCallParameter} parameter=Expression |
	{CallParameterList} parameters+=CallParameterItem (',' parameters+=CallParameterItem)*;
```



####  CallParameterItem  {#CallParameterItem}
A **call parameter item** is a definition of one of the (implicit or 
explicit) parameters of a call, with their identifier (i.e., not an unnamed
single call parameter definition).
It can be: 

  - A named call parameter item (`NamedCallParameterItem`), that 
associates a value to one of the input parameters of the called 
program unit (in case of input parameter, the value of `value` 
will be assigned to `parameter`; in case of output parameter, 
the value of `parameter` will be assigned to `value`);
  - An EN call parameter item (`EnCallParameterItem`), that assigns
value to the implicit input parameter `EN` of the called program unit;
  - A return value parameter item (`RetValCallParameterItem`), that 
assigns the return value of the called function to the given
`parameter` after the call.

- **Validation:**
   * [INVALID\_FB\_OUTPUT\_ASSIGNMENT] In a FunctionBlock's call the parameter should not refer to variables defined in the VAR\_OUT block (p. 15-51)
   * [INVALID_OUTPUT_PARAMETER] `value` shall be an extended (i.e., assignable) variable if `parameter` is defined in `VAR_IN_OUT` or `VAR_OUT` (p. 15-51) (named or unnamed constants are not allowed)
   * [ILLEGAL_SCL_ELEMENT] `RetValCallParameterItem` is only permitted in STL, not in SCL.

**Refers to:**
- [AssignmentOperator](#AssignmentOperator)
- [Expression](#Expression)
- [IdLike](#IdLike)

**Referred by:**
- [CallParameter](#CallParameter)


```
CallParameterItem: 
	{NamedCallParameterItem} parameter=[Variable|IdLike] assignmentOperator=AssignmentOperator value=Expression |
	{EnCallParameterItem} 'EN' assignmentOperator=AssignmentOperator value=Expression | 
	{RetValCallParameterItem} ('RET_VAL'|'#RET_VAL') assignmentOperator=AssignmentOperator value=Expression;
```



####  AssignmentOperator (enum) {#AssignmentOperator}
**Call parameter assignment operator**.

- **Validation:**
   * [INVALID\_CALL\_ASSIGNMENT\_OPERATOR] The use of FC\_OUTPUT is only permitted for Function call's VAR\_OUT assignments (p. 15-51) (but even in this case it is not mandatory, can be replaced with GENERAL)
   * FC\_INOUT is not supported by old STEP 7.

Literals:
- FC_INOUT (`:=>`)
- FC_OUTPUT (`=>`)
- GENERAL (`:=`)

```
enum AssignmentOperator:
	GENERAL = ':=' | FC_OUTPUT = '=>' | FC_INOUT = ':=>';
```



####  MemoryAddress  {#MemoryAddress}
An **absolute memory address** is a direct addressing of a bit, byte, word or 
double word in one of the memory areas.
			

If a single bit is addressed, `BitMemoryAddress` will be used, otherwise
`LongMemoryAddress`.
			

- **Validation:**
   * In the new (Tia Portal) versions it is mandatory to start the memory addresses with '%' ('%' is optional in old STEP 7).
   * [ILLEGAL_SCL_ELEMENT] `L` prefix is only permitted in STL.
   * [ILLEGAL\_SCL\_ELEMENT] Whitespace in the MemoryAddress is only permitted in STL.
- **Examples:**
   * `M0.1`
   * `MB12`

**Refers to:**
- [BitMemoryAddressString](#BitMemoryAddressString)
- [LongMemoryAddressString](#LongMemoryAddressString)
- [StlBitMemoryAddressString](#StlBitMemoryAddressString)
- [StlLongMemoryAddressString](#StlLongMemoryAddressString)

**Referred by:**
- [LeftValue](#LeftValue)


```
MemoryAddress:
	{BitMemoryAddress} value=(BitMemoryAddressString|StlBitMemoryAddressString) | 
	{LongMemoryAddress} value=(LongMemoryAddressString|StlLongMemoryAddressString);
```



####  BitMemoryAddressString  {#BitMemoryAddressString}
A STEP 7 **absolute address to a bit**.

- **Examples:**
   * `M0.1`

**Referred by:**
- [MemoryAddress](#MemoryAddress)

**Returns:** `dt::S7MemoryAddress`

```
BitMemoryAddressString returns dt::S7MemoryAddress:
	BIT_MEMORY_ADDRESS;
```



####  StlBitMemoryAddressString  {#StlBitMemoryAddressString}
A STEP 7 **absolute address to a bit**, in STL format (permitting whitespace 
inside).

- **Examples:**
   * `M   0.1`

**Refers to:**
- [BOOL_MEMORY_PREFIX](#BOOL_MEMORY_PREFIX)
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)

**Referred by:**
- [MemoryAddress](#MemoryAddress)

**Returns:** `dt::S7MemoryAddress`

```
StlBitMemoryAddressString returns dt::S7MemoryAddress:
		BOOL_MEMORY_PREFIX FIX_POINT_NUMBER;
```



####  LongMemoryAddressString  {#LongMemoryAddressString}
A STEP 7 **absolute address to a non-bit**, i.e., to a data area that is at 
least one byte long.

- **Examples:**
   * `MW0`

**Referred by:**
- [MemoryAddress](#MemoryAddress)

**Returns:** `dt::S7MemoryAddress`

```
LongMemoryAddressString returns dt::S7MemoryAddress:
	LONG_MEMORY_ADDRESS;
```



####  StlLongMemoryAddressString  {#StlLongMemoryAddressString}
A STEP 7 **absolute address to a non-bit** in STL format (permitting 
whitespace inside).

- **Examples:**
   * `MW   0`

**Refers to:**
- [INTSTR](#INTSTR)
- [LONG_MEMORY_PREFIX](#LONG_MEMORY_PREFIX)

**Referred by:**
- [MemoryAddress](#MemoryAddress)

**Returns:** `dt::S7MemoryAddress`

```
StlLongMemoryAddressString returns dt::S7MemoryAddress:
		LONG_MEMORY_PREFIX INTSTR;
```



####  BOOL_MEMORY_PREFIX (terminal) {#BOOL_MEMORY_PREFIX}
Absolute address prefix for bit addresses.

- **Examples:**
   * `I`
   * `IX`

**Refers to:**
- [ADDRESS_IDENTIFIER](#ADDRESS_IDENTIFIER)
- [BIT_DATA_ELEMENT_SIZE](#BIT_DATA_ELEMENT_SIZE)

**Referred by:**
- [BitIoPrefix](#BitIoPrefix)
- [StlBitMemoryAddressString](#StlBitMemoryAddressString)

```
terminal BOOL_MEMORY_PREFIX:
	ADDRESS_IDENTIFIER BIT_DATA_ELEMENT_SIZE?;
```



####  LONG_MEMORY_PREFIX (terminal) {#LONG_MEMORY_PREFIX}
Absolute address prefix for non-Boolean addresses (addressing memory area 
that is at least one byte long).

- **Examples:**
   * `IB`
   * `MW`

**Refers to:**
- [ADDRESS_IDENTIFIER](#ADDRESS_IDENTIFIER)
- [NONBIT_DATA_ELEMENT_SIZE](#NONBIT_DATA_ELEMENT_SIZE)

**Referred by:**
- [BitIoPrefix](#BitIoPrefix)
- [StlLongMemoryAddressString](#StlLongMemoryAddressString)

```
terminal LONG_MEMORY_PREFIX:
	ADDRESS_IDENTIFIER NONBIT_DATA_ELEMENT_SIZE;
```



####  BIT_MEMORY_ADDRESS (terminal) {#BIT_MEMORY_ADDRESS}
Absolute address to a bit.

- **Examples:**
   * `M0.0`

**Refers to:**
- [BOOL_MEMORY_PREFIX](#BOOL_MEMORY_PREFIX)
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)


```
terminal BIT_MEMORY_ADDRESS:
	'%'? BOOL_MEMORY_PREFIX FIX_POINT_NUMBER;
```



####  LONG_MEMORY_ADDRESS (terminal) {#LONG_MEMORY_ADDRESS}
Absolute address to a memory area at least one byte long.

- **Examples:**
   * `%MW12`

**Refers to:**
- [INTSTR](#INTSTR)
- [LONG_MEMORY_PREFIX](#LONG_MEMORY_PREFIX)


```
terminal LONG_MEMORY_ADDRESS:
	'%'? LONG_MEMORY_PREFIX INTSTR;
```



####  ADDRESS_IDENTIFIER (terminal fragment) {#ADDRESS_IDENTIFIER}
Memory area identified for absolute addresses.

- **Validation:**
   * [ILLEGAL_SCL_ELEMENT] The prefix `L` is only permitted in STL.
- **Examples:**
   * `I`
   * `m`


```
terminal fragment ADDRESS_IDENTIFIER:
	'I' | 'i' |
	'Q' | 'q' |
	'M' | 'm' |
	'L' | 'l' |
	('P'|'p')('Q'|'q') | 
	('P'|'p')('I'|'i');
```



####  NONBIT_DATA_ELEMENT_SIZE (terminal fragment) {#NONBIT_DATA_ELEMENT_SIZE}
Non-Boolean memory element size identifier.
It can be `B` for a byte, `W` for word, or `D` for double
word. Case insensitive.



```
terminal fragment NONBIT_DATA_ELEMENT_SIZE:
	'B' | 'b' |
	'D' | 'd' |
	'W' | 'w';
```



####  BIT_DATA_ELEMENT_SIZE (terminal fragment) {#BIT_DATA_ELEMENT_SIZE}
Bit memory element size identifier.
It can only be `X`. Case insensitive.



```
terminal fragment BIT_DATA_ELEMENT_SIZE:
	'X' | 'x';
```



####  DatablockFieldAddress  {#DatablockFieldAddress}


**Referred by:**
- [DirectRef](#DirectRef)

**Returns:** `dt::S7DatablockFieldAddress`

```
DatablockFieldAddress returns dt::S7DatablockFieldAddress:
	DATABLOCK_FIELD_ADDRESS;
```



####  DATABLOCK_FIELD_ADDRESS (terminal) {#DATABLOCK_FIELD_ADDRESS}


**Refers to:**
- [BIT_DATA_ELEMENT_SIZE](#BIT_DATA_ELEMENT_SIZE)
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [INTSTR](#INTSTR)
- [NONBIT_DATA_ELEMENT_SIZE](#NONBIT_DATA_ELEMENT_SIZE)

**Referred by:**
- [DataBlock](#DataBlock)

```
terminal DATABLOCK_FIELD_ADDRESS: 	('D'|'d')('B'|'b') NONBIT_DATA_ELEMENT_SIZE INTSTR | 
	('D'|'d')('D'|'W'|'d'|'w') INTSTR | 	('D'|'d') BIT_DATA_ELEMENT_SIZE? FIX_POINT_NUMBER | 	('D'|'d')('B'|'b') BIT_DATA_ELEMENT_SIZE FIX_POINT_NUMBER;
```



####  S7DatablockFieldIndexedPrefix  {#S7DatablockFieldIndexedPrefix}


**Referred by:**
- [DirectRef](#DirectRef)

**Returns:** `dt::S7DatablockFieldPrefix`

```
S7DatablockFieldIndexedPrefix returns dt::S7DatablockFieldPrefix:
	DATABLOCK_FIELD_INDEXED_PREFIX;
```



####  DATABLOCK_FIELD_INDEXED_PREFIX (terminal) {#DATABLOCK_FIELD_INDEXED_PREFIX}


**Refers to:**
- [NONBIT_DATA_ELEMENT_SIZE](#NONBIT_DATA_ELEMENT_SIZE)


```
terminal DATABLOCK_FIELD_INDEXED_PREFIX:
	('D'|'d') NONBIT_DATA_ELEMENT_SIZE '[';
```



####  UnnamedConstant  {#UnnamedConstant}
A STEP 7 **typed literal** (STEP 7 terminology: unnamed constant).
It can be:

  - A Boolean literal (`BoolConstant` having a [Step7BoolConst](#Step7BoolConst) value);
  - An integer literal (`IntConstant` having a [Step7IntConst](#Step7IntConst) value);
  - A floating-point (real value) literal (`RealConstant` having a [RealConstant](#RealConstant) value);
  - A character literal (`CharConstant` having a [Step7CharacterConst](#Step7CharacterConst) value);
  - A date literal (`DateConstant` having a [Step7DateConst](#Step7DateConst) value);
  - A date and time literal (`DateAndTimeConstant` having a [Step7DateAndTimeConst](#Step7DateAndTimeConst) value);
  - A time of day literal (`TimeOfDayConstant` having a [Step7TimeOfDayConst](#Step7TimeOfDayConst) value);
  - A time period literal (`TimeConstant` having a [Step7TimePeriodConst](#Step7TimePeriodConst) value);
  - A character string literal (`StringConstant` having a [Step7StringConst](#Step7StringConst) value).
 
			

Note: Named constants are included in [QualifiedRef](#QualifiedRef).


**Refers to:**
- [Step7BoolConst](#Step7BoolConst)
- [Step7CharacterConst](#Step7CharacterConst)
- [Step7DateAndTimeConst](#Step7DateAndTimeConst)
- [Step7DateConst](#Step7DateConst)
- [Step7IntConst](#Step7IntConst)
- [Step7RealConst](#Step7RealConst)
- [Step7StringConst](#Step7StringConst)
- [Step7TimeOfDayConst](#Step7TimeOfDayConst)
- [Step7TimePeriodConst](#Step7TimePeriodConst)

**Referred by:**
- [NegatedUnnamedConstantRef](#NegatedUnnamedConstantRef)
- [PrimaryExpression](#PrimaryExpression)
- [UnnamedConstantRef](#UnnamedConstantRef)


```
UnnamedConstant:
	{BoolConstant} value=Step7BoolConst |
	{IntConstant} value=Step7IntConst |
	{RealConstant} value=Step7RealConst |
	{CharConstant} value=Step7CharacterConst | 
	{DateConstant} value=Step7DateConst |
	{DateAndTimeConstant} value=Step7DateAndTimeConst | 
	{TimeOfDayConstant} value=Step7TimeOfDayConst |
	{TimeConstant} value=Step7TimePeriodConst |
	{StringConstant} value=Step7StringConst;
```



####  NamedOrUnnamedConstantRef  {#NamedOrUnnamedConstantRef}
A **constant reference** is either a reference to a named constant, 
or a literal (unnamed constant). 

  - _Named constant references_ are represented as `NamedConstantRef`,
referring to a [NamedConstantDeclaration](#NamedConstantDeclaration) in `ref`.
  - _Literal references_ (unnamed constant references) are represented as 
`UnnamedConstantRef`, referring to a [UnnamedConstant](#UnnamedConstant) in 
`constant`.
  - _Negated literals_ are represented as `NegatedUnnamedConstantRef`, 
referring to a [UnnamedConstant](#UnnamedConstant) in `negatedConstant`. This
is a workaround to allow using negated literals where expressions (thus 
unary expressions) are not permitted.


**Refers to:**
- [IdLike](#IdLike)
- [NegatedUnnamedConstantRef](#NegatedUnnamedConstantRef)
- [UnnamedConstantRef](#UnnamedConstantRef)

**Referred by:**
- [AbstractExpression](#AbstractExpression)
- [ArrayInitializationElement](#ArrayInitializationElement)
- [CaseElementValue](#CaseElementValue)
- [DataBlockInitialAssignment](#DataBlockInitialAssignment)
- [StlShiftRotateStatement](#StlShiftRotateStatement)
- [StlUnaryConstantStatement](#StlUnaryConstantStatement)


```
NamedOrUnnamedConstantRef:
	{NamedConstantRef} ref=[NamedConstantDeclaration|IdLike] |
	UnnamedConstantRef | 
	NegatedUnnamedConstantRef;
```



####  UnnamedConstantRef  {#UnnamedConstantRef}
Wrapper for an [UnnamedConstant](#UnnamedConstant).


**Refers to:**
- [UnnamedConstant](#UnnamedConstant)

**Referred by:**
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)
- [StlUnaryConstantStatement](#StlUnaryConstantStatement)


```
UnnamedConstantRef:
	{UnnamedConstantRef} constant=UnnamedConstant;
```



####  NegatedUnnamedConstantRef  {#NegatedUnnamedConstantRef}
Wrapper for an [UnnamedConstant](#UnnamedConstant) that is negated. This
is a workaround to allow using negated literals where expressions (thus 
unary expressions) are not permitted.


**Refers to:**
- [UnnamedConstant](#UnnamedConstant)

**Referred by:**
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)
- [StlUnaryConstantStatement](#StlUnaryConstantStatement)


```
NegatedUnnamedConstantRef:
	{NegatedUnnamedConstantRef} '-' negatedConstant=UnnamedConstant;
```



####  Step7BoolConst  {#Step7BoolConst}
Optionally prefixed **Boolean literal**, defined by the text
_false_ or _true_. Case insensitive.
			

The literal `BOOL#1` and `1` are also valid Boolean literals,
but as it cannot be distinguished properly by the parser, they will be 
parsed as [integer literals](#Step7IntConst) and then handled separately
later.


**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7Bool`

```
Step7BoolConst returns dt::S7Bool:
	'BOOL#'? ('FALSE' | 'TRUE');
```



####  IntegerLiteral  {#IntegerLiteral}



**Returns:** `ecore::EInt`

```
IntegerLiteral returns ecore::EInt:
	INTSTR;
```



####  Step7IntConst  {#Step7IntConst}
STEP 7 **integer or word literal**.
It also parses Boolean literals defined using binary digits.
This literal may be prefixed.

- **Examples:**
   * `10`
   * `INT#10`

**Refers to:**
- [INTSTR](#INTSTR)
- [PREFIXED_BIT_OR_INT_CONST](#PREFIXED_BIT_OR_INT_CONST)

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7Integer`

```
Step7IntConst returns dt::S7Integer: 	INTSTR | PREFIXED_BIT_OR_INT_CONST;
```



####  PREFIXED_BIT_OR_INT_CONST (terminal) {#PREFIXED_BIT_OR_INT_CONST}
Prefixed integer or bit constant. The prefix can be either a type prefix (e.g., `INT#`),
a number base prefix (e.g., `2#`) or both. 
Note: SIGN cannot be the first item, this case is covered by [UnaryExpression](#UnaryExpression) or 
[NegatedUnnamedConstantRef](#NegatedUnnamedConstantRef).

- **Examples:**
   * `INT#123`
   * `INT#-123`
   * `INT#16#1FF`
   * `16#1FF`
   * `INT#-16#1FF` (checked with SCL 5.6 compiler)

**Refers to:**
- [BINARY_DIGIT_STRING](#BINARY_DIGIT_STRING)
- [CONST_TYPE_PREFIX](#CONST_TYPE_PREFIX)
- [HEXA_DIGIT_STRING](#HEXA_DIGIT_STRING)
- [INTSTR](#INTSTR)
- [OCTAL_DIGIT_STRING](#OCTAL_DIGIT_STRING)
- [SIGN](#SIGN)

**Referred by:**
- [Step7IntConst](#Step7IntConst)

```
terminal PREFIXED_BIT_OR_INT_CONST returns ecore::EString:
	(CONST_TYPE_PREFIX  SIGN? (INTSTR | BINARY_DIGIT_STRING | OCTAL_DIGIT_STRING | HEXA_DIGIT_STRING)) | 
	((BINARY_DIGIT_STRING | OCTAL_DIGIT_STRING | HEXA_DIGIT_STRING));
```



####  CONST_TYPE_PREFIX (terminal fragment) {#CONST_TYPE_PREFIX}
**Prefix of Boolean, integer and word literals**. Case insensitive.

- **Validation:**
   * [ILLEGAL_SCL_ELEMENT] The `L#` prefix is only permitted in STL.
- **Examples:**
   * `B#` (byte)
   * `Bool#` (Boolean)
   * `DINT#` (double integer, SCL)
   * `L#` (double integer, STL)
   * `WORD#` (16-bit word)


```
terminal fragment CONST_TYPE_PREFIX:
	'B#'|'b#'| 	('D'|'d')?('W'|'w')'#' | 	'L#' | 'l#' | 	('B'|'b')('O'|'o')('O'|'o')('L'|'l')'#' | 	('B'|'b')('Y'|'y')('T'|'t')('E'|'e')'#' | 	(('D'|'d')|('L'|'l'))?('W'|'w')('O'|'o')('R'|'r')('D'|'d')'#' | 	('U'|'u')?(('D'|'d')|('S'|'s')|('L'|'l'))?('I'|'i')('N'|'n')('T'|'t')'#';
```



####  BINARY_DIGIT_STRING (terminal fragment) {#BINARY_DIGIT_STRING}
**String of binary digits**.

- **Examples:**
   * `2#1010_0101`

**Refers to:**
- [BINARY_DIGIT](#BINARY_DIGIT)


```
terminal fragment BINARY_DIGIT_STRING:
	'2#' BINARY_DIGIT (BINARY_DIGIT | '_')*;
```



####  BINARY_DIGIT (terminal fragment) {#BINARY_DIGIT}
**Binary digit**.

- **Examples:**
   * `1`


```
terminal fragment BINARY_DIGIT: '0' | '1';
```



####  OCTAL_DIGIT_STRING (terminal fragment) {#OCTAL_DIGIT_STRING}
**String of octal digits**.

- **Examples:**
   * `8#1234_5670`

**Refers to:**
- [OCTAL_DIGIT](#OCTAL_DIGIT)


```
terminal fragment OCTAL_DIGIT_STRING:
	'8#' OCTAL_DIGIT (OCTAL_DIGIT| '_')*;
```



####  OCTAL_DIGIT (terminal fragment) {#OCTAL_DIGIT}
**Octal digit**.

- **Examples:**
   * `7`


```
terminal fragment OCTAL_DIGIT: '0'..'7';
```



####  HEXA_DIGIT_STRING (terminal fragment) {#HEXA_DIGIT_STRING}
**String of hexadecimal digits**.

- **Examples:**
   * `16#1a2b_3C4F`

**Refers to:**
- [HEXA_DIGIT](#HEXA_DIGIT)


```
terminal fragment HEXA_DIGIT_STRING:
	'16#' HEXA_DIGIT (HEXA_DIGIT | '_')*;
```



####  HEXA_DIGIT (terminal fragment) {#HEXA_DIGIT}
**Hexadecimal digit**. Case insensitive.

- **Examples:**
   * `F`
   * `f`


```
terminal fragment HEXA_DIGIT: '0'..'9' | 'a'..'f' | 'A'..'F';
```



####  INTSTR (terminal) {#INTSTR}
**String of decimal digits**.
			

It may contain underscore (`_`) characters to improve readability,
but they do not change the semantics of the digit string.

- **Examples:**
   * `123`
   * `123_000`

**Refers to:**
- [DECIMAL_DIGIT](#DECIMAL_DIGIT)

**Referred by:**
- [Step7IntConst](#Step7IntConst)
- [StlLongMemoryAddressString](#StlLongMemoryAddressString)

```
terminal INTSTR: 
	DECIMAL_DIGIT (DECIMAL_DIGIT | '_')*;
```



####  Step7RealConst  {#Step7RealConst}
STEP 7 **floating-point literal**.
May be prefixed or non-prefixed.

- **Examples:**
   * `3.1415`
   * `REAL#3.1415`
   * `REAL#+6e23`

**Refers to:**
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [REAL_NUM_CONST](#REAL_NUM_CONST)

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7Real`

```
Step7RealConst returns dt::S7Real:
	FIX_POINT_NUMBER | REAL_NUM_CONST;
```



####  FIX_POINT_NUMBER (terminal) {#FIX_POINT_NUMBER}
Simple non-integer, unsigned **fixed-point number**.
			

Note: integers will be parsed as `FIX_POINT_NUMBER`, but as
`INTSTR`.
			

- **Examples:**
   * `3.1415`

**Refers to:**
- [INTSTR](#INTSTR)

**Referred by:**
- [BlockAttribute](#BlockAttribute)
- [Step7RealConst](#Step7RealConst)
- [StlBitMemoryAddressString](#StlBitMemoryAddressString)

```
terminal FIX_POINT_NUMBER: 	INTSTR '.' INTSTR;
```



####  REAL_NUM_CONST (terminal) {#REAL_NUM_CONST}
Optionally prefixed and signed **fixed-point number**.
			

Note: sign without `REAL#` prefix is not accepted by this rule (e.g., `+3.1415` or `-2`).
However, this is consistent with the integer number handling.
			

- **Examples:**
   * `3.1415` (but will be parsed as `FIX_POINT_NUMBER` as it is defined before)
   * `REAL#3.1415`
   * `REAL#+6e23`

**Refers to:**
- [EXPONENT](#EXPONENT)
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [INTSTR](#INTSTR)
- [REAL_PREFIX](#REAL_PREFIX)
- [SIGN](#SIGN)

**Referred by:**
- [Step7RealConst](#Step7RealConst)

```
terminal REAL_NUM_CONST:
	((REAL_PREFIX SIGN?)? FIX_POINT_NUMBER) |
	((REAL_PREFIX SIGN?)? (INTSTR | FIX_POINT_NUMBER) EXPONENT);
```



####  REAL_PREFIX (terminal fragment) {#REAL_PREFIX}
**Prefix of REAL and LREAL literals**. Case insensitive.

- **Examples:**
   * `real#`
   * `LReal#`


```
terminal fragment REAL_PREFIX:
	('L'|'l')?('R'|'r')('E'|'e')('A'|'a')('L'|'l')'#';
```



####  SIGN (terminal fragment) {#SIGN}
Sign.



```
terminal fragment SIGN:
	'+' | '-';
```



####  EXPONENT (terminal fragment) {#EXPONENT}
Exponent.

- **Examples:**
   * `e23`
   * `E-14`

**Refers to:**
- [INTSTR](#INTSTR)
- [SIGN](#SIGN)


```
terminal fragment EXPONENT:
	('e' | 'E') SIGN? INTSTR;
```



####  Step7CharacterConst  {#Step7CharacterConst}
STEP 7 **characater literal**. May be prefixed or non-prefixed.
If prefixed, it can be given using an integer as ASCII code too.

- **Examples:**
   * `'a'`
   * `CHAR#'a'`
   * `CHAR#123`

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7Char`

```
Step7CharacterConst returns dt::S7Char:
	CHARACTER_CONST;
```



####  CHARACTER_CONST (terminal) {#CHARACTER_CONST}
**Character literal** representing a [Step7CharacterConst](#Step7CharacterConst).


**Refers to:**
- [CHARACTER](#CHARACTER)
- [CHARACTER_PREFIX](#CHARACTER_PREFIX)
- [INTSTR](#INTSTR)


```
terminal CHARACTER_CONST:
	(CHARACTER_PREFIX? "'" CHARACTER "'") | (CHARACTER_PREFIX INTSTR);
```



####  CHARACTER_PREFIX (terminal fragment) {#CHARACTER_PREFIX}
**Prefix of CHAR literals**. Case insensitive.

- **Examples:**
   * `char#`
   * `Char#`


```
terminal fragment CHARACTER_PREFIX:
	('C'|'c')('H'|'h')('A'|'a')('R'|'r')'#';
```



####  CHARACTER (terminal fragment) {#CHARACTER}
A STEP 7 **character**.

- **Examples:**
   * `a` (character `a`)
   * `0` (character `0`)
   * `$N` (new line character)
   * `$'` (apostrophe character)
   * `$$` (dollar sign character)
   * `$3F` (character with ASCII code 63, `?`)

**Refers to:**
- [HEXA_DIGIT](#HEXA_DIGIT)


```
terminal fragment CHARACTER:
	(!("'" | "$")) |
	("$" ('P' | 'L' | 'R' | 'T' | 'N' | "'" | "$")) |
	("$" HEXA_DIGIT HEXA_DIGIT);
```



####  Step7StringConst  {#Step7StringConst}
STEP 7 **string literal**.

- **Examples:**
   * `'abcd efgh'`

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7String`

```
Step7StringConst returns dt::S7String:
	STRING_CONST;
```



####  STRING_CONST (terminal) {#STRING_CONST}
**String literal** representing a [Step7StringConst](#Step7StringConst).


**Referred by:**
- [BlockAttribute](#BlockAttribute)
- [TitleAttribute](#TitleAttribute)

```
terminal STRING_CONST:  
	"'" ( '\\' .  | !('\\'|"'") )* "'";
```



####  Step7DateConst  {#Step7DateConst}
STEP 7 **date literal**. It is mandatory to be prefixed.

- **Examples:**
   * `D#1990-03-01`
   * `Date#2013-06-01`

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7Date`

```
Step7DateConst returns dt::S7Date:
	DATE_CONST;
```



####  DATE_CONST (terminal) {#DATE_CONST}
**Date literal** representing a [Step7DateConst](#Step7DateConst).


**Refers to:**
- [DATE_PREFIX](#DATE_PREFIX)
- [DATE_VALUE](#DATE_VALUE)


```
terminal DATE_CONST: 
	DATE_PREFIX DATE_VALUE;
```



####  DATE_PREFIX (terminal fragment) {#DATE_PREFIX}
**Prefix of DATE literals**. Case insensitive.

- **Examples:**
   * `D#`
   * `DATE#`


```
terminal fragment DATE_PREFIX:
	'D#' | 'd#' | (('D'|'d')('A'|'a')('T'|'t')('E'|'e')'#');
```



####  DATE_VALUE (terminal fragment) {#DATE_VALUE}
**Date value**, in format _YYYY-MM-DD_.

- **Examples:**
   * `1990-03-01`
   * `1990-3-1` (It is also valid without leading zeros in SCL 5.6.)

**Refers to:**
- [INTSTR](#INTSTR)


```
terminal fragment DATE_VALUE:
	INTSTR '-' INTSTR '-' INTSTR;
```



####  Step7DateAndTimeConst  {#Step7DateAndTimeConst}
STEP 7 **date and time literal**. It is mandatory to be prefixed.

- **Examples:**
   * `DT#2013-06-01-08:30:00`

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7DateAndTime`

```
Step7DateAndTimeConst returns dt::S7DateAndTime:
	DATE_AND_TIME_CONST;
```



####  DATE_AND_TIME_CONST (terminal) {#DATE_AND_TIME_CONST}
**Date and time literal** representing a [Step7DateAndTimeConst](#Step7DateAndTimeConst).

- **Examples:**
   * `DT#2013-06-01-08:30:00`

**Refers to:**
- [DATE_AND_TIME_PREFIX](#DATE_AND_TIME_PREFIX)
- [DATE_VALUE](#DATE_VALUE)
- [TIME_OF_DAY_VALUE](#TIME_OF_DAY_VALUE)


```
terminal DATE_AND_TIME_CONST: 
	DATE_AND_TIME_PREFIX DATE_VALUE '-' TIME_OF_DAY_VALUE;
```



####  DATE_AND_TIME_PREFIX (terminal fragment) {#DATE_AND_TIME_PREFIX}
**Prefix of DATE\_AND\_TIME and LDATE\_AND\_TIME literals**. Case insensitive.

- **Examples:**
   * `DT#`
   * `LDate_And_Time#`


```
terminal fragment DATE_AND_TIME_PREFIX:
	('L'|'l')?('D'|'d')('T'|'t')'#' | 	(('L'|'l')?('D'|'d')('A'|'a')('T'|'t')('E'|'e')'_'('A'|'a')('N'|'n')('D'|'d')'_'('T'|'t')('I'|'i')('M'|'m')('E'|'e')'#');
```



####  Step7TimeOfDayConst  {#Step7TimeOfDayConst}
STEP 7 **time of day literal**. It is mandatory to be prefixed.

- **Examples:**
   * `TOD#8:30:00`

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7TimeOfDay`

```
Step7TimeOfDayConst returns dt::S7TimeOfDay:
	TIME_OF_DAY_CONST;
```



####  TIME_OF_DAY_CONST (terminal) {#TIME_OF_DAY_CONST}
**Time of day literal** representing a [Step7TimeOfDayConst](#Step7TimeOfDayConst).

- **Examples:**
   * `TOD#8:30:00`

**Refers to:**
- [TIME_OF_DAY_PREFIX](#TIME_OF_DAY_PREFIX)
- [TIME_OF_DAY_VALUE](#TIME_OF_DAY_VALUE)


```
terminal TIME_OF_DAY_CONST:
	TIME_OF_DAY_PREFIX TIME_OF_DAY_VALUE;
```



####  TIME_OF_DAY_PREFIX (terminal fragment) {#TIME_OF_DAY_PREFIX}
**Prefix of TIME\_OF\_DAY and LTIME\_OF\_DAY literals**. Case insensitive.

- **Examples:**
   * `TOD#`
   * `LTime_of_Day#`


```
terminal fragment TIME_OF_DAY_PREFIX:
	('L'|'l')?('T'|'t')('O'|'o')('D'|'d')'#' | 	('L'|'l')?('T'|'t')('I'|'i')('M'|'m')('E'|'e')'_'('O'|'o')('F'|'f')'_'('D'|'d')('A'|'a')('Y'|'y')'#';
```



####  TIME_OF_DAY_VALUE (terminal fragment) {#TIME_OF_DAY_VALUE}
**Time of day value**, in format _hh:mm:ss_ or _hh:mm:ss.fff_.
It is mandatory to specify the seconds (i.e., `23:59` is not acceptable in 
SCL 5.6).
			

Note: validation of the value ranges is done when parsed as 
`S7TimeOfDay` or `S7DateAndTime`.

- **Examples:**
   * `23:59:59.999`
   * `23:59:59`

**Refers to:**
- [INTSTR](#INTSTR)


```
terminal fragment TIME_OF_DAY_VALUE:
	INTSTR ':' INTSTR ':' INTSTR ('.' INTSTR)?;
```



####  Step7TimePeriodConst  {#Step7TimePeriodConst}
STEP 7 **time period literal**, with 1 ms precision.
It is mandatory to be prefixed.

- **Examples:**
   * `T#90m`
   * `T#1h30m`
   * `T#1.5H`

**Referred by:**
- [UnnamedConstant](#UnnamedConstant)

**Returns:** `dt::S7TimePeriod`

```
Step7TimePeriodConst returns dt::S7TimePeriod:
	TIME_PERIOD_CONST;
```



####  TIME_PERIOD_CONST (terminal) {#TIME_PERIOD_CONST}
**Time period literal** representing a [Step7TimePeriodConst](#Step7TimePeriodConst).

- **Examples:**
   * `T#1h30m`

**Refers to:**
- [SIMPLE_OR_COMPOSITE_TIME](#SIMPLE_OR_COMPOSITE_TIME)
- [TIME_PREFIX](#TIME_PREFIX)


```
terminal TIME_PERIOD_CONST:
	TIME_PREFIX SIMPLE_OR_COMPOSITE_TIME;
```



####  TIME_PREFIX (terminal fragment) {#TIME_PREFIX}
**Prefix of TIME and LTIME literals**. Case insensitive.

- **Examples:**
   * `T#`
   * `TIME#`
   * `LTime#`


```
terminal fragment TIME_PREFIX:
	'T#' | 't#' | 	(('L'|'l')?('T'|'t')('I'|'i')('M'|'m')('E'|'e')'#');
```



####  SIMPLE_OR_COMPOSITE_TIME (terminal) {#SIMPLE_OR_COMPOSITE_TIME}
A **time period literal** without prefix.
It may contain the period with precision of days, hours, minutes, seconds
or milliseconds.
			

Note: validity is checked while parsing as `S7TimePeriod`.

- **Examples:**
   * `1D` (1 day)
   * `1d_2h` (1 day and 2 hours)
   * `1d_345ms` (1 day and 345 ms)
   * `12345s` (12345 seconds)
   * `12.34s` (12 seconds and 340 ms)

**Refers to:**
- [DAYS](#DAYS)
- [HOURS](#HOURS)
- [MILLISECONDS](#MILLISECONDS)
- [MINUTES](#MINUTES)
- [SECONDS](#SECONDS)


```
terminal SIMPLE_OR_COMPOSITE_TIME:
	DAYS HOURS? MINUTES? SECONDS? MILLISECONDS? |
	HOURS MINUTES? SECONDS? MILLISECONDS? |
	MINUTES SECONDS? MILLISECONDS? |
	SECONDS MILLISECONDS? |
	MILLISECONDS?;
```



####  DAYS (terminal fragment) {#DAYS}


**Refers to:**
- [DAYS_ABBREV](#DAYS_ABBREV)
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [INTSTR](#INTSTR)


```
terminal fragment DAYS:
	(INTSTR | FIX_POINT_NUMBER) DAYS_ABBREV '_'?;
```



####  HOURS (terminal fragment) {#HOURS}


**Refers to:**
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [HOURS_ABBREV](#HOURS_ABBREV)
- [INTSTR](#INTSTR)


```
terminal fragment HOURS:
	(INTSTR | FIX_POINT_NUMBER) HOURS_ABBREV '_'?;
```



####  MINUTES (terminal fragment) {#MINUTES}


**Refers to:**
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [INTSTR](#INTSTR)
- [MINUTES_ABBREV](#MINUTES_ABBREV)


```
terminal fragment MINUTES:
	(INTSTR | FIX_POINT_NUMBER) MINUTES_ABBREV '_'?;
```



####  SECONDS (terminal fragment) {#SECONDS}


**Refers to:**
- [FIX_POINT_NUMBER](#FIX_POINT_NUMBER)
- [INTSTR](#INTSTR)
- [SECONDS_ABBREV](#SECONDS_ABBREV)


```
terminal fragment SECONDS:
	(INTSTR | FIX_POINT_NUMBER) SECONDS_ABBREV '_'?;
```



####  MILLISECONDS (terminal fragment) {#MILLISECONDS}


**Refers to:**
- [INTSTR](#INTSTR)
- [MILLISECONDS_ABBREV](#MILLISECONDS_ABBREV)


```
terminal fragment MILLISECONDS:
	INTSTR MILLISECONDS_ABBREV '_'?;
```



####  DAYS_ABBREV (terminal fragment) {#DAYS_ABBREV}



```
terminal fragment DAYS_ABBREV: 'D'|'d';
```



####  HOURS_ABBREV (terminal fragment) {#HOURS_ABBREV}



```
terminal fragment HOURS_ABBREV: 'H'|'h';
```



####  MINUTES_ABBREV (terminal fragment) {#MINUTES_ABBREV}



```
terminal fragment MINUTES_ABBREV: 'M'|'m';
```



####  SECONDS_ABBREV (terminal fragment) {#SECONDS_ABBREV}



```
terminal fragment SECONDS_ABBREV: 'S'|'s';
```



####  MILLISECONDS_ABBREV (terminal fragment) {#MILLISECONDS_ABBREV}



```
terminal fragment MILLISECONDS_ABBREV: ('M'|'m')('S'|'s');
```



####  Step7TimerConst  {#Step7TimerConst}
A STEP 7 **timer literal**.

- **Examples:**
   * `T5`

**Referred by:**
- [StandaloneExpression](#StandaloneExpression)

**Returns:** `dt::S7Timer`

```
Step7TimerConst returns dt::S7Timer:
	TIMER_CONST;
```



####  TIMER_CONST (terminal) {#TIMER_CONST}
**Timer literal** representing a [Step7TimerConst](#Step7TimerConst).


**Refers to:**
- [INTSTR](#INTSTR)


```
terminal TIMER_CONST:
	'T' INTSTR;
```



####  Step7CounterConst  {#Step7CounterConst}
A STEP 7 **counter literal**.

- **Examples:**
   * `C5`

**Referred by:**
- [StandaloneExpression](#StandaloneExpression)

**Returns:** `dt::S7Counter`

```
Step7CounterConst returns dt::S7Counter:
	COUNTER_CONST;
```



####  COUNTER_CONST (terminal) {#COUNTER_CONST}
**Counter literal** representing a [Step7CounterConst](#Step7CounterConst).


**Refers to:**
- [INTSTR](#INTSTR)


```
terminal COUNTER_CONST:
	'C' INTSTR;
```



####  NamedElement  {#NamedElement}
A **named element** is a STEP 7 element with name: a program unit, variable or constant declaration, or a label. Not used in the grammar.


**Refers to:**
- [Label](#Label)
- [ProgramUnit](#ProgramUnit)
- [VariableOrConstDeclaration](#VariableOrConstDeclaration)



```
NamedElement: ProgramUnit | VariableOrConstDeclaration | Label;
```



####  VariableOrConstDeclaration  {#VariableOrConstDeclaration}
Represents a variable or constant declaration. Not used in the grammar.


**Refers to:**
- [NamedConstantDeclaration](#NamedConstantDeclaration)
- [Variable](#Variable)

**Referred by:**
- [NamedElement](#NamedElement)


```
VariableOrConstDeclaration: Variable | NamedConstantDeclaration;
```



####  LabeledStatement  {#LabeledStatement}
A **labeled statement** is either an SCL or STL labeled statement. Used only to have a common ancestor of these classes.


**Refers to:**
- [LabeledSclStatement](#LabeledSclStatement)
- [LabeledStlStatement](#LabeledStlStatement)



```
LabeledStatement: LabeledSclStatement | LabeledStlStatement;
```



####  SubroutineCall  {#SubroutineCall}
A **call** is either an SCL or STL call. Used only to have a common ancestor of these classes.

- **Validation:**
   * [INVALID_SUBROUTINE_CALL] The `calledUnit` shall refer to a Function, a FunctionBlock or a local variable with type FunctionBlock (p. 15-50)
   * [INVALID_SUBROUTINE_CALL] The `hasExplicitDataBlock` shall be false if the `calledUnit` is a Function or a local variable with FunctionBlock (p. 15-50)
   * [INVALID_FB_CALL_DB] If defined, the data type of `dataBlock` should match the `calledUnit`
   * If `calledUnit` is a FunctionBlock (or a local variable representing an FB instance), the CallParameter should not contain output assignments (p. 12-30)
   * [INVALID\_SUBROUTINE\_CALL] The 'callParameter' may only be null for FB calls, or FC calls calling parameterless functions.
   * [INVALID_CALL_PARAMETERS] If the `calledUnit` is a Function, all parameters (input, output and input-output variables) have to be assigned.

**Refers to:**
- [SclSubroutineCall](#SclSubroutineCall)
- [StlSubroutineCall](#StlSubroutineCall)



```
SubroutineCall: SclSubroutineCall | StlSubroutineCall;
```



####  IdOrSymbol  {#IdOrSymbol}


**Refers to:**
- [IdLike](#IdLike)
- [SYMBOL](#SYMBOL)

**Referred by:**
- [DataBlock](#DataBlock)
- [DirectNamedRef](#DirectNamedRef)
- [FbOrUdtDT](#FbOrUdtDT)
- [Function](#Function)
- [FunctionBlock](#FunctionBlock)
- [OrganizationBlock](#OrganizationBlock)
- [SclSubroutineCall](#SclSubroutineCall)
- [StlParameterlessCallStatement](#StlParameterlessCallStatement)
- [StlSubroutineCall](#StlSubroutineCall)
- [UserDefinedDataType](#UserDefinedDataType)
- [Variable](#Variable)

**Returns:** `ecore::EString`

```
IdOrSymbol returns ecore::EString:
	IdLike | SYMBOL;
```



####  IdLike  {#IdLike}


**Refers to:**
- [BitIoPrefix](#BitIoPrefix)
- [ID](#ID)
- [SclKeyword](#SclKeyword)
- [StlKeyword](#StlKeyword)

**Referred by:**
- [BlockAttribute](#BlockAttribute)
- [CallParameterItem](#CallParameterItem)
- [IdOrSymbol](#IdOrSymbol)
- [Label](#Label)
- [LabeledSclStatement](#LabeledSclStatement)
- [NamedConstantDeclaration](#NamedConstantDeclaration)
- [NamedOrUnnamedConstantRef](#NamedOrUnnamedConstantRef)
- [SclGotoStatement](#SclGotoStatement)
- [StlJumpStatement](#StlJumpStatement)
- [TitleAttribute](#TitleAttribute)
- [Variable](#Variable)

**Returns:** `ecore::EString`

```
IdLike returns ecore::EString:
	ID | SclKeyword | StlKeyword | BitIoPrefix;
```



####  BitIoPrefix  {#BitIoPrefix}


**Refers to:**
- [BOOL_MEMORY_PREFIX](#BOOL_MEMORY_PREFIX)
- [LONG_MEMORY_PREFIX](#LONG_MEMORY_PREFIX)

**Referred by:**
- [IdLike](#IdLike)

**Returns:** `ecore::EString`

```
BitIoPrefix:
	LONG_MEMORY_PREFIX | BOOL_MEMORY_PREFIX;
```



####  StlKeyword  {#StlKeyword}
STL keywords which can be used as names in SCL.
			

Note: `NOT`, `MOD` cannot be a name, reserved words in SCL.


**Referred by:**
- [IdLike](#IdLike)

**Returns:** `ecore::EString`

```
StlKeyword:
	'A' | 'AN' | 'O' | 'ON' | 'X' | 'XN' | 'R' | 'S' | 'SET' | 'CLR' |
	'SAVE' | 'L' | 'T' | 'CALL' | 'D' | 'I' | 'FP' | 'FN' | 'INC' | 'DEC' |
		'JU' | 'JC' | 'JCN' | 'JCB' | 'JNB' | 'JBI' | 'JNBI' | 'JO' |  'JOS' | 
	'JZ' | 'JN' | 'JP' | 'JM' | 'JPZ' | 'JMZ' | 'JUO' | 'LOOP' |
		'ABS' | 'SQR' | 'SQRT' | 'EXP' | 'LN' | 'SIN' | 'COS' | 'TAN' | 'ASIN' | 'ACOS' | 'ATAN' |
		'ITD' | 'DTB' | 'DTR' | 'INVI' | 'INVD' | 'NEGI' | 'NEGD' | 'NEGR' | 'RND' | 'TRUNC' |
	'RND+' | 'RND-' | 'BTI' | 'ITB' | 'BTD' | 'CAW' | 'CAD' |
		'AD' | 'AW' | 'OD' | 'OW' | 'XOD' | 'XOW' |
		'SSI' | 'SSD' | 'SLW' | 'SRW' | 'SLD' | 'SRD' | 'RLD' | 'RRD' | 'RLDA' | 'RRDA' |
		'UC' | 'CC' |
		'TAK' | 'PUSH' | 'POP' | 'ENT' | 'LEAVE' |
		'BE' | 'BEC' | 'BEU'
;
```



####  SclKeyword  {#SclKeyword}
SCL keywords which can be used as names in other languages.
For example, while EXIT is an SCL keyword, it is a valid ID in STL.

Although the SCL v5.3 description permitted to use keywords as identifiers in certain cases
(cf. p. 12-23, Sec. 12.2.8, `ARRAY : ARRAY[1..100] OF INT`), the STEP 7 v5.6+HF1 compiler 
does not permit such construction.

TIA The new Tia keywords shall be permitted if the compatibility mode indicates older software

- **Validation:**
   * [ILLEGAL\_SCL\_ELEMENT] The keywords shall not be used in SCL programs.

**Referred by:**
- [IdLike](#IdLike)

**Returns:** `ecore::EString`

```
SclKeyword:
			 'BY' | 'CONTINUE' | 'DO' | 'EXIT' | 'FOR' | 'GOTO' | 'REPEAT' | 'RETURN' | 'TO';
```



####  VerificationOption  {#VerificationOption}


**Refers to:**
- [CompatbilityLevelEnum](#CompatbilityLevelEnum)

**Referred by:**
- [ProgramFile](#ProgramFile)


```
VerificationOption:
	{CompatbilityLevel} '//#COMPATIBLITY_LEVEL' style=CompatbilityLevelEnum;
```



####  CompatbilityLevelEnum (enum) {#CompatbilityLevelEnum}


Literals:
- STEP7_V55 (`Step7v55`)
- TIA1200 (`Tia1200`)
- TIA1500 (`Tia1500`)
- TIA300 (`Tia300`)

```
enum CompatbilityLevelEnum: STEP7_V55='Step7v55' | TIA300='Tia300' | TIA1200='Tia1200' | TIA1500='Tia1500';
```



####  VerificationAssertion  {#VerificationAssertion}


**Refers to:**
- [SclVerificationAssertion](#SclVerificationAssertion)
- [StlVerificationAssertion](#StlVerificationAssertion)



```
VerificationAssertion: SclVerificationAssertion | StlVerificationAssertion;
```



####  SclVerificationAssertion  {#SclVerificationAssertion}
Verification assertion to be used in SCL programs.
It can optionally have a tag to refer to one or more assertions.


**Refers to:**
- [Expression](#Expression)
- [ID](#ID)

**Referred by:**
- [SclStatement](#SclStatement)
- [VerificationAssertion](#VerificationAssertion)


```
SclVerificationAssertion:
	'//#ASSERT' expression=Expression (tagged?=':' tag=ID)?;
```



####  StlVerificationAssertion  {#StlVerificationAssertion}
Verification assertion to be used in STL programs. 
It can optionally have a tag to refer to one or more assertions.


**Refers to:**
- [Expression](#Expression)
- [ID](#ID)

**Referred by:**
- [StlStatement](#StlStatement)
- [VerificationAssertion](#VerificationAssertion)


```
StlVerificationAssertion:
	'//#ASSERT' expression=Expression (tagged?=':' tag=ID)? ';';
```



####  ID (terminal) {#ID}
A STEP 7 **identifier**. Can contain alphanumeric characters and underscore.
It should not start with number.

- **Validation:**
   * [INVALID\_IDENTIFIER] A STEP 7 identifier must be max. 24 characters long (p. 5-6)
   * [INVALID_IDENTIFIER] A STEP 7 identifier must not contain two `_` signs next to each other (p. 15-3)
- **Examples:**
   * `var1`
   * `VAR_name_123`

**Referred by:**
- [IdLike](#IdLike)
- [SclVerificationAssertion](#SclVerificationAssertion)
- [StlVerificationAssertion](#StlVerificationAssertion)

```
terminal ID:
	('#')?('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;
```



####  DECIMAL_DIGIT (terminal fragment) {#DECIMAL_DIGIT}
Decimal digit.

- **Examples:**
   * `8`


```
terminal fragment DECIMAL_DIGIT: '0'..'9';
```



####  VERIFICATION_DIRECTIVE_PREFIX (terminal) {#VERIFICATION_DIRECTIVE_PREFIX}
**Verification directive prefix**.



```
terminal VERIFICATION_DIRECTIVE_PREFIX: '//#';
```



####  SYMBOL (terminal) {#SYMBOL}
A STEP 7 **symbol**.

- **Examples:**
   * `"symbol1"`
   * `"symbol with whitespace"`

**Referred by:**
- [IdOrSymbol](#IdOrSymbol)

```
terminal SYMBOL returns ecore::EString:  
	'"' ( !('\\'|'"') )* '"' ;
```



####  ML_COMMENT (terminal) {#ML_COMMENT}
**Multi-line comment**. Cannot be nested.

- **Examples:**
   * `(* comment text *)`


```
terminal ML_COMMENT:
	('(*' -> '*)') | ('{' -> '}');
```



####  SL_COMMENT (terminal) {#SL_COMMENT}
**Single-line comment**.

- **Examples:**
   * `// comment text`


```
terminal SL_COMMENT:
	'//' (!('\n'|'\r'|'#') !('\n'|'\r')*)? ('\r'? '\n')? |
	'//#' !('A'|'B'|'C'|'\r'|'\n') !('\n'|'\r')* ('\r'? '\n')? | 
	'//#' 'A' !('S'|'\r'|'\n') !('\n'|'\r')* ('\r'? '\n')?;
```



####  WS (terminal) {#WS}
**Whitespace**. Can be one or more space, tab or new line (`\r` or
`\n`) character(s).



```
terminal WS:
	(' '|'\t'|'\r'|'\n')+;
```



####  ANY_OTHER (terminal) {#ANY_OTHER}



```
terminal ANY_OTHER: .;
```




###  Simplified grammar
**ProgramFile** ::=    _VerificationOption_*   _GlobalVariableBlock_*   _ProgramUnit_*;

**VerificationOption** ::=    `//#COMPATIBLITY_LEVEL`   _CompatbilityLevelEnum_;

**GlobalVariableBlock** ::= _GlobalVariableDeclarationDirection_   _VariableDeclarationLine_+   `END_VAR`;

**ProgramUnit** ::= _ExecutableProgramUnit_ | _DataBlock_ | _UserDefinedDataType_;

**CompatbilityLevelEnum** ::= `Step7v55` | `Tia300` | `Tia1200` | `Tia1500`;

**GlobalVariableDeclarationDirection** ::= `VAR_GLOBAL` | `VAR_GLOBAL_INPUT` | `VAR_GLOBAL_OUTPUT`;

**VariableDeclarationLine** ::= _Variable_   (`,`   _Variable_)*   `:`   _DataType_   (`:=`   _VariableInitialization_)?   `;`;

**ExecutableProgramUnit** ::= _OrganizationBlock_ | _Function_ | _FunctionBlock_;

**DataBlock** ::= `DATA_BLOCK`   (_IdOrSymbol_ | _DATABLOCK_FIELD_ADDRESS_)   _BlockAttributes_?   _DataBlockStructure_   `;`?   `BEGIN`   (_DataBlockInitialAssignment_   `;`)*   `END_DATA_BLOCK`;

**UserDefinedDataType** ::= `TYPE`   _IdOrSymbol_   _BlockAttributes_?   `:`?   _StructDT_   `;`?   `END_TYPE`;

**Variable** ::= _IdOrSymbol_   (`AT`   _IdLike_)?;

**DataType** ::= _ElementaryDT_ | _StringDT_ | _ArrayDT_ | _StructDT_ | _FbOrUdtDT_ | _ParameterDT_;

**VariableInitialization** ::= `[`?   _ArrayInitializationElement_   (   (`,`   _ArrayInitializationElement_)+)?   `]`?;

**OrganizationBlock** ::= `ORGANIZATION_BLOCK`   _IdOrSymbol_   _BlockAttributes_?   _DeclarationSection_   `BEGIN`?   _BuiltinKeyword_?   _StatementList_?   `END_ORGANIZATION_BLOCK`;

**Function** ::= `FUNCTION`   _IdOrSymbol_   `:`   _DataType_   _BlockAttributes_?   _DeclarationSection_   `BEGIN`?   _BuiltinKeyword_?   _StatementList_?   `END_FUNCTION`;

**FunctionBlock** ::= (`FUNCTION_BLOCK` | `PROGRAM`)   _IdOrSymbol_   _BlockAttributes_?   _DeclarationSection_   `BEGIN`?   _BuiltinKeyword_?   _StatementList_?   (`END_FUNCTION_BLOCK` | `END_PROGRAM`);

**IdOrSymbol** ::= _IdLike_ | _SYMBOL_;

**DATABLOCK_FIELD_ADDRESS** ::= ((`D` | `d`)   (`B` | `b`)   _NONBIT_DATA_ELEMENT_SIZE_   _INTSTR_) | ((`D` | `d`)   (`D` | `W` | `d` | `w`)   _INTSTR_) | ((`D` | `d`)   _BIT_DATA_ELEMENT_SIZE_?   _FIX_POINT_NUMBER_) | ((`D` | `d`)   (`B` | `b`)   _BIT_DATA_ELEMENT_SIZE_   _FIX_POINT_NUMBER_);

**BlockAttributes** ::= _BlockAttribute_+;

**DataBlockStructure** ::= _FbOrUdtDT_ | _StructDT_;

**DataBlockInitialAssignment** ::= _LeftValue_   `:=`   _NamedOrUnnamedConstantRef_;

**StructDT** ::=    `STRUCT`   _VariableDeclarationLine_*   `END_STRUCT`;

**IdLike** ::= _ID_ | _SclKeyword_ | _StlKeyword_ | _BitIoPrefix_;

**ElementaryDT** ::= _ElementaryTypeEnum_;

**StringDT** ::=    `STRING`   (`[`   _Expression_   `]`)?;

**ArrayDT** ::= `ARRAY`   `[`   _ArrayDimension_   (`,`   _ArrayDimension_)*   `]`   `OF`   _DataType_;

**FbOrUdtDT** ::= _IdOrSymbol_;

**ParameterDT** ::= _ParameterTypeEnum_;

**ArrayInitializationElement** ::= (   _NamedOrUnnamedConstantRef_) | (   _NamedOrUnnamedConstantRef_   `(`   _VariableInitialization_   `)`);

**DeclarationSection** ::=    (_VariableDeclarationBlock_ | _ConstDeclarationBlock_ | _LabelDeclarationBlock_)*;

**BuiltinKeyword** ::= `//#BUILTIN`;

**StatementList** ::= _SclStatementList_ | _StlStatementList_;

**SYMBOL** ::= `"`   !(`\` | `"`)*   `"`;

**NONBIT_DATA_ELEMENT_SIZE** ::= `B` | `b` | `D` | `d` | `W` | `w`;

**INTSTR** ::= _DECIMAL_DIGIT_   (_DECIMAL_DIGIT_ | `_`)*;

**BIT_DATA_ELEMENT_SIZE** ::= `X` | `x`;

**FIX_POINT_NUMBER** ::= _INTSTR_   `.`   _INTSTR_;

**BlockAttribute** ::= _TitleAttribute_ | (   `AUTHOR`   (`=` | `:`)   (_STRING_CONST_ | _IdLike_)) | (   `KNOW_HOW_PROTECT`) | (   `NAME`   (`=` | `:`)   (_STRING_CONST_ | _IdLike_)) | (   `FAMILY`   (`=` | `:`)   (_STRING_CONST_ | _IdLike_)) | (   `VERSION`   (`=` | `:`)   (_FIX_POINT_NUMBER_ | _STRING_CONST_)) | (   `NON_RETAIN`);

**LeftValue** ::= _NamedValueRef_ | _MemoryAddress_;

**NamedOrUnnamedConstantRef** ::= (   _IdLike_) | _UnnamedConstantRef_ | _NegatedUnnamedConstantRef_;

**ID** ::= `#`?   ([`a`..`z`] | [`A`..`Z`] | `_`)   ([`a`..`z`] | [`A`..`Z`] | `_` | [`0`..`9`])*;

**SclKeyword** ::= `BY` | `CONTINUE` | `DO` | `EXIT` | `FOR` | `GOTO` | `REPEAT` | `RETURN` | `TO`;

**StlKeyword** ::= `A` | `AN` | `O` | `ON` | `X` | `XN` | `R` | `S` | `SET` | `CLR` | `SAVE` | `L` | `T` | `CALL` | `D` | `I` | `FP` | `FN` | `INC` | `DEC` | `JU` | `JC` | `JCN` | `JCB` | `JNB` | `JBI` | `JNBI` | `JO` | `JOS` | `JZ` | `JN` | `JP` | `JM` | `JPZ` | `JMZ` | `JUO` | `LOOP` | `ABS` | `SQR` | `SQRT` | `EXP` | `LN` | `SIN` | `COS` | `TAN` | `ASIN` | `ACOS` | `ATAN` | `ITD` | `DTB` | `DTR` | `INVI` | `INVD` | `NEGI` | `NEGD` | `NEGR` | `RND` | `TRUNC` | `RND+` | `RND-` | `BTI` | `ITB` | `BTD` | `CAW` | `CAD` | `AD` | `AW` | `OD` | `OW` | `XOD` | `XOW` | `SSI` | `SSD` | `SLW` | `SRW` | `SLD` | `SRD` | `RLD` | `RRD` | `RLDA` | `RRDA` | `UC` | `CC` | `TAK` | `PUSH` | `POP` | `ENT` | `LEAVE` | `BE` | `BEC` | `BEU`;

**BitIoPrefix** ::= _LONG_MEMORY_PREFIX_ | _BOOL_MEMORY_PREFIX_;

**ElementaryTypeEnum** ::= `BOOL` | `BYTE` | `WORD` | `DWORD` | `CHAR` | `INT` | `DINT` | `REAL` | `S5TIME` | `TIME` | `TIME_OF_DAY` | `TOD` | `DATE` | `VOID` | `WCHAR` | `WSTRING` | `SINT` | `USINT` | `UINT` | `UDINT` | `LINT` | `ULINT` | `LREAL` | `LWORD` | `LTIME` | `LTOD` | `LTIME_OF_DAY` | `DT` | `DATE_AND_TIME` | `LDT` | `DATE_AND_LTIME` | `DTL`;

**Expression** ::= _ImpliesExpression_ | _StandaloneExpression_;

**ArrayDimension** ::= (   _Expression_   _DOUBLE_DOT_   _Expression_) | (   _CONSTANT_RANGE_);

**ParameterTypeEnum** ::= `TIMER` | `COUNTER` | `ANY` | `ANY_NUM` | `xxxANY_BIT` | `POINTER` | `BLOCK_FC` | `BLOCK_FB` | `BLOCK_DB` | `BLOCK_SDB`;

**VariableDeclarationBlock** ::=    _VariableDeclarationDirection_   _VariableDeclarationLine_*   `END_VAR`;

**ConstDeclarationBlock** ::=    `CONST`   (_NamedConstantDeclaration_   `;`)*   `END_CONST`;

**LabelDeclarationBlock** ::=    `LABEL`   (_Label_   (`,`   _Label_)*   `;`)*   `END_LABEL`;

**SclStatementList** ::=    _AbstractSclStatement_+;

**StlStatementList** ::=    _StlNetwork_+;

**DECIMAL_DIGIT** ::= [`0`..`9`];

**TitleAttribute** ::= (   `TITLE`   (`=` | `:`)   (_STRING_CONST_ | _IdLike_)) | (   _TITLE_LINE_);

**STRING_CONST** ::= `'`   ((`\`   _._) | !(`\` | `'`))*   `'`;

**NamedValueRef** ::= _QualifiedRef_;

**MemoryAddress** ::= (   (_BitMemoryAddressString_ | _StlBitMemoryAddressString_)) | (   (_LongMemoryAddressString_ | _StlLongMemoryAddressString_));

**UnnamedConstantRef** ::=    _UnnamedConstant_;

**NegatedUnnamedConstantRef** ::=    `-`   _UnnamedConstant_;

**LONG_MEMORY_PREFIX** ::= _ADDRESS_IDENTIFIER_   _NONBIT_DATA_ELEMENT_SIZE_;

**BOOL_MEMORY_PREFIX** ::= _ADDRESS_IDENTIFIER_   _BIT_DATA_ELEMENT_SIZE_?;

**ImpliesExpression** ::= _OrExpression_   (   `-->`   _OrExpression_)?;

**StandaloneExpression** ::= (   _Step7TimerConst_) | (   _Step7CounterConst_) | (   `NIL`);

**DOUBLE_DOT** ::= `..`;

**CONSTANT_RANGE** ::= _INTSTR_   `..`   _INTSTR_;

**VariableDeclarationDirection** ::= `VAR_INPUT` | `VAR_OUTPUT` | `VAR` | `VAR_TEMP` | `VAR_IN_OUT`;

**NamedConstantDeclaration** ::= _IdLike_   `:=`   _Expression_;

**Label** ::= _IdLike_;

**AbstractSclStatement** ::= _LabeledSclStatement_ | _SclStatement_;

**StlNetwork** ::=    `NETWORK`   _TitleAttribute_?   _AbstractStlStatement_*;

**TITLE_LINE** ::= `TITLE=`    --> `\n` ;

**QualifiedRef** ::= _UnqualifiedRef_   (   `.`   _UnqualifiedRef_)*;

**BitMemoryAddressString** ::= _BIT_MEMORY_ADDRESS_;

**StlBitMemoryAddressString** ::= _BOOL_MEMORY_PREFIX_   _FIX_POINT_NUMBER_;

**LongMemoryAddressString** ::= _LONG_MEMORY_ADDRESS_;

**StlLongMemoryAddressString** ::= _LONG_MEMORY_PREFIX_   _INTSTR_;

**UnnamedConstant** ::= (   _Step7BoolConst_) | (   _Step7IntConst_) | (   _Step7RealConst_) | (   _Step7CharacterConst_) | (   _Step7DateConst_) | (   _Step7DateAndTimeConst_) | (   _Step7TimeOfDayConst_) | (   _Step7TimePeriodConst_) | (   _Step7StringConst_);

**ADDRESS_IDENTIFIER** ::= `I` | `i` | `Q` | `q` | `M` | `m` | `L` | `l` | ((`P` | `p`)   (`Q` | `q`)) | ((`P` | `p`)   (`I` | `i`));

**OrExpression** ::= _XorExpression_   (   `OR`   _XorExpression_)*;

**Step7TimerConst** ::= _TIMER_CONST_;

**Step7CounterConst** ::= _COUNTER_CONST_;

**LabeledSclStatement** ::= (_IdLike_   `:`)+   _SclStatement_;

**SclStatement** ::= (   `;`) | (_SclAssignmentStatement_   `;`) | (_SclIfStatement_   `;`) | (_SclCaseStatement_   `;`) | (_SclForStatement_   `;`) | (_SclWhileStatement_   `;`) | (_SclRepeatStatement_   `;`) | (_SclSubroutineCall_   `;`) | (   `RETURN`   `;`) | (   `CONTINUE`   `;`) | (   `EXIT`   `;`) | (_SclGotoStatement_   `;`) | _SclVerificationAssertion_;

**AbstractStlStatement** ::= _LabeledStlStatement_ | _StlStatement_;

**UnqualifiedRef** ::= _DirectRef_   (   `[`   _Expression_   (`,`   _Expression_)*   `]`)*;

**BIT_MEMORY_ADDRESS** ::= `%`?   _BOOL_MEMORY_PREFIX_   _FIX_POINT_NUMBER_;

**LONG_MEMORY_ADDRESS** ::= `%`?   _LONG_MEMORY_PREFIX_   _INTSTR_;

**Step7BoolConst** ::= `BOOL#`?   (`FALSE` | `TRUE`);

**Step7IntConst** ::= _INTSTR_ | _PREFIXED_BIT_OR_INT_CONST_;

**Step7RealConst** ::= _FIX_POINT_NUMBER_ | _REAL_NUM_CONST_;

**Step7CharacterConst** ::= _CHARACTER_CONST_;

**Step7DateConst** ::= _DATE_CONST_;

**Step7DateAndTimeConst** ::= _DATE_AND_TIME_CONST_;

**Step7TimeOfDayConst** ::= _TIME_OF_DAY_CONST_;

**Step7TimePeriodConst** ::= _TIME_PERIOD_CONST_;

**Step7StringConst** ::= _STRING_CONST_;

**XorExpression** ::= _AndExpression_   (   `XOR`   _AndExpression_)*;

**TIMER_CONST** ::= `T`   _INTSTR_;

**COUNTER_CONST** ::= `C`   _INTSTR_;

**SclAssignmentStatement** ::= _LeftValue_   `:=`   _Expression_;

**SclIfStatement** ::= `IF`   _Expression_   `THEN`   _SclStatementList_   _ElsifBlock_*   (`ELSE`   _SclStatementList_)?   `END_IF`;

**SclCaseStatement** ::= `CASE`   _Expression_   `OF`   _CaseElement_+   (`ELSE`   `:`?   _SclUnLabeledStatementList_)?   `END_CASE`;

**SclForStatement** ::= `FOR`   _SclAssignmentStatement_   `TO`   _Expression_   (`BY`   _Expression_)?   `DO`   _SclStatementList_   `END_FOR`;

**SclWhileStatement** ::= `WHILE`   _Expression_   `DO`   _SclStatementList_   `END_WHILE`;

**SclRepeatStatement** ::= `REPEAT`   _SclStatementList_   `UNTIL`   _Expression_   `END_REPEAT`;

**SclSubroutineCall** ::= _IdOrSymbol_   (`.`   _IdOrSymbol_)?   `(`   _CallParameter_?   `)`;

**SclGotoStatement** ::= `GOTO`   _IdLike_;

**SclVerificationAssertion** ::= `//#ASSERT`   _Expression_   (`:`   _ID_)?;

**LabeledStlStatement** ::= _Label_   `:`   _StlStatement_;

**StlStatement** ::= _StlUnaryStatement_ | _StlUnaryConstantStatement_ | _StlArgumentlessStatement_ | _StlJumpStatement_ | _StlSubroutineCall_ | _StlParameterlessCallStatement_ | _StlVerificationAssertion_;

**DirectRef** ::= _DirectNamedRef_ | (   _DatablockFieldAddress_) | (   _S7DatablockFieldIndexedPrefix_   _Expression_   `]`) | _FlagRef_ | _StwBitRef_;

**PREFIXED_BIT_OR_INT_CONST** ::= (_CONST_TYPE_PREFIX_   _SIGN_?   (_INTSTR_ | _BINARY_DIGIT_STRING_ | _OCTAL_DIGIT_STRING_ | _HEXA_DIGIT_STRING_)) | (_BINARY_DIGIT_STRING_ | _OCTAL_DIGIT_STRING_ | _HEXA_DIGIT_STRING_);

**REAL_NUM_CONST** ::= ((_REAL_PREFIX_   _SIGN_?)?   _FIX_POINT_NUMBER_) | ((_REAL_PREFIX_   _SIGN_?)?   (_INTSTR_ | _FIX_POINT_NUMBER_)   _EXPONENT_);

**CHARACTER_CONST** ::= (_CHARACTER_PREFIX_?   `'`   _CHARACTER_   `'`) | (_CHARACTER_PREFIX_   _INTSTR_);

**DATE_CONST** ::= _DATE_PREFIX_   _DATE_VALUE_;

**DATE_AND_TIME_CONST** ::= _DATE_AND_TIME_PREFIX_   _DATE_VALUE_   `-`   _TIME_OF_DAY_VALUE_;

**TIME_OF_DAY_CONST** ::= _TIME_OF_DAY_PREFIX_   _TIME_OF_DAY_VALUE_;

**TIME_PERIOD_CONST** ::= _TIME_PREFIX_   _SIMPLE_OR_COMPOSITE_TIME_;

**AndExpression** ::= _EqualityExpression_   (   (`&` | `AND`)   _EqualityExpression_)*;

**ElsifBlock** ::= `ELSIF`   _Expression_   `THEN`   _SclStatementList_;

**CaseElement** ::= _CaseElementValue_   (`,`   _CaseElementValue_)*   `:`   _SclUnLabeledStatementList_;

**SclUnLabeledStatementList** ::= _SclStatement_+;

**CallParameter** ::= (   _Expression_) | (   _CallParameterItem_   (`,`   _CallParameterItem_)*);

**StlUnaryStatement** ::= (   _StlBitLogicOpMnemonic_   _UnaryOrPrimaryExpression_   `;`) | (   `=`   _UnaryOrPrimaryExpression_   `;`) | (   `R`   _UnaryOrPrimaryExpression_   `;`) | (   `S`   _UnaryOrPrimaryExpression_   `;`) | (   `FP`   _UnaryOrPrimaryExpression_   `;`) | (   `FN`   _UnaryOrPrimaryExpression_   `;`) | (   `L`   _UnaryOrPrimaryExpression_   `;`) | (   `T`   _UnaryOrPrimaryExpression_   `;`);

**StlUnaryConstantStatement** ::= (   `+`   (_NegatedUnnamedConstantRef_ | _UnnamedConstantRef_)   `;`) | _StlShiftRotateStatement_ | (   _StlWordLogicMnemonic_   _NamedOrUnnamedConstantRef_   `;`) | (   `INC`   _NamedOrUnnamedConstantRef_   `;`) | (   `DEC`   _NamedOrUnnamedConstantRef_   `;`) | (   `BLD`   _NamedOrUnnamedConstantRef_   `;`) | (   `NOP`   _NamedOrUnnamedConstantRef_   `;`);

**StlArgumentlessStatement** ::= (   `O`   `;`) | (   _StlBitLogicOpMnemonic_   `(`   `;`) | (   `)`   `;`) | (   `NOT`   `;`) | (   `SET`   `;`) | (   `CLR`   `;`) | (   `SAVE`   `;`) | (   _StlComparisonMnemonic_   _StlComparisonDataType_   `;`) | (   _StlConversionMnemonic_   `;`) | (   `MOD`   `;`) | (   _StlArithmeticMnemonic_   _StlComparisonDataType_   `;`) | (   _StlFloatMathFunctionMnemonic_   `;`) | (   `L`   `STW`   `;`) | (   `T`   `STW`   `;`) | (   _StlBlockEndMnemonic_   `;`) | (   _StlMcrMnemonic_   `;`) | (   _StlWordLogicMnemonic_   `;`) | (   _StlAccuArgumentlessMnemonic_   `;`);

**StlJumpStatement** ::= _StlJumpMnemonic_   _IdLike_   `;`;

**StlSubroutineCall** ::= `CALL`   _IdOrSymbol_   (`,`   _IdOrSymbol_)?   (`(`   _CallParameter_?   `)`)?   `;`;

**StlParameterlessCallStatement** ::= _StlParameterlessCallMnemonic_   _IdOrSymbol_   `;`;

**StlVerificationAssertion** ::= `//#ASSERT`   _Expression_   (`:`   _ID_)?   `;`;

**DirectNamedRef** ::= _IdOrSymbol_;

**DatablockFieldAddress** ::= _DATABLOCK_FIELD_ADDRESS_;

**S7DatablockFieldIndexedPrefix** ::= _DATABLOCK_FIELD_INDEXED_PREFIX_;

**FlagRef** ::= (   `ENO`) | (   `OK`) | (   (`RET_VAL` | `#RET_VAL`));

**StwBitRef** ::= _StwBitRefMnemonic_;

**CONST_TYPE_PREFIX** ::= `B#` | `b#` | ((`D` | `d`)?   (`W` | `w`)   `#`) | `L#` | `l#` | ((`B` | `b`)   (`O` | `o`)   (`O` | `o`)   (`L` | `l`)   `#`) | ((`B` | `b`)   (`Y` | `y`)   (`T` | `t`)   (`E` | `e`)   `#`) | (((`D` | `d`) | (`L` | `l`))?   (`W` | `w`)   (`O` | `o`)   (`R` | `r`)   (`D` | `d`)   `#`) | ((`U` | `u`)?   ((`D` | `d`) | (`S` | `s`) | (`L` | `l`))?   (`I` | `i`)   (`N` | `n`)   (`T` | `t`)   `#`);

**SIGN** ::= `+` | `-`;

**BINARY_DIGIT_STRING** ::= `2#`   _BINARY_DIGIT_   (_BINARY_DIGIT_ | `_`)*;

**OCTAL_DIGIT_STRING** ::= `8#`   _OCTAL_DIGIT_   (_OCTAL_DIGIT_ | `_`)*;

**HEXA_DIGIT_STRING** ::= `16#`   _HEXA_DIGIT_   (_HEXA_DIGIT_ | `_`)*;

**REAL_PREFIX** ::= (`L` | `l`)?   (`R` | `r`)   (`E` | `e`)   (`A` | `a`)   (`L` | `l`)   `#`;

**EXPONENT** ::= (`e` | `E`)   _SIGN_?   _INTSTR_;

**CHARACTER_PREFIX** ::= (`C` | `c`)   (`H` | `h`)   (`A` | `a`)   (`R` | `r`)   `#`;

**CHARACTER** ::= !(`'` | `$`) | (`$`   (`P` | `L` | `R` | `T` | `N` | `'` | `$`)) | (`$`   _HEXA_DIGIT_   _HEXA_DIGIT_);

**DATE_PREFIX** ::= `D#` | `d#` | ((`D` | `d`)   (`A` | `a`)   (`T` | `t`)   (`E` | `e`)   `#`);

**DATE_VALUE** ::= _INTSTR_   `-`   _INTSTR_   `-`   _INTSTR_;

**DATE_AND_TIME_PREFIX** ::= ((`L` | `l`)?   (`D` | `d`)   (`T` | `t`)   `#`) | ((`L` | `l`)?   (`D` | `d`)   (`A` | `a`)   (`T` | `t`)   (`E` | `e`)   `_`   (`A` | `a`)   (`N` | `n`)   (`D` | `d`)   `_`   (`T` | `t`)   (`I` | `i`)   (`M` | `m`)   (`E` | `e`)   `#`);

**TIME_OF_DAY_VALUE** ::= _INTSTR_   `:`   _INTSTR_   `:`   _INTSTR_   (`.`   _INTSTR_)?;

**TIME_OF_DAY_PREFIX** ::= ((`L` | `l`)?   (`T` | `t`)   (`O` | `o`)   (`D` | `d`)   `#`) | ((`L` | `l`)?   (`T` | `t`)   (`I` | `i`)   (`M` | `m`)   (`E` | `e`)   `_`   (`O` | `o`)   (`F` | `f`)   `_`   (`D` | `d`)   (`A` | `a`)   (`Y` | `y`)   `#`);

**TIME_PREFIX** ::= `T#` | `t#` | ((`L` | `l`)?   (`T` | `t`)   (`I` | `i`)   (`M` | `m`)   (`E` | `e`)   `#`);

**SIMPLE_OR_COMPOSITE_TIME** ::= (_DAYS_   _HOURS_?   _MINUTES_?   _SECONDS_?   _MILLISECONDS_?) | (_HOURS_   _MINUTES_?   _SECONDS_?   _MILLISECONDS_?) | (_MINUTES_   _SECONDS_?   _MILLISECONDS_?) | (_SECONDS_   _MILLISECONDS_?) | _MILLISECONDS_?;

**EqualityExpression** ::= _ComparisonExpression_   (   _EqualityOperator_   _ComparisonExpression_)?;

**CaseElementValue** ::= (   _NamedOrUnnamedConstantRef_) | (   _NamedOrUnnamedConstantRef_   _DOUBLE_DOT_   _NamedOrUnnamedConstantRef_) | (   _CONSTANT_RANGE_);

**CallParameterItem** ::= (   _IdLike_   _AssignmentOperator_   _Expression_) | (   `EN`   _AssignmentOperator_   _Expression_) | (   (`RET_VAL` | `#RET_VAL`)   _AssignmentOperator_   _Expression_);

**StlBitLogicOpMnemonic** ::= `A` | `AN` | `O` | `ON` | `X` | `XN`;

**UnaryOrPrimaryExpression** ::= _PrimaryExpression_ | _UnaryExpression_;

**StlShiftRotateStatement** ::= _StlShiftRotateMnemonic_   _NamedOrUnnamedConstantRef_?   `;`;

**StlWordLogicMnemonic** ::= `AW` | `OW` | `XOW` | `AD` | `OD` | `XOD`;

**StlComparisonMnemonic** ::= `==` | `<>` | `<` | `>` | `<=` | `>=`;

**StlComparisonDataType** ::= `I` | `D` | `R`;

**StlConversionMnemonic** ::= `ITD` | `DTR` | `INVI` | `INVD` | `NEGI` | `NEGD` | `NEGR` | `RND` | `TRUNC` | `RND+` | `RND-` | `BTI` | `BTD` | `ITB` | `DTB` | `CAW` | `CAD`;

**StlArithmeticMnemonic** ::= `+` | `-` | `*` | `/`;

**StlFloatMathFunctionMnemonic** ::= `ABS` | `SQR` | `SQRT` | `EXP` | `LN` | `SIN` | `COS` | `TAN` | `ASIN` | `ACOS` | `ATAN`;

**StlBlockEndMnemonic** ::= `BE` | `BEC` | `BEU`;

**StlMcrMnemonic** ::= `MCR(` | `)MCR` | `MCRA` | `MCRD`;

**StlAccuArgumentlessMnemonic** ::= `TAK` | `PUSH` | `POP` | `ENT` | `LEAVE`;

**StlJumpMnemonic** ::= `JU` | `JC` | `JCN` | `JCB` | `JNB` | `JBI` | `JNBI` | `JO` | `JOS` | `JZ` | `JN` | `JP` | `JM` | `JPZ` | `JMZ` | `JUO` | `LOOP`;

**StlParameterlessCallMnemonic** ::= `UC` | `CC`;

**DATABLOCK_FIELD_INDEXED_PREFIX** ::= (`D` | `d`)   _NONBIT_DATA_ELEMENT_SIZE_   `[`;

**StwBitRefMnemonic** ::= `OV` | `OS` | `BR`;

**BINARY_DIGIT** ::= `0` | `1`;

**OCTAL_DIGIT** ::= [`0`..`7`];

**HEXA_DIGIT** ::= [`0`..`9`] | [`a`..`f`] | [`A`..`F`];

**DAYS** ::= (_INTSTR_ | _FIX_POINT_NUMBER_)   _DAYS_ABBREV_   `_`?;

**HOURS** ::= (_INTSTR_ | _FIX_POINT_NUMBER_)   _HOURS_ABBREV_   `_`?;

**MINUTES** ::= (_INTSTR_ | _FIX_POINT_NUMBER_)   _MINUTES_ABBREV_   `_`?;

**SECONDS** ::= (_INTSTR_ | _FIX_POINT_NUMBER_)   _SECONDS_ABBREV_   `_`?;

**MILLISECONDS** ::= _INTSTR_   _MILLISECONDS_ABBREV_   `_`?;

**ComparisonExpression** ::= _AdditiveExpression_   (   _ComparisonOperator_   _AdditiveExpression_)?;

**EqualityOperator** ::= `=` | `<>`;

**AssignmentOperator** ::= `:=` | `=>` | `:=>`;

**PrimaryExpression** ::= _UnnamedConstant_ | _LeftValue_ | (`(`   _Expression_   `)`) | _SclSubroutineCall_;

**UnaryExpression** ::=    _UnaryOperator_   _UnaryOrPrimaryExpression_;

**StlShiftRotateMnemonic** ::= `SSI` | `SSD` | `SLW` | `SRW` | `SLD` | `SRD` | `RLD` | `RRD` | `RLDA` | `RRDA`;

**DAYS_ABBREV** ::= `D` | `d`;

**HOURS_ABBREV** ::= `H` | `h`;

**MINUTES_ABBREV** ::= `M` | `m`;

**SECONDS_ABBREV** ::= `S` | `s`;

**MILLISECONDS_ABBREV** ::= (`M` | `m`)   (`S` | `s`);

**AdditiveExpression** ::= _MultiplicativeExpression_   (   _AdditionOperator_   _MultiplicativeExpression_)*;

**ComparisonOperator** ::= `<` | `>` | `<=` | `>=`;

**UnaryOperator** ::= `NOT` | `+` | `-`;

**MultiplicativeExpression** ::= _PowerExpression_   (   _MultiplicationOperator_   _PowerExpression_)*;

**AdditionOperator** ::= `+` | `-`;

**PowerExpression** ::= _UnaryOrPrimaryExpression_   (   `**`   _UnaryOrPrimaryExpression_)?;

**MultiplicationOperator** ::= `*` | `/` | `MOD` | `DIV`;


