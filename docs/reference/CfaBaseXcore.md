##  CfaBase.xcore metamodel description

###  Table of contents
- [Annotable](#anchor12)
- [Annotation](#anchor11)
- [ArrayDimension](#anchor9)
- [ArrayType](#anchor10)
- [AssertionAnnotation](#anchor24)
- [AutomatonAnnotation](#anchor13)
- [AutomatonBase](#anchor4)
- [BlockAutomatonAnnotation](#anchor14)
- [BlockReturnTransitionAnnotation](#anchor29)
- [CaseLocationAnnotation](#anchor18)
- [CfaNetworkBase](#anchor3)
- [ContinueTransitionAnnotation](#anchor27)
- [DataDirection](#anchor8)
- [ElseExpression](#anchor7)
- [ExitTransitionAnnotation](#anchor28)
- [Freezable](#anchor2)
- [GotoTransitionAnnotation](#anchor26)
- [IfLocationAnnotation](#anchor16)
- [LabelledLocationAnnotation](#anchor22)
- [LineNumberAnnotation](#anchor23)
- [Location](#anchor5)
- [LocationAnnotation](#anchor15)
- [LoopLocationAnnotation](#anchor19)
- [NamedElement](#anchor1)
- [RepeatLoopLocationAnnotation](#anchor21)
- [SwitchLocationAnnotation](#anchor17)
- [Transition](#anchor6)
- [TransitionAnnotation](#anchor25)
- [WhileLoopLocationAnnotation](#anchor20)

###  Package `cern.plcverif.base.models.cfa.cfabase`

####  Abstract class `NamedElement` {#anchor1}

_Abstract superclass for elements with mandatory names._

**Extends**: `EObject`

**Attributes**:
- **name**  [1..1]: `EString`
    * _Name of the element (unique identifier in the scope of the _
      _		containing element, used as FQN segment)._
- **displayName**  [1..1]: `EString`
    * _Name of the element (used for human-readable representation)._



```
@GenModel(documentation="Abstract superclass for elements with mandatory names.")
abstract class NamedElement {
				@GenModel(documentation="Name of the element (unique identifier in the scope of the 
		containing element, used as FQN segment).")
	String [1] name
	@GenModel(documentation="Name of the element (used for human-readable representation).")
	String [1] displayName
}
```
####  Abstract class `Freezable` {#anchor2}

_Abstract superclass for elements that can be frozen. Freezing an element _
_	delivers the message that it should not be modified or removed during model transformations._

**Extends**: `EObject`

**Attributes**:
- **frozen** : `EBoolean`



```
@GenModel(documentation="Abstract superclass for elements that can be frozen. Freezing an element 
	delivers the message that it should not be modified or removed during model transformations.")
abstract class Freezable {
	boolean frozen
		}
```
####  Abstract class `CfaNetworkBase` {#anchor3}

_Class to represent a network of control flow automata (CFA)._

**Extends**: [NamedElement](#anchor1)



**Operations**:
- **getMainAutomaton**() : [AutomatonBase](#anchor4)

```
@GenModel(documentation="Class to represent a network of control flow automata (CFA).")
abstract class CfaNetworkBase extends NamedElement {
	op AutomatonBase getMainAutomaton() { throw new UnsupportedOperationException("Must be overridden by the subclasses."); }
}
```
####  Abstract class `AutomatonBase` {#anchor4}

_Class to represent a control flow automaton._

**Extends**: [NamedElement](#anchor1), [Annotable](#anchor12)


**References**:
- **locations**  [0..*]: [Location](#anchor5)
    * _Locations defined in the automaton._
    * Containment: contains
    * Opposite: [Location](#anchor5).`parentAutomaton`
- **initialLocation**  [1..1]: [Location](#anchor5)
    * _The initial location of the automaton._
    * Containment: refers
- **endLocation**  [1..1]: [Location](#anchor5)
    * _The final location of the automaton._
    * Containment: refers
- **transitions**  [0..*]: [Transition](#anchor6)
    * _Transitions contained in the automaton._
    * Containment: contains
    * Opposite: [Transition](#anchor6).`parentAutomaton`
- **annotations**  [0..*]: [AutomatonAnnotation](#anchor13)
    * _Annotations of the automaton. Used for code generation and traceability._
    * Containment: contains
    * Opposite: [AutomatonAnnotation](#anchor13).`parentAutomaton`

**Operations**:
- **getContainer**() : [CfaNetworkBase](#anchor3)
    * _Returns the CFA network containing this automaton. Should be realized in subclasses._

```
@GenModel(documentation="Class to represent a control flow automaton.")
abstract class AutomatonBase extends NamedElement, Annotable {
	@GenModel(documentation="Returns the CFA network containing this automaton. Should be realized in subclasses.")
	op CfaNetworkBase getContainer() { throw new UnsupportedOperationException("Must be overridden by the subclasses."); }
	@GenModel(documentation="Locations defined in the automaton.")
	contains Location[*] locations opposite parentAutomaton
		@GenModel(documentation="The initial location of the automaton.")
	refers Location [1] initialLocation
		@GenModel(documentation="The final location of the automaton.")
	refers Location [1] endLocation
	@GenModel(documentation="Transitions contained in the automaton.")
	contains Transition[*] transitions opposite parentAutomaton
	@GenModel(documentation="Annotations of the automaton. Used for code generation and traceability.")
	contains AutomatonAnnotation[*] annotations opposite parentAutomaton 
}
```
####  Class `Location` {#anchor5}

_Class to represent a CFA control location._

**Extends**: [NamedElement](#anchor1), [Freezable](#anchor2), [Annotable](#anchor12)


**References**:
- **parentAutomaton**  [1..1]: [AutomatonBase](#anchor4)
    * _Automaton which contains the location._
    * Containment: container
    * Opposite: [AutomatonBase](#anchor4).`locations`
- **incoming**  [0..*]: [Transition](#anchor6)
    * _Incoming transitions of the location._
    * Containment: refers
    * Opposite: [Transition](#anchor6).`target`
- **outgoing**  [0..*]: [Transition](#anchor6)
    * _Outgoing transitions of the location._
    * Containment: refers
    * Opposite: [Transition](#anchor6).`source`
- **annotations**  [0..*]: [LocationAnnotation](#anchor15)
    * _Annotations of the location. Used for code generation and traceability._
    * Containment: contains
    * Opposite: [LocationAnnotation](#anchor15).`parentLocation`


```
@GenModel(documentation="Class to represent a CFA control location.")
class Location extends NamedElement, Freezable, Annotable {
	@GenModel(documentation="Automaton which contains the location.")
	container AutomatonBase [1] parentAutomaton opposite locations
	@GenModel(documentation="Incoming transitions of the location.")
	refers Transition[*] incoming opposite target
	@GenModel(documentation="Outgoing transitions of the location.")
	refers Transition[*] outgoing opposite source
	@GenModel(documentation="Annotations of the location. Used for code generation and traceability.")
	contains LocationAnnotation[*] annotations opposite parentLocation 
}
```
####  Abstract class `Transition` {#anchor6}

_Abstract superclass to represent a generic CFA transition. Subclasses are _
_	AssignmentTransition and CallTransition, each implemented for both CFA declarations and instances._

**Extends**: [NamedElement](#anchor1), [Freezable](#anchor2), [Annotable](#anchor12)


**References**:
- **parentAutomaton**  [1..1]: [AutomatonBase](#anchor4)
    * _Automaton which contains the transition._
    * Containment: container
    * Opposite: [AutomatonBase](#anchor4).`transitions`
- **source**  [1..1]: [Location](#anchor5)
    * _Source location of the transition._
    * Containment: refers
    * Opposite: [Location](#anchor5).`outgoing`
- **target**  [1..1]: [Location](#anchor5)
    * _Target location of the transition._
    * Containment: refers
    * Opposite: [Location](#anchor5).`incoming`
- **condition**  [1..1]: `Expression` (unresolved)
    * _Condition of the transition._
    * Containment: contains
- **annotations**  [0..*]: [TransitionAnnotation](#anchor25)
    * _Annotations of the transition. Used for code generation and traceability._
    * Containment: contains
    * Opposite: [TransitionAnnotation](#anchor25).`parentTransition`


```
@GenModel(documentation="Abstract superclass to represent a generic CFA transition. Subclasses are 
	AssignmentTransition and CallTransition, each implemented for both CFA declarations and instances.")
abstract class Transition extends NamedElement, Freezable, Annotable {
	@GenModel(documentation="Automaton which contains the transition.")
	container AutomatonBase [1] parentAutomaton opposite transitions
	@GenModel(documentation="Source location of the transition.")
	refers Location [1] source opposite outgoing
	@GenModel(documentation="Target location of the transition.")
	refers Location [1] target opposite incoming
	@GenModel(documentation="Condition of the transition.")
	contains Expression [1] condition
	@GenModel(documentation="Annotations of the transition. Used for code generation and traceability.")
	contains TransitionAnnotation[*] annotations opposite parentTransition 
}
```
####  Class `ElseExpression` {#anchor7}

_Special expression to denote 'none of the conditions _
_of the other outgoing transitions is true'. If this is the guard of the only _
_transition leaving a location, it is equivalent to the 'true' literal._

**Extends**: `Expression` (unresolved), `ExplicitlyTyped` (unresolved)




```
@GenModel(documentation="Special expression to denote 'none of the conditions 
of the other outgoing transitions is true'. If this is the guard of the only 
transition leaving a location, it is equivalent to the 'true' literal.")
class ElseExpression extends Expression, ExplicitlyTyped {
}
```
####  Class `ArrayDimension` {#anchor9}

_Represents a dimension of an array type. A dimension may be undefined, that is, of _
_	unknown size, although this is not supported in most of the CFA transformation libraries. Array indices _
_	may be negative. The array indices are expected to be signed 16-bit integers._

**Extends**: `EObject`

**Attributes**:
- **defined** : `EBoolean`
    * _If true, de values of 'lowerBound' and 'upperBound' specify the smallest and largest _
      _		index that can be used to index this dimension of the corresponding array. Otherwise the array can be_
      _		indexed with any value or expression, because the size is assumed to be unknown or arbitrary._
- **lowerIndex** : `EInt`
    * _Inclusive lower bound for the index used for this dimension. May be negative. _
      _		Should not be used if 'defined' is false._
    * Default value: -1
- **upperIndex** : `EInt`
    * _Inclusive upper bound for the index used for this dimension. May be negative. _
      _		Should not be used if 'defined' is false._
    * Default value: -1


**Operations**:
- **getSize**() : `EInt`
    * _The number of elements in this dimension of the corresponding array. Throws an _
    _		exception if called on an array of undefined size._
- **isValidIndex**(index :  `ELong`) : `EBoolean`
    * _Returns true if the specified index may be used as an index in this dimension._
- **dimensionEquals**(other :  [ArrayDimension](#anchor9)) : `EBoolean`
    * _Returns true if the specified dimension has the same features as the current one._

```
@GenModel(documentation="Represents a dimension of an array type. A dimension may be undefined, that is, of 
	unknown size, although this is not supported in most of the CFA transformation libraries. Array indices 
	may be negative. The array indices are expected to be signed 16-bit integers.")
class ArrayDimension {
	@GenModel(documentation="If true, de values of 'lowerBound' and 'upperBound' specify the smallest and largest 
		index that can be used to index this dimension of the corresponding array. Otherwise the array can be
		indexed with any value or expression, because the size is assumed to be unknown or arbitrary.")
	boolean defined
	@GenModel(documentation="Inclusive lower bound for the index used for this dimension. May be negative. 
		Should not be used if 'defined' is false.")
	int lowerIndex = "-1"
	@GenModel(documentation="Inclusive upper bound for the index used for this dimension. May be negative. 
		Should not be used if 'defined' is false.")
	int upperIndex = "-1"
	@GenModel(documentation="The number of elements in this dimension of the corresponding array. Throws an 
		exception if called on an array of undefined size.")
	op int getSize() {
		if (defined) {
			return upperIndex - lowerIndex + 1;
		}
		else {
			throw new InvalidUndefinedArrayOperation("Undefined arrays do not have a size.");
		}
	}
	@GenModel(documentation="Returns true if the specified index may be used as an index in this dimension.")
	op boolean isValidIndex(long index) {
		if (isDefined) {
			return index >= lowerIndex && index <= upperIndex;
		}
		else {
			return true;
		}
	}
	@GenModel(documentation="Returns true if the specified dimension has the same features as the current one.")
	op boolean dimensionEquals(ArrayDimension other) {
		return defined == other.defined && lowerIndex == other.lowerIndex && upperIndex == other.upperIndex;
	}
}
```
####  Class `ArrayType` {#anchor10}

_Represents an array type. Multidimensional arrays must be represented as arrays of arrays._

**Extends**: `Type` (unresolved)


**References**:
- **elementType**  [1..1]: `Type` (unresolved)
    * Containment: contains
- **dimension**  [1..1]: [ArrayDimension](#anchor9)
    * Containment: contains

**Operations**:
- **dataTypeEquals**(other :  `Type` (unresolved)) : `EBoolean`
    * _Two array types are equal if they have the same dimensions and the same type of elements._

```
@GenModel(documentation="Represents an array type. Multidimensional arrays must be represented as arrays of arrays.")
class ArrayType extends Type {
	contains Type [1] elementType
	contains ArrayDimension [1] dimension 
	@GenModel(documentation="Two array types are equal if they have the same dimensions and the same type of elements.")
	op boolean dataTypeEquals(Type other) {
		if (other instanceof ArrayType) {
			return elementType.dataTypeEquals(other.elementType) && dimension.dimensionEquals(other.dimension)
		}
		return false
	}
}
```
####  Abstract class `Annotation` {#anchor11}

_Abstract superclass for any kind of annotations._

**Extends**: `EObject`




```
@GenModel(documentation="Abstract superclass for any kind of annotations.")
abstract class Annotation {}
```
####  Interface `Annotable` {#anchor12}


**Extends**: `EObject`



**Operations**:
- **getAnnotations**() : `EEList`

```
interface Annotable {
	op EEList<? extends Annotation> getAnnotations()
}
```
####  Abstract class `AutomatonAnnotation` {#anchor13}

_Abstract superclass for all automaton annotations._

**Extends**: [Annotation](#anchor11)


**References**:
- **parentAutomaton**  [1..1]: [AutomatonBase](#anchor4)
    * Containment: container
    * Opposite: [AutomatonBase](#anchor4).`annotations`


```
@GenModel(documentation="Abstract superclass for all automaton annotations.")
abstract class AutomatonAnnotation extends Annotation {
	container AutomatonBase [1] parentAutomaton opposite annotations
}
```
####  Class `BlockAutomatonAnnotation` {#anchor14}

_Automaton annotation to represent a specific block in PLC code._

**Extends**: [AutomatonAnnotation](#anchor13)

**Attributes**:
- **blockType**  [0..1]: `EString`
- **name**  [1..1]: `EString`
- **stateful** : `EBoolean`
    * Default value: true



```
@GenModel(documentation="Automaton annotation to represent a specific block in PLC code.")
class BlockAutomatonAnnotation extends AutomatonAnnotation {
	String [0..1] blockType
	String [1] name
	boolean stateful = "true"
}
```
####  Abstract class `LocationAnnotation` {#anchor15}

_Abstract superclass for all location annotations._

**Extends**: [Annotation](#anchor11)


**References**:
- **parentLocation**  [1..1]: [Location](#anchor5)
    * Containment: container
    * Opposite: [Location](#anchor5).`annotations`


```
@GenModel(documentation="Abstract superclass for all location annotations.")
abstract class LocationAnnotation extends Annotation {
	container Location [1] parentLocation opposite annotations
}
```
####  Class `IfLocationAnnotation` {#anchor16}

_Location annotation to represent an IF statement that starts at the given location._

**Extends**: [LocationAnnotation](#anchor15)


**References**:
- **endsAt**  [1..1]: [Location](#anchor5)
    * _The location where the two paths merge again._
    * Containment: refers
- **then**  [1..1]: [Transition](#anchor6)
    * _The transition which is followed in case the condition is true._
    * Containment: refers


```
@GenModel(documentation="Location annotation to represent an IF statement that starts at the given location.")
class IfLocationAnnotation extends LocationAnnotation {
	@GenModel(documentation="The location where the two paths merge again.")	
	refers Location [1] endsAt
	@GenModel(documentation="The transition which is followed in case the condition is true.")
	refers Transition [1] then
}
```
####  Class `SwitchLocationAnnotation` {#anchor17}

_Location annotation to represent a SWITCH statement that starts at the given location._

**Extends**: [LocationAnnotation](#anchor15)


**References**:
- **endsAt** : [Location](#anchor5)
    * _The location where the paths of the cases merge again._
    * Containment: refers
- **cases**  [0..*]: [CaseLocationAnnotation](#anchor18)
    * _The annotations marking the locations where the paths of each case start._
    * Containment: refers
    * Opposite: [CaseLocationAnnotation](#anchor18).`switchAnnotation`
- **elseCase** : [CaseLocationAnnotation](#anchor18)
    * _The annotation marking the location where the path of the default or else case starts._
    * Containment: refers
- **selectionExpr** : `Expression` (unresolved)
    * _The selection expression used to choose the path of execution._
    * Containment: contains


```
@GenModel(documentation="Location annotation to represent a SWITCH statement that starts at the given location.")
class SwitchLocationAnnotation extends LocationAnnotation {
	@GenModel(documentation="The location where the paths of the cases merge again.")	
	refers Location endsAt
	@GenModel(documentation="The annotations marking the locations where the paths of each case start.")	
	refers CaseLocationAnnotation[] cases opposite switchAnnotation
	@GenModel(documentation="The annotation marking the location where the path of the default or else case starts.")
	refers CaseLocationAnnotation elseCase
	@GenModel(documentation="The selection expression used to choose the path of execution.")
	contains Expression selectionExpr
}
```
####  Class `CaseLocationAnnotation` {#anchor18}

_Location annotation to represent a CASE statement that starts at the given location._

**Extends**: [LocationAnnotation](#anchor15)


**References**:
- **switchAnnotation** : [SwitchLocationAnnotation](#anchor17)
    * Containment: refers
    * Opposite: [SwitchLocationAnnotation](#anchor17).`cases`


```
@GenModel(documentation="Location annotation to represent a CASE statement that starts at the given location.")
class CaseLocationAnnotation extends LocationAnnotation {
	refers SwitchLocationAnnotation switchAnnotation opposite cases
}
```
####  Abstract class `LoopLocationAnnotation` {#anchor19}

_Location annotation to represent an abstract loop statement that starts at the given location._

**Extends**: [LocationAnnotation](#anchor15)


**References**:
- **loopBodyEntry**  [1..1]: [Transition](#anchor6)
    * _The transition which transfers control from loop administration to the actual body._
    * Containment: refers
- **loopNextCycle**  [1..1]: [Transition](#anchor6)
    * _The transition which transfers control from the loop body back to the beginning of the cycle._
    * Containment: refers
- **loopExit**  [1..1]: [Transition](#anchor6)
    * _The transition which transfers control out of the loop._
    * Containment: refers


```
@GenModel(documentation="Location annotation to represent an abstract loop statement that starts at the given location.")
abstract class LoopLocationAnnotation extends LocationAnnotation {
	@GenModel(documentation="The transition which transfers control from loop administration to the actual body.")
	refers Transition [1] loopBodyEntry
	@GenModel(documentation="The transition which transfers control from the loop body back to the beginning of the cycle.")
	refers Transition [1] loopNextCycle
	@GenModel(documentation="The transition which transfers control out of the loop.")
	refers Transition [1] loopExit
}
```
####  Class `WhileLoopLocationAnnotation` {#anchor20}

_Location annotation to represent a WHILE loop statement that starts at the given location._

**Extends**: [LoopLocationAnnotation](#anchor19)




```
@GenModel(documentation="Location annotation to represent a WHILE loop statement that starts at the given location.")
class WhileLoopLocationAnnotation extends LoopLocationAnnotation {
}
```
####  Class `RepeatLoopLocationAnnotation` {#anchor21}

_Location annotation to represent a REPEAT loop statement that starts at the given location._

**Extends**: [LoopLocationAnnotation](#anchor19)




```
@GenModel(documentation="Location annotation to represent a REPEAT loop statement that starts at the given location.")
class RepeatLoopLocationAnnotation extends LoopLocationAnnotation {
}
```
####  Class `LabelledLocationAnnotation` {#anchor22}

_Location annotation to represent a labeled statement at the given location._

**Extends**: [LocationAnnotation](#anchor15)

**Attributes**:
- **labelName**  [1..1]: `EString`



```
@GenModel(documentation="Location annotation to represent a labeled statement at the given location.")
class LabelledLocationAnnotation extends LocationAnnotation {
	String [1] labelName
}
```
####  Class `LineNumberAnnotation` {#anchor23}

_Location annotation to represent the original line number corresponding to the location._

**Extends**: [LocationAnnotation](#anchor15)

**Attributes**:
- **fileName**  [1..1]: `EString`
    * Default value: unknown
- **lineNumber** : `EInt`



```
@GenModel(documentation="Location annotation to represent the original line number corresponding to the location.")
class LineNumberAnnotation extends LocationAnnotation {
	String [1] fileName = "unknown"
	int lineNumber
}
```
####  Class `AssertionAnnotation` {#anchor24}

_Location annotation to represent an assertion, i.e., a Boolean expression that shall always be satisfied._

**Extends**: [LocationAnnotation](#anchor15)

**Attributes**:
- **name**  [0..1]: `EString`

**References**:
- **invariant**  [1..1]: `Expression` (unresolved)
    * Containment: contains


```
@GenModel(documentation="Location annotation to represent an assertion, i.e., a Boolean expression that shall always be satisfied.")
class AssertionAnnotation extends LocationAnnotation {
	contains Expression [1] invariant
	String [0..1] name }
```
####  Abstract class `TransitionAnnotation` {#anchor25}

_Abstract superclass for all transition annotations._

**Extends**: [Annotation](#anchor11)


**References**:
- **parentTransition**  [1..1]: [Transition](#anchor6)
    * Containment: container
    * Opposite: [Transition](#anchor6).`annotations`


```
@GenModel(documentation="Abstract superclass for all transition annotations.")
abstract class TransitionAnnotation extends Annotation {
	container Transition [1] parentTransition opposite annotations
}
```
####  Class `GotoTransitionAnnotation` {#anchor26}

_Transition annotation to represent a GOTO statement that is represented by the given transition._

**Extends**: [TransitionAnnotation](#anchor25)

**Attributes**:
- **targetLabel**  [1..1]: `EString`



```
@GenModel(documentation="Transition annotation to represent a GOTO statement that is represented by the given transition.")
class GotoTransitionAnnotation extends TransitionAnnotation {
	String [1] targetLabel
}
```
####  Class `ContinueTransitionAnnotation` {#anchor27}

_Transition annotation to represent a CONTINUE statement that is represented by the given transition._

**Extends**: [TransitionAnnotation](#anchor25)




```
@GenModel(documentation="Transition annotation to represent a CONTINUE statement that is represented by the given transition.")
class ContinueTransitionAnnotation extends TransitionAnnotation {
}
```
####  Class `ExitTransitionAnnotation` {#anchor28}

_Transition annotation to represent a EXIT statement that is represented by the given transition._

**Extends**: [TransitionAnnotation](#anchor25)




```
@GenModel(documentation="Transition annotation to represent a EXIT statement that is represented by the given transition.")
class ExitTransitionAnnotation extends TransitionAnnotation {
}
```
####  Class `BlockReturnTransitionAnnotation` {#anchor29}

_Transition annotation to represent a RETURN statement that is represented by the given transition._

**Extends**: [TransitionAnnotation](#anchor25)




```
@GenModel(documentation="Transition annotation to represent a RETURN statement that is represented by the given transition.")
class BlockReturnTransitionAnnotation extends TransitionAnnotation {
}
```

####  Enum `DataDirection` {#anchor8}

_Represents the possible modes in which a field or variable should be interpreted _
_	or used. Used by DirectionFieldAnnotation and DirectionVariableAnnotation. INPUT and INOUT fields or_
_	variables are initialized nondeterministically in the main automaton, unless overridden by settings._

**Literals**:
- `INPUT` 
- `OUTPUT` 
- `INOUT` 
- `LOCAL` 
- `TEMP` 
- `PARAMETER` 

```
@GenModel(documentation="Represents the possible modes in which a field or variable should be interpreted 
	or used. Used by DirectionFieldAnnotation and DirectionVariableAnnotation. INPUT and INOUT fields or
	variables are initialized nondeterministically in the main automaton, unless overridden by settings.")
enum DataDirection {
	INPUT, OUTPUT, INOUT, LOCAL, TEMP, PARAMETER
}
```

