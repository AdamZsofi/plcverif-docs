##  CfaDeclaration.xcore metamodel description

###  Table of contents
- [AssignmentTransition](#anchor9)
- [AutomatonDeclaration](#anchor8)
- [Call](#anchor11)
- [CallTransition](#anchor10)
- [CfaNetworkDeclaration](#anchor7)
- [DataRef](#anchor4)
- [DataStructure](#anchor1)
- [DataStructureAnnotation](#anchor13)
- [DataStructureRef](#anchor2)
- [DataStructureSourceType](#anchor15)
- [DirectionFieldAnnotation](#anchor17)
- [Field](#anchor3)
- [FieldAnnotation](#anchor16)
- [FieldRef](#anchor5)
- [ForLoopLocationAnnotation](#anchor21)
- [Indexing](#anchor6)
- [InternalGeneratedFieldAnnotation](#anchor20)
- [OriginalDataTypeFieldAnnotation](#anchor18)
- [ReturnValueFieldAnnotation](#anchor19)
- [SourceTypeDataStructureAnnotation](#anchor14)
- [VariableAssignment](#anchor12)

###  Package `cern.plcverif.base.models.cfa.cfadeclaration`

####  Class `DataStructure` {#anchor1}

_Class to represent a complex type definition with fields and other embedded complex types._
_	Note that a DataStructure is not a Type and shall be unique in the model. Use DataStructureRef if you need a Type._

**Extends**: `NamedElement` (unresolved), `Annotable` (unresolved)


**References**:
- **fields**  [0..*]: [Field](#anchor3)
    * _The collection of fields that define the layout of this data structure. May be empty._
    * Containment: contains
    * Opposite: [Field](#anchor3).`enclosingType`
- **complexTypes**  [0..*]: [DataStructure](#anchor1)
    * _The collection of data structures nested in this data structure._
    * Containment: contains
    * Opposite: [DataStructure](#anchor1).`enclosingType`
- **enclosingType**  [0..1]: [DataStructure](#anchor1)
    * _The data structure in which the current data structure is nested. If null, _
      _		the data structure is the global root data structure of a CFA network._
    * Containment: refers
    * Opposite: [DataStructure](#anchor1).`complexTypes`
- **annotations**  [0..*]: [DataStructureAnnotation](#anchor13)
    * _Annotations of the data structure. Used for code generation and traceability._
    * Containment: contains
    * Opposite: [DataStructureAnnotation](#anchor13).`parentDataStructure`


```
@GenModel(documentation="Class to represent a complex type definition with fields and other embedded complex types.
	Note that a DataStructure is not a Type and shall be unique in the model. Use DataStructureRef if you need a Type.")
class DataStructure extends NamedElement, Annotable {
	@GenModel(documentation="The collection of fields that define the layout of this data structure. May be empty.")
	contains Field [*] fields opposite enclosingType
	@GenModel(documentation="The collection of data structures nested in this data structure.")
	contains DataStructure [*] complexTypes opposite enclosingType
	@GenModel(documentation="The data structure in which the current data structure is nested. If null, 
		the data structure is the global root data structure of a CFA network.")
	refers DataStructure [0..1] enclosingType opposite complexTypes
	@GenModel(documentation="Annotations of the data structure. Used for code generation and traceability.")
	contains DataStructureAnnotation [*] annotations opposite parentDataStructure
}
```
####  Class `DataStructureRef` {#anchor2}

_Class to represent a complex type defined by a DataStructure._

**Extends**: `Type` (unresolved)


**References**:
- **definition**  [1..1]: [DataStructure](#anchor1)
    * _The DataStrcture that defines the type._
    * Containment: refers

**Operations**:
- **dataTypeEquals**(other :  `Type` (unresolved)) : `EBoolean`
    * _Two data structure references are equal if they have the same definition (which should be unique)._

```
@GenModel(documentation="Class to represent a complex type defined by a DataStructure.")
class DataStructureRef extends Type {
	@GenModel(documentation="The DataStrcture that defines the type.")
	refers DataStructure [1] definition
	@GenModel(documentation="Two data structure references are equal if they have the same definition (which should be unique).")
	op boolean dataTypeEquals(Type other) {
		if (other instanceof DataStructureRef) {
			return definition == other.definition
		}
		return false
	}
}
```
####  Class `Field` {#anchor3}

_Represents a data field of a complex data structure. Note that fields are unique in the model,_
_	referencing them is possible via FieldRef instances, which may be copied. This element can be frozen (see Freezable)._

**Extends**: `NamedElement` (unresolved), `ExplicitlyTyped` (unresolved), `Freezable` (unresolved), `Annotable` (unresolved)


**References**:
- **enclosingType**  [1..1]: [DataStructure](#anchor1)
    * _The data structure that defines this field._
    * Containment: container
    * Opposite: [DataStructure](#anchor1).`fields`
- **initialAssignments**  [0..*]: [VariableAssignment](#anchor12)
    * _The set of initial assignments related to this field. In addition to setting the initial value _
      _		of fields of elementary type, it is also possible to set elements of arrays or override the initial values of the _
      _		subfields for this specific instance in case this is a field of complex type. Use _
      _		cern.plcverif.base.models.cfa.utils.ArrayInitialValueHelper to read and process the initial values of multidimensional_
      _		arrays (arrays of arrays)._
    * Containment: contains
- **annotations**  [0..*]: [FieldAnnotation](#anchor16)
    * _Annotations of the field. Used for code generation and traceability._
    * Containment: contains
    * Opposite: [FieldAnnotation](#anchor16).`parentField`

**Operations**:
- **toString**() : `EString`

```
@GenModel(documentation="Represents a data field of a complex data structure. Note that fields are unique in the model,
	referencing them is possible via FieldRef instances, which may be copied. This element can be frozen (see Freezable).")
class Field extends NamedElement, ExplicitlyTyped, Freezable, Annotable {
	@GenModel(documentation="The data structure that defines this field.")
	container DataStructure [1] enclosingType opposite fields
	@GenModel(documentation="The set of initial assignments related to this field. In addition to setting the initial value 
		of fields of elementary type, it is also possible to set elements of arrays or override the initial values of the 
		subfields for this specific instance in case this is a field of complex type. Use 
		cern.plcverif.base.models.cfa.utils.ArrayInitialValueHelper to read and process the initial values of multidimensional
		arrays (arrays of arrays).")
	contains VariableAssignment [*] initialAssignments
	@GenModel(documentation="Annotations of the field. Used for code generation and traceability.")
	contains FieldAnnotation[*] annotations opposite parentField
			op String toString() {
		return displayName + " : " + ^type.toString()
	}
}
```
####  Abstract class `DataRef` {#anchor4}

_Represents a reference to a piece of data. In the CFA declaration metamodel, data elements are only_
_	declared and not instantiated, so a reference has to specify a path to navigate through the types and specify an instance_
_	of a field or an array element._

**Extends**: `LeftValue` (unresolved), `Freezable` (unresolved)


**References**:
- **prefix**  [0..1]: [DataRef](#anchor4)
    * _The DataRef identifying the enclosing complex data type (an array or a data structure). _
      _		If not specified, the current reference is assumed to refer to a field in the local or global data structure_
      _		(see CfaNetworkDeclaration and AutomatonDeclaration)._
    * Containment: contains


```
@GenModel(documentation="Represents a reference to a piece of data. In the CFA declaration metamodel, data elements are only
	declared and not instantiated, so a reference has to specify a path to navigate through the types and specify an instance
	of a field or an array element.")
abstract class DataRef extends LeftValue, Freezable {
	@GenModel(documentation="The DataRef identifying the enclosing complex data type (an array or a data structure). 
		If not specified, the current reference is assumed to refer to a field in the local or global data structure
		(see CfaNetworkDeclaration and AutomatonDeclaration).")
				contains DataRef [0..1] prefix
}
```
####  Class `FieldRef` {#anchor5}

_Represents a reference to a field. Along with the prefix and the local or global data structure, it_
_	identifies a specific instance of the field._

**Extends**: [DataRef](#anchor4)


**References**:
- **field**  [1..1]: [Field](#anchor3)
    * _The unique field to which this FieldRef refers. The enclosing type of the field should always_
      _		be the type of the prefix DataRef (if any)._
    * Containment: refers

**Operations**:
- **getType**() : `Type` (unresolved)
    * _The type of a FieldRef is always the type of the referred field._
- **toString**() : `EString`

```
@GenModel(documentation="Represents a reference to a field. Along with the prefix and the local or global data structure, it
	identifies a specific instance of the field.")
class FieldRef extends DataRef {
	@GenModel(documentation="The unique field to which this FieldRef refers. The enclosing type of the field should always
		be the type of the prefix DataRef (if any).")
	refers Field [1] field
	@GenModel(documentation="The type of a FieldRef is always the type of the referred field.")
	op Type getType() {
		return field.^type
	}
			op String toString() {
		return (if (prefix === null) "" else (prefix.toString() + "/")) + field.displayName
	}
}
```
####  Class `Indexing` {#anchor6}

_Represents a reference to an element of an array. Along with the prefix and the local or global data _
_	structure, it identifies a specific instance of this element._

**Extends**: [DataRef](#anchor4)


**References**:
- **index**  [1..1]: `Expression` (unresolved)
    * _The index of the referenced element in the enclosing array. It is expected to have a signed 16-bit integer type._
    * Containment: contains
- **dimension**  [1..1]: `ArrayDimension` (unresolved)
    * _Returns the dimension of the enclosing array._
    * Containment: refers
    * Modifiers: Derived

**Operations**:
- **getType**() : `Type` (unresolved)
    * _Returns the element type of the enclosing array._
- **toString**() : `EString`

```
@GenModel(documentation="Represents a reference to an element of an array. Along with the prefix and the local or global data 
	structure, it identifies a specific instance of this element.")
class Indexing extends DataRef {
	@GenModel(documentation="The index of the referenced element in the enclosing array. It is expected to have a signed 16-bit integer type.")
	contains Expression [1] index
	@GenModel(documentation="Returns the dimension of the enclosing array.")
	refers derived ArrayDimension [1] dimension get {
		return (prefix.^type as ArrayType).dimension
	}
	@GenModel(documentation="Returns the element type of the enclosing array.")
	op Type getType() {
		return (prefix.^type as ArrayType).elementType
	}
			op String toString() {
		return (if (prefix === null) "null" else prefix.toString()) + "[" + index.toString() + "]"
	}
}
```
####  Class `CfaNetworkDeclaration` {#anchor7}

_Class to represent the declaration of a network of control flow automata (CFA)._

**Extends**: `CfaNetworkBase` (unresolved)


**References**:
- **automata**  [1..*]: [AutomatonDeclaration](#anchor8)
    * _Automata in the network._
    * Containment: contains
    * Opposite: [AutomatonDeclaration](#anchor8).`container`
- **mainAutomaton**  [1..1]: [AutomatonDeclaration](#anchor8)
    * _Automaton which starts the execution._
    * Containment: refers
- **rootDataStructure**  [1..1]: [DataStructure](#anchor1)
    * _The root data structure containing every other data structure and field. Fields of this _
      _		data structure are considered global._
    * Containment: contains
- **mainContext**  [1..1]: [DataRef](#anchor4)
    * _The variable representing the local context of the main CFA._
    * Containment: contains

**Operations**:
- **isGlobal**(reference :  [DataRef](#anchor4)) : `EBoolean`
    * _Returns true if the specified DataRef starts in the global root data structure._

```
@GenModel(documentation="Class to represent the declaration of a network of control flow automata (CFA).")
class CfaNetworkDeclaration extends CfaNetworkBase {
	@GenModel(documentation="Automata in the network.")
	contains AutomatonDeclaration [1..*] automata opposite ^container
	@GenModel(documentation="Automaton which starts the execution.")
	refers AutomatonDeclaration [1] mainAutomaton
	@GenModel(documentation="The root data structure containing every other data structure and field. Fields of this 
		data structure are considered global.")
	contains DataStructure [1] rootDataStructure
	@GenModel(documentation="The variable representing the local context of the main CFA.")
	contains DataRef [1] mainContext
	@GenModel(documentation="Returns true if the specified DataRef starts in the global root data structure.")
	op boolean isGlobal(DataRef reference) {
		return CfaDeclarationUtils.isGlobal(reference, this);
	}
}
```
####  Class `AutomatonDeclaration` {#anchor8}

_Class to represent the declaration of a control flow automaton._

**Extends**: `AutomatonBase` (unresolved)


**References**:
- **container**  [1..1]: [CfaNetworkDeclaration](#anchor7)
    * _Automaton system to which this automaton belongs._
    * Containment: container
    * Opposite: [CfaNetworkDeclaration](#anchor7).`automata`
- **localDataStructure**  [1..1]: [DataStructureRef](#anchor2)
    * _A reference to the local data structure that serves as the context for local references._
    * Containment: contains

**Operations**:
- **isLocal**(reference :  [DataRef](#anchor4)) : `EBoolean`
    * _Returns true if the specified DataRef starts in the local data structure._

```
@GenModel(documentation="Class to represent the declaration of a control flow automaton.")
class AutomatonDeclaration extends AutomatonBase {
	@GenModel(documentation="Automaton system to which this automaton belongs.")
	container CfaNetworkDeclaration [1] ^container opposite automata
	@GenModel(documentation="A reference to the local data structure that serves as the context for local references.")
	contains DataStructureRef [1] localDataStructure
	@GenModel(documentation="Returns true if the specified DataRef starts in the local data structure.")
	op boolean isLocal(DataRef reference) {
		var ref = reference
		while (ref.prefix !== null) {
			ref = ref.prefix
		}
		return ref instanceof FieldRef && (ref as FieldRef).field.enclosingType == localDataStructure.definition
	}
}
```
####  Class `AssignmentTransition` {#anchor9}

_Class to represent a (potentially guarded) CFA declaration transition with assignment._

**Extends**: `Transition` (unresolved)


**References**:
- **assignments**  [0..*]: [VariableAssignment](#anchor12)
    * _Variable assignments to perform when the transition fires._
    * Containment: contains


```
@GenModel(documentation="Class to represent a (potentially guarded) CFA declaration transition with assignment.")
class AssignmentTransition extends Transition {
	@GenModel(documentation="Variable assignments to perform when the transition fires.")
	contains VariableAssignment[*] assignments
}
```
####  Class `CallTransition` {#anchor10}

_Class to represent a (potentially guarded) CFA declaration transition with an automaton call. _
_	Semantically, this is equivalent to copying the called automata here, replacing this transition, and_
_	inlining the input and output assignments before and after the call._

**Extends**: `Transition` (unresolved)


**References**:
- **calls**  [1..*]: [Call](#anchor11)
    * _The calls performed upon transition. If it contains multiple calls, it is a parallel composition._
      _		In that case, all input assignments are performed before any of the executions and all output assignments are performed_
      _		after all executions._
    * Containment: contains
    * Opposite: [Call](#anchor11).`parentTransition`


```
@GenModel(documentation="Class to represent a (potentially guarded) CFA declaration transition with an automaton call. 
	Semantically, this is equivalent to copying the called automata here, replacing this transition, and
	inlining the input and output assignments before and after the call.")
class CallTransition extends Transition {
	@GenModel(documentation="The calls performed upon transition. If it contains multiple calls, it is a parallel composition.
		In that case, all input assignments are performed before any of the executions and all output assignments are performed
		after all executions.")
	contains Call[1..*] calls opposite parentTransition
}
```
####  Class `Call` {#anchor11}

_Class to represent a CFA automaton declaration call._

**Extends**: `EObject`


**References**:
- **parentTransition**  [1..1]: [CallTransition](#anchor10)
    * _Transition which contains the call._
    * Containment: container
    * Opposite: [CallTransition](#anchor10).`calls`
- **calledAutomaton**  [1..1]: [AutomatonDeclaration](#anchor8)
    * _Automaton to be executed._
    * Containment: refers
- **calleeContext**  [1..1]: [DataRef](#anchor4)
    * _The context in which the function must be called, defining local variables._
    * Containment: contains
- **inputAssignments**  [0..*]: [VariableAssignment](#anchor12)
    * _Input assignments of the called automaton. Performed in arbitrary order, before executing 'calledAutomaton'._
    * Containment: contains
- **outputAssignments**  [0..*]: [VariableAssignment](#anchor12)
    * _Output assignments of the called automaton. Performed in arbitrary order, after executing 'calledAutomaton'._
    * Containment: contains


```
@GenModel(documentation="Class to represent a CFA automaton declaration call.")
class Call {
	@GenModel(documentation="Transition which contains the call.")
	container CallTransition [1] parentTransition opposite calls
	@GenModel(documentation="Automaton to be executed.")
	refers AutomatonDeclaration [1] calledAutomaton
	@GenModel(documentation="The context in which the function must be called, defining local variables.")
	contains DataRef [1] calleeContext
	@GenModel(documentation="Input assignments of the called automaton. Performed in arbitrary order, before executing 'calledAutomaton'.")
	contains VariableAssignment[*] inputAssignments
	@GenModel(documentation="Output assignments of the called automaton. Performed in arbitrary order, after executing 'calledAutomaton'.")
	contains VariableAssignment[*] outputAssignments
}
```
####  Class `VariableAssignment` {#anchor12}

_Class to represent a variable assignment in the CFA declaration._

**Extends**: `Freezable` (unresolved)


**References**:
- **leftValue**  [1..1]: [DataRef](#anchor4)
    * Containment: contains
- **rightValue**  [1..1]: `Expression` (unresolved)
    * Containment: contains


```
@GenModel(documentation="Class to represent a variable assignment in the CFA declaration.")
class VariableAssignment extends Freezable {
	contains DataRef [1] leftValue
	contains Expression [1] rightValue
}
```
####  Abstract class `DataStructureAnnotation` {#anchor13}

_Abstract superclass for data structure annotations._

**Extends**: `Annotation` (unresolved)


**References**:
- **parentDataStructure** : [DataStructure](#anchor1)
    * Containment: refers
    * Opposite: [DataStructure](#anchor1).`annotations`


```
@GenModel(documentation="Abstract superclass for data structure annotations.")
abstract class DataStructureAnnotation extends Annotation {
	refers DataStructure parentDataStructure opposite annotations
}
```
####  Class `SourceTypeDataStructureAnnotation` {#anchor14}

_Data structure annotation to trace the type from which this data structure was transformed._

**Extends**: [DataStructureAnnotation](#anchor13)

**Attributes**:
- **typename** : `EString`
- **sourceType** : [DataStructureSourceType](#anchor15)



```
@GenModel(documentation="Data structure annotation to trace the type from which this data structure was transformed.")
class SourceTypeDataStructureAnnotation extends DataStructureAnnotation {
	String typename
	DataStructureSourceType sourceType
}
```
####  Abstract class `FieldAnnotation` {#anchor16}

_Abstract superclass for field annotations._

**Extends**: `Annotation` (unresolved)


**References**:
- **parentField**  [1..1]: [Field](#anchor3)
    * Containment: container
    * Opposite: [Field](#anchor3).`annotations`


```
@GenModel(documentation="Abstract superclass for field annotations.")
abstract class FieldAnnotation extends Annotation {
	container Field [1] parentField opposite annotations
}
```
####  Class `DirectionFieldAnnotation` {#anchor17}

_Field annotation to indicate mode in which the annotated field should be interpreted or used._

**Extends**: [FieldAnnotation](#anchor16)

**Attributes**:
- **direction**  [1..1]: `DataDirection` (unresolved)



```
@GenModel(documentation="Field annotation to indicate mode in which the annotated field should be interpreted or used.")
class DirectionFieldAnnotation extends FieldAnnotation {
	DataDirection [1] direction
}
```
####  Class `OriginalDataTypeFieldAnnotation` {#anchor18}

_Field annotation to trace the original data type of the field in the PLC code._

**Extends**: [FieldAnnotation](#anchor16)

**Attributes**:
- **plcDataType**  [1..1]: `EString`



```
@GenModel(documentation="Field annotation to trace the original data type of the field in the PLC code.")
class OriginalDataTypeFieldAnnotation extends FieldAnnotation {
	String [1] plcDataType
}
```
####  Class `ReturnValueFieldAnnotation` {#anchor19}

_Field annotation to indicate that the annotated field is used to store return values of an automaton._

**Extends**: [FieldAnnotation](#anchor16)




```
@GenModel(documentation="Field annotation to indicate that the annotated field is used to store return values of an automaton.")
class ReturnValueFieldAnnotation extends FieldAnnotation {
}
```
####  Class `InternalGeneratedFieldAnnotation` {#anchor20}

_Field annotation to indicate that the annotated field was created for internal purposes and it does not correspond to any of the source program's fields/variables._

**Extends**: [FieldAnnotation](#anchor16)




```
@GenModel(documentation="Field annotation to indicate that the annotated field was created for internal purposes and it does not correspond to any of the source program's fields/variables.")
class InternalGeneratedFieldAnnotation extends FieldAnnotation {
}
```
####  Class `ForLoopLocationAnnotation` {#anchor21}

_Location annotation to represent a FOR loop statement that starts at the given location._

**Extends**: `LoopLocationAnnotation` (unresolved)


**References**:
- **loopVariable** : [DataRef](#anchor4)
    * Containment: contains
- **loopVariableInit** : `Transition` (unresolved)
    * Containment: refers
- **loopVariableIncrement** : `Transition` (unresolved)
    * Containment: refers


```
@GenModel(documentation="Location annotation to represent a FOR loop statement that starts at the given location.")
class ForLoopLocationAnnotation extends LoopLocationAnnotation {
	contains DataRef loopVariable
	refers Transition loopVariableInit
	refers Transition loopVariableIncrement
}
```

####  Enum `DataStructureSourceType` {#anchor15}

_Represents the different kinds of PLC structures that are transformed into data structures._

**Literals**:
- `TYPE` 
- `DATA_BLOCK` 
- `FUNCTION_BLOCK` 
- `ORGANIZATION_BLOCK` 
- `FUNCTION` 

```
@GenModel(documentation="Represents the different kinds of PLC structures that are transformed into data structures.")
enum DataStructureSourceType {
	TYPE, DATA_BLOCK, FUNCTION_BLOCK, ORGANIZATION_BLOCK, FUNCTION
}
```

