###  Expression grammar documentation

(C) Copyright CERN 2017-2018. All rights not expressly granted are reserved.
 
  This file is part of the PLCverif project.
 
  Contributors:
    Daniel Darvas

Included grammars:
- `org.eclipse.xtext.common.Terminals`

Included metamodels:
- ecore (`http://www.eclipse.org/emf/2002/Ecore`)
- expr (`cern.plcverif.base.models.expr`)

####  Rules
#####  ExpressionFile  {#ExpressionFile}
An **expression file** contains one or more expressions.
To be used for testing.


**Refers to:**
- [Expression](#Expression)



```
ExpressionFile:
	{ExpressionFile}
	exprs+=Expression (';' exprs+=Expression)* ';'?;
```



#####  Expression  {#Expression}


**Referred by:**
- [BinaryCtlExpr](#BinaryCtlExpr)
- [BinaryLtlExpr](#BinaryLtlExpr)
- [ExpressionFile](#ExpressionFile)
- [PrimaryExpr](#PrimaryExpr)
- [UnaryCtlExpr](#UnaryCtlExpr)
- [UnaryLtlExpr](#UnaryLtlExpr)

**Returns:** `expr::Expression`

```
Expression returns expr::Expression:
	ImpliesExpression;
```



#####  ImpliesExpression  {#ImpliesExpression}


**Refers to:**
- [ImpliesBinaryLogicOperator](#ImpliesBinaryLogicOperator)
- [OrExpression](#OrExpression)


**Returns:** `expr::Expression`

```
ImpliesExpression returns expr::Expression:
	OrExpression ({expr::BinaryLogicExpression.leftOperand=current} operator=ImpliesBinaryLogicOperator rightOperand=OrExpression)?;
```



#####  OrExpression  {#OrExpression}


**Refers to:**
- [OrBinaryLogicOperator](#OrBinaryLogicOperator)
- [XorExpression](#XorExpression)

**Referred by:**
- [ImpliesExpression](#ImpliesExpression)

**Returns:** `expr::Expression`

```
OrExpression returns expr::Expression:
	XorExpression ({expr::BinaryLogicExpression.leftOperand=current} operator=OrBinaryLogicOperator rightOperand=XorExpression)*;
```



#####  XorExpression  {#XorExpression}


**Refers to:**
- [AndExpression](#AndExpression)
- [XorBinaryLogicOperator](#XorBinaryLogicOperator)

**Referred by:**
- [OrExpression](#OrExpression)

**Returns:** `expr::Expression`

```
XorExpression returns expr::Expression:
	AndExpression ({expr::BinaryLogicExpression.leftOperand=current} operator=XorBinaryLogicOperator rightOperand=AndExpression)*;
```



#####  AndExpression  {#AndExpression}


**Refers to:**
- [AndBinaryLogicOperator](#AndBinaryLogicOperator)
- [EqualityExpression](#EqualityExpression)

**Referred by:**
- [XorExpression](#XorExpression)

**Returns:** `expr::Expression`

```
AndExpression returns expr::Expression:
	EqualityExpression ({expr::BinaryLogicExpression.leftOperand=current} operator=AndBinaryLogicOperator rightOperand=EqualityExpression)*;
```



#####  EqualityExpression  {#EqualityExpression}


**Refers to:**
- [BoolType](#BoolType)
- [ComparisonExpression](#ComparisonExpression)
- [EqualityOperator](#EqualityOperator)

**Referred by:**
- [AndExpression](#AndExpression)

**Returns:** `expr::Expression`

```
EqualityExpression returns expr::Expression:
	ComparisonExpression ({expr::ComparisonExpression.leftOperand=current} operator=EqualityOperator rightOperand=ComparisonExpression type=BoolType)?;
```



#####  ComparisonExpression  {#ComparisonExpression}


**Refers to:**
- [AdditiveExpression](#AdditiveExpression)
- [BoolType](#BoolType)
- [ComparisonOperator](#ComparisonOperator)

**Referred by:**
- [EqualityExpression](#EqualityExpression)

**Returns:** `expr::Expression`

```
ComparisonExpression returns expr::Expression:
	AdditiveExpression ({expr::ComparisonExpression.leftOperand=current} operator=ComparisonOperator rightOperand=AdditiveExpression type=BoolType)?;
```



#####  AdditiveExpression  {#AdditiveExpression}


**Refers to:**
- [AdditionOperator](#AdditionOperator)
- [MultiplicativeExpression](#MultiplicativeExpression)

**Referred by:**
- [ComparisonExpression](#ComparisonExpression)

**Returns:** `expr::Expression`

```
AdditiveExpression returns expr::Expression:
	MultiplicativeExpression ({expr::BinaryArithmeticExpression.leftOperand=current} operator=AdditionOperator rightOperand=MultiplicativeExpression)*;
```



#####  MultiplicativeExpression  {#MultiplicativeExpression}


**Refers to:**
- [MultiplicationOperator](#MultiplicationOperator)
- [PowerExpression](#PowerExpression)

**Referred by:**
- [AdditiveExpression](#AdditiveExpression)

**Returns:** `expr::Expression`

```
MultiplicativeExpression returns expr::Expression:
	PowerExpression ({expr::BinaryArithmeticExpression.leftOperand=current} operator=MultiplicationOperator rightOperand=PowerExpression)*;
```



#####  PowerExpression  {#PowerExpression}


**Refers to:**
- [PowerOperator](#PowerOperator)
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)

**Referred by:**
- [MultiplicativeExpression](#MultiplicativeExpression)

**Returns:** `expr::Expression`

```
PowerExpression returns expr::Expression:
	UnaryOrPrimaryExpression ({expr::BinaryArithmeticExpression.leftOperand=current} operator=PowerOperator rightOperand=UnaryOrPrimaryExpression)?;
```



#####  UnaryOrPrimaryExpression  {#UnaryOrPrimaryExpression}
Unary or primary expression. It can be a 
a [primary expression](#PrimaryExpr),
a [temporal logic expression](#TlExpr), or
a [unary logic expression](#UnaryLogicExpression).


**Refers to:**
- [PrimaryExpr](#PrimaryExpr)
- [TlExpr](#TlExpr)
- [UnaryLogicExpression](#UnaryLogicExpression)

**Referred by:**
- [PowerExpression](#PowerExpression)

**Returns:** `expr::Expression`

```
UnaryOrPrimaryExpression returns expr::Expression:
	PrimaryExpr | TlExpr | UnaryLogicExpression;
```



#####  UnaryLogicExpression  {#UnaryLogicExpression}
Unary logic expression.

- **Examples:**
   * `NOT alpha`

**Refers to:**
- [PrimaryExpr](#PrimaryExpr)
- [UnaryLogicOperator](#UnaryLogicOperator)

**Referred by:**
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)

**Returns:** `expr::Expression`

```
UnaryLogicExpression returns expr::Expression:
	{expr::UnaryLogicExpression} operator=UnaryLogicOperator operand=PrimaryExpr;
```



#####  PrimaryExpr  {#PrimaryExpr}
Primary expression.


**Refers to:**
- [BeginningOfCycle](#BeginningOfCycle)
- [BoolLiteral](#BoolLiteral)
- [EndOfCycle](#EndOfCycle)
- [Expression](#Expression)
- [FloatLiteral](#FloatLiteral)
- [IntLiteral](#IntLiteral)
- [PatternParameter](#PatternParameter)
- [ValuePlaceholder](#ValuePlaceholder)

**Referred by:**
- [UnaryLogicExpression](#UnaryLogicExpression)
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)

**Returns:** `expr::Expression`

```
PrimaryExpr returns expr::Expression: 
	'(' Expression ')' |
	ValuePlaceholder |
	PatternParameter |
	EndOfCycle  |
	BeginningOfCycle |
	BoolLiteral |
	IntLiteral | 
	FloatLiteral;
```



#####  ValuePlaceholder  {#ValuePlaceholder}
Placeholder expression. It can represent a reference to an unknown variable-like value.

- **Examples:**
   * `alpha`
   * `alpha[3].beta.5`

**Refers to:**
- [UnknownType](#UnknownType)
- [ValueString](#ValueString)

**Referred by:**
- [PrimaryExpr](#PrimaryExpr)


```
ValuePlaceholder: 
	value=ValueString type=UnknownType;
```



#####  PatternParameter  {#PatternParameter}
Reference to a parameter using the ID number of the pattern parameter.
Expected to have a Boolean type.

- **Examples:**
   * `{3}`

**Refers to:**
- [BoolType](#BoolType)
- INT

**Referred by:**
- [PrimaryExpr](#PrimaryExpr)


```
PatternParameter:
	{PatternParameter} '{' key=INT '}' type=BoolType;
```



#####  EndOfCycle  {#EndOfCycle}
Boolean predicate that is true if and only if the currently active location represents the end of the cyclic execution.

- **Examples:**
   * `{PLC_END}`
   * `{EoC}`

**Refers to:**
- [BoolType](#BoolType)

**Referred by:**
- [PrimaryExpr](#PrimaryExpr)

**Returns:** `expr::EndOfCycle`

```
EndOfCycle returns expr::EndOfCycle:
	{expr::EndOfCycle} '{' ('PLC_END' | 'EoC') '}' type=BoolType;
```



#####  BeginningOfCycle  {#BeginningOfCycle}
Boolean predicate that is true if and only if the currently active location represents the beginning of the cyclic execution.

- **Examples:**
   * `{PLC_START}`
   * `{BoC}`

**Refers to:**
- [BoolType](#BoolType)

**Referred by:**
- [PrimaryExpr](#PrimaryExpr)

**Returns:** `expr::BeginningOfCycle`

```
BeginningOfCycle returns expr::BeginningOfCycle:
	{expr::BeginningOfCycle} '{' ('PLC_START' | 'BoC') '}' type=BoolType;
```



#####  BoolLiteral  {#BoolLiteral}
**Boolean literal**. It has [BoolType](#BoolType) as type.


**Refers to:**
- [BoolType](#BoolType)
- [BooleanConstantValue](#BooleanConstantValue)

**Referred by:**
- [PrimaryExpr](#PrimaryExpr)

**Returns:** `expr::BoolLiteral`

```
BoolLiteral returns expr::BoolLiteral:
	{expr::BoolLiteral} value=BooleanConstantValue type=BoolType;
```



#####  BooleanConstantValue  {#BooleanConstantValue}
**Boolean value**. Can be `true` or `false`.

- **Examples:**
   * `true`

**Referred by:**
- [BoolLiteral](#BoolLiteral)

**Returns:** `ecore::EBoolean`

```
BooleanConstantValue returns ecore::EBoolean: 'true' | 'false';
```



#####  IntLiteral  {#IntLiteral}
**Integer literal**. 
As no size information is available at this point, it has [UnknownType](#UnknownType) as type.


**Refers to:**
- [IntLiteralValue](#IntLiteralValue)
- [UnknownType](#UnknownType)

**Referred by:**
- [PrimaryExpr](#PrimaryExpr)

**Returns:** `expr::IntLiteral`

```
IntLiteral returns expr::IntLiteral:
	{expr::IntLiteral} value=IntLiteralValue type=UnknownType;
```



#####  IntLiteralValue  {#IntLiteralValue}
**Integer value**.

- **Examples:**
   * `27`
   * `-42`

**Refers to:**
- INT

**Referred by:**
- [IntLiteral](#IntLiteral)

**Returns:** `ecore::ELong`

```
IntLiteralValue returns ecore::ELong: '-'? INT;
```



#####  FloatLiteral  {#FloatLiteral}
**Floating point literal**. 
As no size information is available at this point, it has [UnknownType](#UnknownType) as type.


**Refers to:**
- [FloatLiteralValue](#FloatLiteralValue)
- [UnknownType](#UnknownType)

**Referred by:**
- [PrimaryExpr](#PrimaryExpr)

**Returns:** `expr::FloatLiteral`

```
FloatLiteral returns expr::FloatLiteral:
	{expr::FloatLiteral} value=FloatLiteralValue type=UnknownType;
```



#####  FloatLiteralValue  {#FloatLiteralValue}
**Floating point value**. It must contain a decimal point.

- **Examples:**
   * `27.3`
   * `3.14`

**Refers to:**
- INT

**Referred by:**
- [FloatLiteral](#FloatLiteral)

**Returns:** `ecore::EDouble`

```
FloatLiteralValue returns ecore::EDouble: INT '.' INT;
```



#####  AndBinaryLogicOperator (enum) {#AndBinaryLogicOperator}
AND logic operator.


Literals:
- AND (`AND`, `&&`, `∧`)

```
enum AndBinaryLogicOperator returns expr::BinaryLogicOperator:
	AND='AND' | AND='&&' | AND='∧';
```



#####  OrBinaryLogicOperator (enum) {#OrBinaryLogicOperator}
OR logic operator.


Literals:
- OR (`OR`, `∨`)

```
enum OrBinaryLogicOperator returns expr::BinaryLogicOperator:
	OR='OR' | OR='∨';
```



#####  XorBinaryLogicOperator (enum) {#XorBinaryLogicOperator}
XOR logic operator.


Literals:

```
enum XorBinaryLogicOperator returns expr::BinaryLogicOperator:
	XOR='XOR';
```



#####  ImpliesBinaryLogicOperator (enum) {#ImpliesBinaryLogicOperator}
Implication logic operator.


Literals:
- IMPLIES (`-->`, `→`, `⇒`)

```
enum ImpliesBinaryLogicOperator returns expr::BinaryLogicOperator:
	IMPLIES='-->' | IMPLIES='→' | IMPLIES='⇒';
```



#####  EqualityOperator (enum) {#EqualityOperator}
Equality or non-equality comparison operator.


Literals:
- EQUALS (`=`)
- NOT_EQUALS (`<>`)

```
enum EqualityOperator returns expr::ComparisonOperator:
	EQUALS='=' | NOT_EQUALS='<>';
```



#####  ComparisonOperator (enum) {#ComparisonOperator}
Comparison operator.
This does not contain the "equals" and "not equals" operators.


Literals:
- GREATER_EQ (`>=`, `≥`)
- GREATER_THAN (`>`)
- LESS_EQ (`<=`, `≤`)
- LESS_THAN (`<`)

```
enum ComparisonOperator returns expr::ComparisonOperator:
	LESS_THAN='<' | GREATER_THAN='>' | LESS_EQ='<=' | LESS_EQ='≤' | GREATER_EQ='>=' | GREATER_EQ='≥';
```



#####  AdditionOperator (enum) {#AdditionOperator}
Addition-type arithmetic operator (addition or subtraction).


Literals:
- MINUS (`-`)
- PLUS (`+`)

```
enum AdditionOperator returns expr::BinaryArithmeticOperator:
	PLUS='+' | MINUS='-';
```



#####  MultiplicationOperator (enum) {#MultiplicationOperator}
Multiplication-type arithmetic operator (multiplication, division, modulo, integer division).


Literals:
- DIVISION (`/`)
- INTEGER_DIVISION (`DIV`)
- MODULO (`MOD`)
- MULTIPLICATION (`*`)

```
enum MultiplicationOperator returns expr::BinaryArithmeticOperator:
	MULTIPLICATION='*' | DIVISION='/' | MODULO='MOD' | INTEGER_DIVISION='DIV';
```



#####  PowerOperator (enum) {#PowerOperator}
Power arithmetic operator.


Literals:
- POWER (`**`, `^`)

```
enum PowerOperator returns expr::BinaryArithmeticOperator:
	POWER='**' | POWER='^';
```



#####  UnaryLogicOperator (enum) {#UnaryLogicOperator}
Unary logic operator (negation).


Literals:
- NEG (`NOT`, `¬`, `!`)

```
enum UnaryLogicOperator returns expr::UnaryLogicOperator:
	NEG='NOT' | NEG='¬' | NEG='!';
```



#####  TlExpr  {#TlExpr}
**Temporal logic expression**. It can be a CTL or LTL expression.


**Refers to:**
- [CtlExpr](#CtlExpr)
- [LtlExpr](#LtlExpr)

**Referred by:**
- [UnaryOrPrimaryExpression](#UnaryOrPrimaryExpression)

**Returns:** `expr::Expression`

```
TlExpr returns expr::Expression:
	CtlExpr | LtlExpr;
```



#####  CtlExpr  {#CtlExpr}
Root type for a **CTL expression**.
A logic expression that does not have a CTL operator in the root node is not considered to be a `CtlExpression` in the grammar.


**Refers to:**
- [BinaryCtlExpr](#BinaryCtlExpr)
- [UnaryCtlExpr](#UnaryCtlExpr)

**Referred by:**
- [TlExpr](#TlExpr)

**Returns:** `expr::Expression`

```
CtlExpr returns expr::Expression:
	UnaryCtlExpr | BinaryCtlExpr;
```



#####  UnaryCtlExpr  {#UnaryCtlExpr}
Unary CTL operation. The available operators are defined in [UnaryCtlOp](#UnaryCtlOp).
The operand must be surrounded with parentheses.

- **Examples:**
   * `AG(alpha --> beta)`

**Refers to:**
- [Expression](#Expression)
- [TemporalBoolType](#TemporalBoolType)
- [UnaryCtlOp](#UnaryCtlOp)

**Referred by:**
- [CtlExpr](#CtlExpr)

**Returns:** `expr::Expression`

```
UnaryCtlExpr returns expr::Expression:
	{expr::UnaryCtlExpression} operator=UnaryCtlOp '(' operand=Expression ')' type=TemporalBoolType;
```



#####  BinaryCtlExpr  {#BinaryCtlExpr}
Binary CTL operation. It can describe an AU or an EU expression.

- **Examples:**
   * `A[alpha U beta]`
   * `E [delta U gamma]`

**Refers to:**
- [BinaryCtlOpA](#BinaryCtlOpA)
- [BinaryCtlOpE](#BinaryCtlOpE)
- [Expression](#Expression)
- [TemporalBoolType](#TemporalBoolType)

**Referred by:**
- [CtlExpr](#CtlExpr)

**Returns:** `expr::BinaryCtlExpression`

```
BinaryCtlExpr returns expr::BinaryCtlExpression:
	('A' '[' leftOperand=Expression operator=BinaryCtlOpA rightOperand=Expression ']' type=TemporalBoolType) |
	('E' '[' leftOperand=Expression operator=BinaryCtlOpE rightOperand=Expression ']' type=TemporalBoolType);
```



#####  UnaryCtlOp (enum) {#UnaryCtlOp}
Unary CTL operators.


Literals:
- AF (`AF`)
- AG (`AG`)
- AX (`AX`)
- EF (`EF`)
- EG (`EG`)
- EX (`EX`)

```
enum UnaryCtlOp returns expr::UnaryCtlOperator: 
	AG | AF | AX | EG | EF | EX;
```



#####  BinaryCtlOpA (enum) {#BinaryCtlOpA}
Binary CTL operators starting with A.


Literals:

```
enum BinaryCtlOpA returns expr::BinaryCtlOperator:
	AU='U';
```



#####  BinaryCtlOpE (enum) {#BinaryCtlOpE}
Binary CTL operators starting with E.


Literals:

```
enum BinaryCtlOpE returns expr::BinaryCtlOperator:
	EU='U';
```



#####  LtlExpr  {#LtlExpr}
Root type for a **LTL expression**.
A logic expression that does not have a LTL operator in the root node is not considered to be a `LtlExpression` in the grammar.


**Refers to:**
- [BinaryLtlExpr](#BinaryLtlExpr)
- [UnaryLtlExpr](#UnaryLtlExpr)

**Referred by:**
- [TlExpr](#TlExpr)

**Returns:** `expr::Expression`

```
LtlExpr returns expr::Expression:
	UnaryLtlExpr | BinaryLtlExpr;
```



#####  UnaryLtlExpr  {#UnaryLtlExpr}
Unary LTL operation. The available operators are defined in [UnaryLtlOp](#UnaryLtlOp).
The operand must be surrounded with parentheses.

- **Examples:**
   * `G(alpha --> beta)`

**Refers to:**
- [Expression](#Expression)
- [TemporalBoolType](#TemporalBoolType)
- [UnaryLtlOp](#UnaryLtlOp)

**Referred by:**
- [LtlExpr](#LtlExpr)

**Returns:** `expr::Expression`

```
UnaryLtlExpr returns expr::Expression:
	{expr::UnaryLtlExpression} operator=UnaryLtlOp '(' operand=Expression ')' type=TemporalBoolType;
```



#####  BinaryLtlExpr  {#BinaryLtlExpr}
Binary LTL operation. The available operators are defined in [BinaryLtlOp](#BinaryLtlOp).
It is surrounded with square brackets.

- **Examples:**
   * `[alpha U beta]`

**Refers to:**
- [BinaryLtlOp](#BinaryLtlOp)
- [Expression](#Expression)
- [TemporalBoolType](#TemporalBoolType)

**Referred by:**
- [LtlExpr](#LtlExpr)

**Returns:** `expr::BinaryLtlExpression`

```
BinaryLtlExpr returns expr::BinaryLtlExpression:
	'[' leftOperand=Expression operator=BinaryLtlOp rightOperand=Expression ']' type=TemporalBoolType;
```



#####  UnaryLtlOp (enum) {#UnaryLtlOp}
Unary LTL operators.


Literals:
- F (`F`)
- G (`G`)
- X (`X`)

```
enum UnaryLtlOp returns expr::UnaryLtlOperator:
	G | F | X;
```



#####  BinaryLtlOp (enum) {#BinaryLtlOp}
Binary LTL operators.


Literals:
- R (`R`)
- U (`U`)

```
enum BinaryLtlOp returns expr::BinaryLtlOperator:
		U | R;
```



#####  BoolType  {#BoolType}


**Referred by:**
- [BeginningOfCycle](#BeginningOfCycle)
- [BoolLiteral](#BoolLiteral)
- [ComparisonExpression](#ComparisonExpression)
- [EndOfCycle](#EndOfCycle)
- [EqualityExpression](#EqualityExpression)
- [PatternParameter](#PatternParameter)

**Returns:** `expr::BoolType`

```
BoolType returns expr::BoolType: {expr::BoolType};
```



#####  TemporalBoolType  {#TemporalBoolType}


**Referred by:**
- [BinaryCtlExpr](#BinaryCtlExpr)
- [BinaryLtlExpr](#BinaryLtlExpr)
- [UnaryCtlExpr](#UnaryCtlExpr)
- [UnaryLtlExpr](#UnaryLtlExpr)

**Returns:** `expr::TemporalBoolType`

```
TemporalBoolType returns expr::TemporalBoolType: {expr::TemporalBoolType};
```



#####  UnknownType  {#UnknownType}


**Referred by:**
- [FloatLiteral](#FloatLiteral)
- [IntLiteral](#IntLiteral)
- [ValuePlaceholder](#ValuePlaceholder)

**Returns:** `expr::UnknownType`

```
UnknownType returns expr::UnknownType: {expr::UnknownType};
```



#####  ValueString  {#ValueString}
A value string that can be used as a placeholder expression ([ValuePlaceholder](#ValuePlaceholder)).
No linking will be performed at the level of the grammar.

- **Examples:**
   * `alpha`
   * `alpha.beta`
   * `alpha.4`
   * `alpha[4]`
   * `alpha[4].beta`

**Refers to:**
- INT
- [IdOrKeyword](#IdOrKeyword)

**Referred by:**
- [ValuePlaceholder](#ValuePlaceholder)

**Returns:** `ecore::EString`

```
ValueString: IdOrKeyword (('.' IdOrKeyword) | ('.' INT) | ('[' INT ']'))*;
```



#####  Keywords  {#Keywords}
Keywords that can be used as identifier as well.


**Referred by:**
- [IdOrKeyword](#IdOrKeyword)

**Returns:** `ecore::EString`

```
Keywords: 'A' | 'U' | 'W' | 'R' | 'E' | 'AG' | 'AF' | 'AX' | 'EG' | 'EF' | 'EX';
```



#####  IdOrKeyword  {#IdOrKeyword}


**Refers to:**
- [ID](#ID)
- [Keywords](#Keywords)

**Referred by:**
- [ValueString](#ValueString)

**Returns:** `ecore::EString`

```
IdOrKeyword: ID | Keywords;
```



#####  ID (terminal) {#ID}


**Refers to:**
- [LEADING_CHAR](#LEADING_CHAR)
- [NON_LEADING_CHAR](#NON_LEADING_CHAR)

**Referred by:**
- [IdOrKeyword](#IdOrKeyword)

```
@Override
terminal ID : LEADING_CHAR (NON_LEADING_CHAR)*;
```



#####  LEADING_CHAR (terminal fragment) {#LEADING_CHAR}



```
terminal fragment LEADING_CHAR: ('a'..'z'|'A'..'Z'|'_'|'#');
```



#####  NON_LEADING_CHAR (terminal fragment) {#NON_LEADING_CHAR}


**Refers to:**
- [LEADING_CHAR](#LEADING_CHAR)


```
terminal fragment NON_LEADING_CHAR: LEADING_CHAR | '0'..'9';
```




####  Simplified grammar
**ExpressionFile** ::=    _Expression_   (`;`   _Expression_)*   `;`?;

**Expression** ::= _ImpliesExpression_;

**ImpliesExpression** ::= _OrExpression_   (   _ImpliesBinaryLogicOperator_   _OrExpression_)?;

**OrExpression** ::= _XorExpression_   (   _OrBinaryLogicOperator_   _XorExpression_)*;

**ImpliesBinaryLogicOperator** ::= `-->` | `→` | `⇒`;

**XorExpression** ::= _AndExpression_   (   _XorBinaryLogicOperator_   _AndExpression_)*;

**OrBinaryLogicOperator** ::= `OR` | `∨`;

**AndExpression** ::= _EqualityExpression_   (   _AndBinaryLogicOperator_   _EqualityExpression_)*;

**XorBinaryLogicOperator** ::= `XOR`;

**EqualityExpression** ::= _ComparisonExpression_   (   _EqualityOperator_   _ComparisonExpression_   _BoolType_)?;

**AndBinaryLogicOperator** ::= `AND` | `&&` | `∧`;

**ComparisonExpression** ::= _AdditiveExpression_   (   _ComparisonOperator_   _AdditiveExpression_   _BoolType_)?;

**EqualityOperator** ::= `=` | `<>`;

**BoolType** ::= ;

**AdditiveExpression** ::= _MultiplicativeExpression_   (   _AdditionOperator_   _MultiplicativeExpression_)*;

**ComparisonOperator** ::= `<` | `>` | `<=` | `≤` | `>=` | `≥`;

**MultiplicativeExpression** ::= _PowerExpression_   (   _MultiplicationOperator_   _PowerExpression_)*;

**AdditionOperator** ::= `+` | `-`;

**PowerExpression** ::= _UnaryOrPrimaryExpression_   (   _PowerOperator_   _UnaryOrPrimaryExpression_)?;

**MultiplicationOperator** ::= `*` | `/` | `MOD` | `DIV`;

**UnaryOrPrimaryExpression** ::= _PrimaryExpr_ | _TlExpr_ | _UnaryLogicExpression_;

**PowerOperator** ::= `**` | `^`;

**PrimaryExpr** ::= (`(`   _Expression_   `)`) | _ValuePlaceholder_ | _PatternParameter_ | _EndOfCycle_ | _BeginningOfCycle_ | _BoolLiteral_ | _IntLiteral_ | _FloatLiteral_;

**TlExpr** ::= _CtlExpr_ | _LtlExpr_;

**UnaryLogicExpression** ::=    _UnaryLogicOperator_   _PrimaryExpr_;

**ValuePlaceholder** ::= _ValueString_   _UnknownType_;

**PatternParameter** ::=    `{`   _INT_   `}`   _BoolType_;

**EndOfCycle** ::=    `{`   (`PLC_END` | `EoC`)   `}`   _BoolType_;

**BeginningOfCycle** ::=    `{`   (`PLC_START` | `BoC`)   `}`   _BoolType_;

**BoolLiteral** ::=    _BooleanConstantValue_   _BoolType_;

**IntLiteral** ::=    _IntLiteralValue_   _UnknownType_;

**FloatLiteral** ::=    _FloatLiteralValue_   _UnknownType_;

**CtlExpr** ::= _UnaryCtlExpr_ | _BinaryCtlExpr_;

**LtlExpr** ::= _UnaryLtlExpr_ | _BinaryLtlExpr_;

**UnaryLogicOperator** ::= `NOT` | `¬` | `!`;

**ValueString** ::= _IdOrKeyword_   ((`.`   _IdOrKeyword_) | (`.`   _INT_) | (`[`   _INT_   `]`))*;

**UnknownType** ::= ;

**INT** ::= [`0`..`9`]+;

**BooleanConstantValue** ::= `true` | `false`;

**IntLiteralValue** ::= `-`?   _INT_;

**FloatLiteralValue** ::= _INT_   `.`   _INT_;

**UnaryCtlExpr** ::=    _UnaryCtlOp_   `(`   _Expression_   `)`   _TemporalBoolType_;

**BinaryCtlExpr** ::= (`A`   `[`   _Expression_   _BinaryCtlOpA_   _Expression_   `]`   _TemporalBoolType_) | (`E`   `[`   _Expression_   _BinaryCtlOpE_   _Expression_   `]`   _TemporalBoolType_);

**UnaryLtlExpr** ::=    _UnaryLtlOp_   `(`   _Expression_   `)`   _TemporalBoolType_;

**BinaryLtlExpr** ::= `[`   _Expression_   _BinaryLtlOp_   _Expression_   `]`   _TemporalBoolType_;

**IdOrKeyword** ::= _ID_ | _Keywords_;

**UnaryCtlOp** ::= `AG` | `AF` | `AX` | `EG` | `EF` | `EX`;

**TemporalBoolType** ::= ;

**BinaryCtlOpA** ::= `U`;

**BinaryCtlOpE** ::= `U`;

**UnaryLtlOp** ::= `G` | `F` | `X`;

**BinaryLtlOp** ::= `U` | `R`;

**ID** ::= _LEADING_CHAR_   _NON_LEADING_CHAR_*;

**Keywords** ::= `A` | `U` | `W` | `R` | `E` | `AG` | `AF` | `AX` | `EG` | `EF` | `EX`;

**LEADING_CHAR** ::= [`a`..`z`] | [`A`..`Z`] | `_` | `#`;

**NON_LEADING_CHAR** ::= _LEADING_CHAR_ | [`0`..`9`];


