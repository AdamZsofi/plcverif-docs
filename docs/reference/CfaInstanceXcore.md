##  CfaInstance.xcore metamodel description

###  Table of contents
- [AbstractVariable](#anchor2)
- [AbstractVariableRef](#anchor6)
- [ArrayElementInitialValue](#anchor5)
- [ArrayElementRef](#anchor8)
- [ArrayVariable](#anchor4)
- [AssignmentTransition](#anchor11)
- [AutomatonInstance](#anchor10)
- [Call](#anchor13)
- [CallTransition](#anchor12)
- [CfaNetworkInstance](#anchor9)
- [DirectionVariableAnnotation](#anchor16)
- [ForLoopLocationAnnotation](#anchor20)
- [FullyQualifiedName](#anchor1)
- [InternalGeneratedVariableAnnotation](#anchor19)
- [OriginalDataTypeVariableAnnotation](#anchor17)
- [ReturnValueVariableAnnotation](#anchor18)
- [Variable](#anchor3)
- [VariableAnnotation](#anchor15)
- [VariableAssignment](#anchor14)
- [VariableRef](#anchor7)

###  Package `cern.plcverif.base.models.cfa.cfainstance`

####  Class `FullyQualifiedName` {#anchor1}

_Class to represent a fully qualified name, composed of segments that should be unique in their scope._

**Extends**: `EObject`

**Attributes**:
- **identifiers**  [1..*]: `EString`
    * _The sequence of segments or identifiers constituting the fully qualified name._


**Operations**:
- **toString**() : `EString`

```
@GenModel(documentation="Class to represent a fully qualified name, composed of segments that should be unique in their scope.")
class FullyQualifiedName {
	@GenModel(documentation="The sequence of segments or identifiers constituting the fully qualified name.")
	String [1..*] identifiers
			op String toString() {
		return String.join("/", identifiers)
	}
}
```
####  Abstract class `AbstractVariable` {#anchor2}

_Represents a variable or array of the CFA. Note that variables are unique in the model, referencing_
_	them is possible via AbstractVariableRef instances, which may be copied. This element can be frozen (see Freezable)._

**Extends**: `ExplicitlyTyped` (unresolved), `NamedElement` (unresolved), `Freezable` (unresolved), `Annotable` (unresolved)


**References**:
- **container**  [1..1]: [CfaNetworkInstance](#anchor9)
    * _The CFA network instance containing this variable._
    * Containment: container
    * Opposite: [CfaNetworkInstance](#anchor9).`variables`
- **fqn**  [1..1]: [FullyQualifiedName](#anchor1)
    * _The fully qualified name of this variable._
    * Containment: contains
- **annotations**  [0..*]: [VariableAnnotation](#anchor15)
    * _Annotations of the variable. Used for code generation and traceability._
    * Containment: contains
    * Opposite: [VariableAnnotation](#anchor15).`parentVariable`


```
@GenModel(documentation="Represents a variable or array of the CFA. Note that variables are unique in the model, referencing
	them is possible via AbstractVariableRef instances, which may be copied. This element can be frozen (see Freezable).")
abstract class AbstractVariable extends ExplicitlyTyped, NamedElement, Freezable, Annotable {
	@GenModel(documentation="The CFA network instance containing this variable.")
	container CfaNetworkInstance [1] ^container opposite variables 
	@GenModel(documentation="The fully qualified name of this variable.")
	contains FullyQualifiedName [1] fqn
	@GenModel(documentation="Annotations of the variable. Used for code generation and traceability.")
	contains VariableAnnotation[*] annotations opposite parentVariable
}
```
####  Class `Variable` {#anchor3}

_Represents an elementary variable of the CFA. Note that variables are unique in the model, _
_	referencing them is possible via VariableRef instances, which may be copied. This element can be frozen (see Freezable)._

**Extends**: [AbstractVariable](#anchor2)


**References**:
- **initialValue**  [1..1]: `InitialValue` (unresolved)
    * _The initial value of this variable._
    * Containment: contains

**Operations**:
- **toString**() : `EString`

```
@GenModel(documentation="Represents an elementary variable of the CFA. Note that variables are unique in the model, 
	referencing them is possible via VariableRef instances, which may be copied. This element can be frozen (see Freezable).")
class Variable extends AbstractVariable {
	@GenModel(documentation="The initial value of this variable.")
	contains InitialValue [1] initialValue
			op String toString() {
		return fqn.toString()
	}
}
```
####  Class `ArrayVariable` {#anchor4}

_Represents a (multidimensional) array variable of the CFA. Note that variables are unique in the _
_	model, referencing them is possible via ArrayElementRef instances, which may be copied. The type of an ArrayVariable_
_	is always the type of its elements. This element can be frozen (see Freezable)._

**Extends**: [AbstractVariable](#anchor2)


**References**:
- **dimensions**  [1..*]: `ArrayDimension` (unresolved)
    * _The dimension(s) of this array._
    * Containment: contains
- **initialValues**  [0..*]: [ArrayElementInitialValue](#anchor5)
    * _The initial values of array elements. Use cern.plcverif.base.models.cfa.utils.ArrayInitialValueHelper_
      _		to read and process the initial values of multidimensional arrays_
    * Containment: contains

**Operations**:
- **toString**() : `EString`

```
@GenModel(documentation="Represents a (multidimensional) array variable of the CFA. Note that variables are unique in the 
	model, referencing them is possible via ArrayElementRef instances, which may be copied. The type of an ArrayVariable
	is always the type of its elements. This element can be frozen (see Freezable).")
class ArrayVariable extends AbstractVariable {
	@GenModel(documentation="The dimension(s) of this array.")
	contains ArrayDimension [1..*] dimensions
	@GenModel(documentation="The initial values of array elements. Use cern.plcverif.base.models.cfa.utils.ArrayInitialValueHelper
		to read and process the initial values of multidimensional arrays")
	contains ArrayElementInitialValue [*] initialValues
			op String toString() {
		var String ret = fqn.toString() + "[";
		for (dimension : dimensions) {
			if (!ret.endsWith("[")) {
				ret += ","
			}
			ret += dimension.toString()
		}
		return ret + "]";
	}
}
```
####  Class `ArrayElementInitialValue` {#anchor5}

_Represents the initialization of an array element._

**Extends**: `EObject`


**References**:
- **indices**  [0..*]: `Expression` (unresolved)
    * _The indices of the element to initialize._
    * Containment: contains
- **initialValue**  [1..1]: `InitialValue` (unresolved)
    * _The initial value of the identified element._
    * Containment: contains


```
@GenModel(documentation="Represents the initialization of an array element.")
class ArrayElementInitialValue {
	@GenModel(documentation="The indices of the element to initialize.")
	contains Expression [*] indices
	@GenModel(documentation="The initial value of the identified element.")
	contains InitialValue [1] initialValue 
}
```
####  Abstract class `AbstractVariableRef` {#anchor6}

_Represents a reference to a variable or an element of an array._

**Extends**: `LeftValue` (unresolved), `Freezable` (unresolved)



**Operations**:
- **getVariable**() : [AbstractVariable](#anchor2)
    * _Returns the referenced variable._
- **getType**() : `Type` (unresolved)
    * _The type of an AbstractVariableRef is always the type of the referred variable._

```
@GenModel(documentation="Represents a reference to a variable or an element of an array.")
abstract class AbstractVariableRef extends LeftValue, Freezable {
	@GenModel(documentation="Returns the referenced variable.")
	op AbstractVariable getVariable() {
						throw new AbstractMethodError();
	}
	@GenModel(documentation="The type of an AbstractVariableRef is always the type of the referred variable.")
	op Type getType() {
		return variable.^type
	}
}
```
####  Class `VariableRef` {#anchor7}

_Represents a reference to a simple variable._

**Extends**: [AbstractVariableRef](#anchor6)


**References**:
- **variable** : [Variable](#anchor3)
    * _The referenced variable._
    * Containment: refers

**Operations**:
- **toString**() : `EString`

```
@GenModel(documentation="Represents a reference to a simple variable.")
class VariableRef extends AbstractVariableRef {
	@GenModel(documentation="The referenced variable.")
	refers Variable variable
			op String toString() {
		return variable.fqn.toString()
	}
}
```
####  Class `ArrayElementRef` {#anchor8}

_Represents a reference to an array element._

**Extends**: [AbstractVariableRef](#anchor6)


**References**:
- **variable** : [ArrayVariable](#anchor4)
    * _The referenced array._
    * Containment: refers
- **indices**  [1..*]: `Expression` (unresolved)
    * _The indices identifying the array element. The array indices must have signed 16-bit integer types._
    * Containment: contains

**Operations**:
- **toString**() : `EString`

```
@GenModel(documentation="Represents a reference to an array element.")
class ArrayElementRef extends AbstractVariableRef {
	@GenModel(documentation="The referenced array.")
	refers ArrayVariable variable
	@GenModel(documentation="The indices identifying the array element. The array indices must have signed 16-bit integer types.")
	contains Expression [1..*] indices
			op String toString() {
		var String ret = variable.fqn.toString() + "[";
		for (index : indices) {
			if (!ret.endsWith("[")) {
				ret += ","
			}
			ret += index.toString()
		}
		return ret + "]";
	}
}
```
####  Class `CfaNetworkInstance` {#anchor9}

_Class to represent an instance of a network of control flow automata (CFA)._

**Extends**: `CfaNetworkBase` (unresolved)


**References**:
- **variables**  [0..*]: [AbstractVariable](#anchor2)
    * Containment: contains
    * Opposite: [AbstractVariable](#anchor2).`container`
- **automata**  [0..*]: [AutomatonInstance](#anchor10)
    * _Automata in the network._
    * Containment: contains
    * Opposite: [AutomatonInstance](#anchor10).`container`
- **mainAutomaton** : [AutomatonInstance](#anchor10)
    * _Automaton which starts the execution._
    * Containment: refers


```
@GenModel(documentation="Class to represent an instance of a network of control flow automata (CFA).")
class CfaNetworkInstance extends CfaNetworkBase {
	contains AbstractVariable [*] variables opposite ^container
	@GenModel(documentation="Automata in the network.")
	contains AutomatonInstance[] automata opposite ^container
	@GenModel(documentation="Automaton which starts the execution.")
	refers AutomatonInstance mainAutomaton
}
```
####  Class `AutomatonInstance` {#anchor10}

_Class to represent an instance of a control flow automaton._

**Extends**: `AutomatonBase` (unresolved)


**References**:
- **container**  [1..1]: [CfaNetworkInstance](#anchor9)
    * _Automaton system which this automaton belongs to._
    * Containment: container
    * Opposite: [CfaNetworkInstance](#anchor9).`automata`


```
@GenModel(documentation="Class to represent an instance of a control flow automaton.")
class AutomatonInstance extends AutomatonBase {
	@GenModel(documentation="Automaton system which this automaton belongs to.")
	container CfaNetworkInstance [1] ^container opposite automata
}
```
####  Class `AssignmentTransition` {#anchor11}

_Class to represent a (potentially guarded) CFA instance transition with assignment._

**Extends**: `Transition` (unresolved)


**References**:
- **assignments**  [0..*]: [VariableAssignment](#anchor14)
    * _Variable assignments to perform when the transition fires._
    * Containment: contains


```
@GenModel(documentation="Class to represent a (potentially guarded) CFA instance transition with assignment.")
class AssignmentTransition extends Transition {
	@GenModel(documentation="Variable assignments to perform when the transition fires.")
	contains VariableAssignment[] assignments
}
```
####  Class `CallTransition` {#anchor12}

_Class to represent a (potentially guarded) CFA instance transition with automaton call. _
_	Semantically, this is equivalent to copying the called automata here, replacing this transition._

**Extends**: `Transition` (unresolved)


**References**:
- **calls**  [0..*]: [Call](#anchor13)
    * _The calls performed upon transition. If it contains multiple calls, it is a parallel composition._
      _		In that case, all input assignments are performed before any of the executions and all output assignments are _
      _		performed after all executions._
    * Containment: contains
    * Opposite: [Call](#anchor13).`parentTransition`


```
@GenModel(documentation="Class to represent a (potentially guarded) CFA instance transition with automaton call. 
	Semantically, this is equivalent to copying the called automata here, replacing this transition.")
class CallTransition extends Transition {
	@GenModel(documentation="The calls performed upon transition. If it contains multiple calls, it is a parallel composition.
		In that case, all input assignments are performed before any of the executions and all output assignments are 
		performed after all executions.")
	contains Call[] calls opposite parentTransition
}
```
####  Class `Call` {#anchor13}

_Class to represent a CFA automaton instance call._

**Extends**: `EObject`


**References**:
- **parentTransition**  [1..1]: [CallTransition](#anchor12)
    * _Transition which contains the call._
    * Containment: container
    * Opposite: [CallTransition](#anchor12).`calls`
- **calledAutomaton**  [1..1]: [AutomatonInstance](#anchor10)
    * _Automaton to be executed._
    * Containment: refers
- **inputAssignments**  [0..*]: [VariableAssignment](#anchor14)
    * _Input assignments of the called automaton. Performed in arbitrary order, before executing _
      _		'calledAutomaton'._
    * Containment: contains
- **outputAssignments**  [0..*]: [VariableAssignment](#anchor14)
    * _Output assignments of the called automaton. Performed in arbitrary order, after executing _
      _		'calledAutomaton'._
    * Containment: contains


```
@GenModel(documentation="Class to represent a CFA automaton instance call.")
class Call {
	@GenModel(documentation="Transition which contains the call.")
	container CallTransition [1] parentTransition opposite calls
	@GenModel(documentation="Automaton to be executed.")
	refers AutomatonInstance [1] calledAutomaton
	@GenModel(documentation="Input assignments of the called automaton. Performed in arbitrary order, before executing 
		'calledAutomaton'.")
	contains VariableAssignment[] inputAssignments
	@GenModel(documentation="Output assignments of the called automaton. Performed in arbitrary order, after executing 
		'calledAutomaton'.")
	contains VariableAssignment[] outputAssignments
}
```
####  Class `VariableAssignment` {#anchor14}

_Class to represent a variable assignment in the CFA instance._

**Extends**: `Freezable` (unresolved)


**References**:
- **leftValue**  [1..1]: [AbstractVariableRef](#anchor6)
    * Containment: contains
- **rightValue**  [1..1]: `Expression` (unresolved)
    * Containment: contains


```
@GenModel(documentation="Class to represent a variable assignment in the CFA instance.")
class VariableAssignment extends Freezable {
	contains AbstractVariableRef [1] leftValue
	contains Expression [1] rightValue
}
```
####  Abstract class `VariableAnnotation` {#anchor15}

_Abstract superclass for variable annotations._

**Extends**: `Annotation` (unresolved)


**References**:
- **parentVariable**  [1..1]: [AbstractVariable](#anchor2)
    * Containment: container
    * Opposite: [AbstractVariable](#anchor2).`annotations`


```
@GenModel(documentation="Abstract superclass for variable annotations.")
abstract class VariableAnnotation extends Annotation {
	container AbstractVariable [1] parentVariable opposite annotations
}
```
####  Class `DirectionVariableAnnotation` {#anchor16}

_Variable annotation to indicate mode in which the annotated variable should be interpreted or used._

**Extends**: [VariableAnnotation](#anchor15)

**Attributes**:
- **direction**  [1..1]: `DataDirection` (unresolved)



```
@GenModel(documentation="Variable annotation to indicate mode in which the annotated variable should be interpreted or used.")
class DirectionVariableAnnotation extends VariableAnnotation {
	DataDirection [1] direction
}
```
####  Class `OriginalDataTypeVariableAnnotation` {#anchor17}

_Variable annotation to trace the original data type of the variable in the PLC code._

**Extends**: [VariableAnnotation](#anchor15)

**Attributes**:
- **plcDataType**  [1..1]: `EString`



```
@GenModel(documentation="Variable annotation to trace the original data type of the variable in the PLC code.")
class OriginalDataTypeVariableAnnotation extends VariableAnnotation {
	String [1] plcDataType
}
```
####  Class `ReturnValueVariableAnnotation` {#anchor18}

_Variable annotation to indicate that the annotated variable is used to store return values of an automaton._

**Extends**: [VariableAnnotation](#anchor15)




```
@GenModel(documentation="Variable annotation to indicate that the annotated variable is used to store return values of an automaton.")
class ReturnValueVariableAnnotation extends VariableAnnotation {
}
```
####  Class `InternalGeneratedVariableAnnotation` {#anchor19}

_Variable annotation to indicate that the annotated variable was created for internal purposes and it does not correspond to any of the source program's fields/variables._

**Extends**: [VariableAnnotation](#anchor15)




```
@GenModel(documentation="Variable annotation to indicate that the annotated variable was created for internal purposes and it does not correspond to any of the source program's fields/variables.")
class InternalGeneratedVariableAnnotation extends VariableAnnotation {
}
```
####  Class `ForLoopLocationAnnotation` {#anchor20}

_Location annotation to represent a FOR loop statement that starts at the given location._

**Extends**: `LoopLocationAnnotation` (unresolved)


**References**:
- **loopVariable** : [AbstractVariableRef](#anchor6)
    * Containment: contains
- **loopVariableInit** : `Transition` (unresolved)
    * Containment: refers
- **loopVariableIncrement** : `Transition` (unresolved)
    * Containment: refers


```
@GenModel(documentation="Location annotation to represent a FOR loop statement that starts at the given location.")
class ForLoopLocationAnnotation extends LoopLocationAnnotation {
	contains AbstractVariableRef loopVariable
	refers Transition loopVariableInit
	refers Transition loopVariableIncrement
}
```


