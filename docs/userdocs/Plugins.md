## Plug-ins

PLCverif provides a rich interface for plug-ins to extend its base functionality. The plug-ins can add new components to the base model checking functionality (e.g., new verification backends, new requirement description methods), but also new workflows that are related to PLC programs.

![Plug-ins in the formal verification workflow](img/workflow.png)


### Install new plug-ins

Simply copy the `.jar` plug-in files of the (core or GUI) plug-ins to be installed into the `dropins` folder of PLCverif, or to [any of its subfolders](https://help.eclipse.org/oxygen/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Freference%2Fmisc%2Fp2_dropins_format.html). You can create the folder manually if it does not exist. From the next execution of PLCverif, the plug-ins will be loaded. You can update the plug-ins by simply removing the old versions and copying the new versions to the `dropins` folder.