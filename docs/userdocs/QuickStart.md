## Quick introduction

This section briefly overviews the usage of the tool. Although PLCverif is a generic tool that supports various workflows for PLC code analysis, here we focus on its main use case, the formal verification workflow. Note that the description below assumes that only the plug-ins shipped with PLCverif are installed. Obviously, if additional plug-ins are used, more features or options may be available.

The [next section](Demo.md) will overview the features introduced here through concrete examples.

### Installation
PLCverif is developed in **Java 11**, so please be sure to have that version in your system.

PLCverif is available in GUI and command line version, which are distributed separately. Similarly, there are different variants of PLCverif for the different operating systems. Each variant of PLCverif is distributed in (one or more) ZIP file(s). Before executing PLCverif, these ZIP files need to be unpacked into the same folder. Do not mix the files corresponding to the GUI and the command line versions.

Depending on the licensing, PLCverif may or may not contain the external verification engines. By default these model checking engines are located under the `tools` subfolder of PLCverif. If they are not included, they have to be acquired separately and their paths need to be set up correctly, see [below in the Configuration section](#configuration).

### Layout of PLCverif
The layout of PLCverif is similar to an instance of Eclipse.
On the left side of the window, the _Project Explorer_ shows the projects and files in the workspace.
Below, the _Outline_ view shows the outline of a currently open PLC source file.
The middle part is reserved for the editors.
At the bottom of the window, three views can be seen:
the _Progress_ view showing the progress of the ongoing (verification) tasks,
the _Problems_ view shows the potential syntax errors,
and the _Console_ view provides detailed information about the ongoing executions for the expert users.

![PLCverif after startup](img/after-startup.png)

PLCverif (without additional extensions) handles two main types of files for formal verification: source files (PLC program files) and verification case files.
Every file is organized into _Projects_. A new project can be created by clicking on the _PLCverif project..._ button at the top of the _Project Explorer_ view, or from the _File > New..._ menu, by selecting the _PLCverif / PLCverif project_ wizard.

### Sources

A **PLC source file** contains a PLC program written in one of the supported languages. Currently, the basic PLCverif release supports PLC programs written in Siemens SCL or STL language. The extension of the source files can be SCL or AWL, independently from the actual language used in the file. In addition, SCL and STL blocks can be mixed in the same file. Cross-references between the files of the same project are supported.

![PLC code editor in PLCverif](img/code-editor.png)

**Editor features.** The SCL and STL files can be edited directly in PLCverif. This editor provides the following features: 
- *Syntax highlighting*: the SCL and STL keywords will be highlighted.
  
  ![Syntax highlighting in STEP 7 code editor](img/syntax-highlight.png)
- *Validation*: syntactic and certain semantic errors will be shown to the user.
  
  ![Validation in STEP 7 code editor](img/validation.png)
- *Static analysis*: some basic suggestions can also be given to the user to improve the quality of the code.

  ![Static analysis in STEP 7 code editor](img/static-analysis.png)
- *Content assist*: by pressing _Ctrl+Space_, context-sensitive suggestions can be accessed.
  
  ![Content assist in STEP 7 code editor](img/content-assist.png)
- *Code outline*: the outline in the bottom left corner helps to navigate in bigger code base.

  ![Code outline in STEP 7 code editor](img/code-outline.png)

- *Code formatting*: by pressing _Ctrl+Shift+F_ or selecting _Format_ from the context menu (by right-clicking on the source code and selecting _Source / Format_), the code can be automatically formatted.

  ![Code formatting in STEP 7 code editor](img/code-formatting.gif)

- *Obfuscation*: by selecting _Obfuscate_ from the context menu (opened by right-clicking on the source code), the code can be automatically obfuscated. This means that all variable names will be replaced with meaningless variable names, which can help in sharing the code with others without revealing too much about the implementation. 

  ![Code obfuscation in STEP 7 code editor](img/obfuscate.gif)

**Symbol tables.** PLCverif does not support the symbol tables. However, it is possible to **import a symbol table** and represent its contents as global variables and instance data blocks. This will not ensure that the symbolic access will return the same values as the access via the absolute address, thus this solution is only adequate if the access is done always through the symbol names.
To import a symbol table, select the _File > Import..._ menu item, then select the _PLCverif / Symbol table (SDF format)_ wizard. The symbol table to be imported should be exported in System Data Format (SDF) from the STEP 7 development environment.

### Verification case editing and execution

A **verification case file** (with .vc3 extension) contains a single requirement together with its description and configuration.
More precisely, a verification case file describes an execution of the verification workflow on a given set of program files, with a given configuration, checking one or more requirements. (In case of assertion violation checking, multiple assertions can be checked at once.)

![Verification case editor in PLCverif](img/verification-case-editor.png)

The verification case editor is split into sections.

The **Metadata section** contains information needed to identify the verification case (_ID_) and to describe it (_Name_, _Description_).

The **Source files section** defines the scope of the verification. Here the source files to be used in the verification can be selected. Only the selected files will be included in the verification, even if they refer to files outside of the selection. If the source files were modified while the verification case editor is open, the _Reload source files_ button can force to reload the files.
In the _Language frontend_ combobox the parser to be used can be selected. Currently only the _STEP 7_ frontend is included in PLCverif.
The _Entry block_ defines the entry of the verification, i.e., this is the block that is going to be executed in an infinite loop. For example, in case of the verification of a full PLC program, this will be the `OB1`, while if the scope of the verification is only a single function block, that block will be the entry block. If the selected entry block is a function block, an instance of this function block will automatically be created with the name `instance` (thus its member variables can be accessed as `instance.varName`).

![Source file selection](img/sources-section.png)

The **Verification backend section** defines the verification engine to be used. PLCverif supports three verification backends out of the box: NuSMV/nuXmv, Theta and CBMC. Depending on the selected verification backend, various additional settings may be available.

![Verification backend section](img/backend-section.png)

> **[info] Note for CBMC**
> 
> Even when the CBMC binaries are shipped with PLCverif, it depends on external C compilers.
> - On Windows, make sure you run PLCverif as administrator and that Visual Studio is installed, including the C++ command line tooling. Make sure that the path to the _Visual Studio initialization batch file_ (`vcvars64.bat`) in the Preferences is set up correctly. For Visual Studio 2017 Community Edition, the default location of this file is `C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvars64.bat`. Make sure to include the `Desktop development for C++ package`, which contains the `VC++ v141 toolset` including the necessary `vcvars64.bat` file. The file TimeoutExecutor.dll can be downloaded from https://cernbox.cern.ch/index.php/s/aM4W7R1iNlmCI1G.
> - On Linux, you may need to install `gcc` using your favorite package manager.
> In case of difficulties, you may find additional information on the [website of CBMC](https://www.cprover.org/cbmc/).

The **Requirement section** defines the requirement to be checked when executing the verification case. Out of the box PLCverif supports two requirement types (requirement description methods): _Pattern requirement_ and _Assertion requirement_. There is an additional option: _FRET requirement_, but this requires FRET to be installed and its path to be configured (under _Preferences > PLCVerif > Settings_).

If the selected requirement type is _Assertion requirement_, PLCverif will try to verify that the assertions (`//#ASSERT <expression>` or `//#ASSERT <expression> : <tag>`) in the source code under verification cannot be violated (the given expression at the given location can never be evaluated to true). The user needs to select the assertions to be checked. Select the `*` entry under _Assertions to check_ in order to check all present assertions. Otherwise, select the assertion tags to be checked. If the entry `*` is not selected, none of the assertions without tags will be checked.

> **[info] Note for Theta**
> 
> Download it from https://github.com/ftsrg/theta/releases. For Windows, include in your **path** system variable the directory of Theta libraries, which contains the file `libz3.dll`.

![Assertion requirement](img/assertion-requirement.png)

If the selected requirement type is _Pattern requirement_, PLCverif will try to check whether the given requirement is satisfied or violated. The user needs to choose a convenient one from the list of requirement patterns. A requirement pattern is a carefully worded English sentence that contains some placeholders (parameters). The user needs to fill these placeholders with Boolean expressions. These expressions may contain constants (e.g., `true`, `false`, `123`, `-1`), operators (e.g., `NOT`, `AND`, `OR`, `XOR`, `>`, `<`, `>=`, `<=`, `=`, `<>`, `!`, `-->`, as well as their Unicode representations: `¬`, `∧`, `∨`, `→` or `⇒`, `≤`, `≥`) and PLC variables / memory addresses (e.g., `var1`, `dataBlockName.var1`, `I0.0`). Content assist can be accessed by pressing _Ctrl+Space_. The formal representation will be automatically generated based on the pattern selection and parameters provided in the verification case.
In the grey box, the user can see a preview of the filled pattern-based requirement.

![Pattern-based requirement](img/pattern-requirement.png)

If **FRET requirement** is selected (and FRET is installed and FRET settings under _Preferences > PLCVerif > Settings_ are configured), the _Edit in FRET_ button becomes available. It opens FRET in external tool mode and shared the variable glossary with FRET. FRET is the Formal Requirements Elicitation Tool and it enables the construction of requirements in Fretish, a constrained natural language. It also includes further helpful tools for understanding semantics: a simulator and automatically generated descriptions and diagrams about the requirement. Further instructions on how to install and use FRET are available at [https://github.com/NASA-SW-VnV/fret](https://github.com/NASA-SW-VnV/fret).

![FRET requirement UI](img/fret-requirement-gui.png)

The **Requirement – advanced settings section** permits the advanced users to alter the behavior of some of the variables present in the PLC source code. Each variable can be marked as an _input_ (i.e., a new nondeterministic value will be assigned to it at the beginning of each PLC cycle) or a _parameter_ (i.e., a new nondeterministic value will be assigned to it at the beginning of the PLC execution, but not at each consecutive cycle). Variables can also be _bound to_ a constant value, effectively behaving as a constant. The given constant value will be assigned to the variable at the beginning of each cycle. However, the assignments in the program code to this variable will not be removed. The rest of the variables will behave as _static variables_, i.e., their values will be only modified if it is explicitly stated in the source code. Exception from this are the addresses in the `I` memory area and the variables declared in the `VAR_INPUT` block of the entry block (if any), as they will behave automatically as they were marked as inputs in the verification case file.

![Advanced requirement settings](img/req-advanced.png)

The **Reporters section** allows the user to select which reporters are to be used to visualize and/or summarize the outcome of the verification. Note that some reporters are needed for the GUI itself and they will be used even if none of the reporters are selected. 

![Reporters section](img/reporters.png)

The **Additional settings section** permits the expert users to add any valid command line parameter to the verification case file that does not have a graphical interface representation. A list of all defined settings can be browsed using the _Add..._ button.

![Custom settings section](img/custom-settings.png)

![Add custom settings](img/add-custom-setting.png)

After the verification case is filled, the **Verify section** allows the user to start the verification process. By clicking the _Verify!_ button, the model generation, requirement representation, model checker execution and reporting will automatically be performed. After the execution some key information will be displayed, such as the last result and the duration of the execution. More information can be acquired by clicking on the _Open report_ button. The generated reports are automatically saved in the corresponding project's output folder. The ID set in the _Metadata_ section will be used as file name prefix.

![Verify section](img/verify-section.png)

![Verification report](img/verification-report.png)



### Configuration {#configuration}
The verification case files do not have to specify every setting explicitly. Each plug-in has some default values for the optional settings. In addition, PLCverif has default settings which will be used for every verification in every project, unless these values are overwritten explicitly in the verification case files.
The PLCverif defaults can be modified by clicking on the _Preferences_ menu, then selecting the _PLCverif_ node in the preferences tree. 
The settings provided in this window are saved to the `settings` folder of PLCverif. In order to keep these preferences, make sure you are running PLCverif as administrator.

![Preferences](img/preferences.png)



### Console version
PLCverif has a command line version too. Any verification case defined on the graphical interface of PLCverif can be executed by the command line version as well, by executing `plcverif-cli.exe <path-to-verification-case-file>`.
If some parameters are not defined in the verification case file or if they need modification, further settings can be given after the verification case file path, for example `plcverif-cli.exe <path-to-verification-case-file> -job.reporters={plaintext, html}`.
The relative paths used in the verification case file will be resolved compared to the start directory (and not compared to the location of the verification case file).
If `plcverif-cli` is executed without parameter, a help is displayed with all acceptable settings.

Notice that the verification case file (or any settings file) is just a new line-separated list of command line parameters for the more convenient use.

![PLCverif in console](img/console.png)
