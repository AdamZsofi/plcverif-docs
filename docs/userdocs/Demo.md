# PLCverif through an example


## Simple example (ABS function)

In this example we implement a simple absolute value function, then we will check whether it satisfies our expectation, viz. the return value of the function is always non-negative.

1. Open PLCverif.
2. **Create a new PLCverif project.**
   1. Click on the _PLCverif project..._ at the top of the _Project Explorer_ view.
      
      ![Creating a new PLCverif project](img/example-abs/new-project.png)

      - Alternatively, you can right click on the _Project Explorer_ view and select _New / Project..._, then _PLCverif / PLCverif project_. 
   2. Type the name of the project (e.g., `demo-abs`) to the _Project name_ field. Select the _Use built-in functions_ if you would like to use built-in PLC blocks.
   
      ![Filling the New PLCverif project wizard](img/example-abs/new-project-wizard.png)
      - A new directory will be created with the given name under the `<PLCverif installation>/workspace/` directory (if the default location is used). All files of this project will be stored in this directory (verification case files, source files, reports).
3. **Create a new SCL source file** in the newly created project.
   1. Click on the _SCL file..._ at the top of the _Project Explorer_ view.
      
      ![Creating a new SCL file](img/example-abs/new-scl.png)
      - Alternatively, you can right click on the project in the _Project Explorer_ and select _New / Other..._ and then _PLCverif / SCL file_ or _PLCverif / STL file_.
      - The STEP 7 language frontend will determine the language of the source code based on the content of the file, thus it does not matter what is the extension of the file.
    1. Type the name of the source file to the _File name_ field. If the extension is omitted, it will be inserted automatically.
       
       ![Filling the New SCL file wizard](img/example-abs/new-scl-wizard.png)
    2. Open the file by double clicking on it in the _Project Explorer_.
    3. Remove the default content of the new source file and type the following:
       ```
       FUNCTION FuncAbs : INT
         VAR_INPUT
       	   in : INT;
         END_VAR
       BEGIN
         IF in > 0 THEN
           FuncAbs := in;
         ELSE
           FuncAbs := -in;
         END_IF;
       END_FUNCTION
       ```
       - Use _Ctrl+Space_ for content assist.
       - Use _Ctrl+Shift+F_ for code formatting.
       - Use _Ctrl+S_ to save the code.
       
       ![Filling the SCL file](img/example-abs/type-scl-content.gif) 
4. **Create a new verification case file** in the newly created project. 
    1. Click on the _Verification case..._ at the top of the _Project Explorer_ view.
    
       ![Creating a new verification case file](img/example-abs/new-vc.png)
        - Alternatively, you can right click on the project in the _Project Explorer_ view and select _New / Other..._ and then _PLCverif / Verification case_.
    2. Type the name of the verification case file to the _File name_ field. If the extension (`vc3`) is omitted, it will be inserted automatically.
       
       ![Filling the New verification case wizard](img/example-abs/new-vc-wizard.png)
    3. Open the file by double clicking on it in the _Project Explorer_ view.
5. **Fill the verification case file.** The verification case file represents checking one or more requirements on a given set of source files. It contains additional configuration for the verification to be performed, such as settings for the reductions, the verification backend to be used, etc.
   1. **Fill the _Metadata_ section.** This information will be used to identify the verification case (and its results).
      
      ![Filling the Metadata section of the verification case](img/example-abs/fill-vc-metadata.png) 

        - The ID will be used to derive the names for the files generated during the verification. This is typically the same as the name of the verification case (this is the default value as well).
        - The _Name_ is a short, human-readable representation of the verification case.
        - The _Description_ is a longer description of the verification case.
   2. **Fill the _Source files_ section.** Here you should select which source files shall be used for the verification and how should they be interpreted.
       
      ![Filling the Source files section of the verification case](img/example-abs/fill-vc-source.png)
        - In _Source files_, select the source files that need to be parsed to check this verification case. Typically, it is enough to select `*.scl  (all scl files in this project)` meaning that all SCL files will be included in the verification.
          - The file names are case sensitive, even on Windows.
          - If with the given selection one of the dependencies cannot be resolved, a red error message notifies about it.
            
            ![Error message on unresolved dependencies](img/example-unicos/fill-vc-source-unresolved.png)
          - Note that if the built-in blocks are required, they need to be included separately (i.e., the `*.scl` will not include them).
        - In _Language frontend_, select the parser to be used. For Siemens SCL and STL files this shall be `STEP 7`. (Currently there is no other language frontend included in PLCverif.)
        - In _Entry block_, select the PLC block that will be the entry point of the verification. When a full PLC application is analyzed, this should be the `OB1` block. Otherwise, this is typically the block under verification.
          - The inputs of this block (as well as the  input memory area) will be re-initialized using non-deterministic values at the beginning of each PLC cycle. Each other block's input values will be expected to be defined by the callers.
          - If the selected block is a function block, a new instance will be created for it.
   3. **Fill the _Verification backend_ section.** Here you should select the verification engine (verification backend) to be used to perform this verification case. Depending on the selected verification backend, additional settings may be available.
       
       ![Filling the Verification backend section of the verification case](img/example-abs/fill-vc-backend.png)

   4. **Fill the _Requirement_ section with a _Pattern-based requirement_.** Here you should provide the requirement that will be checked upon the execution of the verification case. Currently, two types of requirements are supported: assertions and pattern-based requirements.
       
       ![Filling the Requirement section of the verification case for pattern-based requirements](img/example-abs/fill-vc-patternreq.png)
        - For this example, choose the _Pattern requirement_ for the _Requirement type_ option.
        - The _Pattern_ combobox shows the supported pattern types.
          - It is possible to define your own requirement patterns and to load them in the _Preferences menu_.
          - You can click on the _?_ button to get more information about the available patterns.
          - For this example, choose the "_{1} is always true at the end of the PLC cycle._" pattern.
        - Fill the placeholders of the selected pattern. In this example, there is only one placeholder (`{1}`). Type `FuncAbs.RET_VAL >= 0` to the corresponding textbox.
          - _Ctrl+Space_ can be used to open the content assist menu.
          - If the selected entry block was a function block, the automatically created instance would be available under the name `instance`.
          - If you hover the mouse over the `1:` text, you get more information about the expected expression.
        - Note that the syntax used to fill the placeholders is defined by PLCverif and is independent from the used language frontend (STEP 7).

    > [info] **Alternative solution: use assertion requirement**
    > 
    > Alternatively, it is possible to express the same requirement as an assertion. For this, do the following steps:
    > - Include `//#ASSERT FuncAbs >= 0` in the source code, just before the `END_FUNCTION` keyword. Optionally, the assertion can be named (tagged): `//#ASSERT FuncAbs >= 0 : name_of_the_assertion`. The names (tags) do not have to be unique, several assertions may have the same tag.
    >    - Note that the syntax used to describe assertions is specific to STEP 7, essentially STEP 7 Boolean expressions can be used, plus the `-->` implication operator. Calls are not permitted in the assertions, except for the type conversion calls.
    > - Select _Assertion requirement_ for _Requirement type_ under the _Requirement_ section.
    > - Select in _Assertions to check_ `*` for checking all assertions in the source file, or the names of the assertions to be checked.
    > 
    >   ![Filling the Requirement section of the verification case for assertion requirements](img/example-abs/fill-vc-assertreq.png)
    > - The list of available assertions is not updated automatically. If you edit the source file while the verification case editor is open, click on _Reload source files_ under the _Source files_ section to refresh the list of assertions.
    
6. **Verify!** Now we are ready for verification. In the _Verify_ section, click on the _Verify!_ button to start the verification.
     
   ![Verify!](img/example-abs/fill-vc-verify.png)
  
   After the execution of the verification engine, the result is shown on the form. By clicking on the _Open verification report_ button, more information can be found about the verification result.
  
   ![Result of the verification](img/example-abs/fill-vc-verify2.png)
  
   The _Verification report_ provides additional information about the results.
     
   ![Verification report](img/example-abs/verif-report.png)

     - First, an overview summarizes the performed verification (verification metadata, the source files used, the requirement checked, the verification tool used). In this example the verification result is *violated*, i.e., the requirement "_FuncAbs.RET_VAL >= 0 is always true at the end of the PLC cycle._" is not satisfied by the checked source code.
     - If available, a counterexample is provided. In this case the counterexample shows that if the `FuncAbs.in` variable equals `-32768`, then the return value of the function (`FuncAbs.RET_VAL`) will be `-32768`, which is clearly negative.
     - The report is generated in the `output` folder of the parent PLCverif project. The folder name can be changed in the _Preferences_ menu.
     - The generated HTML report is a self-contained file, it can be copied for archival.
     - By clicking on the _Show/hide more details_, expert information can be accessed.
       - The _Verification stages_ section shows each performed stage of the verification, with the corresponding run time.
       - The _Log_ section shows log messages generated during the verification. It can be configured what is the minimum severity of log messages to be included. The default setting can be changed in the _Preferences menu_, under _PLCverif / HTML verification reporter / Minimum log message severity to report_.
       - The _Verification tool outputs_ shows the standard output and standard error outputs of the verification tool.
       - The _Effective settings used for the verification_ section shows the settings used to perform the verification. This is a compound of the settings in the verification case, the default settings of the plug-ins and the installation-specific settings of PLCverif (which can be accessed in the _Preferences menu_). This can be used to reproduce a verification execution without needing any assumption on the default settings.
  
7. **Fixing the problem.** The next step is to decide what is the problem. This cannot be automated. The expert user needs to decide if the implementation or the requirement is incorrect. In this example, we assume that `FuncAbs(-32768) = -32768` is the desired behavior (like in case of the STEP 7 built-in `ABS` function), thus the requirement is incomplete or not precise enough. Therefore we will modify the requirement to express "_The return value is non-negative if the input is not -32768_".
   - Close the report to go back to the verification case. 
   - Change the metadata accordingly.
   - Change the chosen _Pattern_ in the _Requirement_ section to "_If {1} is true at the end of the PLC cycle, then {2} should always be true at the end of the same cycle._".
   - Change the parameter _1_ (_Condition_) to `FuncAbs.in <> -32768`.
   - Change the parameter _2_ (_Implication_) to `FuncAbs.RET_VAL >= 0`.
     
     ![Updated pattern-based requirement](img/example-abs/fill-vc-patternreq2.png)
   - Click on the _Verify!_ button under the _Verification_ section. The result should now be _Satisfied_.
     
     ![Verification with the updated requirement](img/example-abs/fill-vc-reverify.png)
     - Note that when the content of the verification case is changed, the previous result will turn gray and it will be marked as obsolete, as a previous result may not hold for the updated verification case.
   > [info] **Alternative solution: use assertion requirement**
   > 
   > Alternatively, it is possible to express the new requirement as an assertion too: `//#ASSERT FuncAbs.in <> -36768 --> FuncAbs >= 0`. In assertions, all PLC operators and the `-->` implication operator are accepted.



### Additional options

The essential use cases of PLCverif were mentioned in the example above. In the following, some more expert features are introduced. They can be used optionally to fine-tune the verification.

- **Select the needed reporters.** In the _Reporters_ section you can choose which reporters should be used. The HTML reporter will always be used. All reports will be created in the `output` folder (except if set otherwise in the _Preferences_).
- **Select input and parameter variables.** In the _Requirement – advanced_ section, you can fine-tune the semantics used for the different variables.
  - An _input variable_ gets a non-deterministic value at the beginning of each modeled PLC cycle. By default, every input variable (`VAR_INPUT`) of the entry block is handled as input variable, as well as every part of the input memory area that is being used in the program (e.g., `I0.1`).
    - In the example setting above, `FuncAbs.in` to input would not change the generated model or the result, as `in` is defined as an input variable for the entry block, thus it is automatically handled as input.
  - A _parameter variable_ gets a non-deterministic value at the beginning of the execution. It keeps its value for the rest of the execution.
  - A variable can be _bound to_ a given constant value as well. In that case the variable will be (re)set to this given value at the beginning of every PLC cycle.
    - For example, if in the example above we set `FuncAbs.in` to 3, then the requirement "_FuncAbs.RET_VAL >= 0 is always true at the end of the PLC cycle._" will be satisfied. However, we will only check the case when `in=3`. As in this case the return value is always `3`, the requirement is satisfied. When some of the values are bound, the verifier needs to be extremely cautious.
    
      ![Value binding in the verification case](img/example-abs/fill-vc-valuebinding.png) 
- **Additional settings.** Certain advanced settings are not accessible on the GUI. If they need to be altered, the _Advanced settings_ section can be used. Here new lines can be added to the settings file representing the verification case.
  - By clicking on the _Add new line_ button, a new line is inserted to the list and it can be directly edited.
  - If you click on the _Add..._ button, a new window will show all known plug-ins with all known settings and their documentation.
  - By executing command-line version of PLCverif without any parameter, a list of all plug-ins all settings can be printed to the console. In addition, this list can be found [in the documentation](CmdlineArgs.md). 

> [info] **Content of the verification case file**
> 
> For expert users it is good to know that the verification case editor provides simply a graphical interface for a settings file describing a verification job. The verification case files can be opened with a text editor too: right-click on a verification file, then choose _Open With / Text Editor_. For example, the verification case used in the first verification has the following textual representation:
> ```
>  -description = "The return value of the absolute function shall always be non-negative (zero or positive)."
>  -id = case1
>  -job = verif
>  -job.backend = nusmv
>  -job.backend.algorithm = Classic
>  -job.req = pattern
>  -job.req.pattern_id = pattern-invariant
>  -job.req.pattern_params.1 = "FuncAbs.RET_VAL >= 0"
>  -lf = step7
>  -lf.entry = FuncAbs
>  -name = "Return value is always non-negative"
>  -sourcefiles.0 = *.scl
> ```
>  The effective settings contained in the expert part of the verification report is a superset of these settings:
> ```
> -description = "The return value of the absolute function shall always be non-negative (zero or positive)." 
> -id = case1 
> -job = verif 
> -job.backend = nusmv 
> -job.backend.algorithm = Classic 
> -job.backend.binary_path = .\tools\nuxmv\nuxmv.exe 
> -job.backend.df = false 
> -job.backend.dynamic = true 
> -job.backend.req_as_invar = false 
> -job.backend.timeout = 60 
> -job.reporters.0 = html 
> -job.reporters.0.hide_internal_variables = true 
> -job.reporters.0.includeSettings = true 
> -job.reporters.0.include_stack_trace = false 
> -job.reporters.0.minLogLevel = Debug 
> -job.reporters.0.show_logitem_timestapms = false 
> -job.reporters.1 = summary 
> -job.req = pattern 
> -job.req.pattern_file = "" 
> -job.req.pattern_id = pattern-invariant 
> -job.req.pattern_params.1 = "FuncAbs.RET_VAL >= 0" 
> -lf = step7 
> -lf.compiler = Step7v55 
> -lf.entry = FuncAbs 
> -name = "Return value is always non-negative" 
> -output = C:\temp\PLCverif-output5730644549877847785 
> -reductions.0 = basic_reductions 
> -reductions.0.ExpressionPropagation_maxage = 50 
> -reductions.0.ExpressionPropagation_maxexprsize = 100 
> -reductions.0.ExpressionPropagation_maxlocations = 50000 
> -reductions.0.fine_logging = false 
> -reductions.0.show_progress = true 
> -sourcefiles.0 = C:\xxxxxxxx\source.scl 
> ```    


- Entry blocks.
> [info] **Effect of entry block selection on the result**
> 
> If there are multiple blocks in the selected source files, different **entry blocks** can be selected which influences the semantics and the result of the verification.
> Let us assume that the source file contains a `Wrapper` function block in addition to the `FuncAbs` function:
> ```
> FUNCTION_BLOCK Wrapper
>     VAR_INPUT
>         in : INT;
>     END_VAR
>     VAR_OUTPUT
>         out : INT;
>     END_VAR
> BEGIN
>     IF in = -32768 THEN
>         out := 32767;
>     ELSE
>         out := FuncAbs(in);
>     END_IF;
> END_FUNCTION_BLOCK
> 
> FUNCTION FuncAbs : INT
> 	VAR_INPUT
> 		in : INT;
> 	END_VAR
> BEGIN
> 	IF in >= 0 THEN
> 		FuncAbs := in;
> 	ELSE
> 		FuncAbs := -in;
> 	END_IF;
> 	
> 	//#ASSERT FuncAbs >= 0
> END_FUNCTION
> ```
>   - If the entry block is `FuncAbs`, it will be executed with all possible `in` input values, including `-32768`. Therefore the assertion will be violated.
>   - If the entry block is `Wrapper`, it will be executed with all possible `in` input values. However, due to the conditional statment in the `Wrapper`, it will never call `FuncAbs` with `in = -32768`. Consequently, the assertion will not be violated.

- **Fine-tuning the reductions.** The reductions need to have a trade-off between small enough model size and reasonable reduction execution time. Therefore certain reductions need to be limited to avoid spending excessive resources on the reduction itself. The default values for these limits are carefully chosen, however it may not suit all possible verification cases. For example, in case of exceptionally large programs the only way to get verification results is to allow more resources to be spent on the reductions.
  
  To fine-tune the reductions, the _Advanced settings_ need to be used. First, it has to be explicitly defined which reductions are to be used, for example by adding `-reductions.0 = basic_reductions`. Then by adding for example `-reductions.0.ExpressionPropagation_maxexprsize = 1000`, you can increase the upper limit for the maximum expression size that can be produced by the expression propagation reduction. You can use the _Add new custom setting_ dialog by clicking on the _Add..._ button. Note that the ID of the reduction (`0` in the previous example) needs to be typed manually, as the _Add new custom setting_ dialog will insert `-reductions.<ID>.ExpressionPropagation_maxexprsize` as key.

  The defaults of the reductions can be changed, however as they target the expert users, they don't have a graphical interface. Instead, you can edit the `settings/basic_reductions.settings` file directly in the PLCverif folder.

- **Diagnostic outputs.** For diagnostic purposes, various additional outputs can be generated by the verification job, such as the control-flow automata representations at different stages (e.g., before and after reductions). As this consumes additional resources, it needs to be enabled separately. To enable the diagnostic outputs, go to the _Preferences menu_, then under _PLCverif / Verification job_ select _Diagnostic output generation enabled_. The diagnostic outputs will be written to a temporary directory. The path to this directory is included in the verification report's expert section.

## Time handling example

PLCverif has an abstract time handling strategy. Essentially, it has the following principles:
- Instead of a continuously elapsing time, it is assumed that the PLC cycle is executed in 0 time, which is followed by a non-deterministic delay.
- This non-deterministic delay is determined by the `T_CYCLE` global variable which gets a non-deterministic value between 0 and 255 at the beginning of each PLC cycle. This value corresponds to the cycle time in ms.
- An internal variable (`__GLOBAL_TIME`) keeps track of the sum of `T_CYCLE` values since the execution. It is represented as a `TIME` (32-bit, signed), thus it can (and it will) overflow eventually.
- To limit the state space, it is possible to set the cycle time to a fixed amount. To achieve this, bind the `T_CYCLE` to a value that is maximum 255 ms (`255` or `T#255ms`).
- Read more about the time representation strategy in:
  - [B. Fernández et al. Modelling and formal verification of timing aspects in large PLC programs. Proceedings of the 19th IFAC World Congress, pp. 333-3339. Elsevier, 2014.](https://doi.org/10.3182/20140824-6-ZA-1003.01279)
  - [D. Darvas. Practice-oriented formal methods to support the software development of industrial control systems. PhD thesis, BUTE, 2017. Section 3.3.1](https://doi.org/10.5281/zenodo.162950)

Take the following PLC program. (Assume that the Siemens S7-300 built-in block library is referred.)
```scl
FUNCTION_BLOCK foo
	VAR
		t : TON;
		i : INT;
	END_VAR
BEGIN
	IF i < 5 THEN
		t(IN := TRUE, PT := t#400ms);
		i := i+1;
	ELSE
		//#ASSERT t.Q
	END_IF;
END_FUNCTION_BLOCK
```

Essentially, this program will execute the `TON` instance `t` 5 times with `IN=TRUE`, then it will check whether the output of the timer (`t.Q`) is true. The delay time of `t` is 400 ms. If the `T_CYCLE` is non-deterministically chosen between 0 and 255 ms, then obviously the assertion is violated, as the execution of the first four cycles may take less than 400 ms. (Note that if the `TON` is executed five times, then there are 4 non-deterministic delays inbetween.)

Now bind `T_CYCLE` to `t#100ms`! (The literal `100` is accepted as well as `t#100ms`.)

![T_CYCLE variable bound to 100 ms](img/example-time/bind-tcycle.png)

In this case, there will be exactly 400 ms delay between the first and the last call of the `TON` instance `t`, thus `t.Q` will be set to `TRUE` after the last call. Therefore the assertion will hold, the requirement will be satisfied.

> [warning] **Warning**
> 
> It is worth noting a common mistake. Assume that the `i := i + 1;` assignment is missing from the source code. In this case the variable `i` is never incremented, therefore the `ELSE` branch will never be executed. If an assertion statement is never called, it cannot be violated, thus the verification result will be _Satisfied_.


## STL example

STL programs can be verified in the same way as SCL programs. You can experiment with STL program verification using the following simple program.

```stl
FUNCTION_BLOCK DemoStl
  VAR_INPUT
    a, b, c, d, e : BOOL;
  END_VAR
  VAR_OUTPUT
    out : BOOL;
  END_VAR
BEGIN
  NETWORK
    TITLE: 'Main title'
    A(;
    O     a;
    O     b;
    );
    A(;
    O     c;
    O     d;
    );
    A e;
    
    = out;
    
    //#ASSERT out = ((a OR b) AND (c OR d) AND e);
END_FUNCTION_BLOCK
```

> [info] Note that in STL programs the semicolon after the verification assertion statements is mandatory, while in SCL it is optional.

The verification assertion in the program above is satisfied, and it can be shown using PLCverif.

> [warning] **Warning**
> 
> It is worth noting a common mistake. Assume that the assertion used is the following:
> ```
> //#ASSERT out = (a OR b) AND (c OR d) and e;
> ```
> It seems to be equivalent to the assertion above. However, while the first assertion would be satisfied, this latter one would be violated. This is because this latter assertion would be equivalent to `(out = (a OR b)) AND (c OR d) AND e`, as the `=` has a lower precedence than the `AND` operation.


## Baseline verification example

1. Create a new PLCverif project. Make sure it includes the _S7-300_ built-in functions.
2. Fetch and copy the baseline sources from `https://gitlab.cern.ch/ucpc/Baseline/PLC/PLC_S7_300/tree/master/Baseline/Sources`.
3. Import the symbol table containing symbols like `UNICOS_LiveCounter` (referred from the baseline objects). Alternatively, create the expected global variables like this:
   
   ```
   VAR_GLOBAL 
     UNICOS_LiveCounter : DINT;
     First_Cycle : BOOL;
 	   PID_EXEC_CYCLE : TIME;
   END_VAR 
   ```

4. In `CPC_BASE_Unicos.SCL`, remove the functions from `READ_Profibus_DP` to `CPC_FB_RECIPES` as they include features not supported by PLCverif (low-level DB access, `BLOCK_DB_TO_WORD`, etc.). Now the whole project should be error-free.
5. Let us focus on the `CPC_FB_ONOFF` block's verification. In the following, some example requirements will be set up for this object.
   1. Check the following informally defined requirement: _If the `FuStopI` input is true, then the `FuStopISt` output is true as well._ You can try to express the requirement yourself before reading the step-by-step guide below.
      * Create a new verification case with an appropriate name.
      * Fill the _ID_, _Name_ and _Description_ fields.
      * Select the necessary source files. You have two options
        * Either select all SCL files in the project (`*.SCL`), as well as the built-in blocks; 
        * Or select the `CPC_FB_ONOFF.SCL`, `CPC_BASE_Unicos.SCL`, the built-in blocks, and the file containing the global variables (symbol table representation).
      * Make sure that the language frontend selected is _STEP 7_.
      * As entry block, select `CPC_FB_ONOFF`.
      * You can select any of the verification backends. Feel free to experiment with the different options and check their performance.
      * In the _Requirement_ section:
        * Select _Pattern requirement_ as requirement type,
        * Select the implication pattern ("_If {1} is true at the end of the PLC cycle, then {2} should always be true at the end of the same cycle._").
        * The first placeholder (_Condition_) should be substituted with `instance.FuStopI`.
        * The second placeholder (_Implication_) should be substituted with `instance.FuStopISt`.
      * By default, PLCverif will handle each variable in the `VAR_INPUT` block as input variable. However, according to the UNICOS conventions, the `POnOff` variable (which itself is a struct) is a parameter, meaning that it will not change its value during execution. If this is represented as an input, errors cannot be hidden, but spurious counterexamples may occur. In addition, setting up the appropriate variables as parameters will reduce the state space. In the _Requirement – advanced_, set the following variables as parameters: `instance.POnOff.ParReg`, `instance.POnOff.PPulseLe`, `instance.POnOff.PWDt`.
      * At this point, the verification case should look similar to this:
        ```
        // Verification case created by PLCverif3 GUI.
        -description = "OnOff: if FuStopI input is true, FuStopISt output shall be true"
        -id = OnOff_FuStopI_implies_FuStopISt
        -job = verif
        -job.backend = nusmv
        -job.req = pattern
        -job.req.params.0 = instance.POnOff.ParReg
        -job.req.params.1 = instance.POnOff.PPulseLe
        -job.req.params.2 = instance.POnOff.PWDt
        -job.req.pattern_id = pattern-implication
        -job.req.pattern_params.1 = instance.FuStopI
        -job.req.pattern_params.2 = "instance.FuStopISt"
        -lf = step7
        -lf.entry = CPC_FB_ONOFF
        -name = "OnOff: if FuStopI input is true, FuStopISt output shall be true"
        -sourcefiles.0 = CPC_BASE_Unicos.SCL
        -sourcefiles.1 = CPC_FB_ONOFF.SCL
        -sourcefiles.2 = symbols.SCL
        -sourcefiles.3 = "../.builtin_Siemens S7-300/builtin.scl"
        ``` 
      * Click on the _Verify!_ button! The result of the verification should be _Satisfied_. NuSMV can check this requirement in less than a second.
   2. Check the following informally defined requirement: _If there is a rising edge on the `AuAuMoR` input, the `AuMoSt` output will be true._ Essentially, it means that a rising edge of the auto auto mode request will ensure that the object will be in auto mode. You can try to express the requirement yourself before reading the step-by-step guide below.
      * Except for the requirement, everything needs to be done as in the example above.
      * In the _Requirement_ section:
        * Select _Pattern requirement_ as requirement type,
        * Select the _state change between cycles_ pattern ("_If {1} is true at the end of cycle N and {2} is true at the end of cycle N+1, then {3} is always true at the end of cycle N+1._").
        * The first placeholder should be substituted with `instance.AuAuMoR = false`.
        * The second placeholder should be substituted with `instance.AuAuMoR = true`. 
        * The third placeholder should be substituted with `instance.AuMoSt`. 
        * This will express the following: _If instance.AuAuMoR = false is true at the end of cycle N and instance.AuAuMoR = true is true at the end of cycle N+1, then instance.AuMoSt is always true at the end of cycle N+1._
      * As this requirement is an LTL requirement that is not pure safety requirement, Theta and CBMC will be unable to verify it. Use NuSMV.
      * NuSMV can verify this requirement within couple of seconds. The result should be _Violated_.
      * Open the verification report and check the counterexample! It will look similar to the one below:

        ![Counterexample given for the requirement above](img/example-unicos/auaumor_cex.png) 

          * The red lines will help to identify the input variables. The green lines will help to identify the outputs.
          * Look at the `AuAuMoR` variable! You can see that there is a first rising edge on it between Cycle 1 and Cycle 2. According to our requirement, this should mean that in Cycle 2 the `AuMoSt` = false, which is not satisfied. We have just identified the violation in the counterexample.
          * We can see that in Cycle 2, the `HLD` input is true. If we have a look at the code, we can see that if the `HLD` input is true, the mode of the object will be _local drive_ mode, no matter what other requests are present. However, this is desired, thus our requirement was incorrect (incomplete).
          * Similar counterexample could occur without `HLD` = true. Then more analysis could be necessary, including checking what is the meaning of the `Manreg01` variable's current value according to the [UNICOS-CPC documentation](http://ucpc-resources.web.cern.ch/ucpc-resources/1.10.0/objects/on-off.html#device-inputs). We could conclude that our requirement is very incomplete, as every other mode request has priority over an auto mode request. In addition, in certain modes the auto mode requests are simply discarded.
   3. Check the following informally defined requirement: _If there is a rising edge on the `MFoMoR` input and there is no hardware local drive request (`HLD` = false), the `FoMoSt` output will be true._ Essentially, it means that if there is a forced mode request without `HLD`, then the resulting mode should be forced. This seems to be true as the forced mode has the highest priority if we don't count the HLD mode.
      * Except for the requirement, everything needs to be done as in the previous example.
      * In the _Requirement_ section:
        * Select _Pattern requirement_ as requirement type,
        * Select the _state change between cycles_ pattern ("_If {1} is true at the end of cycle N and {2} is true at the end of cycle N+1, then {3} is always true at the end of cycle N+1._").
        * The first placeholder should be substituted with `instance.ManReg01b[10] = false`. This bit corresponds to the _MFoMoR_ signal according to the [UNICOS-CPC documentation](http://ucpc-resources.web.cern.ch/ucpc-resources/1.10.0/objects/on-off.html#device-inputs).
        * The second placeholder should be substituted with `instance.ManReg01b[10] = true and instance.HLD = false`. 
        * The third placeholder should be substituted with `instance.FoMoSt`. 
        * This will express the following: _If instance.ManReg01b[10] = false is true at the end of cycle N and instance.ManReg01b[10] = true and instance.HLD = false is true at the end of cycle N+1, then instance.FoMoSt is always true at the end of cycle N+1._
      * As this requirement is an LTL requirement that is not pure safety requirement, Theta and CBMC will be unable to verify it. Use NuSMV.
      * NuSMV can verify this requirement within couple of seconds. The result will be _Violated_. By analyzing the counterexample, we can realize that the forced mode can be inhibited, thus `instance.AuIhFoMo = false` should also be included in the requirement.
      * Modify the second placeholder's value to the following: `instance.ManReg01b[10] = true and instance.HLD = false and not instance.AuIhFoMo`. Repeat the verification!
      * Unexpectedly, the requirement is still violated. As it would be good to see the other mode status variables, we replace the third placeholder with this: `instance.FoMoSt and (instance.AuMoSt or instance.FoMoSt or instance.MMoSt or instance.SoftLDSt or instance.LDSt)`. The variables included in the requirement will be included in the counterexample despite the reductions.
      * By analyzing the new counterexample we can realize that after the initialization of the object, the mode will be auto no matter what are the requests in the very first cycle when the `HLD` = false. This is because the internal mode variables (`*MoSt_aux`) are all initialized to false. Then, due to the `NOT(AuMoSt_aux OR MMoSt_aux OR FoMoSt_aux OR SoftLDSt_aux)` condition, the auto mode will be set. This seems to be an inconvenience which could be fixed easily. It is not obvious what is the correct next step: fix the code or modify the requirement.
      * If we decide to fix the code and we set the initial value of `AuMoSt_aux` to true (`AuMoSt_aux : BOOL := TRUE;` in the variable declaration block), we can reveal a new counterexample. If the mode is already _forced_, then a new forced mode request (rising edge on `MFoMoR`) will be discarded. If it occurs simultaneously with a `MAuMoR`, the latter will be taken into account. It can be argued that it is impossible to have this situation in real life, however defensive programming is typically much easier than proving that this cannot happen, despite eventual network glitches, TSPP optimizations or SCADA bugs.
      * If we change the requirement instead (we modify the first placeholder to be filled with `instance.ManReg01b[10] = false and instance.HLD = false`), we can also get a counterexample demonstrating the issue above.
      * This is where we stop discussing this example. It can already demonstrate that formal verification can reveal hidden, rarely occurring issues. In addition, it shows how formal verification can improve our deep understanding of the specification and implementation. However, it also shows that if the desire for quality is only present on the verifier's side, the verification process can easily stall.
   4. Similar exercise can be done with formalizing the following informal requirement: _If there is a rising edge on the `AuOnR` input, the `OutOnOV` output will be true._
      * Some pointers for making the requirement more precise:
        * The Auto On request is only taken into account in Auto mode.
        * Check the priorities between On and Off requests.
        * The `PFsPosOn` parameter (`POnOffb.ParRegb[8]`) may alter the calculation of `OutOnOV`.
        * The `PPulse` parameter (`POnOffb.ParRegb[11]`) not only alters the result, but it also makes the state space huge. Note that you cannot bind this variable to a given value, only `POnOff.ParReg` can be fixed.

