# Development guidelines

## Main principles

* Write testable code. Test your code.
* Write comments if appropriate.
    * Always write documentation comments to classes and non-overriding public methods.
    * Don't write meaningless comments, e.g. "*Getting the value. @returns The value*" for a `getValue` methods. Use more meaningful names and less comments -- comments are rotting.
* Use US English.

## Coding

### Code style
During the development of PLCverif, we follow the [Google Java style guide](https://google.github.io/styleguide/javaguide.html) with the following differences:
* It is also acceptable to have 4 spaces as indentation, but within a single file the indentation (2 or 4 spaces) shall be consistent.
* The 100 characters column limit is used as a guideline only, understandability and readability has a higher priority than line wrapping.

The Google Java style guide is implemented in [this](https://github.com/google/styleguide/blob/gh-pages/eclipse-java-google-style.xml) Eclipse settings file.

### Source file structure

A source file should contain the following parts, in the following order:

* license
* `package` statement
* `import` statements
* exactly one top-level class

### Copyright notice
Each source file shall have the following copyright notice as header (for the moment):
```java
/******************************************************************************
 * (C) Copyright CERN 2017-2019. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * Contributors:
 *   <name to put here> - initial API and implementation
 *****************************************************************************/
```


### Java source file organization
Elements of the Java source files shall be in the following order:

1. static fields
1. instance fields
1. constructors
1. static factory methods
1. getters and setters
1. public methods, immediately followed by the private and protected methods called from them
1. other methods
1. `equals`, `hashCode`, `toString` (if defined)

## Eclipse settings
* Use [this](/uploads/9240fafa3edb278b663ceb7e1880a9fa/plcverif-code-formatting.xml) Java code formatter profile (*Preferences > Java > Code Style > Formatter > Import...*)
* You can set up Eclipse to automatically include the copyright header comment: *Preferences > Java > Code Style > Code Templates*, and here edit the *Comments / File* pattern.
```java
/******************************************************************************
 * (C) Copyright CERN 2017-${year}. All rights not expressly granted are reserved.
 *
 * This file is part of the PLCverif project.
 *
 * Contributors:
 *   <Your Name> - initial API and implementation
 *****************************************************************************/
```
* Eclipse Releng Tools can be used to fix the copyright headers, however it has to be used carefully in order to avoid overwriting existing, more specific copyright headers.


## Static analysis
- **PMD** is regularly used on the PLCverif code base. It is used with a relatively "forgiving" rule set in order to avoid false positives. As any PMD violation shows a warning on the automated build, all PMD violations need to be treated (false positives with `// NOPMD` comments). See the [rule set](https://gitlab.cern.ch/plcverif/cern.plcverif/blob/master/pmd-ruleset.xml) in the repository.
- **FindBugs** is currently not used on PLCverif.
- **SonarLint** provides powerful static for Java programs. It is used during development on the local machine. However, as it is not possible to ignore false positives (and false positives are relatively common) without connecting to a SonarQube instance, it is not integrated into the automatic build. Note that several real semantic bugs were found using SonarLint, thus its use is beneficial despite the high false positive rate.

Note that the code written in Xtend language will not be checked by any of the above static analysis tools.

## Continuous integration
PLCverif is automatically built using GitLab CI based on the Maven/Tycho descriptors.
More details can be found in the [GitLab](GitLab.md) section.
