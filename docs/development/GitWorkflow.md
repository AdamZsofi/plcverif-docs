# Git development workflow

This page briefly describes how to apply feature branch-based Git workflow to PLCverif, partially using Eclipse. This allows efficient collaboration and a clean `master` branch that can be compiled any time. The guide below mainly follows the GitHub flow. See the [Understanding the GitHub flow](https://guides.github.com/introduction/flow/) article for more details.

> [info] Note that this page is not a complete introduction to using Git. A good start to understand the concepts of Git can be the [Atlassian Git tutorial](https://www.atlassian.com/git/tutorials/syncing). For more advanced learners this [visualization](http://git-school.github.io/visualizing-git/) may help. A [good cheatsheet](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf) is provided by GitHub. Make sure that you understand the concepts of local vs. remote repositories, branches, `master` branch, staged files, etc.

1. **Create feature branch.** Before committing your changes, ideally before starting the development of your new feature, create a branch for this feature. This way you will avoid directly modifying the `master` branch.
   
   From Eclipse, you can create a new branch by going to the _Git Repositories_ view, right-click on the repository, then select _Switch To / New Branch..._.
   
   ![Create a new branch from Eclipse](img/eclipse-git/1.png)
    
   You need to name this branch in the next window, then click on the _Finish_ button. 
   
   ![Create Branch window in Eclipse](img/eclipse-git/2.png)

   Alternatively, from command line, you should execute the following:
   ```
   git checkout -b <name_of_new_branch>
   ```

   This is equivalent to creating a new branch using `git branch <name>` and then switching to it by `git checkout <name>`. You can always check the list of your local branches using `git branch -a`.

2. **Commit your changes.** Work on your feature and commit your changes regularly.
   From Eclipse, go to the _Git Staging_ view. Drag and drop the files to be committed from the _Unstaged Changes_ box to the _Staged Changes_ box. Fill the _Commit Message_. Make sure that the _Author_ field is correctly set up. Then click on the _Commit_ button.
   
   ![Git Staging view in Eclipse](img/eclipse-git/3.png)

   Alternatively, from the command line, you should execute the following:
   ```
   git add <files_to_stage>
   git commit -m "Commit message"
   ```
   
   Before your first commit, make sure that your name and e-mail address is set correctly. You can check it by executing `git config --get user.name` and `git config --get user.email`. You can set these globally in the following way:
   ```
   git config --global user.name "First Last"
   git config --global user.email "first.last@cern.ch"
   ```

   You can always check the status of your local files (staged, unstaged, untracked files) by executing `git status`.

3. **Push your modifications.**
   If your feature development has finished (or you just want to store the modifications on the remote server), you can push your modifications.

   With Kerberos authentication, pushing does not seem to work from Eclipse. 

   From command line, you should execute the following:
   ```
   git push origin <feature_branch_name>
   ```

   ```
   C:\workspaces\cern.plcverif.cli\cern.plcverif.cli>git push origin dev-cli-header
   Counting objects: 10, done.
   Delta compression using up to 4 threads.
   Compressing objects: 100% (5/5), done.
   Writing objects: 100% (10/10), 709 bytes | 0 bytes/s, done.
   Total 10 (delta 3), reused 0 (delta 0)
   remote:
   remote: To create a merge request for dev-cli-header, visit:
   remote:   https://gitlab.cern.ch/plcverif/cern.plcverif.cli/merge_requests/new?   merge_request%5Bsource_branch%5D=dev-cli-header
   remote:
   To https://:@gitlab.cern.ch:8443/plcverif/cern.plcverif.cli.git
    * [new branch]      dev-cli-header -> dev-cli-header
   ```

4. **Open a merge request.** When your feature is ready, it needs to be merged into the `master` branch. To create a merge request, go to the GitLab page of the repository. If your push was done recently, typically you will see a banner at the top of the page from where a merge request can be created by simply clicking on _Create merge request_.
   
   ![Banner to open a merge request for a recent push](img/eclipse-git/5.png) 

   Make sure that the _Source branch_ and the _Target branch_ are selected correctly. If your branch is a feature branch, typically you want to _Remove source branch when merge request is accepted_. You may also want to squash the commits. After setting these, click on _Submit merge request_.

   ![Important part of the merge request creation form](img/eclipse-git/6.png) 

5. **Accept merge request.** If the modification can be included in the `master` branch, the person responsible for this need to check and accept the merge request in GitLab. On the page of the merge request, you can see the status of the pipeline (i.e., if it compiles), the required approvals and the eventual discussions. If the compilation is still in progress, click on _Merge when pipeline succeeds_.

   ![Accepting a merge request in GitLab](img/eclipse-git/7.png)

   Once the merge request is accepted, the interested parties will be notified by e-mail.

   ![Notification about a successful merge](img/eclipse-git/8.png)

6. **Switch back to master locally.** Now your modifications are in the `master` branch. You don't need your feature branch anymore.
   
   From Eclipse, you can switch back to the `master` branch by going to the _Git Repositories_ view, right-click on the repository, then select _Switch To / master_.
   
   ![Switching back to the master branch from Eclipse](img/eclipse-git/9.png)

   Alternatively, from command line, you should execute the following:
   ```
   git checkout master
   ```

7. **Fetch the new `master` branch.** You switched back to the `master` branch locally. However, the new contents of the `master` branch are not fetched automatically. Using Kerberos authentication, fetching does not seem to work from Eclipse.

   To fetch and merge your modifications from command line, execute the following:
   ```
   git pull
   ```
   
   You can alternatively use `git fetch` and `git merge` instead of `git pull`. You can always check the status of your active local branch using `git status`.
   
   <!--```
   > git status
   On branch master
   Your branch is up-to-date with 'origin/master'.
   nothing to commit, working directory clean  
   > git fetch
   remote: Enumerating objects: 1, done.
   remote: Counting objects: 100% (1/1), done.
   remote: Total 1 (delta 0), reused 0 (delta 0)
   Unpacking objects: 100% (1/1), done.
   From https://gitlab.cern.ch:8443/plcverif/cern.plcverif.cli
      4ae8d7d..4209875  master     -> origin/master  
   > git merge
   Updating 4ae8d7d..4209875
   Fast-forward
    .../src/cern/plcverif/cli/cmdline/CmdlineAppMain.java             | 2  +-
    1 file changed, 1 insertion(+), 1 deletion(-)
   ```-->

8. **Remove the feature branch.**
   You don't need the feature branch anymore locally. It has been removed from the remote repository after merging. To follow this locally, you should remove it locally.
   
   From Eclipse, right-click on the feature branch that you want to delete, and select _Delete Branch_.

   ![Removing a branch from Eclipse](img/eclipse-git/11.png)
   
   Alternatively, from command line, execute the following:
   ```
   git branch -d <feature_branch_name>
   ```
   
   You can always list the available local branches by executing `git branch`.


## Quick summary

   ```bash
   git checkout -b <branch_name>  # Switch to a newly created branch
   # Make modifications
   git add <files to add>         # Stage files
   git commit -m "<message>"      # Commit staged files
   
   git push origin <branch_name>  # Push your branch to origin

   # After the feature branch was merged remotely:
   git checkout master            # Switch to master locally
   git pull                       # Fetch and merge remote modifs  
   git branch -d <branch_name>    # Delete local feature branch

   git status                     # Check the status of your current branch
   git branch                     # Check your local branches
   ```