# Getting started as a developer

## Prerequisites
-Install **Java 11**
  - For reference, the main development of PLCverif is done on CentOS 7 with  `java-11-openjdk`
  - If several version of Java is installed on your system, e.g. Java 8 and Java 11, make sure to have your system set to Java 11
- Install **Maven**
  - Download from https://maven.apache.org/download.cgi 
  - version 3.6.3 is currently used to build PLCverif
  - Install as described at https://maven.apache.org/install.html
    - On Windows, you just need to unzip the downloaded zip and include the extracted `bin` directory on the `PATH`. Ensure that the `JAVA_HOME` environment variable points to the JDK installation.
- Install **Eclipse**
  - Download from https://www.eclipse.org/downloads/packages/release/2020-12/r/eclipse-ide-java-and-dsl-developers
    - Use the _Eclipse IDE for Java and DSL Developers_ edition as it already contains Xtext.
    - The development was done using _Eclipse 2020-12_, thus it is recommended to get this version.
- Install **Git**
  - Make sure on Windows that it is put on the `PATH`. The command `git version` should print the version of Git executed from anywhere.

- Install static **analysis tools**
  - To install **PMD**, go to _Help menu / Eclipse Marketplace..._, find _pmd-eclipse-plugin 4.0.x_ and click on _Install_. Import the rule configuration in _Window menu / Preferences_, under _PMD / Rule Configuration_ from [this file](https://gitlab.com/plcverif-oss/cern.plcverif/-/raw/master/pmd-ruleset.xml?inline=false).
  - To install **SonarLint**, go to _Help menu / Eclipse Marketplace..._, find _SonarLint 4.0_ and click on _Install_.


## Create a workspace for one of the GitLab repositories
1. **Create new workspace.** Create a new workspace in Eclipse (launch Eclipse and give a new path to create a new, empty workspace). In this example, it will be located under `c:\workspaces\cern.plcverif\`, but obviously this is up to you.
   - The built-in Git support of Eclipse is not the greatest. It can be used for convenient committing, but it is easier to do `clone` and `push` operations directly from the command line.
   - On Windows, set the workspace encoding to UTF-8: _Window / Preferences menu_, then under _General / Workspace_, in _Text file encoding_, select _Other: UTF-8_. On Linux this should be fine by default.
2. **Clone Git repository.** Open a terminal and go to `c:\workspaces\cern.plcverif\`. Clone the Git repository you would like to work with. If it is the repository located at `https://gitlab.com/plcverif-oss/cern.plcverif` (core repository), use the `git clone https://gitlab.com/plcverif-oss/cern.plcverif.git` command.
  - In case of the `step7` project, the default directory name is not appropriate. Newer versions of Eclipse do not take into account the project name defined in `.project`. This can lead to a collision between `cern.plcverif.plc.step7.parent` and `cern.plcverif.plc.step7` projects. Make sure that the `cern.plcverif.plc.step7` repository is cloned into the `cern.plcverif.plc.step7.parent` directory (rename the directory if needed before importing to eclipse). So the result should be a folder called `cern.plcverif.plc.step7.parent` where inside there are several other projects and one of them is called `cern.plcverif.plc.step7`.
3. **Open Project Explorer view.** Go to Eclipse, _Window / Show View / Other... / General / Project Explorer_
    - PLCverif uses hierarchical projects, thus it is highly recommended to select the hierarchical project view: click on the _downwards pointing arrow_ in the top right corner of the view, then select _Projects Presentation / Hierarchical_. It is also advised to select _Package Presentation / Hierarchical_. 
    - Avoid using the old *Package Explorer* view.
4. **Import projects.** Right click on the empty Project Explorer and choose _Import... / General / Existing Projects into Workspaces_. Click _Next_. Choose _Select root directory_ and choose the folder that was resulted by the `git clone`. Select all projects and make sure that _Copy projects into workspace_ is **NOT** ticked, _Search for nested project_ **IS** selected.
   - Certain Eclipse versions may complain that some of the projects were imported already. This is due to the nested projects and this error message can be ignored.
5. **Set target platform.** The target platform defines the available dependencies for the compilation. This is set in `.target` files, which are typically located in the `releng/<repo>.target/` folders.
   - Open the `.target` file in the given project.
   - Click on the _Set as Active Target Platform_ link in the top right corner to activate the selected target platform.
   - With the _Update_ button, the dependencies can be updated. However, due to some Eclipse glitches (incorrect caching), the update will not be effective without restarting Eclipse.
   - If you would like to have the full PLCverif project in your workspace, you will need a special target platform to avoid resolving the PLCverif dependencies from the P2 repository. You can either create the project yourself or clone the project [local.plcverif](https://gitlab.com/plcverif-oss/local.plcverif). Once the project is imported, you will need to open its target file `local.plcverif.target` and click on `Set Active Platform`
6. **Try Maven build.** Execute following command from the root of the local repository (`c:\workspaces\cern.plcverif\cern.plcverif` in this example): `mvn clean verify --settings settings.xml`.
  This should build all parts of the project and execute the tests. See more [here](GitLab.md#maven).
   - As the POM-less build feature of Tycho is used, make sure that you use at least Maven 3.3 version.
   - If you want to deploy the built JARs to the P2 repository, execute `mvn install -P uploadRepo --settings settings.xml`. Based on the `settings.xml`, it is expected that the username to be used for uploading the artifacts is in the `P2REPO_USER` environment variable, the corresponding password is in `P2REPO_PASSWORD`. You may want to create a simple script to set these variables temporarily.
      
      Windows example: 
      ```
      SET P2REPO_USER=jplcver
      @SET P2REPO_PASSWORD=secretpassword
      ```
   Make sure it is in `.gitignore` to avoid accidental commits. Even if it is possible to write the credentials into the `settings.xml` file directly, it is discouraged as it is too easy to commit it accidentally to the Git repository.
   - Maven will generate the sources based on the Xtend, Xtext and Xcore files. However, the `src-gen` and `xtend-gen` folders will not be automatically included in the Eclipse workspace. Make sure to do _Refresh_ on the plug-in projects after the Maven build in Eclipse. Note that typically you don't need to run Maven on your local machine after this initial execution. If an Xtend or Xcore file is modified in Eclipse, the corresponding generated Java file automatically re-generated. The Xtext grammar representations can on-demand be re-generated from Eclipse. However take into account that your local host Eclipse may have a newer version of Xtext than what is used in Maven. This can cause some differences between the code generated manually and generated by Maven.
   
   If you still get errors afterwards, reload target platform and refresh the package explorer. If there are anyway errors after this, go project by project executing `mvn clean verify`, then reload target platform and refresh the package explorer.

7. **Try tests from Eclipse.** Open one of the classes in one of the test projects, then right-click on the file and select _Run As / JUnit Plug-in Test_. The test results should be shown eventually in the _JUnit_ view and every test should be satisfied.
8. **Run PLCverif.** If the project checked out is `cern.plcverif.gui` or `cern.plcverif.cli`, it is possible to execute PLCverif directly from Eclipse. It can be useful for debugging.
  - Go to the `releng/cern.plcverif.<edition>.product` directory and open the `<name>.product` file. Click on _Synchronize_ and _Launch an Eclipse application in Debug mode_. 
  - The _Synchronize_ link will create a Launch configuration based on the product description. However, it sometimes incorrectly resolves the required versions of the dependencies. In case of an error, select _Run menu / Debug As / Run Configurations_. Select the `<name>.product` configuration and in the _Plug-ins_ tab fix the dependencies. If you click on the _Validate Plug-ins_ button, it should indicate _No problems were detected_. Typical issues: wrong version of `org.objectweb.asm` (6.x instead of 5.2.0), wrong version of `com.google.guava` (missing 15.0.0). This issue does not happen from Maven. If there are still some dependency issues, you can try using the _Add Required Plug-ins_ button, but make sure before that the _Include optional dependencies when computing required Plug-ins_ is **NOT** checked.
9. **Import code formatting and error settings.**
  Before starting to edit the code, make sure that you use the common code formatting and error severity settings.
  - **Code formatting.** Download the [code formatting descriptor (`code-formatting.xml`)](https://gitlab.com/plcverif-oss/cern.plcverif/-/raw/master/code-formatting.xml?inline=false). Open _Window menu / Preferences_ and under _Java / Code Style / Formatter_ click on _Import..._ and select the `code-formatting.xml` file.
  - **Error settings.** Download the [error settings (`https://gitlab.com/plcverif-oss/cern.plcverif/-/raw/master/eclipse-warning-preferences.epf?inline=false`)](https://gitlab.com/plcverif-oss/cern.plcverif/-/raw/master/eclipse-warning-preferences.epf?inline=false). Select  _File menu / Import..._, then select _General / Preferences_ and import the `eclipse-warning-preferences.epf` file.
10. **Compiler warnings.** Certain compiler warnings should be turned off in order to be able to focus on the meaningful warnings. In _Window / Preferences_:
  - Under _Java / Compiler / Errors/Warnings_, set _No automatic module name_ to _Ignore_ (as this is a Java 8 project, this warning is not necessary),
  - Under _Xtend / Errors/Warnings_, set _Discouraged reference_ to _Ignore_ (the formatter2 interface of Xtext 2.12 is discouraged, even if it is the suggested way to implement code formatters),
  - Under _Xtend / Errors/Warnings_, set _Unnecessary modifier_ to _Ignore_ (as Java and Xtend codes are mixed in the code base, it is useful in some place to emphasize the visibility of some methods or fields),
  - Under _Xtext / Errors/Warnings_, set _Duplicate enumeration literal_ to _Ignore_ (the STEP 7 grammar defines multiple textual representations for certain literals).
11. **External PLCverif dependencies.** When PLCverif is executed from Eclipse, the external model checker tools will be missing. This is because by default the tools are looked up in the `tools` directory. The path to the external tools can be set up manually in the _Preferences_, but it is easier to extract the `tools` and `settings` directories of the _external dependencies_ compressed file to the directory of Eclipse, next to the Eclipse executable.
  - On Windows, this is enough, as the _working directory_ of an Eclipse application run configuration by default is the directory of Eclipse.
  - On Linux, the working directory needs to be modified. Go to _Run menu / Run Configurations..._, then select the run configuration (`gui.app.product` or `cmdline.app.product`), then go to the _Arguments_ tab, and under the _Working directory:_ select _Other_ and by clicking on the _File System..._ button select the Eclipse directory.


## Troubleshooting tips
- If there are compilation errors in the Eclipse workspace, check the `cern.plcverif.base.common` plug-in project. It should not have any dependency to other PLCverif projects. If that project cannot be compiled, the problem is external.
- Try compiling first using Maven. This will make sure that the artifacts are generated based on the Xtext grammars and Xcore metamodels. (The generated files are not stored in Git.)
- If in Eclipse the _Couldn't find a JDK 1.5 or higher on the project's classpath._ error message is shown, go to _Window / Preferences_, then under _Java / Installed JREs_ click _Search..._. On Linux, JDK is typically located under `/usr/lib/jdk`. Select one of the installed Java 11 JDKs/JREs.
- If the STEP 7 project cannot find the classes representing the AST nodes, you may need to generate the Xtext grammar's Java representation manually, by executing the GenerateStep7Language.mwe2 workflow in the `cern.plcverif.plc.step7` project.
- Check if the `cern.plcverif.lib.log4j` can be compiled. If the `target/depencency/log4j-*.jar` files cannot be found, make sure to execute the Maven build of `cern.plcverif`, it will download the needed JAR files.

## Virtual Machines for development
To ease the development, we can provide some preconfigured virtual machine for VirtualBox. Contact one of the developper if you want to have access to it.
