## Translation of TIA Portal Function Block Diagrams (FBDs)

The TIA Portal FBD translator (`cern.plcverif.tia.fbd`) offers functionality for translating function block diagrams to STL (`.awl`) programs.

> **[info] Translation in the IDE**
>
> While TIA Portal supports the translation of function block diagrams to
> STL for S7-300/400 CPUs, this functionality has been dropped
> for the S7-1500 series.

### TIA Portal XML export

The TIA Portal FBD object model revolves around the concept of parts and wires.
Each part and wire has an assigned identifier, which is unique in its declaring network.
In a network, a _part_ (a child of the `<Parts>` element) can be one of the following:
* An _access_, representing a variable or constant (`<Access>` element).
The specific type of the access is shown by the `Scope` attribute:
    - `LocalVariable` or `GlobalVariable` for local or global variables, respectively,
    - `LocalConstant` a named constant declared in the `VAR_CONST` field of the interface,
    - `TypedConstant` or `LiteralConstant` for literals,
    - `Label` for labels.
* A _part_, (`<Part>` element), representing a built-in instruction or built-in PLC block.
* A _call_, (`<Call>` element), representing a call to a user-defined PLC block.

Different parts (`Part`, `Call`, `Access`) are connected to each other with _wires_.
A `<Wire>` element describes a wire and its possible connections.
A connection can either be a:
* `IdentCon`, connecting to an a `Access` element,
* `NameCon`, connecting to a named port (`EN`, `ENO`, `IN`, `Q`, ...) of an instruction part or a call,
* `OpenCon`, representing an open-ended, unconnected wire (e.g. an unconnected `EN` port).

{% hint style='example' %}
**Example.** 
Consider the following FBD program:

![TIA Portal FBD example](img/tiafbd/fbd_example_tia.PNG)

If we export this PLC block through Openness, we get an XML document similar to this one (some metadata has been pruned from this example for readability):

```xml
<?xml version="1.0" encoding="utf-8"?>
<Document>
  <Engineering version="V15" />
  <SW.Blocks.FC ID="0">
    <AttributeList>
      <Interface>
        <Sections xmlns="http://www.siemens.com/automation/Openness/SW/Interface/v3">
            <Section Name="Input">
              <Member Name="A" Datatype="Bool" Accessibility="Public" />
              <Member Name="B" Datatype="Bool" Accessibility="Public" />
              <Member Name="W1" Datatype="Word" Accessibility="Public" />
              <Member Name="W2" Datatype="Word" Accessibility="Public" />
            </Section>
            <Section Name="Output">
              <Member Name="X" Datatype="Bool" Accessibility="Public" />
            </Section>
            <Section Name="InOut" />
            <Section Name="Temp" />
            <Section Name="Constant" />
            <Section Name="Return">
              <Member Name="Ret_Val" Datatype="Void" Accessibility="Public" />
            </Section>
        </Sections>
      </Interface>
      <ProgrammingLanguage>FBD</ProgrammingLanguage>
    </AttributeList>
    <SW.Blocks.CompileUnit ID="3" CompositionName="CompileUnits">
      <AttributeList>
        <NetworkSource>
          <FlgNet xmlns="http://www.siemens.com/automation/Openness/SW/NetworkSource/FlgNet/v2">
            <Parts>
              <Access Scope="LocalVariable" UId="21">
                <Symbol>
                  <Component Name="W1" />
                </Symbol>
              </Access>
              <Access Scope="LocalVariable" UId="22">
                <Symbol>
                  <Component Name="W2" />
                </Symbol>
              </Access>
              <Access Scope="LocalVariable" UId="23">
                <Symbol>
                  <Component Name="A" />
                </Symbol>
              </Access>
              <Access Scope="LocalVariable" UId="24">
                <Symbol>
                  <Component Name="B" />
                </Symbol>
              </Access>
              <Access Scope="LocalVariable" UId="25">
                <Symbol>
                  <Component Name="X" />
                </Symbol>
              </Access>
              <Part Name="Ne" UId="26">
                <TemplateValue Name="SrcType" Type="Type">Word</TemplateValue>
              </Part>
              <Part Name="A" UId="27">
                <TemplateValue Name="Card" Type="Cardinality">2</TemplateValue>
              </Part>
              <Part Name="O" UId="28">
                <TemplateValue Name="Card" Type="Cardinality">2</TemplateValue>
              </Part>
              <Part Name="Coil" UId="29" />
            </Parts>
            <Wires>
              <Wire UId="30">
                <IdentCon UId="21" />
                <NameCon UId="26" Name="in1" />
              </Wire>
              <Wire UId="31">
                <IdentCon UId="22" />
                <NameCon UId="26" Name="in2" />
              </Wire>
              <Wire UId="32">
                <NameCon UId="26" Name="out" />
                <NameCon UId="28" Name="in1" />
              </Wire>
              <Wire UId="33">
                <IdentCon UId="23" />
                <NameCon UId="27" Name="in1" />
              </Wire>
              <Wire UId="34">
                <IdentCon UId="24" />
                <NameCon UId="27" Name="in2" />
              </Wire>
              <Wire UId="35">
                <NameCon UId="27" Name="out" />
                <NameCon UId="28" Name="in2" />
              </Wire>
              <Wire UId="36">
                <NameCon UId="28" Name="out" />
                <NameCon UId="29" Name="in" />
              </Wire>
              <Wire UId="37">
                <IdentCon UId="25" />
                <NameCon UId="29" Name="operand" />
              </Wire>
            </Wires>
          </FlgNet>
        </NetworkSource>
        <ProgrammingLanguage>FBD</ProgrammingLanguage>
      </AttributeList>
    </SW.Blocks.CompileUnit>
  </SW.Blocks.FC>
</Document>
```
{% endhint %}

An important characteristic of the TIA Portal FBD metamodel is that it uses wires to represent every relation within the program.
Another important caveat is with the ports of the built-in parts.
While TIA Portal has the internal knowledge of all the ports a built-in instruction has (therefore is able to process this XML format), there is no explicit list avaiable as documentation.

### FBD metamodel

The following figure offers a (simplfied and pruned) overview of the FBD network metamodel:
![FBD network metamodel](img/tiafbd/fbd_network_metamodel.png)

The main container of every element in the network is the instance of a `Network` class.
Each is composed of _elements_, represented by the abstract superclass `NetworkElement`.
The most important types of elements are:
- _Instructions_, which are individual instructions such as bit logic, word logic, calls, etc.
- _Values_, non-instruction components which may be used as instruction operands.
- _Wires_, representing a control-flow connection between two instructions.

Every instruction defines a number of _ports_ to represent its operands.
For the most part, a port is equivalent to the ports  you see in the TIA Portal FBD editor.
A port has a _connection_, which is a `Connectable` instance.
A `Wire` is a connectable which can be used to connect two instructions (if the operand of an instruction is the result of another instruction).
A `Value` is a connectable which connects a variable, symbol, constant or label to an instruction as an operand.

{% hint style='example' %}
**Example.** 
The following figure shows the FBD model acquired from the program block shown in the previous example:

![FBD model example](img/tiafbd/fbd_example_model.PNG)

{% endhint %}

### Translation algorithm
The translation algorithm works on the FBD model, walking through the ports and wires of each instruction, starting from the ones at the "end" of the network - we call these instructions _terminal instructions_.

To find the terminal instructions, the code generator uses an interface called `TerminalFinderStrategy`.
The current implementation is `OutputWiresTerminalFinderStrategy` which tries to guess which ports are outputs in any given instruction.
If an instruction has an unconnected output port, it is assumed to be a terminal.
It has some knowledge about the most commonly used built-in instructions, otherwise it assumes that the ports called `ENO`, `OUT`, or `Q` are outputs.

Once the terminal instructions have been found, we traverse them one-by-one and start the code generation.
For every instruction, the algorithm generates the corresponding the STL code for it with some gaps, and then fills those gaps with the code acquired from translating each of its ports.

{% hint style='example' %}
**Example.**
The translation process of the FBD model seen previously involves the steps shown below.
Parts which will be translated in later steps are shown within the `<` and `>` symbols.

First, we use the terminal finder to determine which nodes to start from:

```
<Coil(ASSIGNMENT, #"X")>
```

Translating the coil means traversing the other end of its `IN` port and adding the necessary code for the assignment:

```
<Coil(ASSIGNMENT, #"X")/IN>
= #"X";
```

Bit logic instructions are handled using the nesting stack:

```
O(;
  <Or()/IN1>
);
O(;
  <Or()/IN2>
);
= #"X";
```

When we traverse the first port, we encounter a word logic instruction, which we can translate without further steps:

```
O(;
  L #"W1";
  L #"W2";
  <>I;
);
O(;
  <Or()/IN2>
);
= #"X";
```

Finally, we translate the `AND` instruction:
```
O(;
  L #"W1";
  L #"W2";
  <>I;
);
O(;
  A #"A";
  A #"B";
);
= #"X";
```

{% endhint %}
