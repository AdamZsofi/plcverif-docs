# SCL Features


- Identifiers (Sec. 15.2.2)
    - Identifiers like `FB1` and `DB123` are supported but cannot be mixed with symbolic names (no support for symbol table)
    - Timers (e.g., `T 1`) and counters (e.g., `C 123`) are supported by the grammar, but not supported by CFA translation
- Constants (Sec. 15.2.3)
    - Bit constants (`BOOL`, `BYTE`, `WORD`, `DWORD`): OK
        - ...
    - Integer constants (`INT`, `DINT`): OK
    - Real constant (`REAL`): OK
    - Character constant (`CHAR`): supported by grammar, not supported by CFA translation
    - String constant (`STRING`): supported by grammar, not supported by CFA translation
    - Date constant (`DATE`): OK
    - Time period constant (`TIME`): OK
    - Time of day constant (`TOD`): OK
    - Date and time constant (`DT`): supported by grammar, not supported by CFA translation (BCD representation would be needed)
- Absolute addressing (Sec. 15.2.4)
    - Indexed memory access (e.g., `IB[3]`): supported by grammar (with `builtin.scl`), supported by CFA translation for literal indexes
        - All prefixes (`I`, `Q`, `M`, `PI`, `PQ`) supported
        - Peripheral inputs are handled in the same way as `I` area by the CFA translation
        - The `I` and `PI` areas are represented completely independently from each other; same for `Q` and `PQ` by the CFA translation
        - The potential overlaps between memory areas are not handled by the CFA translation, e.g. `QX0.0 := FALSE` will not modify the value of `MB0`, `MW0`  and `MD0`.
    - Absolute DB access (e.g., `DB123.DW0`): supported by grammar, not supported by CFA translation
    - Indexed DB access (e.g., `DB123.DW[0]`): supported by grammar, not supported by CFA translation
    - Structured DB access (e.g., `DB123.field1`): OK
- Comments (Sec. 15.2.5)
    - Line comment: OK
    - Block comment: OK
- Block attributes (Sec. 15.2.6)
    - Title: OK
    - Version: OK
    - Block protection: OK
    - Author: OK
    - Parameter name: ?
    - Block family: OK
    - System attributes: not supported
    - Compiler options: not supported
- Blocks (Sec. 15.3.2)
    - Organization block (`OB`): OK
        - The organization block `OB1` is treated specially (entry of program). The rest of the organization blocks are not modeled.
    - Function block (`FB`): OK
    - Function (`FC`): OK
        - **TODO**: check what are the return values handled in CFA
    - Data block (`DB`): OK
        - Both with FB structure and inline structure
        - DB assignment section: OK
    - User-defined data type (UDT): OK
- _Declaration sections (Sec. 15.3.3)_
    - OB declaration sections (constants, labels, temporary variables): OK
    - FC declaration sections (constants, labels, temporary variables; parameters): OK
    - FB declaration sections (constants, labels, temporary and static variables; parameters): OK
    - _Variable declaration_
        - Variable declaration: OK
            - System attributes are not supported by the grammar
        - Data type initialization with a constant: OK
        - Data type initialization with an array initialization list: OK
            - Lists (e.g., ` := [1, 2, 3]`), repetitions (e.g., ` := 3(1)`) and their mixture (e.g., ` := 2(1, 2, 3)`) are all supported
        - _Data type specification_
            - Elementary data type: OK
            - Date and time: OK
            - String: supported by grammar, not supported by CFA translation
            - ARRAY: OK
            - STRUCT: OK
            - UDT: OK
            - Parameter data type: supported by grammar, not supported by CFA translation 
    - _Data type support for variable declaration_
        - `BOOL`: OK
        - `BYTE`, `WORD`, `DWORD`: OK
        - `CHAR`: OK
        - `STRING`: supported by grammar, not supported by CFA translation
        - `INT`, `DINT`: OK
        - `REAL`: OK
        - `S5TIME`: supported by grammar, not supported by CFA translation
        - `TIME`: OK
        - `TIME_OF_DAY`: OK
        - `DATE` : OK
        - `TIMER`: supported by grammar, not supported by CFA translation 
        - `COUNTER`: supported by grammar, not supported by CFA translation 
        - `ANY`: partially supported by grammar, not supported by CFA translation 
        - `POINTER`: partially supported by grammar, not supported by CFA translation 
        - `BLOCK_FC`, `BLOCK_FB`, `BLOCK_DB`, `BLOCK_SDB`: supported by grammar, not supported by CFA translation 
        - **TODO check the permitted operations on the different data types**
- _Statements_ (Section 15.3.5)
    - Labeled statement: OK
        - Multiple labels are also supported (`label1: label2: <statement>`)
    - Value assignment: OK
        - Assignment of simple variable (unqualified reference, hierarchical reference, array): OK
        - Assignment of absolute variable (e.g., `Q0.0`): OK
        - Assignment of variable in DB: OK
        - Assignment of variable in local instance: OK
    - _Calls_
        - Function block call with global instance name (e.g., `FB123.DB234();`): OK
        - Function block call with local instance (e.g., `localInstance();`): OK
        - Function call: OK
        - FB parameters (only named): OK
        - FC parameters (named or unnamed if only one input parameter): OK
            - `=>` operator for output variables: OK
        - Implicit parameters
            - `EN`: supported by grammar, not supported by CFA translation
            - `ENO`: supported by grammar, not supported by CFA translation
    - `IF` statement: OK
        - `ELSIF` branches: OK
        - `ELSE` branch: OK
    - `CASE` statement: OK
        - `ELSE` branch: OK
        - Value lists (e.g., `0..3`, `2, 4, 6`, `0..2, 4..5`): OK
        - Limitations: see below
    - `FOR` statement: OK
        - Specification of increment (`BY`): OK
        - **TODO** saving `TO` value to temporary variable in the CFA if not constant to replicate the real SCL 5.6 behavior, see below
    - `WHILE` statement: OK
    - `REPEAT` statement: OK
    - `CONTINUE` statement: OK
    - `EXIT` statement: OK
    - `RETURN` statement: OK
    - `GOTO` statement: OK
- _Expressions_ (Section 15.3.6)
    - Basic logic operations (`AND`, `&`, `OR`, `XOR`): OK 
    - Comparison operations (`<`, `>`, `<=`, `>=`, `=`, `<>`): OK
    - Basic arithmetic operations (`+`, `-`, `*`, `/`, `MOD`, `DIV`): OK
    - Exponentiation (`**`): OK
    - Unary `+` operation: OK
    - Unary `-` operation: OK
    - Unary `NOT` operation: OK
    - Constants (literals, character strings, named constants): OK
    - FC call: OK
- Others
    - `OK`: supported by grammar, not supported by CFA translation
- _Built-in functions_ (implemented in the `builtin.scl`)
    - ... **TODO**

## Unsupported features in PLCverif
* Blocks should be only addressed using the names/symbols used at declaration (e.g. `FUNCTION FC123` should be called as `FC123();` from the code, `FUNCTION foobar` should be called as `foobar();`, even if it is known from the symbol that `foobar` is mapped to `FC124`, `FC124();` is not valid.)
    * TODO: maybe the symbol table should be supported, then the symbolic and address-based representation should be attached to each other
* Reference to non-existent blocks is not supported (e.g. `FC123();` is not supported unless there is a function defined with this name, i.e. `FUNCTION FC123`). Same applies to DBs.
* [WONTFIX] Top-level statements inside a `CASE` block cannot contain labels. E.g. it is not valid:

```
CASE x OF
      1 :          a := b;
           label1: b := x;
      2 : c := d;
END_CASE
```
* The `EN` implicit input parameter is not supported (e.g. `foo(EN := FALSE, ...)`).
* *Bytes* of DBs cannot be accessed via their addresses, e.g. `DB100.DB123` or `datablockname.DB123` are not supported (but `DB100.DW123` and `datablockname.DX0.1` are supported, also `DB100.DBB123`).
 * Indexed DB access is not supported (e.g. `DB11.DW[ADDRESS]` or `WORD_TO_BLOCK_DB(ADDRESSWORD).DW[ADDRESS]`).
* Case range labels need to have whitespace between the lower and upper values and the `..` (e.g. `1..2` is not accepted, but `1 .. 2` is valid).
* Partial assignments of arrays, e.g. `arrayA[1] := arrayB;`, where `arrayA` is a two-dimensional array, `arrayB` is a one-dimensional array.
* `//#test:=...` comments are currently invalid due to the special meaning of `//#`, latter to be changed.
* [not planned] Pointers
* [WONTFIX] Function blocks returning data blocks are not supported in qualified references, e.g.`functionReturningBLOCK_DB().DW123` and `functionReturningBLOCK_DB().DW[123*2]`are not supported
* [WONTFIX] SCL keywords cannot be used as names (e.g. `ARRAY` is not a valid variable name, even though the SCL v5.3 documentation has this in an example on p. 12-23).
* [WONTFIX] Labels must be followed by statements, i.e. they cannot be at the end of the block (e.g. `label: END_FUNCTION_BLOCK` is not supported, but `label: ; END_FUNCTION_BLOCK` is valid).

### Currently unsupported TIA Portal specific features {#tia-unsupported-features}
* `THIS[#var]`
* slice accesses (e.g. `"Engine".Motor.W1`)
* `VARIANT`, `DB_ANY`
* `REGION`, see [S7v14] pp. 4948
* System and hardware data types are not supported (e.g. `IEC_TIMER`, `SSL_HEADER`, `HW_SUBMODULE`, `OB_ANY`), see [S7v14] pp. 3451-3452
* `C#123` (BCD) and `B#(127,200)` formats for `WORD` values, see [S7v14] pp. 3455
* `B#(127,200,0,0)` format for `DWORD`and `LWORD` values, see [S7v14] pp. 3457-3458
* Use of typed constants, e.g. `test : INT := 1;`
  * Using `VARR_CONSTANT ... END_VAR` instead of `CONST ... END_CONST` is supported.
* `#Manreg01.%X2`-style bit access
* New style of function block instance calling
* Member variables of a shared DB need to be surrounded by `STRUCT ... END_STRUCT` (as in old STEP 7). The `VAR ... END_VAR` (as in TIA Portal) is not supported yet.


## SCL particularities and various notes

* `(I|Q|M)(B|D|W|X)?` and `(PI|PQ)(B|D|W)` are reserved (global) array names, however the names can be used for non-array local variables.
* Array dimensions can be simple expressions, e.g. the following is valid: `z : ARRAY [N-1..N+INT#16#F] OF BOOL;` (where `N` is a named constant)
* There is no *short-circuit evaluation* (in STEP 7 with S7-300) for Boolean expressions: in case of `IF FC100() OR FC101() THEN ...` the `FC101` will be called even if `FC100` returned `true`, thus the Boolean expression will be evaluated to true independently from the result of `FC101`. If a function call is present several times in the expression, it will be called several times (e.g. `IF FC100() OR FC101() OR FC101() THEN ...` leads to 2 calls of `FC101`, independently from the return value of the functions).
* If a `RETURN` is invoked in a function before assigning the return value (e.g. in `FUNCTION foo` before the first `foo := ...;` assignment), memory garbage is returned (the returned value is non-deterministic). Old STEP 7 raises a warning only if there is no return value setting at all. It seems that TIA Portal gives a failure if there is any computation path where `RETURN` precedes the setting of return value.
* `FOR` loop:
    - Using the SCL v5.6 compiler (old STEP 7), the _final value_ is evaluated only once, before the initial statement (e.g., `FOR i := 1 TO N` will go to the value of `N` before the loop, even if the value of `N` changes). Using the new SCL compiler (TIA Portal), the final value is evaluated only once, but _after_ the initial statement.
    {% hint style='example' %}
**Example.** Consider the following loop.
```
#i := 10;
FOR #i := 3 TO #i + 5 DO
    // Last iteration will be with 'i = 15' using STEP 7 v5.6 compiler 
    // Last iteration will be with 'i = 8' using TIA Portal's compiler
END_FOR;
```
    {% endhint %}

    - After each cycle, the _loop variable_ is incremented. If the condition is still met (i.e., the value of the loop variable is <= the final value if the increment is positive, or >= if negative), the loop body will be executed again. If the condition is not satisfied, the loop terminates with this already incremented loop variable.
    {% hint style='example' %}
**Example.** Consider the following loops.
```
FOR i := 1 TO 12 BY 2 DO
    ...
    // Value of i here in the last iteration: 11
END_FOR;
// Value of i here: 13
```
```
FOR i := 10 TO 1 BY -2 DO
    ...
    // Value of i here in the last iteration: 2
END_FOR;
// Value of i here: 0
```
    {% endhint %}
