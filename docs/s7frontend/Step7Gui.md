# STEP 7 parser GUI

## Syntax highlighting

The majority of syntax highlighting is provided automatically by Xtext, based on the [Xtext grammar of the STEP 7 languages](../reference/Step7Grammar.md).

However, by default each token used as a keyword will be highlighted as a keyword, independently from its context. This means that for example the `a` in the variable declaration `a : BOOL;` would be highlighted as keyword, because `A` is a keyword in STL (meaning the AND logic operation).
To avoid this, a new semantic highlighting calculator (`cern.plcverif.plc.step7.ide.syntaxcoloring.Step7LanguageSemanticHighlightingCalculator`) is created and registered. This class knows each name-like feature (e.g., name of a variable, name of a function block, label of a statement) which should never be shown as keywords. 

![Syntax highlighting in STEP 7 code editor](img/editor/syntax-highlight.png)

## Validation

Xtext out of the box provides certain validation for the inputs in the generated editor. If the input text cannot be parsed using the defined [grammar](../reference/Step7Grammar.md), it will be signaled to the user.  

In addition, many validation rules are defined in the `cern.plcverif.plc.step7.validation` package (entry point: `Step7LanguageValidator`). These checks will ensure that input not only respects the Xtext grammar, but also the STEP 7 grammar. In many cases, it is not possible (and not practical either) to express all constraints of the STEP 7 grammar in the Xtext grammar.

For each rule in the grammar, the corresponding validation rules are documented and referenced.

![Validation in STEP 7 code editor](img/editor/validation.png)

## Static analysis

In addition to the validation, similar rules are defined with lower severity (Warning or Info) to inform the user about potential code smells, too complex expressions, etc.

![Static analysis in STEP 7 code editor](img/editor/static-analysis.png)


## Content assist

Xtext provides a default content assist for the generated editor, however it is not fully appropriate. Certain customizations are in the 
`cern.plcverif.plc.step7.ui.contentassist.Step7LanguageProposalProvider` class.

![Content assist in STEP 7 code editor](img/editor/content-assist.png)

## Code outline

The outline in the bottom left corner helps to navigate in bigger code base.
This is defined in `cern.plcverif.plc.step7.ui.outline.Step7LanguageOutlineTreeProvider`.

![Code outline in STEP 7 code editor](img/editor/code-outline.png)

## Code formatting

Automated code formatting can make the input source code consistent and easy to overview. By pressing _Ctrl+Shift+F_ or selecting _Format_ from the context menu (by right-clicking on the source code and selecting _Source / Format_), the code can be automatically formatted.
The code formatting rules are defined in `cern.plcverif.plc.step7.formatting2.Step7LanguageFormatter`.

![Code formatting in STEP 7 code editor](img/editor/code-formatting.gif)

## Obfuscation

Obfuscation will replace the variable names to meaningless, automatically generated variable names; removes the comments and attributes. This may permit sharing a source file for verification or diagnostic purposes without revealing too much about the implemented logic.

By selecting _Obfuscate_ from the context menu (that is by right-clicking on the source code), the code can be automatically obfuscated.
The code obfuscation is implemented in `cern.plcverif.plc.step7.ui.handler.ObfuscateHandler`.

By selecting _Remove Comments_ from the context menu (that is by right-clicking on the source code), all comments can be removed from the source code.
The code comment removal is implemented in `cern.plcverif.plc.step7.ui.handler.RemoveCommentsHandler`.

![Code obfuscation in STEP 7 code editor](img/editor/obfuscate-and-remove-comments.gif)


## Auto-sanitization

There are certain minor syntactic features which are not supported by the implemented grammar as it would make the parsing too difficult or impossible using Xtext.
For example, the [_title_ attribute](../reference/Step7Grammar.md#TitleAttribute) (which does not influence the verification at all and does not have any CFA representation) may have one of the following syntaxes in a PLC program:
- `TITLE : lorem`
- `TITLE : 'lorem'`
- `TITLE : 'lorem ipsum dolor sit amet'`
- `TITLE = lorem`
- `TITLE = 'lorem'`
- `TITLE = lorem ipsum dolor sit amet`
- `TITLE = 'lorem ipsum dolor sit amet'`

The whitespace before and after the `:` or `=` is optional. Out of all these options, `TITLE = lorem ipsum dolor sit amet` is not supported, only if there is no whitespace before the `=`:  `TITLE= lorem ipsum dolor sit amet`.

In order to provide a workaround for these unsupported features, a sanitizer is implemented in `cern.plcverif.plc.step7.ui.sanitizer.Step7CodeSanitizer` which performs pre-defined replacements in the opened source file.

### Replacements
- Title attributes in the format `TITLE = lorem ipsum dolor sit amet` will be replaced by `TITLE= lorem ipsum dolor sit amet` (i.e., the whitespace is removed between `TITLE` and `=`).
- The whitespace from references to the `L` memory will be removed. For example, `L   0.3` would be replaced by `L0.3`, `LW  3` would be replaced by `LW3`. The references to other memory areas (e.g., `I  0.3`) will not be modified as they are supported.
- If the lower end of an array dimension definition contains an operation, a whitespace will be inserted after this expression, while this is optional in the STEP 7 languages. For example, `ARRAY[N+1..2*N] OF BOOL` will be replaced by `ARRAY[N+1 ..2*N] OF BOOL`.

See `cern.plcverif.plc.step7.ui.sanitizer.Step7CodeSanitizer.REPLACEMENTS` for more details.

### Auto-sanitization on the GUI
From the user point of view, auto-sanitization looks as follows. When a file is opened which contains any of the above problematic features, the user is notified:

![Notification of the user if sanitization is needed](img/autosanitization-question.png)

If the user chooses _No_, no action is performed. If the user chooses _Yes_, the unsupported features will be replaced with equivalents which still mean the same in SCL or STL. After the replacement, the user gets an overview of the performed modifications:

![Result of the sanitization](img/autosanitization-report.png)

The modifications performed on the file are not saved. Based on the results of the sanitization, the user may decide if they would like to keep these modifications and save the file, or discard the modifications. Undo can also be used in the opened editor.

## Importation of TIA Portal FBDs

Starting from the S7-1400 CPU series, TIA Portal dropped support for generating STL sources from function block diagrams (FBDs).
This makes the verification impossible for programs written in LAD or FBD.
It is possible, however, to export FBD programs in TIA Portal's XML format through Openness and translate this XML to STL within PLCverif.

Right click on the target project in the project view and select _Import..._ from the context menu. Under the _PLCverif_ category, select the _TIA Portal FBD XML_ option.

![TIA Portal FBD example](img/tiafbd/fbd_import_gui.png)

In the upcoming window, select the XML files you wish to import and the target directory.
Make sure to select the correct TIA Portal version from version dropdown.
After hitting _Finish_ button, the corresponding STL files will be generated into the target directory for each input XML file.

