## STL features


### STL instructions (and current state of support)
| STL mnemonic | Grammar rule | Grammar mnemonic | Grammar argument | Typing | CFA support |
| --- | --- | --- | --- | --- | --- |
| `+ ` | StlAddIntConstStatement | --- | _NamedOrUnnamedConstantRef_ | _+_ | **-** |
| `= ` | StlAssignStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `) ` | StlBitLogicNestingCloseStatement | --- | _---_ | _NA_ | **+** |
| `+AR1 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `+AR2 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `+D ` | StlArithmeticStatement | _PLUS, DINT_ | _---_ | _NA_ | **+** |
| `-D` | StlArithmeticStatement | _MINUS, DINT_ | _---_ | _NA_ | **+** |
| `*D ` | StlArithmeticStatement | _MULTIPLICATION, DINT_ | _---_ | _NA_ | **+** |
| `/D ` | StlArithmeticStatement | _DIVISION, DINT_ | _---_ | _NA_ | **-** |
| `==D` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `<>D` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `>D` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `<D` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `>=D` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `<=D` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `+I ` | StlArithmeticStatement | _PLUS, INT_ | _---_ | _NA_ | **+** |
| `-I` | StlArithmeticStatement | _MINUS, INT_ | _---_ | _NA_ | **+** |
| `*I ` | StlArithmeticStatement | _MULTIPLICATION, INT_ | _---_ | _NA_ | **+** |
| `/I ` | StlArithmeticStatement | _DIVISION, INT_ | _---_ | _NA_ | **-** |
| `==I` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `<>I` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `>I` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `<I` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `>=I` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `<=I` | StlComparisonStatement | --- | _---_ | _NA_ | **+** |
| `+R ` | StlArithmeticStatement | _PLUS, REAL_ | _---_ | _NA_ | **+** |
| `-R` | StlArithmeticStatement | _MINUS, REAL_ | _---_ | _NA_ | **+** |
| `*R ` | StlArithmeticStatement | _MULTIPLICATION, REAL_ | _---_ | _NA_ | **+** |
| `/R ` | StlArithmeticStatement | _DIVISION, REAL_ | _---_ | _NA_ | **-** |
| `==R` | StlComparisonStatement | --- | _---_ | _NA_ | **-** |
| `<>R` | StlComparisonStatement | --- | _---_ | _NA_ | **-** |
| `>R` | StlComparisonStatement | --- | _---_ | _NA_ | **-** |
| `<R` | StlComparisonStatement | --- | _---_ | _NA_ | **-** |
| `>=R` | StlComparisonStatement | --- | _---_ | _NA_ | **-** |
| `<=R` | StlComparisonStatement | --- | _---_ | _NA_ | **-** |
| `A ` | StlBitLogicStatement | _AND_ | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `A( ` | StlBitLogicNestingOpenStatement | _AND_ | _---_ | _NA_ | **+** |
| `ABS ` | StlFloatMathFunction | _ABS_ | _---_ | _NA_ | **-** |
| `ACOS ` | StlFloatMathFunction | _ACOS_ | _---_ | _NA_ | **-** |
| `AD ` | StlWordLogicArglessStatement | _AND_DWORD_ | _---_ | _NA_ | **+** |
| `AD ..` | StlWordLogicStatement | _AND_DWORD_ | _---_ | _NA_ | **-** |
| `AN ` | StlBitLogicStatement | _AND_NOT_ | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `AN( ` | StlBitLogicNestingOpenStatement | _AND_NOT_ | _---_ | _NA_ | **+** |
| `ASIN ` | StlFloatMathFunction | _ASIN_ | _---_ | _NA_ | **-** |
| `ATAN ` | StlFloatMathFunction | _ATAN_ | _---_ | _NA_ | **-** |
| `AW ` | StlWordLogicArglessStatement | _AND_WORD_ | _---_ | _NA_ | **+** |
| `AW ..` | StlWordLogicStatement | _AND_WORD_ | _NamedOrUnnamedConstantRef_ | _+_ | **-** |
| `BE ` | StlBlockEndStatement | _BLOCK_END_ | --- | _NA_ | **+** |
| `BEC ` | StlBlockEndStatement | _BLOCK_END_CONDITIONAL_ | --- | _NA_ | **+** |
| `BEU ` | StlBlockEndStatement | _BLOCK_END_UNCONDITIONAL_ | --- | _NA_ | **+** |
| `BLD ` | StlBldStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `BTD ` | StlConversionStatement | _BCD_TO_DINT_ | _---_ | _np_ | **np** |
| `BTI ` | StlConversionStatement | _BCD_TO_INT_ | _---_ | _np_ | **np** |
| `CAD ` | StlConversionStatement | _CAD_ | _---_ | _np_ | **np** |
| `CALL ` | StlSubroutineCall | --- | --- | _NA_ | **+** |
| `CALL ` | StlSubroutineCall | --- | --- | _NA_ | **+** |
| `CALL ` | StlSubroutineCall | --- | --- | _NA_ | **+** |
| `CAR ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `CAW ` | StlConversionStatement | _CAW_ | _---_ | _np_ | **np** |
| `CC ` | StlParameterlessCallStatement | _CONDITIONAL_CALL_ | --- | _NA_ | **+** |
| `CD ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `CDB ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `CLR ` | StlClrRloStatement | --- | _---_ | _NA_ | **+** |
| `COS ` | StlFloatMathFunction | _COS_ | _---_ | _NA_ | **-** |
| `CU ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `DEC ` | StlAccuDecrementStatement | --- | _NamedOrUnnamedConstantRef_ | _+_ | **+** |
| `DTB ` | StlConversionStatement | _DINT_TO_BCD_ | _---_ | _NA_ | **np** |
| `DTR ` | StlConversionStatement | _DINT_TO_REAL_ | _---_ | _NA_ | **-** |
| `ENT ` | StlAccuArglessStatement | _ENTER_ACCU_STACK_ | _---_ | _NA_ | **-** |
| `EXP ` | StlFloatMathFunction | _EXP_ | _---_ | _NA_ | **-** |
| `FN ` | StlFnStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `FP ` | StlFpStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `FR ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `FR ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `INC ` | StlAccuIncrementStatement | --- | _NamedOrUnnamedConstantRef_ | _+_ | **+** |
| `INVD ` | StlConversionStatement | _DINT_ONES_COMPLEMENT_ | _---_ | _NA_ | **+** |
| `INVI ` | StlConversionStatement | _INT_ONES_COMPLEMENT_ | _---_ | _NA_ | **+** |
| `ITB ` | StlConversionStatement | _INT_TO_BCD_ | _---_ | _NA_ | **np** |
| `ITD ` | StlConversionStatement | _INT_TO_DINT_ | _---_ | _NA_ | **+** |
| `JBI ` | StlJumpStatement | _JUMP_IF_BR_TRUE_ | _---_ | _NA_ | **+** |
| `JC ` | StlJumpStatement | _JUMP_IF_RLO_TRUE_ | _---_ | _NA_ | **+** |
| `JCB ` | StlJumpStatement | _JUMP_IF_RLO_TRUE_WITH_BR_ | _---_ | _NA_ | **+** |
| `JCN ` | StlJumpStatement | _JUMP_IF_RLO_FALSE_ | _---_ | _NA_ | **+** |
| `JL ` | - | --- | --- | --- | **-** |
| `JM ` | StlJumpStatement | _JUMP_IF_MINUS_ | _---_ | _NA_ | **+** |
| `JMZ ` | StlJumpStatement | _JUMP_IF_MINUS_OR_ZERO_ | _---_ | _NA_ | **+** |
| `JN ` | StlJumpStatement | _JUMP_IF_NOT_ZERO_ | _---_ | _NA_ | **+** |
| `JNB ` | StlJumpStatement | _JUMP_IF_RLO_FALSE_WITH_BR_ | _---_ | _NA_ | **+** |
| `JNBI ` | StlJumpStatement | _JUMP_IF_BR_FALSE_ | _---_ | _NA_ | **+** |
| `JO ` | StlJumpStatement | _JUMP_IF_OV_TRUE_ | _---_ | _NA_ | **-** |
| `JOS ` | StlJumpStatement | _JUMP_IF_OS_TRUE_ | _---_ | _NA_ | **-** |
| `JP ` | StlJumpStatement | _JUMP_IF_PLUS_ | _---_ | _NA_ | **+** |
| `JPZ ` | StlJumpStatement | _JUMP_IF_PLUS_OR_ZERO_ | _---_ | _NA_ | **+** |
| `JU ` | StlJumpStatement | _JUMP_UNCONDITIONAL_ | _---_ | _NA_ | **+** |
| `JUO ` | StlJumpStatement | _JUMP_IF_UNORDERED_ | _---_ | _NA_ | **+** |
| `JZ ` | StlJumpStatement | _JUMP_IF_ZERO_ | _---_ | _NA_ | **+** |
| `L ` | StlLoadStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `L DBLG ` | - | --- | --- | --- | **-** |
| `L DBNO ` | - | --- | --- | --- | **-** |
| `L DILG ` | - | --- | --- | --- | **-** |
| `L DINO ` | - | --- | --- | --- | **-** |
| `L STW ` | StlLoadStwStatement | --- | _---_ | _NA_ | **np** |
| `L ` | (no counter/timer support yet) | --- | _UnaryOrPrimaryExpression_ | _np_ | **np** |
| `L ` | (no counter/timer support yet) | --- | _UnaryOrPrimaryExpression_ | _np_ | **np** |
| `LAR1 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `LAR1 <D> ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `LAR1 AR2 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `LAR2 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `LAR2 <D> ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `LC ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `LC ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `LEAVE ` | StlAccuArglessStatement | _LEAVE_ACCU_STACK_ | _---_ | _NA_ | **-** |
| `LN ` | StlFloatMathFunction | _LN_ | _---_ | _NA_ | **-** |
| `LOOP ` | StlJumpStatement | _LOOP_ | _---_ | _NA_ | **-** |
| `MCR( ` | StlMcrStatement | _BEGIN_MCR_ | --- | --- | **np** |
| `)MCR ` | StlMcrStatement | _END_MCR_ | --- | --- | **np** |
| `MCRA ` | StlMcrStatement | _ACTIVATE_MCR_ | --- | --- | **np** |
| `MCRD ` | StlMcrStatement | _DEACTIVATE_MCR_ | --- | --- | **np** |
| `MOD ` | StlModStatement | --- | _---_ | _NA_ | **+** |
| `NEGD ` | StlConversionStatement | _DINT_TWOS_COMPLEMENT_ | _---_ | _NA_ | **-** |
| `NEGI ` | StlConversionStatement | _INT_TWOS_COMPLEMENT_ | _---_ | _NA_ | **-** |
| `NEGR ` | StlConversionStatement | _REAL_NEGATE_ | _---_ | _NA_ | **-** |
| `NOP 0 ` | StlNopStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `NOP 1 ` | StlNopStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `NOT ` | StlNegateRloStatement | --- | _---_ | _NA_ | **+** |
| `O ` | StlBitLogicStatement | _OR_ | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `O` | StlAndBeforeOr | --- | _---_ | _NA_ | **+** |
| `O( ` | StlBitLogicNestingOpenStatement | _OR_ | _---_ | _NA_ | **+** |
| `OD ` | StlWordLogicArglessStatement | _OR_DWORD_ | _---_ | _NA_ | **+** |
| `OD ..` | StlWordLogicStatement | _OR_DWORD_ | _NamedOrUnnamedConstantRef_ | _+_ | **-** |
| `ON ` | StlBitLogicStatement | _OR_NOT_ | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `ON( ` | StlBitLogicNestingOpenStatement | _OR_NOT_ | _---_ | _NA_ | **+** |
| `OPN ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `OW ` | StlWordLogicArglessStatement | _OR_WORD_ | _---_ | _NA_ | **+** |
| `OW ..` | StlWordLogicStatement | _OR_WORD_ | _NamedOrUnnamedConstantRef_ | _+_ | **-** |
| `POP ` | StlAccuArglessStatement | _POP_ACCU_ | _---_ | _NA_ | **-** |
| `POP ` | StlAccuArglessStatement | _POP_ACCU_ | _---_ | _NA_ | **-** |
| `POP ` | StlAccuArglessStatement | _POP_ACCU_ | _---_ | _NA_ | **-** |
| `PUSH ` | StlAccuArglessStatement | _PUSH_ACCU_ | _---_ | _NA_ | **-** |
| `PUSH ` | StlAccuArglessStatement | _PUSH_ACCU_ | _---_ | _NA_ | **-** |
| `R ` | StlResetStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `R ` | StlResetStatement | --- | _UnaryOrPrimaryExpression_ | _np_ | **-** |
| `R ` | StlResetStatement | --- | _UnaryOrPrimaryExpression_ | _np_ | **-** |
| `RLD ` | StlShiftRotateStatement | _ROTATE_LEFT_DWORD_ | _NamedOrUnnamedConstantRef, optional_ | _+_ | **-** |
| `RLDA ` | StlShiftRotateStatement | _ROTATE_LEFT_DWORD_VIA_CC1_ | _---_ | _NA_ | **-** |
| `RND ` | StlConversionStatement | _ROUND_ | _---_ | _NA_ | **-** |
| `RND– ` | StlConversionStatement | _RND_MINUS_ | _---_ | _NA_ | **?** |
| `RND+ ` | StlConversionStatement | _RND_PLUS_ | _---_ | _NA_ | **?** |
| `RRD ` | StlShiftRotateStatement | _ROTATE_RIGHT_DWORD_ | _NamedOrUnnamedConstantRef, optional_ | _+_ | **-** |
| `RRDA ` | StlShiftRotateStatement | _ROTATE_RIGHT_DWORD_VIA_CC1_ | _---_ | _+_ | **-** |
| `S ` | StlSetStatement | --- | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `S ` | StlSetStatement | --- | _UnaryOrPrimaryExpression_ | _np_ | **-** |
| `SAVE ` | StlSaveRloStatement | --- | _---_ | _NA_ | **+** |
| `SD ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `SE ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `SET ` | StlSetRloStatement | --- | _---_ | _NA_ | **+** |
| `SF ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `SIN ` | StlFloatMathFunction | _SIN_ | _---_ | _NA_ | **-** |
| `SLD ` | StlShiftRotateStatement | _SHIFT_LEFT_DWORD_ | --- | _+_ | **-** |
| `SLW ` | StlShiftRotateStatement | _SHIFT_LEFT_WORD_ | --- | _+_ | **-** |
| `SP ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `SQR ` | StlFloatMathFunction | _SQR_ | _---_ | _NA_ | **-** |
| `SQRT ` | StlFloatMathFunction | _SQRT_ | _---_ | _NA_ | **-** |
| `SRD ` | StlShiftRotateStatement | _SHIFT_RIGHT_DWORD_ | _NamedOrUnnamedConstantRef, optional_ | _+_ | **-** |
| `SRW ` | StlShiftRotateStatement | _SHIFT_RIGHT_WORD_ | _NamedOrUnnamedConstantRef, optional_ | _+_ | **-** |
| `SS ` | (no counter/timer support yet) | --- | --- | _np_ | **np** |
| `SSD ` | StlShiftRotateStatement | _SHIFT_RIGHT_SIGN_DINT_ | _NamedOrUnnamedConstantRef, optional_ | _+_ | **-** |
| `SSI ` | StlShiftRotateStatement | _SHIFT_RIGHT_SIGN_INT_ | _NamedOrUnnamedConstantRef, optional_ | _+_ | **-** |
| `T ` | StlTransferStatement | --- | _UnaryOrPrimaryExpression_ | _!!!_ | **+** |
| `T STW  ` | StlTransferStwStatement | --- | _---_ | _NA_ | **-** |
| `TAK ` | StlAccuArglessStatement | _TOGGLE_ACCUS_ | _---_ | _NA_ | **-** |
| `TAN ` | StlFloatMathFunction | _TAN_ | _---_ | _NA_ | **-** |
| `TAR1 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `TAR1 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `TAR1 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `TAR2 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `TAR2 ` | (no address register support yet) | --- | --- | _np_ | **np** |
| `TRUNC ` | StlConversionStatement | _TRUNCATE_ | _---_ | _NA_ | **-** |
| `UC ` | StlParameterlessCallStatement | _UNCONDITIONAL_CALL_ | --- | _+_ | **+** |
| `X ` | StlBitLogicStatement | _XOR_ | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `X( ` | StlBitLogicNestingOpenStatement | _XOR_ | _---_ | _NA_ | **+** |
| `XN ` | StlBitLogicStatement | _XOR_NOT_ | _UnaryOrPrimaryExpression_ | _+_ | **+** |
| `XN( ` | StlBitLogicNestingOpenStatement | _XOR_NOT_ | _---_ | _NA_ | **+** |
| `XOD ` | StlWordLogicArglessStatement | _XOR_DWORD_ | _---_ | _NA_ | **+** |
| `XOD ..` | StlWordLogicStatement | _XOR_DWORD_ | _NamedOrUnnamedConstantRef_ | _+_ | **-** |
| `XOW ` | StlWordLogicArglessStatement | _XOR_WORD_ | _---_ | _NA_ | **+** |
| `XOW ..` | StlWordLogicStatement | _XOR_WORD_ | _NamedOrUnnamedConstantRef_ | _+_ | **-** |

(**np**: implementation not planned)



### Missing features

- Floating point arithmetic not supported (yet?).
Example:
```
L 3.1415; 
L 2.5;
*R;     // ACCU1: 7.85375 = 0x40fb51ec
T MD4;  // result: 0x40fb51ec (checked on S7-300 hardware)
TRUNC;
T MD8;  // result: 7 
```

### Currently unsupported TIA Portal specific features {#tia-unsupported-features}

- Local FB instance call in the format of `CALL <FB_name>, <local_instance_name>`. Only the `CALL <local_instance_name>` is supported currently (as it is required in the old STEP 7 IDE).



### Notes on STL semantics

- "The STA status bit has no effect on the processing of STL statements." [Berger2005, Sec. 15.1, p. 218]
- "When the CPU sets the OV status bit, it also always sets the OS status bit. However, while the next properly executed operation resets OV, OS remains set." (OV: Overflow, OS: Stored overflow) [Berger2005, Sec. 15.1, p. 219]
- "S7-300 CPUs (except for CPU 318) do not load status bits /FC, STA and OR into the accumulator [with `L STW`]; the accumulator contains "0" at these locations." [Berger2005, Sec. 15.1, p. 220]
- The bit logic instructions (`A`, `AN`, `O`, `ON`, `X`, `XN`) also accept the following arguments, with the meaning in the brackets after them [Berger2005, Sec. 15.3, p. 222]:
    * `>0` (`not CC0 and CC1`)
    * `>=0` (`not CC0`)
    * `<0` (`CC0 and not CC1`)
    * `<=0` (`not CC1`)
    * `<>0` (`(not CC0 and CC1) or (CC0 and not CC1)`)
    * `==0` (`not CC0 and not CC1`)
    * `UO` (`CC0 and CC1`)
    * `OV` (`OV`)
    * `OS` (`OS`)
    * `BR` (`BR`)
- "You are advised to avoid data transfer [at calls] via intenal registers (for example, accumulators, address registers, RLO)" [Berger2005, Sec. 18.1.1, p. 237]

[Berger2005] Hans Berger. Automating with STEP 7 in STL and SCL. 3rd edition. Publicis, 2005. ISBN 3-89578-2743-2.